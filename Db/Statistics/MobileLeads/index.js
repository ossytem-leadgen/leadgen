const { CONSTANTS } = require('../../../helpers/constants');
const mobileLeadsStatsCol = CONSTANTS.DB.COLLECTIONS.MOBILE_LEADS_STATISTICS.NAME;

module.exports = {
    addMobileLeadsStats: mobileLeadData => require('../../templates/add')(mobileLeadsStatsCol, mobileLeadData),
    getMobileLeadsStats: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../../templates/get')(mobileLeadsStatsCol, fields, skipValue, limitValue, sort, sortField);
    },
    updateMobileLeadsStats: (findField, setField) => require('../../templates/update')(mobileLeadsStatsCol, findField, setField)
}