const { CONSTANTS } = require('../../helpers/constants');
const olegContactsCollection = CONSTANTS.DB.COLLECTIONS.OLEG_LEADS.NAME;

module.exports = {
    addOlegContact: (olegContactsData) => require('../templates/add')(olegContactsCollection, olegContactsData),
    getOlegContacts: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(olegContactsCollection, fields, skipValue, limitValue, sort, sortField);
    },
    updateOlegContacts: (findField, setField) => require('../templates/update')(olegContactsCollection, findField, setField)
}