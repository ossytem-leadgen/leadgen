const { CONSTANTS } = require('../../helpers/constants');
const acceptedConnectsCollection = CONSTANTS.DB.COLLECTIONS.ACCEPTED_CONNECTS.NAME;

module.exports = {
    addAcceptedConnects: (acceptedConnectsData) => require('../templates/add')(acceptedConnectsCollection, acceptedConnectsData),
    getAcceptedConnects: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(acceptedConnectsCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateAcceptedConnects: (findField, setField) => require('../templates/update')(acceptedConnectsCollection, findField, setField)
}