const { CONSTANTS } = require('../../helpers/constants');
const usersCollection = CONSTANTS.DB.COLLECTIONS.USERS.NAME;

module.exports = {
    addUsers: (usersData) => require('../templates/add')(usersCollection, usersData),
    getUsers: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(usersCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateUsers: (findField, setField) => require('../templates/update')(usersCollection, findField, setField)
}