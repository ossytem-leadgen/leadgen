const { CONSTANTS } = require('../../helpers/constants');
const salesleadsCollection = CONSTANTS.DB.COLLECTIONS.SALESLEADS.NAME;

module.exports = {
    addSalesLeads: salesLeadsData => require('../templates/add')(salesleadsCollection, salesLeadsData),
    getSalesLeads: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(salesleadsCollection, fields, skipValue, limitValue, sort, sortField);
    },
    updateSalesLeads: (findField, setField) => require('../templates/update')(salesleadsCollection, findField, setField)
}