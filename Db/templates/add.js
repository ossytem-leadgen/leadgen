const { log } = require('../../modules');
const dbConnection = require('../../helpers/dbConnection');

module.exports = (collectionName, collectionData) => {
    
    log(`Adding some data into ${collectionName} collection`);

    return new Promise(async (resolve, reject) => {
        try {
            const db = await dbConnection.get();
            
            if (!collectionName || (typeof collectionName !== 'string')) {
                resolve('');
                return;
            }

            const collection = db.collection(collectionName);

            if (Array.isArray(collectionData)) {
                collection.insertMany(collectionData, err => {
                    if (err) {
                        console.error('Unable to find data', err);
                    } else {
                        console.log('Inserted all data into ', collectionName, ' collection');

                        resolve('Inserted all data');
                    }
                });
            } else {
                collection.insertOne(collectionData, (err, result) => {
                    if (err) {
                        console.error('Unable to find data', err);
                    } else {
                        console.log('Inserted one data ', collectionName, ' collection');
                        
                        resolve('Inserted all data', result);
                    }
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}
