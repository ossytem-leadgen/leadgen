const mongoDbQueue = require('mongodb-queue');
const { log } = require('../../../modules');
const dbConnection = require('../../../helpers/dbConnection');

module.exports = async (queueCollectionName, sizeObj) => {

    log(`Getting some data from ${queueCollectionName} collection`);

    return new Promise(async (resolve, reject) => {
        try {
            const db = await dbConnection.get();
                
            if (!queueCollectionName || (typeof queueCollectionName !== 'string')) {
                resolve('');
                return;
            }
    
            const queue = await mongoDbQueue(db, queueCollectionName);
    
            if (sizeObj) {
                queue.size( (err, count) => {
                    if (err) reject(err);
                    resolve(count);
                    return;
                });
            } else {
                queue.get((err, msg) => {
                    if (err) {
                        console.error('Unable to get data from the queue', err);
                        reject(err);
                    } else {
                       if (msg) {
                        queue.ack(msg.ack, (err, id) => {
                            if (err) reject(err);
                            resolve(msg.payload);
                            console.log('Removed from the queues:', id);
                        });
                       } else {
                        resolve('');
                       }
                    }
                });
            }
        } catch (error) {
            console.error('An error with adding link', error);
        }
    });
}