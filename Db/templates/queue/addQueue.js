const mongoDbQueue = require('mongodb-queue');
const { log } = require('../../../modules');
const dbConnection = require('../../../helpers/dbConnection');

module.exports = async (queueCollectionName, queueData) => {

    log(`Adding some queue data into ${queueCollectionName} collection`);

    try {
        const db = await dbConnection.get();
            
        if (!queueCollectionName || (typeof queueCollectionName !== 'string')) {
            resolve('');
            return;
        }

        const queue = mongoDbQueue(db, queueCollectionName);

        if (Array.isArray(queueData)) {
            for (let data of queueData) {
                queue.add(data, (err, id) => {
                    if (err) {
                        console.error('Unable to add data to queue', err);
                    } else {
                        console.log('Some data was added to queue', id);
                    }
                   
                });
            }
        } else {
            queue.add(queueData, (err, id) => {
                if(err){
                    console.error('Unable to add to queue', err);
                } else {
                    console.log('This queue has been added', id);
                }
            });
        }
    } catch (error) {
        console.error('An error with adding link', error);
    }
}