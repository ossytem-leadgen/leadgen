const { log } = require('../../modules');
const dbConnection = require('../../helpers/dbConnection');

module.exports = (collectionName, fields) => {
    log(`Updating some data in ${collectionName} collection`);

    return new Promise(async (resolve, reject) => {
        try {
            const db = await dbConnection.get();

            if (!collectionName || (typeof collectionName !== 'string')) {
                resolve('');
                return;
            }

            const collection = db.collection(collectionName);

            collection.removeMany(fields, (err, res) => {
                if (err) {
                    console.error("Unable to find data to delete from ", collectionName, " collection", err);
                    reject(err);
                } else {
                    console.log(res.result.n + " document(s) deleted from ", collectionName, " collection");
                    resolve('');
                }
            });
        } catch (error) {
            reject(error);
        }
    });
};