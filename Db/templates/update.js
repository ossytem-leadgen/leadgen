const { log } = require('../../modules');
const dbConnection = require('../../helpers/dbConnection');

module.exports = (collectionName, findField, setField) => {

    log(`Updating some data in ${collectionName} collection`);

    return new Promise(async (resolve, reject) => {
        try {
            const db = await dbConnection.get();
            
            if (!collectionName || (typeof collectionName !== 'string')) {
                resolve('');
                return;
            }

            const collection = db.collection(collectionName);

            const updateToDocument = setField.$inc ? setField : { $set: setField };
            
            collection.update(findField, updateToDocument, { upsert: false }, (err, res) => {
                if (err) {
                    console.error('Unable to find data ', err, 'from ', collectionName, ' collection');
                    //reject(err);
                } else {
                    console.log('Updated from ', collectionName, ' collection', res.result);
                    resolve(`Updated! ${res.result}`);
                }
            });
            
        } catch (error) {
            reject(error);
        }
    });
};