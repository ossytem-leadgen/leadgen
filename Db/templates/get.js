const { log } = require('../../modules');
const dbConnection = require('../../helpers/dbConnection');

module.exports = (collectionName, fields, skipValue = 0, limitValue = 0, sort = false, sortField) => {

    log(`Getting some data from ${collectionName} collection`);

    return new Promise(async (resolve, reject) => {
        try {
            const db = await dbConnection.get();
            
            if (!collectionName || (typeof collectionName !== 'string')) {
                resolve('');
            }

            const collection = db.collection(collectionName);
            let filter;

            if (sort) {
                console.log('\nSorting some data');

                filter = limitValue === 0 && skipValue === 0 ? collection.find(fields).sort(sortField) : collection.find(fields).sort(sortField).skip(skipValue).limit(limitValue);
            } else if (skipValue || limitValue) {
                console.log('\nSkipping some data');

                filter = collection.find(fields).skip(skipValue).limit(limitValue);
            } else {
                console.log('\nRaw data');

                filter = !fields ? collection.find({}) : collection.find(fields);
            }
            
            filter.toArray((err, result) => {
                if (err) {
                    console.error('Unable to find data from ', collectionName, ' collection', err);
                } else if (result.length) {
                    console.log('Returning all data from ', collectionName, ' collection');
                    resolve(result);
                } else {
                    resolve('');
                    console.log('No data to show from ', collectionName, ' collection');
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}