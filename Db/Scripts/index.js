const { CONSTANTS } = require('../../helpers/constants');
const scriptsCollection = CONSTANTS.DB.COLLECTIONS.SCRIPTS.NAME;

module.exports = {
    addScripts: scriptsData => require('../templates/add')(scriptsCollection, scriptsData),
    getScripts: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(scriptsCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateScripts: (findField, setField) => require('../templates/update')(scriptsCollection, findField, setField)
}