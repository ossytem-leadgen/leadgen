const { CONSTANTS } = require('../../helpers/constants');
const inboxesCollection = CONSTANTS.DB.COLLECTIONS.INBOXES.NAME;

module.exports = {
    addInbox: (inboxData) => require('../templates/add')(inboxesCollection, inboxData),
    getInbox: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(inboxesCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateInbox: (findField, setField) => require('../templates/update')(inboxesCollection, findField, setField)
}