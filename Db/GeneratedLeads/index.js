const { CONSTANTS } = require('../../helpers/constants');
const generatedLeadsCollection = CONSTANTS.DB.COLLECTIONS.GENERATED_LEADS.NAME;

module.exports = {
    addGeneratedLeads: (generatedLeadData) => require('../templates/add')(generatedLeadsCollection, generatedLeadData),
    getGeneratedLeads: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(generatedLeadsCollection, fields, skipValue, limitValue, sort, sortField);
    },
    updateGeneratedLeads: (findField, setField) => require('../templates/update')(generatedLeadsCollection, findField, setField)
}