const { CONSTANTS } = require('../../helpers/constants');
const cookiesCollection = CONSTANTS.DB.COLLECTIONS.COOKIES.NAME;

module.exports = {
    addCookies: (cookiesData) => require('../templates/add')(cookiesCollection, cookiesData),
    getCookies: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(cookiesCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateCookies: (findField, setField) => require('../templates/update')(cookiesCollection, findField, setField)
}