const { CONSTANTS } = require('../../helpers/constants');
const recuitmentCompanyCollection = CONSTANTS.DB.COLLECTIONS.RECRUITING_COMPANY.NAME;

module.exports = {
    addRecruitingCompany: (recuitmentNameObject) => require('../templates/add')(recuitmentCompanyCollection, recuitmentNameObject),
    getRecruitingCompany: (fields) => require('../templates/get')(recuitmentCompanyCollection, fields)
}