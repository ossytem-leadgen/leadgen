const { MongoClient } = require('mongodb');
const mongoDbQueue = require('mongodb-queue');
const { log } = require('../modules');
const dbConnection = require('./connect');

require('dotenv').config();

const ENV = process.env;
const url = `${ENV.DB_PROVIDER}://${ENV.DB_USER}:${encodeURIComponent(ENV.DB_PWORD)}@${ENV.DB_HOST}:${ENV.DB_PORT}/${ENV.DB_NAME}`;

//NOVEMBER 1, 2018 - THIS FILE IS NO LONGER IN USE IN THE PROJECT, 
//IT WAS REPLACED WITH THE USE OF TEMPLATES INSTEAD OF ONE FILE WITH ALL FUNCTIONS

const addCompanyLinkToQueue = async (searchResLink) => {
    try {
        const db = await dbConnection.connect();
        const linkedin_search_queue = mongoDbQueue(db, 'companies-searchres-queue');

        linkedin_search_queue.add(searchResLink, function(err, id) {
            if(err){
                console.error('Unable to add to queue', err);
            } else {
                console.error('This queue has been added');
            }
        });
    } catch (error) {
        console.error('An error with adding company link', error);
    }
}

const getCompanyLinkFromQueue = () => {

    return new Promise( async (resolve, reject) => {

        try {
            const db = await dbConnection.get();
            const linkedin_search_queue = mongoDbQueue(db, 'companies-searchres-queue');
            
            linkedin_search_queue.get(function(err, msg) {
                if(err){
                    console.error('Unable to get link from the queue', err);
                } else {
                   if(msg){
                    linkedin_search_queue.ack(msg.ack, function(err, id) {
                        if(err) reject(err);
                        let link = msg.payload;
                        resolve(link);
                        console.log('This message has now been removed from the queues', id);
                    });
                   } else {
                    resolve('null');
                   }
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

const addEmpPgsLinkToQueue = async (employeesPageUrl) => {
    try {
        const db = await dbConnection.connect();
        const employees_pages_url = mongoDbQueue(db, 'employees-pages-url-queue');
        employees_pages_url.add(employeesPageUrl, function(err, id) {
            if(err){
                console.error('Unable to add to employee link to queue', err);
            } else {
                console.error('This queue has been added');
            }
        });
    } catch (error) {
        console.error('Error with adding employee link to queue');
    }
}

const getEmpPgLinkFromQueue = () => {
    return new Promise( async (resolve, reject) => {
        try {
            const db = await dbConnection.connect();
            let employees_pages_url = mongoDbQueue(db, 'employees-pages-url-queue');
            employees_pages_url.get(function(err, msg) {
                if(err){
                    console.error('Unable to get employee page link from the queue', err);
                    reject(err);
                } else {
                   if(msg){
                    employees_pages_url.ack(msg.ack, function(err, id) {
                        if(err) reject(err);
                        let link = msg.payload
                        resolve(link);
                        console.log('This message has now been removed from the queues', id)
                    })
                   } else {
                    resolve('null');
                   }
                }
            });
        } catch (error) {
            reject('An error while trying to get employee link from queue', error);
        }
    });
}

const addDbDataToQueue = async (data, mongodoc) => {
    try {
        if(mongodoc === "company") {
            const queue = await mongoDbQueue(db, 'companies-queue');
            add(queue);
        } else if (mongodoc === "employee") {
            const queue = await mongoDbQueue(db, 'employees-queue');
            add(queue);
        } else if(mongodoc === "oleg"){
            const queue = await mongoDbQueue(db, 'oleg-leads-queue');
            add(queue);
        } else {
            console.log('Your mongo document value isnt correct when trying to add data from queue')
        }

        function add(queue) {
            if(Array.isArray(data) && data.length){

                for(let elem of data){
                    queue.add(elem, function(err) {
                        if (err) {
                            console.error('Unable to add data to queue', err);
                        } else {
                            console.log('Some data was added to queue');
                        }
                       
                    });
                }

            } else {
                queue.add(data, function(err) {
                    if(err){
                        console.error('Unable to add data to queue', err);
                    } else {
                        console.log('Some data was added to queue');
                    }
                   
                });
            }
        }
    } catch (error) {
        console.error('Error while trying to add db data to queue', error)
    }

}

// The MongoDB Document must be specified
const getDbDataFromQueue = (mongodoc) => {
    return new Promise( async (resolve, reject) => {
        try {
            if(mongodoc === "company"){
                let queue = await mongoDbQueue(db, 'companies-queue');
                get(queue);
            } else if (mongodoc === "employee") {
                let queue = await mongoDbQueue(db, 'employees-queue');
                get(queue);
            } else if (mongodoc === "oleg") {
                let queue = await mongoDbQueue(db, 'oleg-leads-queue');
                get(queue);
            }else {
                console.log('getDbDataFromQueue(): Your mongo document value isnt correct');
            }
            
            function get(queue) {
                queue.get(function(err, msg) {
                    if(err){
                        console.error('Unable to get data from the queue', err);
                        reject(err);
                    } else {
                       if(msg){
                        queue.ack(msg.ack, function(err, id) {
                            if(err) reject(err);
                            let data = msg.payload;
                            resolve(data);
                            console.log('Removed from the queues:', id)
                        })     
                       } else {
                        resolve('null');
                       }
                      
                    }
                });
            }
        } catch (error) {
            reject('Error while getting data from queue', error);
        }
    });
}

const getQueueSize = (mongodoc) => {
    return new Promise( async (resolve, reject) => {
        try {
            let queueName = '';

            switch (mongodoc) {
                case "companyLink": queueName = 'companies-searchres-queue'; break;
                case "employeeLink": queueName = 'employees-pages-url-queue'; break;
                case "company": queueName = 'companies-queue'; break;
                case "employee": queueName = 'employees-queue'; break;
                case "personLink": queueName = 'people-url-queue'; break;
                default: console.log('Your mongo document value isnt correct when trying to get data from queue');
            }

            const queue = await mongoDbQueue(db, queueName);
            
            getSize(queue);
            
            function getSize(queue){
                queue.size( (err, count) => {
                    if(err) reject(err)
                    resolve(count);
                });
            }
        } catch (error) {
            reject('Error while getting the queue size', error);
        }
    });
}

const allCompanies = (field, data) => {
    return new Promise( async (resolve, reject) => {
        try {
            const db = await dbConnection.connect();
            const collection = db.collection('companies');
            let filter;

            field === undefined || data === undefined ? filter = collection.find({}) : filter = collection.find({ [field]: data })
            
            filter.toArray(function(err, result){
                if(err){
                    console.error('Unable to find data', err);
                } else if(result.length) {
                    console.log('Returning all data');
                    resolve(result);
                } else {
                    resolve('');
                    console.log('No data to show');
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

const insertEmployees = (employeesData) => {
    try {
        const collection = db.collection('employees');

        if (Array.isArray(employeesData)) {
            collection.insertMany(employeesData, function(err, result){
                if(err){
                    console.error('insertEmployees(): Unable to find data', err);
                } else{
                    console.log('insertEmployees(): Inserted all data');
                }
            });
        } else {
            collection.insertOne(employeesData, function(err, result){
                if(err){
                    console.error('Unable to find data', err);
                } else {
                    console.log('Inserted all data');
                }
            });
        }
    } catch (error) {
        console.error('An error while trying to inserting employees', error);
    }
}

const insertCompany = (companyData) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('insertCompany(): Unable to connect to the server', err);
        } else {
            console.log('insertCompany(): Connected successfully');
            let collection = db.collection('companies');
            if(Array.isArray(companyData)){
                collection.insertMany(companyData, function(err, result){
                    if(err){
                        console.error('Unable to find data', err);
                    } else{
                        console.log('Inserted all data', result);
                    }
                   
                });
            } else {
                collection.insertOne(companyData, function(err, result){
                if(err){
                    console.error('Unable to find data', err);
                } else {
                    console.log('Inserted all data');
                }
                });
            }
            
        }
    });
}

const updateCompany = (findField, setField) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('updateCompany(): Unable to connect to the server', err);
        } else {
            console.log('updateCompany(): Connected successfully');
            let collection = db.collection('companies');
            collection.update(findField, { $set: setField }, function(err, res){
                if(err){
                    console.error('Unable to find data', err);
                } else {
                    console.log('Updated!', res.result);
                }
            });
        }
    });
}

const insertRecrutingCompany = (name) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('insertRecrutingCompany(): Unable to connect to the server', err);
        } else {
            console.log('Connected successfully, Ready to insert recruiting company');
            let collection = db.collection('recruiting_companies');
            if(Array.isArray(name)){
                collection.insertMany(name, function(err, result){
                    if(err){
                        console.error('Unable to find recruiting company', err);
                    } else{
                        console.log('Inserted all Recruitment companies');
                    }
                });
            } else {
                collection.insertOne(name, function(err, result){
                if(err){
                    console.error('Unable to find recruiting company', err);
                } else {
                    console.log('Inserted Recruiting company');
                }
            });
            }
        }
    });
}

const doesEmployeeExist = (name, position) => {
    
    return new Promise( (resolve, reject) => { 
        MongoClient.connect(url, async (err, db) => {
            if(err){
                console.error('Unable to connect to the server', err);
                reject(err)
            } else {
                // console.log('Connected to the server successfully');
                let collection = db.collection('employees');
                let result = await collection.findOne({ fullname: name, position: position });
                if(result === null){
                    resolve(false)
                } else {
                    resolve(true)
                }
            }
        });
    });
}

const doesRecruitingCompanyExist = (name) => {
    
    return new Promise( (resolve, reject) => { 
        MongoClient.connect(url, async (err, db) => {
            if(err){
                console.error('Unable to connect to the server', err);
                reject(err)
            } else {
                // console.log('Connected to the server successfully');
                let collection = db.collection('recruiting_companies');
                let result = await collection.findOne({"name": name});
                if(result === null){
                    resolve(false)
                } else {
                    resolve(true)
                }
            }
        });
    });
}


const updateEmployees = (findField, setField) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('updateEmployees(): Unable to connect to the server', err);
        } else {
            console.log('updateEmployees(): Connected successfully');
            let collection = db.collection('employees');
            collection.update(findField, { $set: setField }, function(err, res){
                if(err){
                    console.error('Unable to update employee data', err);
                } else {
                    console.log('Updated employees data!', res.result);
                }
               
            });
        }
    });
}

const insertSpreadsheetData = (sheetData) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('insertSpreadsheetData(): Unable to connect to the server', err);
        } else {
            console.log('insertSpreadsheetData(): Connected successfully');
            let collection = db.collection('generated_leads');
            if(Array.isArray(sheetData)){
                collection.insertMany(sheetData, function(err, result){
                    if(err){
                        console.error('insertSpreadsheetData(): Unable to find spreadhsheet data', err);
                    } else{
                        console.log('Inserted all spreadhsheet data');
                    }
                });
            } else {
                collection.insertOne(sheetData, function(err, result){
                if(err){
                    console.error('Unable to find spreadhsheet data', err);
                } else {
                    console.log('Inserted all spreadhsheet data');
                }
            });
            }
        }
    });
}

const updateSpreadsheetData = (id, fields) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('Unable to connect to the server', err);
        } else {
            console.log('Connected successfully');
            let collection = db.collection('generated_leads');
            collection.update({ _id: id },{ $set: fields }, { upsert: false }, (err, res) => {
                if (err) throw err;
                console.log(res.result);
            });
        }
    });
}

const allSpreadSheetData = (fields, skipValue = 0, limitValue = 0, sort = false, sortField, sortVal) => {

    return new Promise( (resolve, reject) => {
        MongoClient.connect(url, (err, db) => {
            if (err) {
                console.error('allSpreadSheetData(): Unable to connect to the server', err);
                reject(err)
            } else {
                let collection = db.collection('generated_leads');
                let filter;
                if (sort) {
                    filter = limitValue === 0 && skipValue === 0 ? collection.find(fields).sort({ [sortField]: sortVal }) : collection.find(fields).sort({ [sortField]: sortVal }).skip(skipValue).limit(limitValue)
                } else {
                    filter = limitValue === 0 && skipValue === 0 ? collection.find(fields) : collection.find(fields).skip(skipValue).limit(limitValue)
                }
                filter.toArray(function(err, result) {
                    if (err) {
                        console.error('allSpreadSheetData(): Unable to find data', err);
                    } else if (result.length) {
                        console.log('Returning all spreadsheet data');
                        resolve(result)
                    } else {
                        resolve('')
                        console.log('No spreadsheet data to show');
                    }
                });
            }
        });
    });
}

const addPeopleLinkToQueue = (personUrl) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('addPeopleLinkToQueue(): Unable to connect to the server', err);
        } else {
            let peopleUrlQueue = mongoDbQueue(db, 'people-url-queue');
            peopleUrlQueue.add(personUrl, function(err, id) {
                if(err){
                    console.error('Unable to add to person link to queue', err);
                } else {
                    console.error('This queue has been added');
                }
            });
        }
    });
}

const getPersonLinkFromQueue = () => {

    return new Promise( (resolve, reject) => {
        MongoClient.connect(url, (err, db) => {
            if(err) reject(err);
            let peopleUrlQueue = mongoDbQueue(db, 'people-url-queue');
            peopleUrlQueue.get(function(err, msg) {
                if(err){
                    console.error('Unable to get people page link from the queue', err);
                    reject(err)
                } else {
                   if(msg){
                    peopleUrlQueue.ack(msg.ack, function(err, id) {
                        if(err) reject(err);
                        let link = msg.payload
                        resolve(link);
                        console.log('This message has now been removed from the queues', id)
                    })
                   } else {
                    resolve('null');
                   }
                }
            });
        });
    });
}

const insertOlegLeads = (leadData) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('insertBossExistingLeads(): Unable to connect to the server', err);
        } else {
            console.log('insertBossExistingLeads(): Connected successfully');
            let collection = db.collection('oleg_contacts');
            if (Array.isArray(leadData)) {
                collection.insertMany(leadData, function(err, result){
                    if(err){
                        console.error('insertBossExistingLeads(): Unable to find data', err);
                    } else{
                        console.log('insertBossExistingLeads(): Inserted all data');
                    }
                });
            } else {
                collection.insertOne(leadData, function(err, result){
                    if(err){
                        console.error('Unable to find data', err);
                    } else {
                        console.log('Inserted all data');
                    }
                });
            }
        }
    });
}

const allOlegLeads = (fields, skipValue = 0, limitValue = 0, sort = false, sortField, sortVal) => {

    return new Promise( (resolve, reject) => {
        MongoClient.connect(url, (err, db) => {
            if (err) {
                console.error('allSpreadSheetData(): Unable to connect to the server', err);
                reject(err)
            } else {
                let collection = db.collection('oleg_contacts');
                let filter;
                if (sort) {
                    filter = limitValue === 0 && skipValue === 0 ? collection.find(fields).sort({ [sortField]: sortVal }) : collection.find(fields).sort({ [sortField]: sortVal }).skip(skipValue).limit(limitValue)
                } else {
                    filter = limitValue === 0 && skipValue === 0 ? collection.find(fields) : collection.find(fields).skip(skipValue).limit(limitValue)
                }
                filter.toArray(function(err, result) {
                    if (err) {
                        console.error('allOlegLeads(): Unable to find data', err);
                    } else if (result.length) {
                        console.log('Returning all of olegs leads');
                        resolve(result)
                    } else {
                        resolve('')
                        console.log('None of Olegs leads were found');
                    }
                });
            }
        });
    });
}

const updateOlegLeads = (id, fields) => {

    MongoClient.connect(url, (err, db) => {
        if(err){
            console.error('Unable to connect to the server', err);
        } else {
            console.log('Connected successfully');
            let collection = db.collection('oleg_contacts');
            collection.update({ _id: id },{ $set: fields }, { upsert: false }, (err, res) => {
                if (err) throw err;
                console.log(res.result);
            });
        }
    });
}

const playground = (id, fields) => {
    
    MongoClient.connect(url, async (err, db) => {
        if(err){
            console.error('Unable to connect to the server', err);
        } else {
            // console.log('Connected to the server successfully');
            let collection = db.collection('generated_leads');
            //collection.update({ _id: id },{ $set: fields }, { upsert: false }, (err, res) =>
            // await collection.find({batch_number:"batch1", category:{$ne: "N/A"}}).toArray((err, res) => {
            collection.update({ _id: id },{ $set: fields }, { upsert: false }, (err, res) => {
                if(err) throw err;
                console.log(res.result);
            });
        }
    });
}
// playground(ObjectId("5bb1eed19f2b3a64d421bd0d"), {"headcount": 0, "date_script_ran": "1998-10-20"})

module.exports = {
    // addCompanyLinkToQueue: addCompanyLinkToQueue, +
    // addEmpPgsLinkToQueue: addEmpPgsLinkToQueue, +
    // addPeopleLinkToQueue: addPeopleLinkToQueue, + 
    // addDbDataToQueue: addDbDataToQueue, + 
    
    // getCompanyLinkFromQueue: getCompanyLinkFromQueue, + 
    // getPersonLinkFromQueue: getPersonLinkFromQueue, +
    // getEmpPgLinkFromQueue: getEmpPgLinkFromQueue, +
    // getDbDataFromQueue: getDbDataFromQueue, +
    // getQueueSize: getQueueSize, + 

    //allCompanies: allCompanies, +
    //insertCompany: insertCompany, +
    //updateCompany: updateCompany, +
    
    //insertRecrutingCompany: insertRecrutingCompany, + 
    //doesRecruitingCompanyExist: doesRecruitingCompanyExist, +

    //allEmployees: require('./getAllEmployees'), +
    //insertEmployees: insertEmployees, +
    //updateEmployees: updateEmployees, +
    
    //allSpreadSheetData: allSpreadSheetData, +
    //insertSpreadsheetData: insertSpreadsheetData, +
    //updateSpreadsheetData: updateSpreadsheetData, +
    
    //allOlegLeads: allOlegLeads, + 
    //insertOlegLeads: insertOlegLeads, +
    //updateOlegLeads:  +
}
