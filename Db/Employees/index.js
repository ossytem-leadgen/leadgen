const { CONSTANTS } = require('../../helpers/constants');
const employeeCollection = CONSTANTS.DB.COLLECTIONS.EMPLOYEE.NAME;

module.exports = {
    addEmployee: (employeesData) => require('../templates/add')(employeeCollection, employeesData),
    getEmployee: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(employeeCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateEmployee: (findField, setField) => require('../templates/update')(employeeCollection, findField, setField)
}