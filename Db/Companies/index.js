const { CONSTANTS } = require('../../helpers/constants');
const companyCollection = CONSTANTS.DB.COLLECTIONS.COMPANY.NAME;

module.exports = {
    addCompany: (companyData) => require('../templates/add')(companyCollection, companyData),
    getCompany: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(companyCollection, fields, skipValue, limitValue, sort, sortField)
    },
    updateCompany: (findField, setField) => require('../templates/update')(companyCollection, findField, setField),
    deleteCompany: fields => require('../templates/delete')(companyCollection, fields)
};