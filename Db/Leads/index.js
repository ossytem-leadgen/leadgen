const { CONSTANTS } = require('../../helpers/constants');
const leadsCollection = CONSTANTS.DB.COLLECTIONS.LEADS.NAME;

module.exports = {
    addLeads: leadData => require('../templates/add')(leadsCollection, leadData),
    getLeads: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(leadsCollection, fields, skipValue, limitValue, sort, sortField);
    },
    updateLeads: (findField, setField) => require('../templates/update')(leadsCollection, findField, setField)
}