const { CONSTANTS } = require('../../helpers/constants');
const queueCollection = CONSTANTS.DB.COLLECTIONS.QUEUE;

module.exports = {
    addEmployeeLinkToQueue: employeeLinkData => require('../templates/queue/addQueue')(queueCollection.EMPLOYEE_LINK, employeeLinkData),
    addCompanyLinkToQueue: companyLinkData => require('../templates/queue/addQueue')(queueCollection.COMPANY_LINK, companyLinkData),
    addPeopleLinkToQueue: peopleLinkData => require('../templates/queue/addQueue')(queueCollection.PERSON_LINK, peopleLinkData),
    addCompanyDataToQueue: companyData => require('../templates/queue/addQueue')(queueCollection.COMPANY, companyData),
    addEmployeeDataToQueue: employeeData => require('../templates/queue/addQueue')(queueCollection.EMPLOYEE, employeeData),
    addOlegContactDataToQueue: olegContactData => require('../templates/queue/addQueue')(queueCollection.OLEG_CONTACTS, olegContactData)
}