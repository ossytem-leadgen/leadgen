const { CONSTANTS } = require('../../helpers/constants');
const queueCollection = CONSTANTS.DB.COLLECTIONS.QUEUE;

module.exports = {
    getEmpPgLinkFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.EMPLOYEE_LINK),
    getCompanyLinkFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.COMPANY_LINK),
    getPersonLinkFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.PERSON_LINK),
    getCompanyDataFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.COMPANY),
    getEmployeeDataFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.EMPLOYEE),
    getOlegContactDataFromQueue: _ => require('../templates/queue/getQueue')(queueCollection.OLEG_CONTACTS),
    getQueueSize: (collectionName) => require('../templates/queue/getQueue')(collectionName, { size: true })
}