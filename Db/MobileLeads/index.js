const { CONSTANTS } = require('../../helpers/constants');
const mobileLeadsCollection = CONSTANTS.DB.COLLECTIONS.MOBILE_LEADS.NAME;

module.exports = {
    addMobileLeads: (mobileLeadData) => require('../templates/add')(mobileLeadsCollection, mobileLeadData),
    getMobileLeads: (fields, skipValue, limitValue, sort, sortField) => {
        return require('../templates/get')(mobileLeadsCollection, fields, skipValue, limitValue, sort, sortField);
    },
    updateMobileLeads: (findField, setField) => require('../templates/update')(mobileLeadsCollection, findField, setField)
}