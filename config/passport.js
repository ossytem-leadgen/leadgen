const LocalStrategy = require('passport-local').Strategy;
const { getUsers } = require('../Db/Users');

module.exports = passport => {
    passport.use(new LocalStrategy({usernameField: 'secret'}, async (username, password, done) => {
      const [user] = await getUsers({password});

      if (!user) {
        done(null, false, {message: 'Password incorrect'});
      } else {
        done(null, user);
      }
    }
  ));

  passport.serializeUser((user, done) => {
    done(null, user.id)
  });

  passport.deserializeUser(async (_id, done) => {
    const [user] = await getUsers({_id: id});
    
    done("", user);
  });
}
