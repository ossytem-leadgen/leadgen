const puppeteer = require('puppeteer');
require('dotenv').config();

const dbConnection = require('../../helpers/dbConnection');
const { cookies, CONSTANTS, URLS } = require('../../helpers/constants');
const { addCompanyLinkToQueue } = require('../../Db/Queues/addToQueue');
const { genRandNum, log, getCookie } = require('../../modules');
const { getCompanyLinks } = require('../../modules/scrappers/salesNavigator');

const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const searchUrl = URLS.salesNavigator.url;
const pagination = URLS.salesNavigator.pagination;


const generateUrls = (pageUrl, pageAmount) => {
    let urlArr = [pageUrl];
    const regex = /(&page=)[0-9]+/;

    for (let i = 2; i <= pageAmount; i++) {

        let url = pageUrl.replace(regex, `$1${i}`);
        urlArr.push(url)
    }
    return urlArr;
}

const getCompaniesUrls = async () => {
    const pagesUrl = generateUrls(searchUrl, pagination);

    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);

        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
    }
    await page.setViewport({ width: 1366, height: 868 });

    for (let pageUrl of pagesUrl) {

        const pageResponse = await page.goto(pageUrl, {
            timeout: 120000,
            waitUntil: 'networkidle2',
        });

        if (pageResponse._status < 400) {
            await page.evaluate(async () => {
                await new Promise((resolve, reject) => {
                    try {
                        const maxScroll = Number.MAX_SAFE_INTEGER;
                        let lastScroll = 0;
                        
                        const interval = setInterval(() => {
                            window.scrollBy(0, 100);
                            const scrollTop = document.documentElement.scrollTop;
                            
                            if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                clearInterval(interval);
                                resolve();
                            } else {
                                lastScroll = scrollTop;
                            }
                        }, 100);
                    } catch (err) {
                        console.log(err);
                        reject(err.toString());
                    }
                });
            });
            
            const pageHandle = await page.evaluateHandle(() => document.body);
            const resultpageHandle = await page.evaluateHandle(body => body.innerHTML, pageHandle);
            const html = await resultpageHandle.jsonValue();
            let companyUrls = getCompanyLinks(html);

            for (let url of companyUrls) {
                log(url);
                await addCompanyLinkToQueue(url);
            }
            
            await pageHandle.dispose();
        }
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);
        await page.waitFor(randNo);

        let lastUrl = pagesUrl[pagesUrl.length - 1];
        if (lastUrl === pageUrl) {
          await browser.close();
        }
    }

    process.on("unhandledRejection", (reason, p) => {
        console.error("ERROR", p, "reason:", reason);
        browser.close();
    });
}

module.exports = generateUrls;