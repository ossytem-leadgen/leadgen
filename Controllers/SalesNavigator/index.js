const fs = require('fs');
const { log } = require('../../modules');
const puppeteer = require('puppeteer');
const { cookies } = require('../../helpers/constants');
const clipboardy = require('clipboardy');

// Loop through each search result page 
// - scroll, get recuired scrapper, save in company queue

//Go to company page, 
// - copy to clipboard - click see all - get html - scrap company data
//- take employee

//functions - generatePaginationUrls for employees, 



//This would be used on company page to get company linkedinurl
const runner = async () => {
    const puppeteer = require('puppeteer');

    const { cookies } = require('../../helpers/constants');

    const url = 'https://www.linkedin.com/sales/company/937548/people?_ntb=oYA76G0eRDSp780tmRnPPg%3D%3D';

    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();

    try {
        await page.setCookie(...cookies.acc1);
    } catch (error) {
        console.error(error);
    }

    await page.goto(url);

    await page.waitFor(5000);

    // company-topcard-actions__overflow-toggle button-round-tertiary-medium-muted
    await page.click('button.company-topcard-actions__overflow-toggle');
    await page.waitFor(500);
    await page.click('ul.company-topcard-actions__overflow-dropdown a.copy-linkedin-overflow');

    let res = clipboardy.readSync();

    console.log(res)

    await browser.close();
}

runner();