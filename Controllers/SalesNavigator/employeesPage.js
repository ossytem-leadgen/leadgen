const puppeteer = require('puppeteer');
require('dotenv').config();

const { CONSTANTS } = require('../../helpers/constants');
const dbConnection = require('../../helpers/dbConnection');
const { getEmployee } = require('../../Db/Employees');
const { addEmployeeDataToQueue } = require('../../Db/Queues/addToQueue');
const { getEmpPgLinkFromQueue, getQueueSize } = require('../../Db/Queues/getFromQueue');
const { genRandNum, date, time, log, pageScroller, len, getCookie, getTimeDiffAndTimeZone } = require('../../modules');
const { getEmployeeData } = require('../../modules/scrappers/salesNavigator');
const { errorHandler } = require('../olegLeadsController');

let randNo;
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { EMPLOYEE_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;

const getEmployeeDataFromPage = async () => {
    let startTime = new Date().getTime();
    let queueSize = await getQueueSize(EMPLOYEE_LINK);

    if (queueSize) {
        console.log('The size of the queue is ',queueSize);

        const browser = await puppeteer.launch({
            headless: false
        });

        const page = await browser.newPage();
        
        try {
            const account = process.argv[3];
            
            await setCookies(page, account)
        } catch (error) {
            const imageName = `${date()}-${time()}.png`;
            const errorLocation = 'companypage in SalesNavigator getCompanyDataFromPage()';
            const extraInfo = `Error while adding caching. Image name in google drive: ${imageName}`;
            
            await errorHandler(page, error, errorLocation, extraInfo, imageName);
            process.exit();
            dbConnection.close();
        }
        
        for (let i = 0; i < queueSize; i++) {
            try {
                const salesNavEmployee = await getEmpPgLinkFromQueue();

                await page.goto(salesNavEmployee.url, { timeout: 250000, waitUntil: 'networkidle2'});

                log(`\n\nOn employee page now waiting for 5secs`);

                await pageScroller(page);

                await page.waitFor(3000);

                const employeesHandle = await page.evaluateHandle(() => document.body);
                const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                const employeesHtmlPage = await resultEmployeesHandle.jsonValue();

                const { employees } = getEmployeeData(employeesHtmlPage);
                console.log('Scrapped Employees', employees);
                
                if (Array.isArray(employees) && len(employees)) {
                    await insertEmployees(employees, salesNavEmployee);
                } else {
                    console.log('We did not find anyone on this page');
                }

                const newSize = await getQueueSize(EMPLOYEE_LINK);
                
                console.log('\nThe queue size is now', newSize);
                
                await endIfOnLastPage(browser, newSize, startTime);
                
                randNo = genRandNum(MIN_TIME, MAX_TIME);
                
                console.log(`\n\nWaiting for ${randNo} seconds then move to the next URL`);

                await page.waitFor(randNo); 

            } catch (error) {
                const imageName = `${date()}-${time()}.png`;
                const errorLocation = 'companypage in SalesNavigator getCompanyDataFromPage()';
                const extraInfo = `Error while adding caching. Image name in google drive: ${imageName}`;
                
                await errorHandler(page, error, errorLocation, extraInfo, imageName);

                randNo = genRandNum(MIN_TIME, MAX_TIME);
                console.log(`\n\nWaiting for ${randNo} seconds then move to the next URL`);
                await page.waitFor(randNo); 
            }
        }

        process.on("unhandledRejection", (reason, p) => {
            console.error("ERROR", p, "reason:", reason);
            browser.close();
            dbConnection.close();
        });          
        
    } else {
        console.log('The Queue is empty');
        process.exit();
        dbConnection.close();
    }
};

const setCookies = async (page, account) => {
    let cookie = [];
    
    switch (account) {
        case 'acc1':
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
            break;
        case 'acc2':
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
            break;
        default:
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
    }

    await page.setCookie(...cookie);
};

const insertEmployees = async (employees, companyInfo) => {
    const { name, employee_url, position, country, timezone, time_diff } = employees;

    let employee_table_data = [];

    for (let i = 0; i < employees.length; i++) {
        let doesEmployeeExist = await getEmployee({ fullname: name, position: position });

        if (!doesEmployeeExist) {
            employee_table_data.push({
                "fullname" : name,
                "position" : position,
                "company" : companyInfo.company,
                "company_url" : companyInfo.company_url,
                "country" : country,
                "linkedin_url" : employee_url,
                "email" : "No email",
                "phone" : "No Phone number",
                timezone, time_diff,
                "created_at" : date()
            }); 
        } else {
            log('Employee already exists');
        }
    }
    console.log("\n\nEmployees data", employee_table_data); 

    await addEmployeeDataToQueue(employee_table_data);
};

const endIfOnLastPage = async (browser, newSize, startTime) => {
    if (Number(newSize) === 0) {
        console.log('The Queue is now empty');

        let endTimeMilli =  new Date().getTime() - startTime;
        let endTimeMin = Math.floor(endTimeMilli / 60000);
        let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);

        if (endTimeSec == 60) {
            endTimeMin = endTimeMin + 1;
            endTimeSec = 0;
        }

        console.log(`\nScript 3 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);
        
        await browser.close();
        
        process.exit();
    }
};

module.exports = getEmployeeDataFromPage;