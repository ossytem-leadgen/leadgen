const puppeteer = require('puppeteer');
const clipboardy = require('clipboardy');
require('dotenv').config();

const { CONSTANTS } = require('../../helpers/constants');
const dbConnection = require('../../helpers/dbConnection');
const { getCompany } = require('../../Db/Companies');
const { getEmployee } = require('../../Db/Employees');
const { addEmployeeLinkToQueue, addCompanyDataToQueue, addEmployeeDataToQueue } = require('../../Db/Queues/addToQueue');
const { getCompanyLinkFromQueue, getQueueSize } = require('../../Db/Queues/getFromQueue');
const { genRandNum, date, time, log, getCookie, pageScroller, getTimeDiffAndTimeZone } = require('../../modules');
const { getCompanyData, getEmployeeData } = require('../../modules/scrappers/salesNavigator');
const { errorHandler } = require('../olegLeadsController');

let startTime = new Date().getTime();
let randNo;
// Change this numbers for wait time between requests
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { COMPANY_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;

const getCompanyDataFromPage = async () => {
    const descriptionBtn = 'a.copy-linkedin-overflow';
    const threeDots = 'button.company-topcard-actions__overflow-toggle';
    const copyLinkedinUrlBtn = 'ul.company-topcard-actions__overflow-dropdown a.copy-linkedin-overflow';

    let queueSize = await getQueueSize(COMPANY_LINK);

    if (queueSize) {
        console.log('The size of the queue is ',queueSize);

        const browser = await puppeteer.launch({
            headless: false
        });

        const page = await browser.newPage();
        
        try {
            const account = process.argv[3];
            
            await setCookies(page, account);
        } catch (error) {
            const imageName = `${date()}-${time()}.png`;
            const errorLocation = 'companypage in SalesNavigator getCompanyDataFromPage()';
            const extraInfo = `Error while adding caching. Image name in google drive: ${imageName}`;
            
            await errorHandler(page, error, errorLocation, extraInfo, imageName);
            process.exit();
            dbConnection.close();
        }
        
        for (let i = 0; i < queueSize; i++) {
            try {
                const salesNavCompanyUrl = await getCompanyLinkFromQueue();

                await page.goto(salesNavCompanyUrl, { timeout: 250000, waitUntil: 'networkidle2'});

                log(`\n\nOn companing page now waiting for 5secs`);
                await page.waitFor(5000);
                await page.click(threeDots);
                await page.waitFor(500);
                await page.click(copyLinkedinUrlBtn);
            
                let company_linkedin_url = clipboardy.readSync();

                await page.waitFor(500);
                
                if (await page.$(descriptionBtn) !== null) await page.click(descriptionBtn);

                const companyHandle = await page.evaluateHandle(() => document.body);
                const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
                const companyHtmlPage = await resultcompanyHandle.jsonValue();

                const scrappedCompData = getCompanyData(companyHtmlPage);

                let foundCompany = await getCompany({ name: scrappedCompData.name});

                if (!foundCompany) {
                    await insertCompany(scrappedCompData, company_linkedin_url, salesNavCompanyUrl);
                } else {
                    log(`Company ${scrappedCompData.name} exists`);
                }

                await companyHandle.dispose();

                randNo = genRandNum(MIN_TIME, MAX_TIME);
                console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to employees url`);
                await page.waitFor(randNo);

                await page.goto(scrappedCompData.employees_link, { timeout: 250000, waitUntil: 'networkidle2'});

                await pageScroller(page);

                await page.waitFor(3000);

                const employeesHandle = await page.evaluateHandle(() => document.body);
                const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                const employeesHtmlPage = await resultEmployeesHandle.jsonValue();

                const { pagination, employees } = getEmployeeData(employeesHtmlPage);
                console.log('Scrapped Employees', employees);
                
                if (Array.isArray(employees) && employees.length) {
                    await insertEmployees(employees, scrappedCompData);
                } else {
                    console.log('We did not find anyone on this page');
                }

                if (pagination) generatePaginationUrls(pagination, scrappedCompData);

                randNo = genRandNum(MIN_TIME, MAX_TIME);
                console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to company url`);
                await page.waitFor(randNo);
            } catch (error) {
                const imageName = `${date()}-${time()}.png`;
                const errorLocation = 'companypage in SalesNavigator getCompanyDataFromPage()';
                const extraInfo = `Error while getting companydata. Image name in google drive: ${imageName}`;
                
                await errorHandler(page, error, errorLocation, extraInfo, imageName);

                randNo = genRandNum(MIN_TIME, MAX_TIME);
                console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to company url`);
                await page.waitFor(randNo);
            }
        }
        process.on("unhandledRejection", (reason, p) => {
            console.error("ERROR", p, "reason:", reason);
            browser.close();
            dbConnection.close();
        });          
        
    } else {
        console.log('The Queue is empty');
        process.exit();
        dbConnection.close();
    }
}

const setCookies = async (page, account) => {
    let cookie = [];
    switch (account) {
        case 'acc1':
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
            break;
        case 'acc2':
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
            break;
        default:
            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
    }

    await page.setCookie(...cookie);
}

const insertCompany = async (compObj, linkedin_url, sales_url) => {
    const { name, employees_link, country, company_url, description} = compObj;
    
    log(`I am adding the company ${name} to the db`);

    await addCompanyDataToQueue({
        name: name,
        linkedin_url,
        sales_url,
        technologies: "",
        employees_link: employees_link,
        country: country,
        company_url: company_url,
        created_at: date(),
        took_all_employees: true,
        description: description,
        sheet_range: "",
        response_count: {
            positive: 0,
            negative: 0
        }
    });
}

const insertEmployees = async (employees, company) => {
    const { name, employee_url, position, country } = employees;

    const employee_table_data = [];

    for (let i = 0; i < employees.length; i++) {
        const doesEmployeeExist = await getEmployee({ fullname: name, position: position });

        if (!doesEmployeeExist) {
            const { timezone, time_diff } = getTimeDiffAndTimeZone(country);
            
            employee_table_data.push({
                "fullname" : name,
                position,
                "company" : company.name,
                "company_url" : company.company_url,
                country,
                "linkedin_url" : employee_url,
                "email" : "No email",
                "phone" : "No Phone number",
                timezone, time_diff,
                "created_at" : date()
            }); 
        } else {
            log('Employee already exists');
        }
    }
    console.log("\n\nEmployees data", employee_table_data); 

    await addEmployeeDataToQueue(employee_table_data);
}

const generatePaginationUrls = async (pagination, companyData) => {
    console.log("Generating url paginations and passing to queue");
    
    const regex = /(&page=)[0-9]+/;
    let employeePaginationsUrls = [];
    
    for (let i = 2; i <= pagination; i++) {
        let url = companyData.employees_link.replace(regex, `$1${i}`);
        let company = companyData.name;
        let company_url = companyData.website;
        employeePaginationsUrls.push({url, company, company_url});
    }

    console.log(employeePaginationsUrls);

    if (Array.isArray(employeePaginationsUrls) && employeePaginationsUrls.length){
        for (let paginate of employeePaginationsUrls) {
            await addEmployeeLinkToQueue(paginate)
        }
    }
}

module.exports = getCompanyDataFromPage;