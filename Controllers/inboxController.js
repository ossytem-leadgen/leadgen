const cheerio = require('cheerio');
const {google} = require('googleapis');
const { URLS, CONSTANTS } = require('../helpers/constants');
const { log, date, time, sleep, sheetAuthentication, getCookie, getBrowser } = require('../modules');
const { addInbox, getInbox, updateInbox } = require('../Db/Inboxes');
const { addAcceptedConnects } = require('../Db/AcceptedConnects');
const { closeAllMsgBoxes } = require('./olegLeadsController');
const sendEmail = require('./emailSenderController');
const {sendMsgToAdmin} = require('./botController');

const { GET_ACCEPTED_CONNECTS } = CONSTANTS.SCRIPT_PROCESS;
const { ID, STATS_SHEETS: { INBOX } } = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN_STATISTICS;

const messageList = 'ul.msg-conversations-container__conversations-list li';
const messageListName = '.msg-conversation-card__row h4.msg-conversation-listitem__participant-names';
const messageInMsgList = '.msg-conversation-card__row p.msg-overlay-list-bubble__message-snippet.msg-overlay-participant-headline';
const unreadCount = "p.msg-conversation-card__unread-count span";
const unreadListTag = messageList.concat(' ', unreadCount);
const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';

// Run 24 hours
const monitorPage = async (asyncFuntion, functionName) => {
    log('Monitoring page has been triggered');

    const url = URLS.home;

    const browser = await getBrowser();

    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);
        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }

    await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2'});

    if (functionName && functionName === GET_ACCEPTED_CONNECTS) {
        await page.click('button.msg-overlay-bubble-header__button');
    }

    if (await page.$(closeMsgBox) !== null) await page.click(closeMsgBox);

    const runInterval = () => {
        const interval = setInterval(async () => {
            await asyncFuntion(page, closeMsgBox);

            clearInterval(interval);

            runInterval();
        }, 30000);
    };

    runInterval();
};

const watchForNewMessage = async (page, closeMsgBox) => {
    const newInbox = 'button.msg-overlay-bubble-header__show-hide';

    console.log('\nI am checking for new messages');

    if (await page.$(newInbox) !== null) {
        await page.click('button.msg-overlay-bubble-header__button');
        let searchHandle = await page.evaluateHandle(() => document.body);
        let resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
        let html = await resultHandle.jsonValue();
        let $ = cheerio.load(html);

        await page.waitFor(2000);

        let newInboxesArr = [];

        $('ul.msg-conversations-container__conversations-list li').each((i, elem) => {
            const unreadNumString = $(elem).find("p.msg-conversation-card__unread-count span").text().trim();
            const unreadName = $(elem).find('.msg-conversation-card__row h4.msg-conversation-listitem__participant-names').text().trim();

            if (unreadNumString) {
                const unreadNum = Number(unreadNumString);
                newInboxesArr.push({name: unreadName, num: unreadNum});
            }
        });

        console.log('newInboxesArr',newInboxesArr);
        await page.click('button.msg-overlay-bubble-header__button');

        const totalInboxes = (await page.$$(newInbox)).length;

        for (let i = 0; i < totalInboxes; i++) {
            await page.click(newInbox);

            await page.waitFor(1000);

            searchHandle = await page.evaluateHandle(() => document.body);
            resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
            html = await resultHandle.jsonValue();
            $ = cheerio.load(html);
            
            await page.waitFor(1000);

            const leadname = $('header.msg-overlay-bubble-header.msg-overlay-conversation-bubble--header h4').text().trim();
            const allMessages = [];
            // const message = $('div.msg-s-message-list-container ul li').last().find('div.msg-s-event-listitem__message-bubble').text().trim();
            $('div.msg-s-message-list-container ul li').each((i, elem) => {
                let msg = $(elem).find('div.msg-s-event-listitem__message-bubble p').text().trim();
                if (msg) allMessages.push(msg)
            });

            let message = '';
            newInboxesArr.some(val => {
                if (val.name === leadname) {
                    let len = allMessages.length;
                    for (let i = 1; i <= val.num; i++) {
                        message = allMessages[len - i] + '. ' + message;
                    }
                }
            });
            
            if (message.length) {
                // url = https://www.linkedin.com/messaging/?searchTerm=firstname%20lastname
                const check4Space = /\s/gi;
                const linkedin_url = URLS.messages.concat('?searchTerm=', leadname.replace(check4Space, '%20'));
                console.log('Linkedin Url', linkedin_url);
    
                const inboxData = {
                    "name" : leadname,
                    "linkedin_url" : linkedin_url,
                    "time" : time(),
                    "date" : date(),
                    "message" : message,
                    "email_sent" : false
                };
    
                log('Inbox: ', inboxData);
    
                log('Saving the message to the db');
                await searchHandle.dispose();
                await addInbox(inboxData);
                await pushToSheet(inboxData);
            } else {
                log('Message was empty');
            }

            await page.click(closeMsgBox);
        }
    } else {
        log('No new inbox');
    }
};

const watchForAcceptedConnects = async (page, closeMsgBox) => {
    console.log('\nI am checking for new connects');

    if (await page.$(unreadCount) !== null) {
        const searchHandle = await page.evaluateHandle(() => document.body);
        const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
        const html = await resultHandle.jsonValue();
        // const html = fs.readFileSync(process.cwd() + page, 'utf8');
        const $ = cheerio.load(html);
    
        let acceptedConnects = [];
    
        $(messageList).each(async (i, elem) => {
            const unreadNumString = $(elem).find(unreadCount).text().trim();
            const newConnect = $(elem).find(messageInMsgList).text().trim();
            const connectRegex = /\s+is now a connection/g;
            const testIfNewConnect = connectRegex.test(newConnect);
            
            if (testIfNewConnect && unreadNumString) {
                const unreadName = $(elem).find(messageListName).text().trim();
                acceptedConnects.push({lead_name: unreadName, date: date(), time: time() });
            }

            await page.waitFor(2000);
        });
        
        log('Click unread message');
        
        if (await page.$(unreadListTag) !== null) {
            const totalUnreadListTag = (await page.$$(unreadListTag)).length;
            
            for (let i = 0; i < totalUnreadListTag; i++) {
                if (await page.$(unreadListTag) !== null) await page.click(unreadListTag);
                await sleep(0.5);
            }
        }

        console.log('Accepted Connects: ', acceptedConnects);
        
        if (Array.isArray(acceptedConnects) && acceptedConnects.length) {
            await addAcceptedConnects(acceptedConnects);
        }
    
        await searchHandle.dispose();

        await closeAllMsgBoxes(page, closeMsgBox, '', '', `${date()}-${time()}.png`);
    } else {
        log('No new connects');
    }
};

const pushToSheet = (leadObj) => {
    return new Promise((resolve, reject) => {
      sheetAuthentication( (auth) => {
        const sheets = google.sheets({ version: 'v4', auth });

        sheets.spreadsheets.values.append({
            spreadsheetId: ID,
            range: INBOX.NAME,
            valueInputOption: 'RAW',
            resource: {
                "values": [
                    [leadObj.name, leadObj.linkedin_url, leadObj.date, leadObj.time, leadObj.message]
                ]
            }
        }, async (err, res) => {
            if (err) return reject(`pushToSheet() The API returned an error: ${err}`);
            
            log('Pushed completely', res.data);
            
            resolve('Pushed completely');
          });
      });
    });
};

//Run every 9am
const emailStatistics = async _ => {
    log('Sending Statistics has been triggered');

    try {
        const yesterdayInboxes = await getInbox({email_sent: false});
    
        if (Array.isArray(yesterdayInboxes) && yesterdayInboxes.length) {
    
            await sendEmail({reply: true, messages: yesterdayInboxes});
            
            await sleep(3);
        
            for (let inbox of yesterdayInboxes) {
                const { _id } = inbox;
        
                await updateInbox({_id}, {email_sent: true});
            }
        } else {
            console.log('There was no inbox yesterday');
        }
    } catch (error) {
        await sendMsgToAdmin(`This error occured in emailStatistics.\n${error}`);
    }
};


const getMsgsOfPast3days = async () => {
    const url = URLS.home;
    const browser = await getBrowser();

    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
        process.exit();
    }

    await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2'});

    await page.click('button.msg-overlay-bubble-header__button');

    const searchHandle = await page.evaluateHandle(() => document.body);
    const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
    const html = await resultHandle.jsonValue();
    // const html = fs.readFileSync(process.cwd() + page, 'utf8');
    const $ = cheerio.load(html);

    let acceptedConnects = [];

    $(messageList).each(async (i, elem) => {
        const unreadNumString = $(elem).find(unreadCount).text().trim();
        const newConnect = $(elem).find(messageInMsgList).text().trim();
        const connectRegex = /\s+is now a connection/g;
        const testIfNewConnect = connectRegex.test(newConnect);
        
        if (testIfNewConnect && unreadNumString) {
            const unreadName = $(elem).find(messageListName).text().trim();
            acceptedConnects.push({lead_name: unreadName,  message: newConnect, date: date(), time: time() });
        }

        await page.waitFor(2000);
    });
    
    log('Click unread message');
    
    if (await page.$(unreadListTag) !== null) {
        const totalUnreadListTag = (await page.$$(unreadListTag)).length;
        
        for (let i = 0; i < totalUnreadListTag; i++) {
            if (await page.$(unreadListTag) !== null) await page.click(unreadListTag);
            await sleep(0.5);
        }
    }

    console.log('Accepted Connects: ', acceptedConnects);
    
    if (Array.isArray(acceptedConnects) && acceptedConnects.length) {
        await addAcceptedConnects(acceptedConnects);
    }

    await searchHandle.dispose();

    await closeAllMsgBoxes(page, closeMsgBox, '', '', `${date()}-${time()}.png`);

};

module.exports = {
    monitorPage,
    watchForNewMessage,
    watchForAcceptedConnects,
    emailStatistics
}