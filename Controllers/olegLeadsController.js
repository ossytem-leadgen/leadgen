const cheerio = require('cheerio');
const {google} = require('googleapis');
const emojiLogger = require('console-emoji');
require('dotenv').config();

const sendEmail = require('./emailSenderController');
const uploadToDrive = require('./googleDriveController');
const { date, time, genRandNum, log, emojis, len, getBrowser, getTimeDiffAndTimeZone,
        testIfNameIsWord, testIfNameHasDot, handleUncaughtError, getCookie, sleep, pageScroller,
        sheetAuthentication, closeAndWait, seenMsg, repliedMsg, getAuthenticatedSheet, checkIfUrlIsValidCompany
    } = require('../modules');
const { CONSTANTS, URLS, dbFields } = require('../helpers/constants');
const { getOlegContacts, updateOlegContacts, addOlegContact } = require('../Db/OlegContacts');
const { getMobileLeads } = require('../Db/MobileLeads');
const { getSalesLeads } = require('../Db/SalesLeads');
const { getGeneratedLeads } = require('../Db/GeneratedLeads');
const { appendToSheet, updateSheet } = require('./spreadsheetController');
const login = require('./authorization');
const {sendMsgToAdmin} = require('./botController');
const { getAcceptedConnects } = require('../Db/AcceptedConnects');
const { getCompany } = require('../Db/Companies');
const { addCompanyLinkToQueue } = require('../Db/Queues/addToQueue');
const { getAllEmployeeList } = require('../modules/scrappers');
const scrapCompanyHtmlPageInfo = require('./General/scrapCompanyHtmlPageInfo');

const { CONDITIONS, SCRIPT_STATUS, SEND_MSG_INTERVALS } = CONSTANTS.OLEG_MSG;
const { CHATED, NOT_CHATED, IN_USA, NOT_USA } = CONDITIONS;
const { DONE, RESPONDED } = SCRIPT_STATUS;
const { BEFORE_LAUNCH, AFTER_LAUNCH } = SEND_MSG_INTERVALS;
const { 
    LEADGEN: { ID, MAIN_SHEETS: { DEAD_LEADS, OLEG_CONTACTS, OLEG_USA_CONTACTS }}, 
    THREE_REGION_CAMPAIGN
    } = CONSTANTS.SPREAD_SHEET_PARAMS;
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const errorLocation = 'olegsLeadsController';
const extraInfo = `This error was caught by uncaughtException`;
const linkedin = URLS.home;
let randNo;

const getCategoryMessages = (nextMsg, condition) => {
    return new Promise((resolve, reject) => {
        sheetAuthentication((auth) => {
            const sheets = google.sheets({version: 'v4', auth});
            let result;
            sheets.spreadsheets.values.get({
                spreadsheetId: ID,
                range: `${DEAD_LEADS.NAME}!A2:G2`,
            }, (err, res) => {
                if (err) reject('The API returned an error: ' + err);
                const rows = res.data.values;
                if (nextMsg === 1 && condition === NOT_CHATED) result = rows[0][0];
                if (nextMsg === 1 && condition === CHATED) result = rows[0][1];
                if (nextMsg === 2) result = rows[0][2];
                if (nextMsg === 3) result = rows[0][3];
                if (nextMsg === 4 && condition === IN_USA) result = rows[0][4];
                if (nextMsg === 4 && condition === NOT_USA) result = rows[0][5];
                if (nextMsg === 5) result = rows[0][6];
                resolve(result);
            });
        });
    });
};

const sendAllMsgsToDeadLeads = async () => {
    const olegsLeads = await getOlegContacts({
        sendMessage: true, num_msg_sent: 1, date_msg_one_responded: ""
    });
    log('WE ARE WORKING WITH ', olegsLeads.length, ' LEADS');

    const browser = await getBrowser();

    const page = await browser.newPage();

    const url = URLS.connections;

    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        const errorLocation = 'Setting cookies in olegsLeadsController.js, function sendAllMsgsToDeadLeads()';
        const imageName = `${date()}-${time()}.png`;
        const extraInfo = `I was trying to set the cookies with Olegs account while it fell apart. Image name in google drive: ${imageName}`;

        await errorHandler(page, error, errorLocation, extraInfo, imageName);
        const pid = process.pid;
        process.kill(pid);
    }
    
    await page.setViewport({ width: 1366, height: 868 });

    const searchResponse = await page.goto(url, {
        timeout: 250000,
        waitUntil: 'networkidle2'
    });

    for (let lead of olegsLeads) {
        const { _id, lead_name, country, ifChatted, num_msg_sent, inUSA  } = lead;
        const discardBtn = 'button.msg-modal__button.ok-btn.button-primary-large';
        const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';

        const {firstname} = getFirstAndLastName(lead_name);
        console.log('\nfirstname', firstname);
        
        const firstAndLastName = mergeFirstandLastname(lead_name, firstname);
        console.log('\nfirstAndLastName', firstAndLastName);

        randNo = genRandNum(MIN_TIME, MAX_TIME);

        try {
            let { originalFollowupMsg, leadStatus, dateMsgSentField, 
                    dateMsgReadField, dateMsgRepliedField 
                  } = await determineLeadNextStep(num_msg_sent, ifChatted);

            let newFollowupMsg = '';

            if (originalFollowupMsg.length) {

                if (originalFollowupMsg === DONE) {

                  newFollowupMsg = '';

                } else {
                  newFollowupMsg = originalFollowupMsg.replace(/--name--/g, firstname);
                  newFollowupMsg = newFollowupMsg.replace(/--capital city--/g, country);
                }
    
                log('\n-----------------------------------------------------------------------');
                log(`CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nMessage: ${newFollowupMsg}\nCurrent Time:${time()}\n`);
      
                if (searchResponse._status < 400) {
                    await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
                    
                    const searchBox = '.msg-connections-typeahead__search-field.msg-connections-typeahead__search-field--no-recipients';
                    await page.click('button.msg-overlay-bubble-header__control');

                    await page.waitFor(1000);
                    
                    if (await page.$(searchBox) !== null) {

                        await page.type(searchBox, firstAndLastName, { delay: 50 });

                        await page.waitFor(10000);
    
                        const nameSearchResBtn = 'button.msg-connections-typeahead__search-result';
                       
                        if (await page.$(nameSearchResBtn) !== null) {
                            await page.click(nameSearchResBtn);
                            await page.waitFor(5000);
                            
                            const searchHandle = await page.evaluateHandle(() => document.body);
                            const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
                            const html = await resultHandle.jsonValue();
                            
                            await page.waitFor(2000);
                
                            let send = true;
                            let reply = repliedMsg(html, lead_name);
        
                        //   if (dateMsgReadField.length) {
                            if (seenMsg(html)) {
                              log('This lead read our last message', lead_name);
                              
                              await updateOlegContacts({_id}, { [dateMsgReadField] : date() });
                              
                              await page.waitFor(2000);
                            }
    
                            if (reply.replied) {
                             
                              log(`Lead replied, here is what he said: ${reply.message}`);
    
                              const fields = {
                                "lead_status" : RESPONDED,
                                [dateMsgRepliedField] : date(),
                                "date_of_last_interaction" : date()
                              };
    
                              await updateOlegContacts({ _id }, fields);
                              
                              await page.waitFor(2000);
    
                              send = false;
                            // } else {
                            //   send = true;
                            }
                        //   } else {
                        //     //Send the message
                        //     log(lead_name, ' did not reply lets send the next one');
                        //     send = true;
                        //   }
            
                          if (send) {
    
                            if (newFollowupMsg.length) {
    
                              log('Typing.....');
    
                              await page.type('.msg-form__contenteditable', newFollowupMsg, { delay: 50 });
    
                              log('Sent :)');
    
                              await page.click('.msg-form__send-button');
    
                              let fields = {
                                [dateMsgSentField] : date(),
                                "num_msg_sent" : num_msg_sent + 1,
                                "date_last_interaction" : date()
                              };
    
                              await updateOlegContacts({_id}, fields);
    
                              await page.waitFor(3000);
                            } else {
                              //If the message is empty that means this is the 5th message
                              log('\nThis lead replied to none of our messages, update db and thats all');
    
                              let fields = {
                                date_last_interaction: date(),
                                lead_status: leadStatus
                              };
    
                              await updateOlegContacts({ _id: _id}, fields);
                            }
    
                            await searchHandle.dispose();
    
                            await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
                            
                            await closeAndWait(page, randNo);    
                          } else {
                            await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
                            
                            await closeAndWait(page, randNo);
                          }
    
                          await searchHandle.dispose();
    
                        } else {
                          log('Didnt find lead ',lead_name ,' in search box');
                          
                          await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);

                          await closeAndWait(page, randNo);
                        }
                    }                    
                  }
            } else {
                log('\nWas not able to pick a message to send');
            }   
        } catch (error) {

            const errorLocation = 'olegsLeadsController.sendAllMsgsToDeadLeads()';
            const imageName = `${date()}-${time()}.png`;
            const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n`;

            await errorHandler(page, error, errorLocation, extraInfo, imageName);
            
            await closeAndWait(page, randNo);
            const pid = process.pid;
            process.kill(pid);
        }
    }
};

const sendFirstMsgToAllDeadLeads = async _ => {
    const olegsLeads = await getOlegContacts({sendMessage: true, num_msg_sent: 1});
    const url = URLS.connections;
    
    log('WE ARE WORKING WITH', olegsLeads.length, 'LEADS');

    let finished = false;
    const browser = await getBrowser();
    const page = await browser.newPage();

    await page.setViewport({ width: 1366, height: 868 });

    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);

    } catch (error) {
        const errorLocation = 'Setting cookies in olegsLeadsController.js, function sendFirstMsgToAllDeadLeads()';
        const imageName = `${date()}-${time()}.png`;
        const extraInfo = `I was trying to set the cookies with Olegs account while it fell apart. Image name in google drive: ${imageName}`;
        
        await errorHandler(page, error, errorLocation, extraInfo, imageName);
    }

    await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2' });

    if (olegsLeads.length) {

        for (let lead of olegsLeads) {

            const { _id, lead_name, ifChatted, num_msg_sent, time_diff, sendMessage } = lead;

            if (num_msg_sent === 0 || num_msg_sent == 0 && sendMessage) {
                const {firstname} = getFirstAndLastName(lead_name);
                const firstAndLastName = mergeFirstandLastname(lead_name, firstname);
                
                randNo = genRandNum(MIN_TIME, MAX_TIME);
        
                try {
                    let condition;
                    let originalFollowupMsg;
                    let newFollowupMsg;
            
                    switch(num_msg_sent) {
                        case 1:
                          condition = ifChatted === true ? CHATED : NOT_CHATED;
                          originalFollowupMsg = await getCategoryMessages(2, condition);
                          break;

                        default:
                          log('There is no message for this message number', num_msg_sent);
                          break;
                    }
            
                    if (originalFollowupMsg.length) {

                        const searchResDiv = '.mn-connection-card';
                        const discardBtn = 'button.msg-modal__button.ok-btn.button-primary-large';
                        const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';
                        const sendBtn = '.msg-form__send-button';
                        
                        newFollowupMsg = originalFollowupMsg.replace(/--name--/g, firstname);

                        log(`CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name: ${lead_name}\nCurrent Time: ${time()}\nMessage: ${newFollowupMsg}\n`);
    
                        // if (searchResponse._status < 400) {
                            log('Clearing old message');

                            const searchInputHandle = await page.$('.mn-connections__search-input');
                            
                            await searchInputHandle.click();
                            await searchInputHandle.focus();
                            await searchInputHandle.click({clickCount: 3});
                            await searchInputHandle.press('Backspace');
                            
                            log('Searching......', emojis.typing);
                            
                            await searchInputHandle.type(firstAndLastName, { delay: 50 });
                            
                            await page.waitFor(10000);
    
                            if (await page.$(searchResDiv) !== null) {
                                log('Check if online and if lead is in working hours');

    
                                //profileDiv = await page.evaluate(() => document.querySelector('div.presence-indicator.presence-entity__indicator span').innerText);
                                // if (profileDiv.trim() != "Status is offline" && checkIfWorkingHour(time_diff)) {
                                if (checkIfWorkingHour(time_diff)) {
                                    await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
    
                                    await page.click('button.message-anywhere-button');
                                    await page.waitFor(1000);
                                    
                                    log('Typing.....', emojis.typing);
                                    
                                    await page.type('.msg-form__contenteditable', newFollowupMsg, { delay: 50 });
                                    
                                    await page.waitFor(1000);
                                    
                                    log('Sent', emojis.coolGlasses);
                                    
                                    await page.click(sendBtn);
                                    await page.waitFor(1000);
                                    await page.click(closeMsgBox);
    
                                    const fields = {
                                        date_msg_one_sent : date(),
                                        "num_msg_sent" : 1,
                                        "date_last_interaction" : date()
                                    };
                                    
                                    await updateOlegContacts({ _id: _id}, fields);
                                    
                                    log(`Waiting for ${randNo} seconds`);
                                    
                                    await page.waitFor(randNo);

                                    await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
                                } else {
                                    log('This lead is neither online nor in a working hour now');
                                    await closeAndWait(page, randNo);
                                }
                            } else {
                                log('Didnt find lead ', lead_name , ' in search box');
                                await closeAndWait(page, randNo);
                            }
                        // }
                    } else {
                        await closeAndWait(page, randNo);
                    } 
                } catch (error) {
                    const errorLocation = 'olegsLeadsController.sendFirstMsgToAllDeadLeads()';
                    const imageName = `${date()}-${time()}.png`;
                    const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n. Image name in google drive: ${imageName}`;
    
                    await errorHandler(page, error, errorLocation, extraInfo, imageName);
                    const pid = process.pid;
                    process.kill(pid);
                }
            } else {
                log('We have already send message to this lead. Number of msg sent is', num_msg_sent);
            }
        }
    } else {
        log('We have sent the 1st message to all the leads');
    }
};

const getOlegLeadsProfile = async () => {
    const fields = {
        linkedin_url: {$ne: ""},
        invalid_linkedin_url: {$exists: false},
        skype: {$exists: false}
    };

    const olegLeads = await getOlegContacts(fields);
    // const olegLeads = await getOlegContacts({linkedin_url: {$ne: ""}, country: ""});

    let totalErrors = 0;

    if (len(olegLeads)) {
        log('The size of the people we are working with is: ', len(olegLeads));

        const browser = await getBrowser();

        const page = await browser.newPage();
        
        try {
            const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

            await page.setCookie(...cookie);
        } catch (error) {
            console.error(error);
        }

        await page.setViewport({ width: 1366, height: 868 });

        for (const oLead of olegLeads) {
            const { _id, linkedin_url, lead_name, position } = oLead;

            log(`\n\nCURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name: ${lead_name}`);

            try {
                if (linkedin_url) {
                    log("\n\n1. Let's make request to employees url");
                    
                    await page.goto(linkedin_url, { timeout: 200000, waitUntil: 'networkidle2' });

                    const unAvailableProfile = ".profile-unavailable";
    
                    await page.waitFor(2000);
    
                    let olegCollection = dbFields.new_oleg_contacts;
    
                    await page.waitFor(1000);
    
                    const contactInfo = ".pv-top-card-v2-section__links a.pv-top-card-v2-section__link.pv-top-card-v2-section__link--contact-info";
                    
                    if (await page.$(contactInfo) !== null) {

                        // STAGE ONE: Get all contact information
                        await page.click(contactInfo);
        
                        await page.waitFor(5000);
        
                        const personHandle = await page.evaluateHandle(() => document.body);
                        const resultpersonHandle = await page.evaluateHandle(body => body.innerHTML, personHandle);
                        const html = await resultpersonHandle.jsonValue();
                        const $ = cheerio.load(html);
        
                        olegCollection.lead_name = $('h1.pv-top-card-section__name').text().trim() || lead_name;
    
                        const { firstname, lastname } = getFirstAndLastName(olegCollection.lead_name);
                        olegCollection.firstname = firstname;
                        olegCollection.lastname = lastname;
    
                        olegCollection.position = $('h2.pv-top-card-section__headline').text().trim() || position;
                        olegCollection.country = $('h3.pv-top-card-section__location').text().trim() || "";
                        olegCollection.company = $('.pv-top-card-v2-section__entity-name.pv-top-card-v2-section__company-name').text().trim() || "";
                        olegCollection.linkedin_url =  $('section.ci-vanity-url .pv-contact-info__ci-container a').attr('href') || linkedin_url;
                        olegCollection.website = $('section.ci-websites li.pv-contact-info__ci-container a').attr('href') || "";
                        olegCollection.phone = $('section.ci-phone li span').text().trim() || "";
                        olegCollection.address = $('section.ci-address .pv-contact-info__ci-container a').text().trim() || "";
                        olegCollection.email = $('section.ci-email .pv-contact-info__ci-container a').text().trim() || "";
                        olegCollection.twitter = $('section.ci-twitter li.pv-contact-info__ci-container a').attr('href') || "";
                        olegCollection.skype = $('section.ci-ims li span').text().trim() || "";
                        olegCollection.birthday = $('section.ci-birthday .pv-contact-info__ci-container span').text().trim() || "";
                        
                        const { timezone, time_diff } = getTimeDiffAndTimeZone(olegCollection.country);
                        olegCollection.timezone = timezone;
                        olegCollection.time_diff = time_diff;
                        
                        olegCollection.connected_date = $('section.ci-connected span.pv-contact-info__contact-item').text().trim() || "";
                        
                        const company_url = $('li.pv-profile-section__card-item-v2 a').attr('href') || "";
                        olegCollection.company_url = `${linkedin}${company_url}`;

                        // STAGE TWO: Get company information
                        const companySearchFields = {
                            $or: [
                                {name: olegCollection.company },
                                {linkedin_url: olegCollection.linkedin_url}
                            ]
                        };

                        const companyExists = await getCompany(companySearchFields);
                        
                        if (!len(companyExists) && checkIfUrlIsValidCompany(olegCollection.company_url)) {
                            log('\nThe company doenst exist and its a valid url lets add the Company url to queue');
                            
                            const companyDetails = await scrapCompanyHtmlPageInfo(browser, olegCollection.company_url, 0);
                            
                            if (len(companyDetails)) {
                                console.log('\n\nFound the company details');
                                
                                const [{name = "", company_size = "", founded="", description = ""}] = companyDetails;
                                
                                //If the name is avaible then use it else pick them name of the persons page
                                name ? olegCollection.company = name : olegCollection.company;

                                olegCollection.company_founded = founded;
                                olegCollection.company_headcount = company_size;
                                olegCollection.company_description = description;
                            }
                        } else if (len(companyExists)) {
                            log('\nThe company exists');

                            const [{company_size = "", founded="", description = ""}] = companyExists;
                            
                            olegCollection.company_founded = founded;
                            olegCollection.company_headcount = company_size;
                            olegCollection.company_description = description;
                        } else {
                            olegCollection.company_founded = "";
                            olegCollection.company_headcount = "";
                            olegCollection.company_description = "";
                        }

                        // STAGE THREE: Push to the spreadsheet
                        const sheetFields = Object.keys(olegCollection)
                                            .filter((val, i) => i < 18)                      
                                            .map(data => olegCollection[data] || "-");
                        olegCollection.sheetrange = await appendToSheet(OLEG_CONTACTS.NAME, sheetFields);
                        
                        console.log(olegCollection);

                        // STAGE FOUR: Update in the database
                        await updateOlegContacts({_id}, {...olegCollection});
        
                        await personHandle.dispose();
                    } else if (await page.$(unAvailableProfile) !== null) {
                        await updateOlegContacts({_id}, {invalid_linkedin_url: true});
                    } else {
                        await updateOlegContacts({_id}, {unknown_error: true});                        
                    }
                } else {
                    log('I did not get this url for the person');
                }
                
                console.log(`Total amount of error is ${totalErrors}`);
            } catch (error) {
                totalErrors++;

                const errorLocation = 'olegsLeadsController.getOlegLeadsProfile';
                const imageName = `${date()}-${time()}.png`;
                const extraInfo = `CURRENT LEAD DATA:\n
                                   Lead ID: ${_id}\n
                                   Lead Name: ${lead_name}\n
                                   Current Time:${time()}\n
                                   Image name in google drive: ${imageName}`;
                
                await errorHandler(page, error, errorLocation, extraInfo, imageName);
                
                if (totalErrors === 3) {
                    await login(CONSTANTS.CREDENTIALS.OLEG.NAME,
                                CONSTANTS.CREDENTIALS.OLEG.PWD);
                    
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);
                    await page.setCookie(...cookie);
                } else if (totalErrors > 3) {
                    const pid = process.pid;
                    process.kill(pid);
                }
            }

            const PAUSE = CONSTANTS.SCRIPT_PAUSE.SEND_CONNECTS;

            randNo = genRandNum(PAUSE.MIN_TIME, PAUSE.MAX_TIME);

            log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);

            await page.waitFor(randNo);
        }
    } else {
        log('The queue is now empty');
    }
};

const updateOlegConnects = async () => {
    const oLeads = await getOlegContacts({"disconnected": false});
    // const oLeads = await getOlegContacts({threeRegionCampaign: {$exists: false}, company_url: {$exists: true, $regex: /company/g}});
    // const oLeads = await getOlegContacts({company_description: "", company_founded: "",company_headcount: "" , company_url: {$exists: true, $regex: /company/g}});

    log(`We are working with ${len(oLeads)} people`);
    const allCompanies = [];

    for (const lead of oLeads) {
        let { _id, lead_name } = lead;

        // const { firstname, lastname } = getFirstAndLastName(lead_name);
        // lead.firstname = firstname;
        // lead.lastname = lastname;
        
        const fields = Object.keys(dbFields.new_oleg_contacts)
                                .filter((val, i) => i < 18)
                                .map(val => lead[val] || '-');

        const sheetrange = await appendToSheet(OLEG_CONTACTS.NAME, fields);
        
        await updateOlegContacts({ _id }, {sheetrange});
        
        await sleep(2);

        
        // if (company) {
        //     const companyExists = await getCompany({ $or: [{name: company},{company_url}]});

        //     if (len(companyExists)) {
        //         // const [{company_size = "", founded="", description = ""}] = companyExists;

        //         // lead.num = (Number(sheetrange.split("Z")[1]) - 1).toString();
        //         // lead.firstname = firstname;
        //         // lead.lastname = lastname;
        //         // lead.company_founded = founded;
        //         // lead.company_headcount = company_size;
        //         // lead.company_description = description;
        //         // lead.company_updated = true;

        //         // console.log(lead);

        //         // const fields = Object.keys(dbFields.new_oleg_contacts)
        //         //                 .filter((val, i) => i < 15)
        //         //                 .map(val => len(lead[val]) ? lead[val] : '-');

        //         await updateSheet(sheetrange, fields, THREE_REGION_CAMPAIGN.ID);
                
        //         // await updateOlegContacts({ _id }, {...lead});
        //     } else {
        //         log('This company doesnt exist in our db ', company);
        //         // await addCompanyLinkToQueue(company_url);
        //         allCompanies.push({company,  company_url})
        //     }
        // }
        // await sleep(2);
    }
    // console.log('The total number of companies are: ', len(allCompanies));

    // console.log(allCompanies);

    process.exit();
};

const pushAllLeadsToSheet = async () => {
    const oLeads = await getOlegContacts({inUSA: true});
    // const oLeads = await getOlegContacts({ uploaded: {$exists: false}, country: {$ne: ""} });

    for (let olead of oLeads) {
        log(olead)
        const { _id, lead_name } = olead;
        const { firstname, lastname } = getFirstAndLastName(lead_name);
        olead.firstname = firstname;
        olead.lastname = lastname;

        const fields = Object.keys(dbFields.new_oleg_contacts)
                        .filter((val, i) => i < 18)
                        .map(aval => olead[aval] || '-');
        // console.log(fields)

        const sheetrange = await appendToSheet(OLEG_USA_CONTACTS.NAME, fields);

        // console.log("fields", fields);
        console.log("sheetrange", sheetrange, "\n\n");

        await updateOlegContacts({_id}, {isUsaRange: sheetrange, uploaded: true});

        await sleep(2);
    }
};

const getCompaniesInfo = async () => {
    // Leads whose company url is valid
    const oLeads = await getOlegContacts({threeRegionCampaign: {$exists: true}, company_url: {$exists: true, $ne: "", $regex: /company/g}});
    // const oLeads = await getOlegContacts({company_url: {$exists: true, $ne: "", $regex: /company/g}});
    // const oLeads = await getOlegContacts({firstname: {$exists: true}, $expr: {$eq: ["$firstname", "$lastname"]}});

    log(`We are working with ${len(oLeads)} people`);

    const companiesNo = [];
    const companyUrls = [];
    const repeatedCompanies = [];
    
    for (const lead of oLeads) {
        const { _id, company_url, company = "" } = lead;

        
        if (company) {
            const companyExists = await getCompany({ name: company });
            
            if (!len(companyExists)) {
                
                if (!companyUrls.includes(company_url)) {
                    
                    companyUrls.push(company_url);

                    await addCompanyLinkToQueue(company_url);
                } else {
                    log('This is a repeat');

                    repeatedCompanies.push(company);
                }
            } else companiesNo.push(companyExists);
        }
    }

    console.log(`Existing companies`, companiesNo);

    console.log(`Repeated companies`, repeatedCompanies, `And there are ${len(repeatedCompanies)} of them`);

    log(`\n\n The total length is ${len(companiesNo)}`);
};

const getThreeRgionCampLeadsFromSheet = async () => {
    const sheets = await getAuthenticatedSheet();
    const spreadsheetId = THREE_REGION_CAMPAIGN.ID;
    const range = THREE_REGION_CAMPAIGN.STATS_SHEETS.NORTHERN_EUROPE.NAME;
    const category = "nothern_europe";
    // const range = THREE_REGION_CAMPAIGN.STATS_SHEETS.GERMANY.NAME + `!A25:Z25`;

    sheets.spreadsheets.values.get({
        spreadsheetId,
        range
    }, async (err, response) => {
      if (err) {
        return console.log('Getsheet The API returned an error: ' + err);
      }

      const rows = response.data.values;

      // Remove first array in rows because that is the title
      rows.shift()
      let no = 2;

      for (const row of rows) {
        const sheetrange = `${range}!A${no}:Z${no}`;
        const linkedin_url = row[10];
        
        console.log(linkedin_url);

        const threeRegionCampaign = {
            sheetrange, category
        };

        console.log(threeRegionCampaign);

        await updateOlegContacts({linkedin_url}, {threeRegionCampaign});
        no++;
        // break;
      }
  
    //   console.log(response.data.values[1]);
    //   console.log(rows[0][10]);
    });
};

const getLinkedinUrl = async () => {
    const oLeads = await getOlegContacts({"removed_connect": true});

    const url = URLS.connections;
    
    log('WE ARE WORKING WITH', oLeads.length, 'LEADS');

    const browser = await getBrowser();
    const page = await browser.newPage();

    await page.setViewport({ width: 1366, height: 868 });

    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        const errorLocation = 'Setting cookies in olegsLeadsController.js, function sendFirstMsgToAllDeadLeads()';
        const imageName = `${date()}-${time()}.png`;
        const extraInfo = `I was trying to set the cookies with Olegs account while it fell apart. Image name in google drive: ${imageName}`;
        
        await errorHandler(page, error, errorLocation, extraInfo, imageName);
    }

    await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2' });

    for (const lead of oLeads) {
        const { _id, lead_name } = lead;

        const names = lead_name.split(' ');
        const firstAndLastName = len(names) > 3 ? mergeFirstandLastname(lead_name) : lead_name;

        log(`Clearing old message.\n\nFullname: ${lead_name}\n\nFirstandLastname: ${firstAndLastName}`);

        const searchInputHandle = await page.$('.mn-connections__search-input');
        
        await searchInputHandle.click();
        await searchInputHandle.focus();
        await searchInputHandle.click({clickCount: 3});
        await searchInputHandle.press('Backspace');
        
        log('Searching......', emojis.typing);
        
        await searchInputHandle.type(lead_name, { delay: 50 });

        await page.waitFor(2000);

        const searchResDiv = '.mn-connection-card';

        const not_found = "section.mn-connections .mn-connections__empty-search";

        if (await page.$(not_found) !== null) {
            log('Person not found');
        } else if (await page.$(searchResDiv) !== null) {
            await page.waitFor(1000);

            log('Found the person');
            const linkedin_url = await page.$eval('a.mn-connection-card__link', a => a.href);

            console.log(lead_name, linkedin_url);

            await page.waitFor(2000);

            await updateOlegContacts({_id}, {linkedin_url});
        }

        await page.waitFor(7000);
    }
};

const getUsaLeads = async () => {
    const usLeads = "https://www.linkedin.com/search/results/people/?facetGeoRegion=%5B%22us%3A0%22%5D&facetNetwork=%5B%22F%22%5D&origin=FACETED_SEARCH";
    const pages = [];
    const notFoundPeople = [];
    const browser = await getBrowser();
    
    const page = await browser.newPage();

    await page.setViewport({ width: 1920, height: 1080 });
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
    }

    for (let i = 2; i < 64; i++) {
        const pageNumber = `&page=${i}`;

        pages.push(`${usLeads}${pageNumber}`);
    }

    for (const url of pages) {
        await page.goto(url);

        await pageScroller(page, 5731);
        
        const pageHandle = await page.evaluateHandle(() => document.body);
        const resultPageHandle = await page.evaluateHandle(body => body.innerHTML, pageHandle);
        const employeesHtmlPage = await resultPageHandle.jsonValue();
        const [employees] = getAllEmployeeList(employeesHtmlPage);

        console.log(employees)

        for (const employee of employees) {
            const {name} = employee;
            
            const lead = await getOlegContacts({lead_name: name});

            if (len(lead)) {
                const [{ _id }] = lead;

                await updateOlegContacts({_id}, {inUSA: true})
            } else {
                log("\n\nCouldn't find this person");

                notFoundPeople.push(employee);
            }
        }

        await page.waitFor(12251);
    }

    console.log('Total people not found', notFoundPeople);
};

const checkIfRepeats = async () => {
    const oLeads = await getOlegContacts();
    const names = [];
    const repeat = [];

    for (const lead of oLeads) {
        const {lead_name} = lead;

        if (names.includes(lead_name)) {
            repeat.push(lead);
        } else {
            names.push(lead_name)
        }
    }

    console.log('Repeated', len(repeat), repeat);
};

const compareDownloadedLeadsWithDb = async () => {
    // const { connects } = require('../modules/data/connects');
    // const oLeads = await getOlegContacts({"removed_connect": true});
    // log(`There are ${len(oLeads)} people total who disconnected`);
    const names = [];

    for (const newLead of connects) {
        const { firstname, lastname} = newLead;

        if (firstname) {
            const lead_name = firstname.concat(' ', lastname);

            const oLead = await getOlegContacts({lead_name});

            if (!len(oLead)) {
                names.push(newLead);
            }
        }
    }

    let similar = 0;
    const foundPeople = [];

    // for (const oLead of oLeads) {
    //     const {lead_name} = oLead;

    //     for (const name of names) {
    //         const regex = new RegExp(name);
    //         const found = regex.test(lead_name);

    //         if (found) {
    //             similar++;
    //             if (!foundPeople.includes(lead_name)) {
    //                 foundPeople.push(lead_name);
    //                 console.log('Name from json ',name, ' Name from db ',lead_name)
    //             }
    //         } else {
    //             // similar++;
    //             // console.log('not found', found)
    //         }
    //     }
    // }


    console.log('Names', names);
    console.log('Total found', len(names));
    // console.log('Similar', similar);
};


/**
 * @param {String} timezone Looks like this: "+2", "-6" or "0"
 */
const checkIfWorkingHour = timezone => {
    let isInWorkingHour;
    let currentTime;

    if (timezone === undefined) {
        log('ERROR!! Please enter a timezone.\nUsing default time');
        currentTime = time();
    } else if (timezone === "0") {
        currentTime = time();
    } else if ((timezone.charAt(0) === "-" || timezone.charAt(0) === "+") && timezone.charAt(1)) {
        let arithmeticOption = timezone.charAt(0);
        let value = Number(timezone.charAt(1));
        currentTime = time(value, arithmeticOption);
    } else {
        log('ERROR!! Wrong timezone format. \nNot: ', timezone, '\nBut +2, -6 or 0');
        currentTime = time()
    }

log('This is the current time of this lead', currentTime);

    if(currentTime >= BEFORE_LAUNCH.BEGIN && currentTime <= BEFORE_LAUNCH.END) {
        isInWorkingHour = true;
    } else 
    if(currentTime >= AFTER_LAUNCH.BEGIN && currentTime <= AFTER_LAUNCH.END) {
        isInWorkingHour = true;
    } else {
        isInWorkingHour = false;
    }
    return isInWorkingHour;
};


/**
 * @param {Puppeteer} page Page constant
 * @param {Exception} error Error from catch
 * @param {String} errorLocation Location of the error
 * @param {String} extraInfo More infor about the error
 * @param {String} imageName More infor about the error
 */
const errorHandler = async (page, error, errorLocation, extraInfo, imageName = 'No image') => {
    const emailFields = {
        error: true,
        errorMsg: error,
        errorLocation: errorLocation,
        extraInfo: extraInfo ? extraInfo : 'No extra information'
    };

    emojiLogger(`${extraInfo}`, 'warn');
    emojiLogger(`An Error occured, Sending email to system administrator ${emojis.typing}`, 'err');

    if (page) {
        await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
        await page.waitFor(500);
    
        await uploadToDrive(imageName);
    }

    try {
        await sendEmail(emailFields);
    } catch (err) {
        await sendMsgToAdmin(`Could not send email ${err}`);
    }
};


/**
 * Close all open message boxes
 * @param {Object[]} page The page created from the browser object
 * @param {String} closeMsgBox Html path to close button 
 * @param {String} discardBtn Html path to discard button 
 * @param {Object} lead Object containing the details about lead
 * @param {String} imageName Name of the screenshot saved
 */
const closeAllMsgBoxes = async (page, closeMsgBox, discardBtn, lead, imageName) => {
    const { _id, lead_name } = lead || { _id: 'No ID', lead_name: 'No lead_name'};
    
    if (!discardBtn) discardBtn = 'button.msg-modal__button.ok-btn.button-primary-large';
    
    log('Closing all the opened msg boxes');
    
    try {
        if (await page.$(closeMsgBox) !== null) {
            const totalCloseBtn = (await page.$$(closeMsgBox)).length;
            
            for (let i = 0; i < totalCloseBtn; i++) {
                if (await page.$(closeMsgBox) !== null) await page.click(closeMsgBox);
                if (await page.$(discardBtn) !== null) await page.click(discardBtn);
            }
        }
    
        log('I have closed them all');

    } catch (error) {
        const errorLocation = 'close button in closeAllMsgsBoxes module';
        const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n. Image name in google drive: ${imageName}`;
        
        await errorHandler(page, error, errorLocation, extraInfo, imageName);

        if (await page.$(closeMsgBox) !== null) await page.click(closeMsgBox);
        
        if (await page.$(discardBtn) !== null) await page.click(discardBtn);
    }
};

const mergeFirstandLastname = (lead_name, firstname) => {
    const names = lead_name.split(' ');

    if (!firstname) {
        const fullname = getFirstAndLastName(lead_name);
        firstname = fullname.firstname;
    }

    const firstAndLastName = names[1] ? firstname.concat(' ', names[1]) : firstname;
    
    return firstAndLastName;
};

function getFirstAndLastName(lead_name) {
    const names = lead_name.split(' ');
    const ifDoctor = name => RegExp('dr.*', 'i').test(name);
    
    let firstname = names[0];
    let lastname = names[1] ? names.slice(1).join(' ') : '';

    if (firstname.length <= 2 || ifDoctor(firstname) || !testIfNameIsWord(firstname)) {
        if (testIfNameHasDot(firstname)) {
            firstname = firstname.concat(' ', names[1]);
            lastname = names.slice(2).join(' ');
        } else if (ifDoctor(firstname) && firstname.length <= 2) {
        /* if `Dr Trishan Panch` then firstname will be Trishan
        else if Dr Trishan then firstname will be Dr Trishan */
            firstname = names[2] ? names[1] : firstname.concat(' ', names[1]);
            lastname = names[2] ? names[2] : names.slice(2).join(' ');
        } else if (!testIfNameIsWord(firstname)) {
            console.log('here')
            firstname = names[1];
            lastname = names.slice(2).join(' ');
        } else if (firstname.length === 1) {
            firstname = lead_name;
            lastname = '';
        }
    }
    
    return {firstname, lastname};
};

const determineLeadNextStep = async (num_msg_sent, ifChatted) => {
    let condition = '';
    let originalFollowupMsg = '';
    let leadStatus = '';
    let dateMsgSentField = '';
    let dateMsgReadField = '';
    let dateMsgRepliedField = '';

    switch(num_msg_sent) {
        case 0:
          condition = ifChatted === true ? CHATED : NOT_CHATED;
          originalFollowupMsg = await getCategoryMessages(1, condition);
          dateMsgSentField = "date_msg_one_sent";
          dateMsgReadField = "";
          dateMsgRepliedField = "";
          break;
        case 1:
          originalFollowupMsg = await getCategoryMessages(2);
          dateMsgSentField = "date_msg_two_sent";
          dateMsgReadField = "date_msg_one_read";
          dateMsgRepliedField = "date_msg_one_responded";
          break;
        case 2:
          originalFollowupMsg = await getCategoryMessages(3);
          dateMsgSentField = "date_msg_three_sent";
          dateMsgReadField = "date_msg_two_read";
          dateMsgRepliedField = "date_msg_two_responded";
          break;
        case 3:
          condition = inUSA === true ? IN_USA : NOT_USA;
          originalFollowupMsg = await getCategoryMessages(4, condition);
          dateMsgSentField = "date_msg_four_sent";
          dateMsgReadField = "date_msg_three_read";
          dateMsgRepliedField = "date_msg_three_responded";
          break;
        case 4:
          originalFollowupMsg = await getCategoryMessages(5);
          dateMsgSentField = "date_msg_five_sent";
          dateMsgReadField = "date_msg_four_read";
          dateMsgRepliedField = "date_msg_four_responded";
          break;
        case 5:
          originalFollowupMsg = DONE;
          leadStatus = DONE;
          dateMsgReadField = "date_msg_five_read";
          dateMsgRepliedField = "date_msg_five_responded";
          break;
        default:
          log('There is no message for this message number', num_msg_sent);
          break;
    };

    const nextStep = { originalFollowupMsg, leadStatus, dateMsgSentField, dateMsgReadField, dateMsgRepliedField };
    
    return nextStep;
};

handleUncaughtError({errorLocation, extraInfo});

module.exports = {
    sendAllMsgsToDeadLeads,
    sendFirstMsgToAllDeadLeads,
    errorHandler, getOlegLeadsProfile,
    closeAllMsgBoxes, checkIfWorkingHour,
    getFirstAndLastName, mergeFirstandLastname
}
