                        /* THIS FILE IS A TEST PROJECT */

const { len } = require('../modules');
const { addLeads, getLeads, updateLeads } = require('../Db/Leads');
const { getGeneratedLeads } = require('../Db/GeneratedLeads');
const { getMobileLeads } = require('../Db/MobileLeads');
const { getOlegContacts } = require('../Db/OlegContacts');
const { getSalesLeads } = require('../Db/SalesLeads');


const addEveryColHere = async () => {
    const gLeads = await getGeneratedLeads();
    // const mLeads = await getMobileLeads();
    // const oLeads = await getOlegContacts();
    // const sLeads = await getSalesLeads();

    const commonFields = [
        "lead_name",
        "linkedin_url",
        "status",
        "connected",
        "connect_sent",
        "connect_accepted",
        "check_before_sending_msg",
        "message",
        "max_messages",
        "created_at",
        "date_script_ran",
        "time_diff",
        "type",
    ];

    for (const gLead of gLeads) {
        const { 
            company, country, headcount, description, linkedin_url, company_url, category, 
            lead_name, lead_linkedin_url, position, lead_title_ranking, lead_email, date_connect_sent,
            date_connect_expire, date_connect_accepted, status, date_last_interaction, found_by_script1, found_by_script2, found_by_script3,
            script_status, sheetrange, created_at, uploaded, check_before_sending_msg, date_script_ran, date_script_ran, time_diff, batch_number
        } = gLead;
        let connected = false,
            connect_sent = false;

        if (len(date_connect_sent)) {
            connect_sent = true;
        }

        if (len(date_connect_accepted)) {
            connected = true;
        }

        const lead = {
            lead_name,
            linkedin_url: lead_linkedin_url,
            status,
            connected,
            connect_sent,
            date_connect_expire,
            date_connect_accepted,
            check_before_sending_msg,
            message: "",
            max_messages: "",
            created_at,
            date_script_ran,
            time_diff,
            batch_number,
            type: "generated",
        };
    }
};