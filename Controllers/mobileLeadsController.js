const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const {google} = require('googleapis');
const { 
    genRandNum, sleep, date, time, emojis, log, closeAndWait, getTimeDiffAndTimeZone, getBrowser,
    sheetAuthentication, reduceDay, pageScroller, getCookie, len, seenMsg, repliedMsg, getAuthenticatedSheet
} = require('../modules');
const { CONSTANTS, URLS, dbFields } = require('../helpers/constants');
const { closeAllMsgBoxes, errorHandler, getFirstAndLastName, checkIfWorkingHour, mergeFirstandLastname } = require('./olegLeadsController');
const { updateSheet } = require('./spreadsheetController');
const { addMobileLeads, getMobileLeads, updateMobileLeads } = require('../Db/MobileLeads');
const { getInbox } = require('../Db/Inboxes');
const { addMobileLeadsStats } = require('../Db/Statistics/MobileLeads');
const { updateStatistics } = require('../Controllers/statisticsController');
const { getAllConnections } = require('../modules/scrappers');
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { ID, MAIN_SHEETS: { MOBILE_LEADS } } = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN;
const { STATS_SHEETS } = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN_STATISTICS;
const linkedin = URLS.home;
const week = `${reduceDay(7)} to ${date()}`;
let randNo;

// THis script is a cron job to run every sunday to get those who Oleg added with the previous week
const getCategoryMessages = nextMsg => {
    return new Promise((resolve, reject) => {
        sheetAuthentication((auth) => {
            const sheets = google.sheets({version: 'v4', auth});
            let result;
            sheets.spreadsheets.values.get({
                spreadsheetId: ID,
                range: `${MOBILE_LEADS.TEXT}!A2:C2`,
            }, (err, res) => {
                if (err) reject('The API returned an error: ' + err);
                const rows = res.data.values;
                if (nextMsg === 0) result = rows[0][0];
                if (nextMsg === 1) result = rows[0][1];
                if (nextMsg === 2) result = rows[0][2];
                resolve(result);
            });
        });
    });
};

const sendMlMessages = async num_msg_sent => {
    await updateRepliedMessages();

    const messages = [];

    for (let i = 0; i < 3; i++) {
        const message = await getCategoryMessages(i);
        messages.push(message);
    }

    const mobileLeads = await getMobileLeads({"chatted" : false, $or: [{replied: ""}, {replied: false}], completed: false, num_msg_sent, date_last_interaction: {$lt: date()}});
    const discardBtn = 'button.msg-modal__button.ok-btn.button-primary-large';
    const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';

    log('WE ARE WORKING WITH ', len(mobileLeads), ' LEADS');

    if (!len(mobileLeads)) {
        log('There are no leads, killing process');
        const pid = process.pid;
        process.kill(pid);
    }

    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();

    const url = URLS.connections;

    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        const errorLocation = 'Cookies in mobileLeadsController.js,sendMlMessages()';
        const imageName = `${date()}-${time()}.png`;
        const extraInfo = `I was trying to set the cookies with Olegs accountt. Image: ${imageName}`;

        await errorHandler(page, error, errorLocation, extraInfo, imageName);
        const pid = process.pid;
        process.kill(pid);
    }
    
    await page.setViewport({ width: 1366, height: 868 });

    await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2'});

    randNo = genRandNum(MIN_TIME, MAX_TIME);

    const loop = async mobileLeads => {
        for (const mobileLead of mobileLeads) {
            const { _id, lead_name, num_msg_sent, max_msgs, time_diff, date_last_interaction, check_before_sending } = mobileLead;
            try {
                const { firstname } = getFirstAndLastName(lead_name);
        
                console.log('\nfirstname', firstname);
                
                const firstAndLastName = mergeFirstandLastname(lead_name, firstname);
                console.log('\nfirstAndLastName', firstAndLastName);
        
                const message = messages[num_msg_sent];
                const newMessage = message.replace(/--name--/g, firstname);
                
                console.log('\nmessage', newMessage);
        
        
                if (checkIfWorkingHour(time_diff)) {
                    await closeAllMsgBoxes(page, closeMsgBox, discardBtn, mobileLead, `${date()}-${time()}.png`);
                    
                    const searchBox = '.msg-connections-typeahead__search-field.msg-connections-typeahead__search-field--no-recipients';
                    await page.click('button.msg-overlay-bubble-header__control');
        
                    await page.waitFor(1000);
                    
                    if (await page.$(searchBox) !== null) {
        
                        await page.type(searchBox, firstAndLastName, { delay: 50 });
        
                        await page.waitFor(10000);
        
                        const nameSearchResBtn = 'button.msg-connections-typeahead__search-result';
                       
                        if (await page.$(nameSearchResBtn) !== null) {
                            await page.click(nameSearchResBtn);
                            await page.waitFor(5000);
                            
                            const searchHandle = await page.evaluateHandle(() => document.body);
                            const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
                            const html = await resultHandle.jsonValue();
                            
                            await page.waitFor(2000);
                
                            let send = true;
                            let reply = repliedMsg(html, lead_name);
        
                            if (reply.replied) {
                             
                              log(`Lead replied, here is what he said: ${reply.message}`);
        
                              const fields = {
                                completed: true,
                                replied: true,
                                "date_of_last_interaction" : date()
                              };
        
                              await updateMobileLeads({ _id }, fields);
                              
                              await page.waitFor(2000);
        
                              send = false;
                            } else if (check_before_sending && !seenMsg(html)) {
                                if (date_last_interaction >= reduceDay(1)) {
                                    console.log('Didnt read msg, send tomorrow');
                
                                    const fields = {
                                        check_before_sending: false,
                                        date_script_ran: date()
                                    };
                
                                    await updateMobileLeads({_id}, fields);
                                    send = false;
                                }
                            }
            
                          if (send) {
                            // if (newFollowupMsg.length) {
                              log('Typing.....');
        
                              await page.type('.msg-form__contenteditable', newMessage, { delay: 60 });
        
                              log('Sent :)');
        
                              await page.click('.msg-form__send-button');
                              
                              const new_msg_sent = num_msg_sent + 1;
        
                              let fields = {
                                "num_msg_sent": new_msg_sent,
                                "date_last_interaction" : date()
                              };
        
                              await updateMobileLeads({_id}, fields);
        
                              await page.waitFor(3000);
                            // } else {
                            //   //If the message is empty that means this is the 5th message
                            //   log('\nThis lead replied to none of our messages, update db and thats all');
        
                            //   let fields = {
                            //     date_last_interaction: date(),
                            //     lead_status: leadStatus
                            //   };
        
                            //   await updateMobileLeads({ _id: _id}, fields);
                            // }
        
                            await searchHandle.dispose();
        
                            await closeAllMsgBoxes(page, closeMsgBox, discardBtn, mobileLead, `${date()}-${time()}.png`);
                            
                            await closeAndWait(page, randNo);    
                          } else {
                            await closeAllMsgBoxes(page, closeMsgBox, discardBtn, mobileLead, `${date()}-${time()}.png`);
                            
                            await closeAndWait(page, randNo);
                          }
        
                          await searchHandle.dispose();
        
                        } else {
                          log('Didnt find lead ',lead_name ,' in search box');
                          
                          await closeAllMsgBoxes(page, closeMsgBox, discardBtn, mobileLead, `${date()}-${time()}.png`);
        
                          await closeAndWait(page, randNo);
                        }
                    }                    
                } else {
                    console.log('Either not in working how or we just wrote a message to lead');
    
                    if (num_msg_sent === max_msgs) {    
                        console.log('Gotten to max messages ', num_msg_sent);
                       await updateMobileLeads({_id}, {completed: true});
                    }
    
                    await sleep(60);
                }
            } catch (error) {
                const errorLocation = 'mobileLeadsController.sendMlMessages()';
                const imageName = `${date()}-${time()}.png`;
                const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n`;
    
                await errorHandler(page, error, errorLocation, extraInfo, imageName);
                
                await closeAndWait(page, randNo);
            }
        }

        log('Waiting for 1 hour then check mobile leads again');
        await sleep(3600);

        const Leads = await getMobileLeads({"chatted" : false, $or: [{replied: ""}, {replied: false}], completed: false, num_msg_sent, date_last_interaction: {$lt: date()}});
    
        if (len(Leads)) loop(Leads);
        else {
            const pid = process.pid;
            process.kill(pid);
        }
    };
    
    if (len(mobileLeads)) loop(mobileLeads);
    else {
        const pid = process.pid;
        process.kill(pid);
    }
};

const getAcceptedConnections = async () => {
    log(URLS.connections);
    const browser = await getBrowser();
  
    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
      
        await page.goto(URLS.connections, { timeout: 120000, waitUntil: 'networkidle2' });
        
        await page.waitFor(3633);
        
        // await pageScroller(page);

        const prevBatch = await getMobileLeads({batch: {$type:"number"}}, 0, 1, true, {batch: -1});
        const nextBatch = prevBatch[0].batch + 1;

        const connectionsHandle = await page.evaluateHandle(() => document.body);
        const resConnectionHandle = await page.evaluateHandle(body => body.innerHTML, connectionsHandle);
        const connectionsPage = await resConnectionHandle.jsonValue();

        const mobileLeadsField = {
            page: connectionsPage,
            batch: nextBatch,
            week
        };
    
        const resentConnects = getAllConnections(mobileLeadsField, 8);

        if (resentConnects.length) {
            console.log('Connects length', resentConnects.length);

            if (resentConnects.length && Array.isArray(resentConnects)) {
                for (const lead of resentConnects) {
                    const { lead_name } = lead;
                    const foundLead = await getMobileLeads({lead_name});

                    if (!len(foundLead)) {
                        await addMobileLeads(lead);
                    }
                }
            }
            else console.log('\n\nThere are no new connections from 1 week ago');
        }
        
        await browser.close();

        await connectionsHandle.dispose();
    } catch (error) {
        const imageName = `${date()}-${time()}.png`;
        const errorLocation = 'mobileLeadsController.js, function getAcceptedConnections()';
        const extraInfo = `Something went wrong in the whole getAcceptedConnections function. Image name in google drive: ${imageName}`;
        
        await errorHandler(page, error, errorLocation, extraInfo, imageName);

        await browser.close();
    }
};

const updateRepliedMessages = async () => {
    const inboxes = await getInbox({date: {$gt: reduceDay(3)}});
    
    if (len(inboxes)) {
        for (const inbox of inboxes) {
            const { name, date } = inbox;
    
            const mLead = await getMobileLeads({lead_name: name});
    
            if (len(mLead)) {
                const [{_id, lead_name,num_msg_sent }] = mLead;
                
                log('This lead has replied', lead_name, num_msg_sent);
    
                const fields = {
                    completed: true,
                    replied: true,
                    date_of_last_interaction: date
                };
    
                await updateMobileLeads({ _id }, fields);
            }
        }
    } else {
        log('You can go on, there are no inboxes');
    }
};

const getPendingConnects = async () => {
    const connectsAccepted = await getMobileLeads({ week, chatted: false });
    const connects_accepted = connectsAccepted.length;

    const url = URLS.sent;
    
    const pages = url => {
        let urls = [url];
        
        for (let i = 2; i < 6; i++) {
            const nextPage = url.concat(`/?page=${i}`);
            urls.push(nextPage);
        }

        return urls;
    };

    const allPages = pages(url);
    
    const browser = await puppeteer.launch({headless: false});
    
    const page = await browser.newPage();
    
    let connects_pending = 0;

    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
        const pid = process.pid;
        process.kill(pid);
    }

    for (const pageUrl of allPages) {
        await page.goto(pageUrl);
        
        await pageScroller(page);

        await page.waitFor(3000);
        
        const pageHandle = await page.evaluateHandle(() => document.body);
        const resultPageHandle = await page.evaluateHandle(body => body.innerHTML, pageHandle);
        const htmlPage = await resultPageHandle.jsonValue();

        const $ = cheerio.load(htmlPage);

        let pgTotal = 0;

        $('li.invitation-card.invitation-card--selectable').each((i, el) => {
            const inviteTxt = $(el).find('.invitation-card__custom-message span.lt-line-clamp__line.lt-line-clamp__line--last').text().trim();

            if (!inviteTxt.length) {
                connects_pending += 1;
                pgTotal += 1;
            }
          });
          console.log('Total on page is ', pgTotal);

        //   randNo = genRandNum(MIN_TIME, MAX_TIME);
            await pageHandle.dispose();

          log(`Waiting for 7280 seconds`);
          
          await page.waitFor(7280);
    }

    const connects_sent = connects_accepted + connects_pending;

    const mobileLeadsStatsData = {
        connects_sent,
        connects_pending,
        connects_accepted,
        week,
        uploaded: true
    };

    const data = [week, connects_sent, connects_pending, connects_accepted];
    
    await updateStatistics(STATS_SHEETS.MOBILE_LEADS, data);
    
    await addMobileLeadsStats(mobileLeadsStatsData);
    
    await browser.close();
    
    process.exit(1);
};

const checkIfChatted = async () => {
    const mobileLeads = await getMobileLeads({ checked: false, connected: true });
    const url = URLS.connections;
    const hideBrowser = process.env.HIDE_BROWSER ? process.env.HIDE_BROWSER : 'false';

    let browser = null;

    if (hideBrowser === 'true') {
        browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    } else {
        browser = await puppeteer.launch({
            headless: false
        });
    }
    
    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
    }
    
    await page.goto(url);
    
    await page.waitFor(5000);
    
    for (const lead of mobileLeads) {
        const { _id, lead_name, linkedin_url, connected_date, batch, week } = lead;
        const searchResDiv = '.mn-connection-card';
        const discardBtn = 'button.msg-modal__button.ok-btn.button-primary-large';
        const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';
        const chatDiv = '.msg-s-message-list-container';
        
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        
        log(`\n\nID: ${_id} || Name: ${lead_name}`);

        try {
            log('I want  to clear the old message');
            const searchInputHandle = await page.$('.mn-connections__search-input');
            
            await searchInputHandle.click();
            await searchInputHandle.focus();
            await searchInputHandle.click({clickCount: 3});
            await searchInputHandle.press('Backspace');
            
            log('Searching......', emojis.typing);
            
            await searchInputHandle.type(lead_name, { delay: 50 });
    
            await page.waitFor(5000);
            
            if (await page.$(searchResDiv) !== null) {
                log('\nOkay Best, I found your lead');
    
                await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
    
                await page.waitFor(500);
    
                await page.click('button.message-anywhere-button');
                
                await page.waitFor(1700);
    
                log('Let me check if there was a chat with Oleg Sergevich');
    
                if (await page.$(chatDiv) === null) {
                    log('\n\nChat box is empty');
    
                    await updateMobileLeads({ _id }, { checked: true });

                    // await pushToSheet({
                    //     lead_name, linkedin_url, connected_date,
                    //     batch, week
                    // });

                } else {
                    log('We have chatted in the past');
                    
                    await updateMobileLeads({ _id }, { chatted: true, checked: true });
                }
    
                await page.waitFor(1234);
    
                await closeAllMsgBoxes(page, closeMsgBox, discardBtn, lead, `${date()}-${time()}.png`);
            } else console.log('Lead was not found by me, Sorry Best');
    
            log(`\n\nWaiting for ${randNo} seconds, then move to next`);
                                        
            await page.waitFor(randNo);
            
        } catch (error) {
            const errorLocation = 'mobileLeadsController.chechIfChatted()';
            const imageName = `${date()}-${time()}.png`;
            const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n. Image name in google drive: ${imageName}`;

            await errorHandler(page, error, errorLocation, extraInfo, imageName);
            log(`Waiting for ${randNo} seconds`);
            await page.waitFor(randNo);
        }
    }

    await browser.close();
};

const pushToSheet = lead => {
    return new Promise((resolve, reject) => {
        sheetAuthentication(auth => {
            const {_id, lead_name, linkedin_url, position, company, country, connected_date, batch, week} = lead || {
                company: '', country: '', position: ''
            };
            const sheets = google.sheets({ version: 'v4', auth });

            sheets.spreadsheets.values.append({
                spreadsheetId: ID,
                range: MOBILE_LEADS.NAME,
                valueInputOption: 'RAW',
                resource: {
                    "values": [
                        [lead_name, linkedin_url, position, company, country, connected_date, week, batch]
                    ]
                }
            }, async (err, res) => {
                if (err) reject(`pushToSheet () The API returned an error: ${err}`);
                
                log('Pushed completely', res.data);
                const sheetrange = res.data.updates.updatedRange;
      
                await updateMobileLeads({_id}, {sheetrange, uploaded: true});
                
                resolve('Pushed completely');
            });
        });
    });
};

const getMlProfileData = async () => {
    const mLeads = await getMobileLeads({"chatted" : false, country: {"$exists" : false, $ne: ""}});

    if (len(mLeads)) {
        log('The size of the queue is: ', len(mLeads));

        const browser = await getBrowser();

        const page = await browser.newPage();
        
        try {
            const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
    
            await page.setCookie(...cookie);
        } catch (error) {
            console.error(error);
        }

        await page.setViewport({ width: 1366, height: 868 });

        for (let mLead of mLeads) {
            const { _id, linkedin_url } = mLead;

            if (linkedin_url) {
                log("\n\n1. Let's make request to employees url");
                
                const personResponse = await page.goto(linkedin_url, {
                    timeout: 120000,
                    waitUntil: 'networkidle2'
                });

                await page.waitFor(2000);

                if (personResponse._status < 400) {
                    await page.waitFor(1000);

                    const contactInfo = ".pv-top-card-v2-section__links a.pv-top-card-v2-section__link.pv-top-card-v2-section__link--contact-info";

                    await page.click(contactInfo);

                    await page.waitFor(5000);

                    const personHandle = await page.evaluateHandle(() => document.body);
                    const resultpersonHandle = await page.evaluateHandle(body => body.innerHTML, personHandle);
                    const html = await resultpersonHandle.jsonValue();
                    const $ = cheerio.load(html);

                    let company_url = $('li.pv-profile-section__card-item-v2 a').attr('href') || '';
                    company_url = `${linkedin}${company_url}`;
                    const position = $('h2.pv-top-card-section__headline').text().trim();
                    const company = $('.pv-top-card-v2-section__entity-name.pv-top-card-v2-section__company-name').text().trim();
                    const country = $('h3.pv-top-card-section__location').text().trim();
                    const { timezone, time_diff } = getTimeDiffAndTimeZone(country);

                    const connectedDate = $('section.ci-connected span.pv-contact-info__contact-item').text().trim();
                    const profileUrl =  $('section.ci-vanity-url .pv-contact-info__ci-container a').attr('href') || '';


                    console.log('position', position);
                    console.log('company', company);
                    console.log('company_url', company_url);
                    console.log('country', country);
                    console.log('timezone', timezone);
                    console.log('time_diff', time_diff);
                    console.log('connectedDate', connectedDate);
                    console.log('profileUrl', profileUrl);

                    // await updateMobileLeads({_id}, {position, company, company_url, country, timezone, time_diff});

                    await personHandle.dispose();
                }
            } else {
                log('I did not get this url for the person');
            }

            process.exit();

            // randNo = genRandNum(MIN_TIME, MAX_TIME);

            // log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);

            // await page.waitFor(randNo);
        }
    } else {
        log('The queue is now empty');
    }
};

const getRepeated = async () => {
    const mLeads = await getMobileLeads({chatted: false});

    const names = []
    let repeat = 0;

    for (const lead of mLeads) {
        const { _id, lead_name } = lead;

        if (names.includes(lead_name)) {
            repeat += 1;
            await updateMobileLeads({_id}, {double: true});
        } 
        else names.push(lead_name);
    }

    log(`Repeated are ${repeat}`);

};

const uploadAllToSheet = async () => {
    const mLeads = await getMobileLeads({"chatted" : false, country: {"$exists" : true,$ne: ""}});

    const countries = [];
    let repeat = 0;

    for (const lead of mLeads) {
        // const { country } = lead;
        // const comma = /,/g.test(country);
        // let nCountry = '';

        // if (comma) {
        //     const arr = /\,\s([a-z A-Z]+)/g.exec(country)
        //     nCountry = arr[arr.length - 1];
        // } else {
        //     nCountry = country
        // }

        // // console.log(country, '----', nCountry);
        // if (countries.includes(nCountry)) {
        //     repeat += 1;
        //     log(nCountry)
        // } else {
        //     countries.push(nCountry)
        // }
        await pushToSheet(lead);
        await sleep(0.5);
    }

    // log(repeat, len(countries));

};

const getTzFromSheet = async () => {
    sheetAuthentication(auth => {
        const sheets = google.sheets({ version: 'v4', auth });

        sheets.spreadsheets.values.get({
            spreadsheetId: ID,
            range: MOBILE_LEADS.NAME,
        }, async (err, res) => {
            if (err) console.log(`pushToSheet () The API returned an error: ${err}`);
            // console.log(res)
            const rows = res.data.values;
            const regex1 = RegExp('-');

            for (const row of rows) {
                const lead_name = row[0];
                const no = row[len(row) - 1];
                const lead = await getMobileLeads({lead_name});

                if (len(lead)) {
                    const [{_id}] = lead;
                    const time_diff = regex1.test(no) ? no : `+${no}`;

                    await updateMobileLeads({_id}, {time_diff});
                }
            }
        });
    });
};

const addFirstLastNames = async () => {
    const mLeads = await getMobileLeads({"chatted" : false, uploaded: {"$exists" : true}});
    console.log(len(mLeads));
    let total = 0;

    for (const lead of mLeads) {
        const { sheetrange, lead_name } = lead;
        const { firstname, lastname } = getFirstAndLastName(lead_name);

        const fields = [firstname, lastname, lead_name];

        await updateSheet(sheetrange, fields, ID);

        await sleep(2);
        
        total += 1;
    }
    console.log('Total: ', total);
};

module.exports = {
    checkIfChatted,
    getPendingConnects,
    getAcceptedConnections,
    sendMlMessages
};