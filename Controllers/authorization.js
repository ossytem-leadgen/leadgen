const { updateCookies } = require('../Db/Cookies');
const { date, time, getBrowser } = require('../modules');

const { URLS, CONSTANTS: {CREDENTIALS, COOKIES_OWNER} } = require('../helpers/constants');

const Login = async (name, pwd) => {
    const url = URLS.home;
    const owner = COOKIES_OWNER.OLEG;
    const updatedAt = `${date()}->${time()}`;

    const browser = await getBrowser();

    const page = await browser.newPage();

    await page.goto(url);

    await page.type('input.login-email', name, { delay: 50 });
    
    await page.waitFor(5000);

    await page.type('input.login-password', pwd, { delay: 50 });
    
    await page.waitFor(6000);
    
    page.click('.submit-button');
    
    await page.waitForNavigation();
    
    await page.waitFor(5000);
    
    const cookie = await page.cookies();

    await updateCookies({owner}, {cookie, updatedAt});
    
    await page.close();

    await browser.close();
};

// Login(CREDENTIALS.KORZHENKO.NAME, CREDENTIALS.KORZHENKO.PWD);
module.exports = Login;