const puppeteer = require('puppeteer');
const { CONSTANTS, URLS } = require('../helpers/constants');
const { log, date, time, sleep, genRandNum, getCookie, len } = require('../modules');
const { addSalesLeads, getSalesLeads, updateSalesLeads } = require('../Db/SalesLeads');
const { getMobileLeads } = require('../Db/MobileLeads');
const { getOlegContacts } = require('../Db/OlegContacts');
const { getGlCategoryMessages } = require('./generatedLeadsController');
const { errorHandler } = require('./olegLeadsController');

const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE.SEND_CONNECTS;
const { MAIN_SHEETS:{ BATCH_ZERO } } = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN;

// Save the data into the db
const saveFromJsonToDb = async () => {
    const salesLeads = await getSalesLeads({url: {$type: "array"}});
    let lurl = '';
    
    for (const lead of salesLeads) {
        const { _id, url } = lead;
        lurl = _id
        // await updateSalesLeads({_id: _id }, { firstname: firstname.trim()});
    }
    console.log(lurl);
};

const sendSalesConnectMsg = async () => {
    const connectBtn = 'button.pv-s-profile-actions.pv-s-profile-actions--connect';
    const pendingSelector = 'button.pv-s-profile-actions.pv-s-profile-actions--connect.pv-s-profile-actions--pending';
    const dotsSelector = 'button.pv-s-profile-actions__overflow-toggle.pv-top-card-section__inline-overflow-button';
    const moreBtn = 'button.pv-s-profile-actions__overflow-toggle';
    const removeConnection = 'button.pv-s-profile-actions.pv-s-profile-actions--disconnect';
    const addNote = 'div.send-invite__actions button.button-secondary-large';
    const sendInvite = 'div.send-invite__actions button.button-primary-large';

    const fields = {
        connect_message_sent: false, 
        already_connected : false, 
        email_req: false,
        $where: "!Array.isArray(this.url) && this.url.length",
        error: { $lte: 1 }
    };

    const salesLeads = await getSalesLeads(fields, 0, 60);

    if (!salesLeads.length) {
        log('We have sent everyone connect messages');
        return;
    }
    
    log('Length of leads', salesLeads.length);

    const connect = { category: "Active business",  msgType: "connect" };
    const message = await getGlCategoryMessages(connect.category, connect.msgType, BATCH_ZERO.NAME);

    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

        await page.setCookie(...cookie);
    } catch (error) {
        const imageName = `${date()}-${time()}-cookies-sendconnect.png`;
        const errorLocation = 'salesLeadController sendConnectMsg()';
        const extraInfo = `Error while setting cookies. Image name in google drive: ${imageName}`;
        
        await errorHandler(page, error, errorLocation, extraInfo, imageName);
        process.exit();
    }

    await page.setViewport({ width: 1366, height: 868 });

    for (const lead of salesLeads) {
        const { _id, firstname, url, connect_message_sent, already_connected } = lead;
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        try {

            if (url.length && connect_message_sent === false && already_connected === false) {
                let newCnctMsg = message ? message.replace(/--name--/g, firstname.trim()) : '';
                log(newCnctMsg);
                
                // await page.setRequestInterception(true);
                
                // page.on('request', async request => {
                //     if (request.isNavigationRequest() && request.redirectChain().length) {
                //         request.abort();
                //         log('The page is redirecting, stopping script');
                //         await browser.close();
                //         process.exit();
                //     }
                //     else
                //         request.continue();
                // });
        
                await page.goto(url, { timeout: 120000, waitUntil: 'networkidle2' });
                await page.waitFor(2000);
        
                let send = true;
                let done = false;
        
                if (await page.$(pendingSelector) !== null) {
                    log(`We have sent a connect message to this user ${firstname} but no response\n`);
                    
                    await updateSalesLeads({_id}, { connect_message_sent: true, pending: true });

                    log(`Waiting for ${randNo} seconds`);
                    await page.waitFor(randNo);
                } else {
                    log(`We have not sent a connect message to user\n`);
                    
                    // if connect message not sent and connect button exists
                    if (await page.$(connectBtn) !== null) {
                        log('Click Connect button');
        
                        await page.click(connectBtn);
                    } else {
                        // if no connect button, but 3 dots exists
                        if (await page.$(dotsSelector) !== null) {
                            log('No connect button, so click 3 dots\n');
            
                            await page.click(dotsSelector);
                            await page.waitFor(1000);
            
                            log('Click connect in drop down menu\n');
            
                            await page.click(connectBtn);
                        // if no 3 dots that means we are connected
                        } else if (await page.$(moreBtn) !== null) {
                            log('There is a more button\n');

                            await page.click(moreBtn);

                            if (await page.$(removeConnection) !== null) {
                                log('We are already connected to this user, SKIP\n');
                
                                await updateSalesLeads({_id}, { already_connected : true});
                
                                send = false;
                            } else if (await page.$(connectBtn) !== null) {
                                log('There is a connect button, I am clicking it\n');
                                
                                await page.click(connectBtn);
                            } else {
                                const imageName = `${date()}-${time()}.png`;
                                const error = 'Something happened on this page, I don"t understand what happened\n'
                                const errorLocation = 'Sending connect message to sales lead';
                                const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${firstname}\nCurrent Time:${time()}\n. Image name in google drive: ${imageName}`;
                                
                                await errorHandler(page, error, errorLocation, extraInfo, imageName);
                            
                                await updateSalesLeads({_id}, {$inc: {error: 1}});
                                
                                send = false;
                            }
                        } else {
                            log('Something happened on this page, I don"t understand what happened\n');
                            
                            await updateSalesLeads({_id}, {$inc: {error: 1}});
                            
                            send = false;
                        }
                    }

                    if (send) {
                        if (await page.$('#email') !== null) {
                            log('We need email to send this lead message\n');
            
                            await updateSalesLeads({_id}, {email_req: true});
            
                            await page.waitFor(1000);
                        } else {
                            await page.waitFor(2000);
            
                            log('Typing.....................\n');

                            if (await page.$(addNote) !== null) {
                                await page.click(addNote);
                
                                await page.type('#custom-message', newCnctMsg, { delay: 50 });
                
                                await page.waitFor(6399);
                
                                log('Sent');
                
                                await page.click(sendInvite);
                
                                let fields = { date_connect_sent: date(), connect_message_sent: true };
                
                                await updateSalesLeads({ _id: _id}, fields);
                            } else {
                                log('There is no option to add note\n');
                            }

                            done = true;
                        }

                        log(`Waiting for ${randNo} seconds`);
            
                        await page.waitFor(randNo);
            
                    } else {
                        log(`Waiting for ${randNo} seconds`);
            
                        await page.waitFor(randNo);
                    }
                } 
            }
        } catch (error) {
            const imageName = `${date()}-${time()}.png`;
            const errorLocation = 'salesLeadController sendConnectMsg()';
            const extraInfo = `Error while sending connect message. Image name in google drive: ${imageName}`;
            
            await errorHandler(page, error, errorLocation, extraInfo, imageName);
            
            await updateSalesLeads({_id}, {$inc: {error: 1}});
            
            console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to company url`);
            
            await page.waitFor(randNo);
        }
    }
};

const removeAlreadyConnected = async () => {
    const salesLeads = await getSalesLeads({
        connect_message_sent: false, 
        already_connected : false, 
        email_req: false,
        $where: "!Array.isArray(this.url) && this.url.length",
        // error: { $lte: 1 }
    });

    let found = 0;

    for (let lead of salesLeads) {
        // lead = salesLeads[100];
        
        const { _id, firstname, lastname } = lead;
        const fullname = firstname.concat(' ', lastname);
        console.log(fullname.trim())

        // const olegLeads = await getOlegContacts({$where: `RegExp('${fullname}').test(this.fullname)`});
        const olegLeads = await getOlegContacts({fullname});
        const mobileLeads = await getMobileLeads({ lead_name: fullname });     
        // const olegLeads = await getOlegContacts({$where: `RegExp('${fullname}').test(this.fullname)`});
        // const mobileLeads = await getMobileLeads({ $where: `RegExp('${fullname}').test(this.lead_name)`});
        
        console.log(typeof olegLeads, typeof mobileLeads);
        
        if (olegLeads.length || mobileLeads.length) {
            found += 1; 
            
            log('Found');
            await updateSalesLeads({ _id }, { already_connected: true })
        }
        else log('not found');

        await sleep(0.4);
    }

    console.log(found);
};

const getAcceptedConnects = async () => {
    const {log} = console;
    // const mobileLeads = await getMobileLeads({chatted: true});
    const slead = await getSalesLeads();

    // const sleadNames = [];
    // const mleadNames = [];

    for (const lead of slead) {
        const { _id, firstname, lastname } = lead;
        const lead_name = firstname.concat(' ', lastname)
        await updateSalesLeads({_id}, {lead_name});
    }

    // let connected = 0;

    // for (const m_lead of mobileLeads) {
    //     const { lead_name } = m_lead;
    //     mleadNames.push(lead_name);
    // }

    // for (const lead_name of mleadNames) {
    //     if (sleadNames.includes(lead_name)) connected += 1;
    // }
        // if (len(slead)) connected += 1;
        // else notConnected += 1; 

    //   const names = lead_name.split(' ');

    //   const [first_name, ...other_names] = names;

    //   const last_name = other_names.join(' ');

    //   log(first_name, last_name);


//   log(`Connected: ${connected}`);
    // log(`Names ${sleadNames}`);

};


// getAcceptedConnects();


module.exports = {
    sendSalesConnectMsg
};