const {sendMsgToAdmin} = require('./botController');
const { CONSTANTS } = require('../helpers/constants');
const { date, reduceDay } = require('../modules/dateAndTime');
const { len } = require('../modules/common');
const mailAuthentication = require('../modules/authMail');
const { getAcceptedConnects } = require('../Db/AcceptedConnects');

const { inboxMsgsTemplate, titleForInboxMsgs, composeErrorMessage } = require('../modules/composeEmailMessage');

/**
 * Sending email for 2 reasons 
 * 1) Replied linkedin message
 * 2) To send developer error message
 * @param {Object} emailFields Fields containing boolean for reply or for error
 * if reply then there must be an object that holds the array of messages
 * if error then there must be d following objects: errorMsg, errorLocation, extraInfo
 */
const sendEmail = emailFields => {
  return new Promise(async (resolve, reject) => {
    const { reply, error } = emailFields;
  
    const gmail = await mailAuthentication();
    const best_company_email = CONSTANTS.EMAIL.CONTACTS.BEST.COMPANY;
    const script_email = CONSTANTS.EMAIL.CONTACTS.SCRIPT;
    
    let send = false;
    let raw;
  
    if (reply) {
      const { messages } = emailFields;
      const oleg_email = CONSTANTS.EMAIL.CONTACTS.OLEG
      const denis_email = CONSTANTS.EMAIL.CONTACTS.DENIS;

      const dayYesterday = date({ dayInText: true, minus: 1 });
      const emailTitle = titleForInboxMsgs({ day: dayYesterday, full_date: reduceDay(1) });

      const yesterdayConnects = await getAcceptedConnects({date: reduceDay(1)});

      const emailMessage = inboxMsgsTemplate(messages, len(yesterdayConnects));

      try {
        await sendMsgToAdmin(`There were ${len(messages) || "No"} inboxes & ${len(yesterdayConnects) || "No"} connects yesterday.`);
      } catch (error) {
        console.log('Error with message via bot, in EmailSenderController', error);
      }

      // raw = makeBody(script_email, best_company_email, emailTitle, emailMessage);
      raw = makeBody(script_email, [best_company_email, oleg_email, denis_email], emailTitle, emailMessage);
  
      send = true;
    } else if (error) {
      const { errorMsg, errorLocation, extraInfo } = emailFields;
      const emailTitle = CONSTANTS.EMAIL.ERROR.SUBJECT.TEXT;
      // let composeMsg = CONSTANTS.EMAIL.ERROR.BODY.TEXT
      const emailMessage = await composeErrorMessage(errorMsg, errorLocation, extraInfo);
      
      try {
        // await sendMsgToAdmin(`Error: ${errorMsg}.\nLocation: ${errorLocation}`);
      } catch (error) {
        console.log('Error with message via bot, in EmailSenderController', error);
      }
      
      raw = makeBody(script_email, best_company_email, emailTitle, emailMessage);
      send = true;
    }
  
    if (send) {
      gmail.users.messages.send({userId: 'me',resource: {raw: raw}}, (err, response) => {
        if (err) {
          const error_msg = err.response.data.error;
          const error_description = err.response.data.error_description;
          const returnError = `Error msg: ${error_msg}\nError desc: ${error_description}`;

          console.log(returnError);

          reject(returnError);
        } else if (response) {
          response_status = response.data.labelIds;

          console.log(`\nEmail status: ${response_status}`);          
          
          resolve(`\nEmail status: ${response_status}`);
        }
      });
    } else {
      // console.log('Could not send email because of wrong emailFields', emailFields);
      gmail.users.threads.list({'userId': 'me' }, async (err, res) => {
        if (err) return console.log(`Error getting inbox from gmail: ${err}`);
  
        console.log(res.data.threads[0].snippet);
  
      })
    }

  });
}

const makeBody = (from, to, subject, message) => {
  const str = ["Content-Type: text/html; charset=\"UTF-8\"\n",
      "MIME-Version: 1.0\n",
      "Content-Transfer-Encoding: 7bit\n",
      "to: ", to, "\n",
      "from: ", from, "\n",
      "subject: ", subject, "\n\n",
      message
  ].join('');

  const encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');

  return encodedMail;
}

module.exports = sendEmail
