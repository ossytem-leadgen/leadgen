const { getCompany, addCompany } = require('../../Db/Companies');
const { addRecruitingCompany, getRecruitingCompany } = require('../../Db/RecruitingCompanies');
const { log, len, getBrowser } = require('../../modules');
const { checkIfRecruiting, getCompanyPageData } = require('../../modules/scrappers');


const getCompanyDataFromPage = (chromBrowser, linkedin_url, batch_number) => {
    return new Promise(async resolve => {
        const browser = chromBrowser || await getBrowser();
        const page = await browser.newPage();

        let companyTableData = [];

        try {
            if (linkedin_url) {
                console.log('Companys linkedin_url is: ', linkedin_url);

                log("\n\nMaking request to company url");

                const companyResponse = await page.goto(linkedin_url+ 'about/', {
                    timeout: 120000,
                    waitUntil: 'networkidle2',
                });
                    
                if (companyResponse._status < 400) {
                    const companyHandle = await page.evaluateHandle(() => document.body);
                    const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
                    const companyHtmlPage = await resultcompanyHandle.jsonValue();
                    
                    console.log("\n\n2. Checking if it is recruiting");
                    const ifRecruiting = checkIfRecruiting(companyHtmlPage);

                    const companyData = getCompanyPageData(companyHtmlPage, linkedin_url);
                    
                    const { name } = companyData;

                    if (!ifRecruiting) {
                        console.log("\n\nHurray, This is not recruiting :)");

                        console.log("\n\n3. Lets grab company data since it is not a recruitment agency");

                        const companySearchFields = {$or: [{name}, {linkedin_url}]};

                        const doesCompanyExist = await getCompany(companySearchFields);

                        if (!len(doesCompanyExist)) {
                            console.log('\nNew Company or we did not finish working with it');

                            companyTableData = [{ ...companyData, batch_number}];

                            console.log(companyTableData);

                            await addCompany(companyTableData);

                            resolve(companyTableData);
                        } else {
                            console.log("This company already exists so let return that company");
                            
                            resolve(doesCompanyExist);
                        }
                    } else {
                        console.log("\n\nThis is a recruiting company, Let us black list it for future purposes:(");
                        
                        const recruitingCompany = await getRecruitingCompany({ name });

                        if (!recruitingCompany) {
                            console.log('This is a new recruitment company, lets save it');

                            console.log("\nRecruitment Company data", { name });

                            try {
                                await addRecruitingCompany({ name });
                            } catch(err){
                                console.log('\nError inserting company into Mongodb', err)
                            }
                        } else {
                            console.log('\nThis recruitment company has already been added');
                        }
                    }

                    await companyHandle.dispose();
                }
            } else {
                await page.close();
                process.exit();
            }
        } catch(error){
            console.error('Error with puppetter: ', error);
        }

        await page.close();
        
        resolve(companyTableData);
    });
}

module.exports = getCompanyDataFromPage;
