const { statisticsToSheet, drawTableBorder, getNextColumnNum, getNextAlphRange, len, log } = require('../modules');
const { CONSTANTS } = require('../helpers/constants');
const { getAcceptedConnects } = require('../Db/AcceptedConnects');

const updateAcceptedConnectStats = async () => {
    const { EARLY_MORNING, LATE_MORNING, AFTERNOON, NIGHT } = CONSTANTS.ACCEPTED_CONNECTS_STATS;
    const leadsWhoAccepted = await getAcceptedConnects();

    let early_morning = 0,
        late_morning = 0,
        afternoon = 0,
        night = 0;

    for (const lead of leadsWhoAccepted) {
        const { time } = lead;

        if (time >= EARLY_MORNING.START && time <= EARLY_MORNING.END) {
            early_morning += 1;
        } else if (time >= LATE_MORNING.START && time <= LATE_MORNING.END) {
            late_morning += 1;
        } else if (time >= AFTERNOON.START && time <= AFTERNOON.END) {
            afternoon += 1;
        } else if (time >= NIGHT.START && time <= NIGHT.END) {
            night += 1;
        }
    }

    const ACCEPTED_CONNECTS = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN_STATISTICS.STATS_SHEETS.ACCEPTED_CONNECTS;
    const range = ACCEPTED_CONNECTS.NAME + "!F5:F8";
    const statsArr = [early_morning, late_morning, afternoon, night];
    const sumStats = early_morning + late_morning + afternoon + night;

    log('This is the statistics for today', statsArr);

    log('\nTotal accepted ', len(leadsWhoAccepted), '\nTotal Stats ', sumStats);

    await statisticsToSheet(range, statsArr);

    process.exit();
};

const updateStatistics = async (SHEET, DATA) => {
    const nextColNum = await getNextColumnNum(SHEET.NAME);
    
    await drawTableBorder(SHEET.ID, nextColNum.start, nextColNum.end);
    
    const nextAlphRange = getNextAlphRange(nextColNum.start);

    const range = SHEET.NAME + nextAlphRange;
    console.log('data', DATA)
    
    const res = await statisticsToSheet(range, DATA);
    
    console.log(res);
};
    
module.exports = {
    updateStatistics, updateAcceptedConnectStats
};