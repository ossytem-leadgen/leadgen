const fs = require('fs');
const { google } = require('googleapis');
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const {
  genRandNum, date, time, reduceDay, increaseDay, emojis, log, testIfNameHasDot,  sleep, len, closeAndWait,
  testIfNameIsWord, sheetAuthentication, seenMsg, repliedMsg, handleUncaughtError, getCookie, pageScroller,
  getBrowser
} = require('../modules');
const { CONSTANTS, URLS } = require('../helpers/constants');
const sendEmail = require('./emailSenderController');
const { errorHandler, mergeFirstandLastname, getFirstAndLastName } = require('./olegLeadsController');
const { getOlegContacts, addOlegContact } = require('../Db/OlegContacts');
const { addMobileLeads, getMobileLeads } = require('../Db/MobileLeads');
const { getGeneratedLeads, updateGeneratedLeads } = require('../Db/GeneratedLeads');
const { addAcceptedConnects, getAcceptedConnects } = require('../Db/AcceptedConnects');
const { addCompany } = require('../Db/Companies');
const leadgen_spreadsheet_id = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN.ID;
const startTime = new Date().getTime();
const { getAllConnections } = require('../modules/scrappers');
const {SEND_CONNECTS, MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const errorLocation = 'generatedLeadsController';
const extraInfo = `This error was caught by uncaughtException`;
let randNo;

const getGlCategoryMessages = (category, msgType, range) => {
  return new Promise((resolve, reject) => {
    sheetAuthentication( (auth) => {
      const sheets = google.sheets({ version: 'v4', auth });
      let result;

      sheets.spreadsheets.values.get({
        spreadsheetId: leadgen_spreadsheet_id,
        range: range+'!B2:F4',
      }, (err, res) => {
        if (err) reject('The API returned an error: ' + err);

        const rows = res.data.values;
        //Active business connect batch1
        if (category === "Active business" && msgType === "connect") result = rows[0][0] || 'No message';
        if (category === "Active business" && msgType === "msg1") result = rows[0][1] || 'No message';
        if (category === "Active business" && msgType === "msg2") result = rows[0][2] || 'No message';
        if (category === "Active business" && msgType === "msg3") result = rows[0][3] || 'No message';
        if (category === "Active business" && msgType === "msg4") result = rows[0][4 || 'No message'];
        if (category === "Startup" && msgType === "connect") result = rows[1][0] || 'No message';
        if (category === "Startup" && msgType === "msg1") result = rows[1][1] || 'No message';
        if (category === "Startup" && msgType === "msg2") result = rows[1][2] || 'No message';
        if (category === "Startup" && msgType === "msg3") result = rows[1][3] || 'No message';
        if (category === "Startup" && msgType === "msg4") result = rows[1][4] || 'No message';
        if (category === "Outsource" && msgType === "connect") result = rows[2][0] || 'No message';
        if (category === "Outsource" && msgType === "msg1") result = rows[2][1] || 'No message';
        if (category === "Outsource" && msgType === "msg2") result = rows[2][2] || 'No message';
        if (category === "Outsource" && msgType === "msg3") result = rows[2][3] || 'No message';
        if (category === "Outsource" && msgType === "msg4") result = rows[2][4] || 'No message';

        resolve(result);
      });
    });
  });
};

const fromSheetIntoDb = () => {
  sheetAuthentication( async (auth) => {
    const sheets = google.sheets({version: 'v4', auth});

    return new Promise((resolve, reject) => {
      sheets.spreadsheets.values.get({
        spreadsheetId: '1HOKxvFRn8y75vYU84SonqZr_SIUC5FPqvvnVAhbhnS4',
        range: 'Russian_Belarus',
      }, async (err, res) => {
        if (err) reject('The API returned an error: ' + err);
        const rows = res.data.values;
        //0 - comp_name, 1-country, 2-headcount, 3-description, 4-linkedinlink, 5-website, 6-category, 7-leadname, 8-linkedinurl_lead, 9-title, 10-ranking
        //11-email, 12-date_connection_sent, 13-date-connection_accptd, 14-status, 15-date_of_lastinteraction, 16-script1, 17-script2, 18-script3,
        //19-datebyscript1, 20-datebyscript2, 21-datebyscript3, 22-script_status, 23 - batchnumber
          // for(row of rows){
          //   console.log(row[19]);
          // }
          let bela = 0;

          for (const row of rows) {
            const [num, name, linkedin_url, country] = row;
            if (country === 'Belarus') {
              await addCompany({
                name, linkedin_url, technologies: [], employees_link: "", country, company_url: "",
                created_at: date(), took_all_employees: false, description: "", batch_number: 5, sales_url: ""
              });
              await sleep(0.5);
            }
          }
        });
    });
  })
};

/* possible parameters {
*   cs : connect sent
*   ca : connect accepted
*   ms : message sent -> ms1 - 1st message, ms2 - 2nd message, ms3 - 3rd message, ms4 - 4th message
*   mr : message responded
*   nr : no response
* }*/
const updateSheet = async (condition, _id) => {
  sheetAuthentication( async (auth) => {
    const sheets = google.sheets({version: 'v4', auth});

    switch(condition) {
      case 'cs':
        console.log('Update sheet if Connect has been sent');
        let arg =  {"$exists" : true, "$ne" : ""}
        sheetCollection = await getGeneratedLeads({_id, "date_connect_sent": arg});
        break;
      case 'ca':
        console.log('Update sheet if Connect was accepted');
        sheetCollection = await getGeneratedLeads({_id, "status": "Connected"});
        break;
      case 'ms1':
        console.log('Update sheet if Message 1 was sent');
        sheetCollection = await getGeneratedLeads({_id, "status": "1st message"});
        break;
      case 'ms2':
        console.log('Update sheet if Message 2 was sent');
        sheetCollection = await getGeneratedLeads({_id, "status": "2nd message"});
        break;
      case 'ms3':
        console.log('Update sheet if Message 3 was sent');
        sheetCollection = await getGeneratedLeads({_id, "status": "3rd message"});
        break;
      case 'ms4':
        console.log('Update sheet if Message 4 was sent');
        sheetCollection = await getGeneratedLeads({_id, "status": "4th message"});
        break;
      case 'mr':
        console.log('Update sheet if Message was responded to');
        sheetCollection = await getGeneratedLeads({_id, "status": "Responded"});
        break;
      case 'nr':
        console.log('Update sheet if No response');
        sheetCollection = await getGeneratedLeads({_id, "status": "Unresponsive"});
        break;
      case 'del':
        console.log('Update sheet if Deleted');
        sheetCollection = await getGeneratedLeads({"status": "Deleted"});
        break;
      case '':
        console.log('Update a specified _id')
        sheetCollection = await getGeneratedLeads({_id});
        break;
      default:
        console.log('Check your args in updateSheet(), no option fits the switch case')
    }

    if (len(sheetCollection)) {
      for (let i = 0; i < len(sheetCollection); i++) {
        if (sheetCollection[i].category !== "") {
          (function(i) {
              setTimeout(async () => {
                let s = sheetCollection[i];
                let sheetRange = s.sheetrange;

                console.log(sheetRange)

                sheets.spreadsheets.values.update({
                  spreadsheetId: leadgen_spreadsheet_id,
                  range: sheetRange,
                  valueInputOption: 'RAW',
                  resource: { 
                      "values": [
                        [s.company, s.country, s.headcount, s.description, s.linkedin_url, s.company_url, s.category, s.lead_name, s.lead_linkedin_url, s.position, s.lead_title_ranking, s.lead_email, s.date_connect_sent, s.date_connect_expire, s.date_connect_accepted, s.status, s.date_last_interaction, s.found_by_script1, s.found_by_script2, s.found_by_script3, s.date_found_by_script1, s.date_found_by_script2, s.date_found_by_script3, s.script_status, s.batch_number]
                      ]
                  }
              }, async (err, res) => {
                  if (err) return console.log('updateSheet The API returned an error: ' + err);
                  console.log('\nUpdated the Leads field. Updated range:', res.data.updatedRange)
                });

                if ((i + 1) === len(sheetCollection)){
                  setTimeout(() => { logTime(); }, 1000)
                }
              }, i * 1000);
          }(i))
        }
      }
    } else {
      log('Such lead was not found');
    }
  });
};

const sendGlConnectMsg = async () => {
  try {
    // const fields = {
    //   category : { $ne:"N/A" },
    //   script_status : { $ne:"Moved" },
    //   "status" : { $ne:"Existing" },
    //   lead_title_ranking : { $ne:"", $gt:0 }, 
    //   date_connect_sent : "", 
    //   batch_number : { 
    //     $in : ["batch1","batch2","batch3", "batch4"]
    //   }
    // }
    const sheetData = await getGeneratedLeads({script_status: "Start", batch_name: "5_RUSSIA"});
  
    console.log('We have got ', sheetData.length, ' leads to send connects to');
  
    const browser = await getBrowser();
  
    const page = await browser.newPage();
    
    try {
      const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);
      await page.setCookie(...cookie);
    } catch (error) {
      const errorLocation = 'Setting cookies in generatedLeadsController.js, function sendConnectMsg()';
      const imageName = `${date()}-${time()}.png`;
      const extraInfo = `I was trying to set the cookies with Olegs account while it fell apart. Image name in google drive: ${imageName}`;
      await errorHandler(page, error, errorLocation, extraInfo, imageName);
    }//end of try/catch
  
    await page.setViewport({ width: 1366, height: 868 });
    
    for (let data of sheetData) {
      const { 
        _id, category, lead_linkedin_url, lead_name, batch_number, lead_title_ranking, company, script_status, batch_name
      } = data;
  
      const connectSelector = 'button.pv-s-profile-actions.pv-s-profile-actions--connect';
      const pendingSelector = 'button.pv-s-profile-actions.pv-s-profile-actions--connect.pv-s-profile-actions--pending';
      const dotsSelector = 'button.pv-s-profile-actions__overflow-toggle.pv-top-card-section__inline-overflow-button';
      const addNote = 'div.send-invite__actions button.button-secondary-large';
      const sendInvite = 'div.send-invite__actions button.button-primary-large';
  
      randNo = genRandNum(SEND_CONNECTS.MIN_TIME, SEND_CONNECTS.MAX_TIME);
  
      try {
        const connectsToday = await getGeneratedLeads({"date_connect_sent": date()});
  
        log('We have sent ', connectsToday.length, ' messages');
        
        if (connectsToday.length === 60) {
            log('We have sent connect to 70 leads already');
    
            await browser.close();
        
            const pid = process.pid;
            process.kill(pid);
        } else {
          if (category.length && category !== "N/A" && script_status !== "Moved" ) {
            const doctor = ['Dr', 'Dr.', 'DR','DR.', 'dr', 'dr.','dR'];
            const nameArray = lead_name.split(' ');
            let firstname = nameArray[0];

            if (firstname.length <= 2 || doctor.includes(firstname) || !testIfNameIsWord(firstname)) {
              if (testIfNameHasDot(firstname)) {
                  firstname = firstname.concat(' ', nameArray[1]);
              } else if (doctor.includes(firstname)) {
              /* if `Dr Trishan Panch` then firstname will be Trishan
              else if Dr Trishan then firstname will be Dr Trishan */
                  firstname = nameArray[2] ? nameArray[1] : firstname.concat(' ', nameArray[1]);
              } else if (!testIfNameIsWord(firstname)) {
                  firstname = nameArray[1];
              } else if(firstname.length === 1)  {
                  firstname = lead_name;
              }
            }

            log(category, " connect ", batch_name);

            let origCnctMsg = await getGlCategoryMessages(category, "connect", batch_name);

            let newCnctMsg = origCnctMsg ? origCnctMsg.replace(/--\w+--/g, firstname) : '';
  
            log(newCnctMsg);
  
            await page.goto(lead_linkedin_url, { timeout: 120000, waitUntil: 'networkidle2' });
            await page.waitFor(2000);
  
            let send = true;
            let done = false;
  
            // if pending
            if (await page.$(pendingSelector) !== null) {
                log(`We have sent a connect message to this user ${firstname} but no response`);
    
                await page.waitFor(11345);
            } else {
              log(`We have not sent a connect message to user`);
              
              // if connect message not sent and connect button exists
                if (await page.$(connectSelector) !== null) {
                  log('Click main button');
  
                  await page.click(connectSelector);
                } else {
                  // if no connect button, but 3 dots exists
                  if (await page.$(dotsSelector) !== null) {
                    log('No main button click 3 dots');
  
                    await page.click(dotsSelector);
                    await page.waitFor(1000);
  
                    log('Click connect in drop down menu');
  
                    await page.click(connectSelector);
  
                    // if no 3 dots that means we are connected
                  } else {
                    log('We are already connected to this user');
  
                    await updateGeneratedLeads({ _id }, {"status" : "Existing"});
  
                    await page.waitFor(1000);
  
                    updateSheet('', _id);
  
                    send = false;
  
                    await page.waitFor(2000);
                  }
                }

                if (send) {
                  if (await page.$('#email') !== null) {
                    log('We need email to send this lead message');
  
                    let nextRanking = (parseInt(lead_title_ranking) + 1 ).toString();
                    let fields = {
                      "status" : "Deleted", 
                      "date_last_interaction" : date(), 
                      "script_status" : "Moved"
                    };
  
                    await updateGeneratedLeads({ _id }, fields);
  
                    await page.waitFor(1000);
  
                    updateSheet('', _id);
  
                    let nextPerson = await getGeneratedLeads({"company": company,"lead_title_ranking": nextRanking});
                    
                    if (len(nextPerson)) {
                      let [{ _id }] = nextPerson;
                      await updateGeneratedLeads({_id}, { "script_status" : "Start" });
                      await page.waitFor(1000);
                      updateSheet('', _id);
                    }
  
                  } else {
                    await page.waitFor(2000);
  
                    log('Typing.....................');
  
                    await page.click(addNote);
  
                    await page.type('#custom-message', newCnctMsg, { delay: 80 });
  
                    await page.waitFor(6399);
  
                    log('Sent');
  
                    await page.click(sendInvite);
  
                    const fields = {
                      date_connect_sent: date(), 
                      date_last_interaction: date(), 
                      date_connect_expire: increaseDay(1),
                      script_status: "Pending"
                    };
  
                    await updateGeneratedLeads({ _id }, fields);
  
                    await page.waitFor(2000);
  
                    await updateSheet('cs', _id);
  
                    done = true;
                }
  
                log(`Waiting for ${randNo} seconds`);
  
                await page.waitFor(randNo);
  
                const last = sheetData[sheetData.length - 1];
  
                if (last.lead_name === lead_name && done === true) {
                  await browser.close();
                  logTime();
                  // const pid = process.pid;
                  // process.kill(pid);
                }
  
              } else {
                log(`Waiting for ${randNo} seconds`);
  
                await page.waitFor(randNo);
              }
            }
          }
        }
      } catch (error) {
        const errorLocation = 'generatedLeadsController.sendConnectMsg()';
        const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n`;
        const imageName = `${date()}-${time()}.png`;

        await errorHandler(page, error, errorLocation, extraInfo, imageName);
        const pid = process.pid;
        process.kill(pid);
      }
    }
  } catch (error) {
    const errorLocation = 'generatedLeadsController.js, function sendConnectMsg()';
    const imageName = `${date()}-${time()}.png`;
    const extraInfo = `Something went wrong in the whole sendConnectMsg function. Image name in google drive: ${imageName}`;

    await errorHandler('', error, errorLocation, extraInfo, imageName);
  }
};

const getGlAcceptedConnects = async () => {
  log(URLS.connections);

  const browser = await getBrowser();

  const page = await browser.newPage();
  
  try {
    const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);
    await page.setCookie(...cookie);

    await page.goto(URLS.connections, { timeout: 120000, waitUntil: 'networkidle2' });

    await page.waitFor(5000);
    
    // await pageScroller(page);

    const connectionsHandle = await page.evaluateHandle(() => document.body);
    const resConnectionHandle = await page.evaluateHandle(body => body.innerHTML, connectionsHandle);
    const connectionsPage = await resConnectionHandle.jsonValue();

    const resentConnects = getAllConnections({page: connectionsPage}, 0);

    console.log(resentConnects);

    if (len(resentConnects)) {
      for (const resentConnect of resentConnects) {
        console.log("\n\n===============================================================================================\n");
        
        const { lead_name, connected_time, connected_date, linkedin_url, position } = resentConnect;
        
        const existingConnect = await getAcceptedConnects({lead_name});

        let collection = "";

        if (!len(existingConnect)) {
          log('Lets add this person to the accepted connects', lead_name);

          const lead = await getGeneratedLeads({lead_name});

          await page.waitFor(3000);

          if (Array.isArray(lead) && len(lead)) {
            const [{ _id, date_connect_accepted, lead_name, country, position }] = lead;
            
            collection = "generated_leads";

            console.log(`\nThis is a Generated Lead by Script: ${lead_name}`);
            
            if (!date_connect_accepted) {
              console.log("\nMr/Mrs ", lead_name, " just accepted our connect, sent by me the script");
              
              const newContact = { lead_name, country, linkedin_url, position };

              await addOlegContact({...newContact});

              const fields = {
                "status" : "Connected",
                "date_last_interaction" : date(),
                "script_status" : "In process",
                "date_connect_accepted" : connected_date
              };
  
              await updateGeneratedLeads({_id}, fields);
  
              await page.waitFor(2000);
  
              await updateSheet('ca', _id);

            } else log('\nNoted that this person accepted our connect in generated leads');

          } else {
            collection = "mobile_leads";
            const foundLead = await getMobileLeads({lead_name});

            if (!len(foundLead)) {       
              const newContact = { lead_name, country: "", linkedin_url, position };

              await addOlegContact({...newContact});

              const mLead = {
                lead_name, linkedin_url, position, connected_date, connected: true, 
                chatted: false, checked: true, uploaded: false
              };

              await addMobileLeads(mLead);
            }

            console.log(`\nThis is a Mobile Lead: ${lead_name}`);
          }

          const acceptedConnect = {
            lead_name,
            linkedin_url, 
            position,
            date: connected_date,
            time: connected_time,
            collection
          };
          
          await addAcceptedConnects(acceptedConnect);
        } else {
          log('This person has been added to accepted connected ', lead_name);
        }
        console.log("===============================================================================================");
      }
    } else {
      console.log('\n\nThere are no new connections from 24 hours ago');
    }

    await connectionsHandle.dispose();
    
    process.exit();
  } catch (error) {
    randNo = genRandNum(MIN_TIME, MAX_TIME);
    const errorLocation = 'generatedLeadsController.js, function getAcceptedConnections()';
    const imageName = `${date()}-${time()}.png`;
    const extraInfo = `Something went wrong in the whole getAcceptedConnections function. Image name in google drive: ${imageName}`;
    
    await errorHandler(page, error, errorLocation, extraInfo, imageName);
  }
};

const updateGlNotAcceptConnects = async () => {
  try {
    const fields = {
      "status": "",
      "date_connect_sent": { "$exists" : true, "$ne" : "" },
      "date_connect_accepted": "",
      "script_status" : "Pending"
    };

    const sheetData = await getGeneratedLeads(fields);

    if (sheetData.length) {
      for (let data of sheetData) {
        let { _id, company, lead_title_ranking, date_connect_expire, script_status } = data;
  
        if (date_connect_expire <= date() && script_status !== "Moved") {
            const nextRanking = (parseInt(lead_title_ranking) + 1 ).toString();
    
            await updateGeneratedLeads({_id}, {script_status: "Moved", date_last_interaction: date()});
    
            await sleep(2);
    
            updateSheet('', _id);
    
            const nextPerson = await getGeneratedLeads({"company": company,"lead_title_ranking": nextRanking})
            
            if (nextPerson.length) {
                const [{_id}] = nextPerson;

                await updateGeneratedLeads({_id}, {"script_status" : "Start"});
          
                await sleep(2);
          
                updateSheet('', _id);
            } else {
              console.log('There is no one next in this company');
            }

            await sleep(2);
        }
      }
    }
    //process.exit();
    
  } catch (error) {
    randNo = genRandNum(MIN_TIME, MAX_TIME);
    const errorLocation = 'generatedLeadsController.js, function updateNotAcceptedConnections()';
    const imageName = `${date()}-${time()}.png`;
    const extraInfo = `Something went wrong in the whole updateNotAcceptedConnections function. Image name in google drive: ${imageName}`;

    await errorHandler('', error, errorLocation, extraInfo, imageName);
  }
};

const sendMsgsToGl = async () => {
  const sheetData = await getGeneratedLeads({script_status: "In process", batch_name: "5_RUSSIA"});
  log('WE ARE WORKING WITH ', len(sheetData), ' LEADS');

  const accptdStatus = ["Connected", "1st message", "2nd message", "3rd message", "4th message"];
  const url = URLS.jobs;
  // const nottAccptdStatus = ["Unresponsive", "Responded"];
  
  const browser = await getBrowser();

  const page = await browser.newPage();

  try {
    const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

    await page.setCookie(...cookie);

  } catch (error) {
    console.error(error);
  }
  
  await page.setViewport({ width: 1366, height: 868 });
  
  const searchResponse = await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2' });
  
  for (let data of sheetData) {
    const { 
      _id, status, lead_name, lead_title_ranking, category, batch_number, company, country, check_before_sending_msg,
      date_last_interaction, lead_linkedin_url, date_script_ran, batch_name
    } = data;
    const nameSearchResBtn = 'button.msg-connections-typeahead__search-result';

    randNo = genRandNum(MIN_TIME, MAX_TIME);
    
    try {
      if (category.length && accptdStatus.includes(status) && lead_title_ranking && date_script_ran < date()) {

        const {firstname} = getFirstAndLastName(lead_name);
        
        const firstAndLastNameOnly = mergeFirstandLastname(lead_name, firstname);

        let origCnctMsg;
        let newFollowupMsg;
        let newStatus;
        let shortStatus;

        /* Connected,1st message,2nd message,3rd message,4th message,Responded */
        switch(status) {
          case "Connected":
            origCnctMsg = await getGlCategoryMessages(category, "msg1", batch_name);
            newStatus = "1st message";
            shortStatus = "ms1";
            break;
          case "1st message":
            origCnctMsg = await getGlCategoryMessages(category, "msg2", batch_name);
            newStatus = "2nd message";
            shortStatus = "ms2";
            break;
          case "2nd message":
            origCnctMsg = await getGlCategoryMessages(category, "msg3", batch_name);
            newStatus = "3rd message";
            shortStatus = "ms3";
            break;
          case "3rd message":
            origCnctMsg = await getGlCategoryMessages(category, "msg4", batch_name);
            newStatus = "4th message";
            shortStatus = "ms4";
            break;
          case "4th message":
            origCnctMsg = 'No message'
            newStatus = "Unresponsive";
            shortStatus = "nr";
            break;
          default:
            console.log('None of the status fit');
            break;
        }

        if (origCnctMsg.length) {
          
          if (origCnctMsg === 'No message') {
            newFollowupMsg = '';
          } else {
            newFollowupMsg = origCnctMsg.replace(/--name--/g, firstname);
            newFollowupMsg = newFollowupMsg.replace(/--company--/g, company);
            newFollowupMsg = newFollowupMsg.replace(/--country--/g, country);
          }

          log('\n\n---------------------------------------------------------------------------------------------------------------')
          log(`CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name: ${lead_name}\nCurrent Time: ${time()}\nMessage: ${newFollowupMsg}\n`);

          if (searchResponse._status < 400) {
            
            await page.click('button.msg-overlay-bubble-header__control');
            await page.waitFor(1000);
            await page.type('.msg-connections-typeahead__search-field.msg-connections-typeahead__search-field--no-recipients', firstAndLastNameOnly, { delay: 50 });
            await page.waitFor(10000);
            
            
            //Continue if the name of lead appears from search we made 2 lines above
            if (await page.$(nameSearchResBtn) !== null) {
              await page.click(nameSearchResBtn);
              await page.waitFor(5000);
              const searchHandle = await page.evaluateHandle(() => document.body);
              const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
              const html = await resultHandle.jsonValue();
              await page.waitFor(2000);

              let send = false;
              let {replied, message} = repliedMsg(html, lead_name);

              if (replied) {
                console.log('This lead has replied your message', lead_name);

                const fields = {
                  "status" : "Responded",
                  response: message || '',
                  "script_status" : "Done",
                  "date_script_ran" : date()
                };

                await updateGeneratedLeads({_id}, fields);

                await page.waitFor(2000);
                await updateSheet('mr',_id);
                await page.waitFor(2000);

              } else if (!check_before_sending_msg) {
                console.log('You checked this lead yesterday send him a message', lead_name);

                send = true;
              } else if (check_before_sending_msg) {
                if (shortStatus === "ms1" || seenMsg(html)) {
                  console.log('Lets check this lead\nThis lead read our message so lets send him the next message now', lead_name)
                  send = true;
                } else {
                  if(date_last_interaction >= reduceDay(1)) {

                    console.log('This lead did not read our message so lets wait till tomorrow', lead_name);

                    const fields = {
                      "check_before_sending_msg" : false,
                      "date_script_ran" : date()
                    };

                    await updateGeneratedLeads({_id}, fields);
                  } else {
                    log('This lead did not read our message from few days ago so lets send him', lead_name);
                    send = true;
                  }
                }
              }

              if (send) {
                if (newFollowupMsg.length) {
                  console.log('Typing.....');

                  await page.type('.msg-form__contenteditable', newFollowupMsg, { delay: 50 });
                  console.log('Sent :)');
                  await page.click('.msg-form__send-button');

                  const fields = {
                    "status" : newStatus,
                    "date_last_interaction" : date(),
                    "date_script_ran" : date()
                  };

                  await updateGeneratedLeads({_id}, fields);

                  await page.waitFor(3000);  
                  await updateSheet(shortStatus, _id); 
                } else {
                  log('\nThis lead replied to none of our messages, lets move to the next person in the company');

                  const fields = {
                    "status" : newStatus,
                    "date_last_interaction" : date(),
                    "date_script_ran" : date(),
                    "script_status" : "Moved"
                  };

                  await updateGeneratedLeads({_id}, fields);

                  await updateSheet(shortStatus, _id);

                  let nextRanking = (parseInt(lead_title_ranking) + 1 ).toString();
                  let nextPerson = await getGeneratedLeads({ "company" : company, "lead_title_ranking" : nextRanking });

                  if (nextPerson.length) {
                    let next2ndRanking = (parseInt(nextPerson[0].lead_title_ranking) + 1 ).toString();
                    
                    let next2ndPerson = await getGeneratedLeads({ "company" : company, "lead_title_ranking" : next2ndRanking });

                    if (nextPerson[0].date_connect_sent === "") {

                      await updateGeneratedLeads({ _id: nextPerson[0]._id }, {"script_status" : "Start"});

                      await page.waitFor(3000);

                      updateSheet('', nextPerson[0]._id);

                    } else if (next2ndPerson.length) {
                      if (next2ndPerson[0].date_connect_sent === "") {
                        await updateGeneratedLeads({ _id: next2ndPerson[0]._id }, {"script_status" : "Start"});

                        await page.waitFor(3000);

                        updateSheet('', next2ndPerson[0]._id);

                      } else {
                        log('\nThe 2nd next lead is in process already');
                      }
                    } else {
                      log('\nThe next lead is in process and there is no one after him in the company hierarchy');
                    }
                  } else {
                    log('There is no other person in the hierarchy');
                  }
                }

                await searchHandle.dispose();

                await closeAndWait(page, randNo, true);

                // await pageScroller(page, genRandNum(5000, 10000));

              } else {
                await closeAndWait(page, randNo, true);

                // await pageScroller(page, genRandNum(5000, 10000));
              }
            } else {
              log('Didnt find lead ',lead_name ,' in search box');

              await closeAndWait(page, randNo, true);

              // await pageScroller(page, genRandNum(5000, 10000));
            }
          }
        } else {
          log('\nWas not able to pick a message to send');
        }
      } else {
        log('Not sending this lead ');
      }
    } catch (error) {
      const errorLocation = 'generatedLeadsController.sendConnectMsg()';
      const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n`;

      const emailFields = {
        error: true,
        errorMsg: error,
        errorLocation: errorLocation,
        extraInfo: extraInfo
      };

      log('An error occured while working with this lead.\n')
      log(`CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${lead_name}\nCurrent Time:${time()}\n`);
      log('Sending email to system administrator', emojis.typing);

      await sendEmail(emailFields);

      log(`Waiting for ${randNo} seconds`);

      await page.waitFor(randNo);
    }
  }
};

/**
 * Get the replies to our message of today and send replies as emails to boss
 */
const getLinkedinReplies = async () => {
  const sheetData = await getGeneratedLeads({"script_status": "In process"});
  // Only send messages to people with the following status
  const accptdStatus = ["1st message", "2nd message", "3rd message", "4th message"];

  const browser = await puppeteer.launch({
    headless: false
  });
  const page = await browser.newPage();

  try {
    const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);

    await page.setCookie(...cookie);

  } catch (error) {
    console.error(error);
  }
  
  const url = 'https://www.linkedin.com/feed/';
  await page.setViewport({ width: 1366, height: 868 });

  const searchResponse = await page.goto(url, {
    timeout: 250000,
    waitUntil: 'networkidle2'
  });

  for (let data of sheetData) {
    let { _id, status, lead_name, lead_title_ranking, category, lead_linkedin_url } = data;
    
    if (category.length && accptdStatus.includes(status) && lead_title_ranking) {
        log('\n\n---------------------------------------------------------------------------------------------------------------')
        if (searchResponse._status < 400) {
          await page.click('button.msg-overlay-bubble-header__control');
          await page.waitFor(2960);
          //msg-overlay-list-bubble-search__search-typeahead-input
          await page.type('.msg-connections-typeahead__search-field.msg-connections-typeahead__search-field--no-recipients', lead_name, { delay: 50 });
          await page.waitFor(5000);
          let nameSearchResBtn = 'button.msg-connections-typeahead__search-result'
          if (await page.$(nameSearchResBtn) !== null) {
            await page.click(nameSearchResBtn);
          } else {
            log('Didnt find lead in search box');
          }
          
          await page.waitFor(5000);
    
          const searchHandle = await page.evaluateHandle(() => document.body);
          const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
          const html = await resultHandle.jsonValue();
          await page.waitFor(2000);
          let repliedStatus = repliedMsg(html, lead_name);

          if (repliedStatus.replied === true) {
            console.log('\nReplied', lead_name);
            let fields = {
              "status" : "Responded",
              "date_script_ran" : date(),
              "script_status" : "Done"
            }
            await updateGeneratedLeads({ _id: _id}, fields);
            await page.waitFor(2000);
            await updateSheet('mr',_id);
            await page.waitFor(2000);
            log(`Sening email.....${emojis.typing}`);
            let emailFields = {
              reply: true,
              leadName: lead_name,
              leadUrl: lead_linkedin_url,
              leadResponse: repliedStatus.message
            }
            await sendEmail(emailFields);
        } else {
          log('Lead data:\nID:', _id,'\nName:', lead_name, '\nStatus:', status);
        }
        await page.click('button.msg-overlay-bubble-header__control.js-msg-close');
        await searchHandle.dispose();
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        log(`Waiting for ${randNo} seconds`);
        await page.waitFor(randNo);
      }
    } else {
      log('No data fits our criteria');
    }
  }
};


const logTime = () => {

  let endTimeMilli =  new Date().getTime() - startTime;
  let endTimeMin = Math.floor(endTimeMilli / 60000);
  let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);

  if (endTimeSec === 60){
      endTimeMin = endTimeMin + 1;
      endTimeSec = 0;
  }

  let result = `Script 4 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`
  console.log(result);
};

// handleUncaughtError({errorLocation, extraInfo});

module.exports = {
  updateSheet,
  sendGlConnectMsg,
  getGlAcceptedConnects,
  updateGlNotAcceptConnects,
  sendMsgsToGl,
  getLinkedinReplies,
  fromSheetIntoDb,
  getGlCategoryMessages
};