const { google } = require('googleapis');

const { emojis, len, log, sheetAuthentication, sleep, date, getAuthenticatedSheet } = require('../modules');
// const sendEmail = require('./emailSenderController');
const { CONSTANTS } = require('../helpers/constants');
const { getCompany, updateCompany } = require('../Db/Companies');
const { getGeneratedLeads, updateGeneratedLeads } = require('../Db/GeneratedLeads');

const spreadsheetId = CONSTANTS.SPREAD_SHEET_PARAMS.LEADGEN.ID;

const appendToSheet = (range, fields) => {
  return new Promise(async (resolve, reject) => {
    const sheets = await getAuthenticatedSheet();
  
    sheets.spreadsheets.values.append({
      spreadsheetId,
      range,
      valueInputOption: 'RAW',
      resource: {
          "values": [ fields ]
      }
    }, (err, res) => {
      if (err) reject(`companyspreadsheet The API returned an error: ${err}`);
      const sheetrange = res.data.updates.updatedRange;
  
      log(`\nColumns Changed: ${sheetrange}\n`);

      resolve(sheetrange);
    });
  });
};

const getUpdatesFromSheet = async () => {
  const sheets = await getAuthenticatedSheet();
  const sheetCollection = await getGeneratedLeads({batch_number: 5, category: ""});

  if (len(sheetCollection)) {
      for (let i = 0; i < len(sheetCollection); i++) {
          log('\n\n');
          let s = sheetCollection[i];
          let sheetRange = s.sheetrange;

          sheets.spreadsheets.values.get({
              spreadsheetId,
              range: sheetRange,
          }, async (err, res) => {
              if (err) log('The API returned an error: ' + err);
              const row = res.data.values;
              //0 - comp_name, 1-country, 2-headcount, 3-description, 4-linkedinlink, 5-website,
              //6-category, 7-leadname, 8-linkedinurl_lead, 9-title, 10-ranking
              //11-email, 12-date_connection_sent, 13-date_connection_expired, 14-date-connection_accptd,
              //15-status, 16-date_of_lastinteraction, 17-script1, 18-script2, 19-script3,
              //20-datebyscript1, 21-datebyscript2, 22-datebyscript3, 23-script_status 24-batch_number
              let category = row[0][6];
              let ranking = row[0][10];
              let name = row[0][7];
              let linkedin_url = row[0][8];

              if (name === s.lead_name) {
                  log(`Category ${category}`);
                  log(`Ranking ${ranking}`);
                  log(`Range: ${sheetRange}`);

                  if (category.length) {

                      if (s.lead_name == "LinkedIn Member") {

                          await updateGeneratedLeads({ _id: s._id}, { "lead_name" : name, "lead_linkedin_url" : linkedin_url });
                      }

                      if (ranking === "1") {
                          log('Ranking is equals to 1');

                          await updateGeneratedLeads({ _id: s._id}, { script_status: "In process" });
                      }

                      await updateGeneratedLeads({ _id: s._id}, {"category" : category, "lead_title_ranking" : ranking});
                  }
              } else {
                  log(`The names are different\nDB: ${s.lead_name} - API:${name}`);
                  console.log('Range', sheetRange);
              }
          });
          await sleep(2);
      }
      process.exit();
  } else {
      log('No lead was gotten today');
  }
};

const updateSheet = async (range, fields, spreadsheetId) => {
  const sheets = await getAuthenticatedSheet();
  
  sheets.spreadsheets.values.update({
    spreadsheetId,
    range,
    valueInputOption: 'RAW',
    resource: { 
        values: [ fields ]
    }
  }, (err, res) => {
    if (err) {
      return console.log('updateSheet() The API returned an error: ' + err);
    }

    console.log('\nUpdated the Leads field. Updated range:', res.data.updatedRange);
  });
};

const processedCompanyToSheet = async () => {
  let allCompanies = await getCompany({ batch_number: 'batch3' });
  
  if(allCompanies.length ){
    for (let i = 0; i < allCompanies.length; i++) {
      let { took_all_employees } = allCompanies[i]
      if(took_all_employees === true) {
        (function(i) {
          setTimeout(async () => {
              let { name, batch_number } = allCompanies[i];
              let processed;
              sheetCollection = await getGeneratedLeads({"company" : name});
              if(sheetCollection.length) {
                for (let i = 0, len = sheetCollection.length; i < len; i++) {
                  let { category } = sheetCollection[i];
                  processed = category.length === 0 || category === "" ? "N" : "Y"
                  if(processed === "Y") {
                    break;
                  }
                }
                sheets.spreadsheets.values.append({
                  spreadsheetId,
                  range: 'Companies Progress!B2:C',
                  valueInputOption: 'RAW',
                  resource: { 
                      "values": [
                          [name, processed, batch_number]
                      ]
                  }
                }, async (err, res) => {
                  if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
                  let tableRange = res.data.updates.updatedRange;
                  log(`\Row Changed: ${tableRange}\n`)
                  //
                  await updateCompany({ name: name }, { sheet_range: tableRange });
                });                  
              } else {
                console.log('This company has no lead in the sheet');
              }

            if((i + 1) === sheetCollection.length){
              setTimeout(() => { logTime(); }, 1000)
            }
          }, i * 1000)
      }(i)) 
      } 
    }
  } else {
    log(`There are no new data to add to sheet ${emojis.sad}`)
    logTime()
  } 
};

module.exports = {
  appendToSheet,
  getUpdatesFromSheet,
  processedCompanyToSheet,
  updateSheet
};


