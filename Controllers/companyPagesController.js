const puppeteer = require('puppeteer');
require('dotenv').config();

const { CONSTANTS } = require('../helpers/constants');
const { getCompany, addCompany } = require('../Db/Companies');
const { getEmployee } = require('../Db/Employees');
const { addRecruitingCompany, getRecruitingCompany } = require('../Db/RecruitingCompanies');
const { addEmployeeLinkToQueue, addCompanyDataToQueue, addEmployeeDataToQueue } = require('../Db/Queues/addToQueue');
const { getCompanyLinkFromQueue, getQueueSize } = require('../Db/Queues/getFromQueue');
const { genRandNum, date, time, log, getCookie, len, getBrowser, pageScroller } = require('../modules');
const { checkIfRecruiting, getCompanyPageData, getAllEmployeeList } = require('../modules/scrappers');

let startTime = new Date().getTime();
let randNo;

// Change this numbers for wait time between requests
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { COMPANY_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;

console.log(`\n\n--------------------------THIS IS THE LOG FOR COMPANIES CRAWLER SCRIPT [SECOND SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);

const getCompanyDataFromPage = async (account, gotoEmployeesPage = true) => {
    const queueSize = await getQueueSize(COMPANY_LINK);

    if (queueSize) {
        console.log('The size of the queue is ', queueSize);

        const browser = await getBrowser();
        const page = await browser.newPage();
        
        await page.setViewport({ width: 1920, height: 1080 });

        try {
            let cookie = null;

            switch (account) {
                case 'acc1':
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
                    break;
                case 'acc2':
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
                    break;
                default:
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
            }

            await page.setCookie(...cookie);
        } catch (error) {
            console.error(error);
        }
        
        for (let i = 0; i < queueSize; i++) {
            try {
                const company_linkedin_url = await getCompanyLinkFromQueue();

                const existingCompany = [];

                if (company_linkedin_url) {
                    console.log('Companys company_linkedin_url is: ', company_linkedin_url);

                    log("\n\n1. Making request to company url");

                    const companyResponse = await page.goto(company_linkedin_url+ 'about/', {
                        timeout: 120000,
                        waitUntil: 'networkidle2',
                    });
                        
                    if (companyResponse._status < 400) {
                        const companyHandle = await page.evaluateHandle(() => document.body);
                        const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
                        const companyPage = await resultcompanyHandle.jsonValue();
                        
                        console.log("\n\n2. Checking if it is recruiting");        
                        const ifRecruiting = checkIfRecruiting(companyPage);

                        const companyData = getCompanyPageData(companyPage, company_linkedin_url);
                        
                        const { name, employees_link, linkedin_url} = companyData;

                        if (!ifRecruiting) {
                            console.log("\n\nHurray, This is not recruiting :)");

                            console.log("\n\n3. Lets grab company data since it is not a recruitment agency");

                            const companySearchFields = {$or: [{name}, {linkedin_url}]};

                            const doesCompanyExist = await getCompany(companySearchFields);

                            if (!len(doesCompanyExist)) {
                                console.log('\nNew Company or we did not finish working with it');
                                
                                // if (!doesCompanyExist) {
                                    const [{batch_number}] = await getCompany({batch_number: {$type:"number"}}, 0, 1, true, {batch_number: -1});

                                    const companyTableData = { ...companyData, batch_number };

                                    console.log(companyTableData);

                                    await addCompany(companyTableData);
                                // }
                                
                                if (gotoEmployeesPage && employees_link) {
                                    console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to employees url`);
                                    
                                    randNo = genRandNum(MIN_TIME, MAX_TIME);
                                    
                                    await page.waitFor(randNo);

                                    console.log("\n\n4. Let's make another request to employees url");
                                    //This concatination is specifically for Greater Boston Area
                                    // let employees_link = companyData.employees_link.concat('&facetGeoRegion=%5B"us%3A14"%5D&origin=FACETED_SEARCH')
                                    const employeesResponse = await page.goto(employees_link, {
                                        timeout: 120000, waitUntil: 'networkidle2',
                                    });
                                    
                                    if (employeesResponse._status < 400) {
                                        await pageScroller(page, 3000);

                                        const employeesHandle = await page.evaluateHandle(() => document.body);
                                        const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                                        const employeesPage = await resultEmployeesHandle.jsonValue();
                                        const employees = getAllEmployeeList(employeesPage);  
                                        console.log('\n\nEmployees',employees);

                                        // Lets get all employees data in a json object if there is a result
                                        console.log("\n\n5. Lets get all employees data in a json object if there are bosses");
                                        if(Array.isArray(employees[0]) && employees[0].length){
                                            let employee_table_data = [];
                                            for (let i = 0; i < employees[0].length; i++) {
                                                let doesEmployeeExist = await getEmployee({ fullname: employees[0][i].name, position: employees[0][i].position });
                                                if (!doesEmployeeExist) {
                                                    employee_table_data.push({
                                                        "fullname" : employees[0][i].name,
                                                        "position" : employees[0][i].position,
                                                        "company" : companyData.name,
                                                        "company_url" : companyData.website,
                                                        "country" : employees[0][i].country,
                                                        "linkedin_url" : employees[0][i].employee_url,
                                                        "email" : "No email",
                                                        "phone" : "No Phone number",
                                                        "created_at" : date()
                                                    }); 
                                                }
                                            }
                                            console.log("\n\nEmployees data", employee_table_data); 
                                            // INSERT EMPLOYEE DATA TO TABLE IF EXISTS => employee_table_data
                                            try{
                                                await addEmployeeDataToQueue(employee_table_data);
                                            } catch(err){
                                                console.log('Error adding company data to queue', err)
                                            }
                                        } else {
                                            console.log("No boss in this company"); 
                                        }
                                        
                                        if (employees[1] && employees[1]) {
                                            // Generate urls [employee_page_url + &page=2]
                                            console.log("Generating url paginations and passing to queue");
                                            let employeePaginationNo = employees[1];
                                            let employeePaginationsUrls = [];
                                            // if(employeePaginationNo <= 5){
                                                for (let i=2; i<=employeePaginationNo; i++) {
                                                    // this url is for Greater boston area
                                                    let url = `${employees_link}&facetGeoRegion=%5B"us%3A14"%5D&origin=FACETED_SEARCH&page=${i}`;
                                                    let company = companyData.name;
                                                    let company_url = companyData.website;
                                                    employeePaginationsUrls.push({url, company, company_url});
                                                }
                                            // } 
                                            // else if(employeePaginationNo > 5){
                                            //     for(let i=2; i<=5; i++){
                                            //         let url = `${employees_link}&page=${i}`;
                                            //         let company = companyData.name;
                                            //         let company_url = companyData.website
                                            //         employeePaginationsUrls.push({url, company, company_url})
                                            //     }
                                            // }
                                            console.log(employeePaginationsUrls)
                                            // Pass the number of employee pages to queue for next script

                                            if (Array.isArray(employeePaginationsUrls) && employeePaginationsUrls.length){
                                                for (let paginate of employeePaginationsUrls) {
                                                    await addEmployeeLinkToQueue(paginate)
                                                }
                                            }
                                        }

                                    await employeesHandle.dispose();
                                    } else {
                                        console.log("\n\nError with employees page");                                        
                                    }
                                }
                            } else {
                                existingCompany.push(name);

                                console.log('Company Exists, no need to take it', name)
                            }
                        } else {
                            console.log("\n\nThis is a recruiting company, Let us black list it for future purposes:(");
                            
                            const recruitingCompany = await getRecruitingCompany({ name });

                            if (!recruitingCompany) {
                                console.log('This is a new recruitment company, lets save it');

                                console.log("\nRecruitment Company data", { name });

                                try {
                                    await addRecruitingCompany({ name });
                                } catch(err){
                                    console.log('\nError inserting company into Mongodb', err)
                                }
                            } else {
                                console.log('\nThis recruitment company has already been added');
                            }
                        }

                        await companyHandle.dispose();          
                    }
                    const newSize = await getQueueSize(COMPANY_LINK);

                    console.log('\nThe queue size is now', newSize);

                    if (!newSize) {
                        console.log('The Queue is now empty');

                        const endTimeMilli =  new Date().getTime() - startTime;
                        let endTimeMin = Math.floor(endTimeMilli / 60000);
                        let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);

                        if (endTimeSec == 60){
                            endTimeMin = endTimeMin + 1;
                            endTimeSec = 0;
                        }

                        console.log('Existing Companies:', existingCompany);

                        console.log(`Script 2 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);

                        await browser.close();

                        process.exit();
                    }
                } else {
                    await browser.close();
                    process.exit();  
                }

                randNo = genRandNum(MIN_TIME, MAX_TIME);

                console.log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);

                await page.waitFor(randNo);
            } catch(error){
                console.error('Error with puppetter: ', error)
            }
        }
    } else {
        console.log('The Queue is empty');

        process.exit();
    }
}

module.exports = getCompanyDataFromPage;
