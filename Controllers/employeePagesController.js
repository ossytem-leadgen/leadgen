const puppeteer = require('puppeteer');
require('dotenv').config();

const { CONSTANTS } = require('../helpers/constants');
const { addEmployeeDataToQueue } = require('../Db/Queues/addToQueue');
const { getEmpPgLinkFromQueue, getQueueSize } = require('../Db/Queues/getFromQueue');
const { getEmployee } = require('../Db/Employees');
const startTime = new Date().getTime();
const { genRandNum, date, time, getCookie, pageScroller, getTimeDiffAndTimeZone, len } = require('../modules');
const { getAllEmployeeList } = require('../modules/scrappers');

const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { EMPLOYEE_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;

let randNo;

console.log(`\n\n--------------------------THIS IS THE LOG FOR EMPLOYEES CRAWLER SCRIPT [THIRD SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);

let queueSize;
getQueueSize(EMPLOYEE_LINK)
    .then(val => queueSize = val)
    .catch(err => console.log('Error with search list', err));

setTimeout( () => {
    if (queueSize) {
        console.log("This is the queueSize: ",queueSize);

        try {
            (async () => {
                const browser = await puppeteer.launch({
                    headless: false
                });

                const page = await browser.newPage();
                
                try {
                    const account = process.argv[3];
                    let cookie = [];

                    switch (account) {
                        case 'acc1':
                            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
                            break;
                        case 'acc2':
                            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
                            break;
                        default:
                            cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
                    }

                    await page.setCookie(...cookie);

                } catch (error) {
                    console.error(error);
                }

                for (let i = 0; i < queueSize; i++) {
                    const link = await getEmpPgLinkFromQueue();
                    
                    if (link) {
                        console.log("\n\n1. Let's make request to employees url");
                        
                        const {url, company, company_url} = link;
                        
                        const employeesResponse = await page.goto(url, {
                            timeout: 120000,
                            waitUntil: 'networkidle2',
                        });
                        
                        if (employeesResponse._status < 400) {
                            await pageScroller(page);

                            const employeesHandle = await page.evaluateHandle(() => document.body);
                            const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                            const employeesPage = await resultEmployeesHandle.jsonValue();
                            const [employees] = getAllEmployeeList(employeesPage);  
                            
                            console.log('\n\nEmployees',employees);
                            console.log("\n\n2. Lets get all employees data in a json object if there are bosses");
                            
                            if (Array.isArray(employees) && len(employees)) {
                                const employee_table_data = [];

                                for (const employee of employees) {
                                    const {name, position, country, employee_url } = employee;

                                    const doesEmployeeExist = await getEmployee({ fullname: name, position: position });
                                    
                                    console.log('doesEmployeeExist: ', doesEmployeeExist);
                                    
                                    if (!doesEmployeeExist) {
                                        const { timezone, time_diff } = getTimeDiffAndTimeZone(country);
                                        
                                        employee_table_data.push({
                                            "fullname" : name,
                                            position, company,
                                            company_url, country,
                                            "linkedin_url" : employee_url,
                                            "email" : "No email",
                                            "phone" : "No Phone number",
                                            timezone, time_diff,
                                            "created_at" : date()
                                        });                                                    
                                    } else {
                                        console.log('This employee already exists in our db');
                                    }
                                }

                                console.log("\n\nEmployees data", employee_table_data);
                                try {
                                    await addEmployeeDataToQueue(employee_table_data);
                                } catch(err){
                                    console.log('Error inserting employees into Mongodb', err)
                                }
                            } else {
                                console.log("No boss in this company"); 
                            }
                        await employeesHandle.dispose();
                        } else {
                            console.log("\n\nError with employees page");
                        }

                        randNo = genRandNum(MIN_TIME, MAX_TIME);

                        const newSize = await getQueueSize(EMPLOYEE_LINK);
                        console.log('\nThe queue size is now', newSize);

                        console.log(`\n\nWaiting for ${randNo} seconds then move to the next URL`);
                        await page.waitFor(randNo);
                    } else {
                        await browser.close();
                        process.exit();  
                    }
                }
                process.on("unhandledRejection", (reason, p) => {
                    console.error("ERROR", p, "reason:", reason);
                    browser.close();
                });             
            })(); 
        } catch(error){
            console.error('Error with puppeteer: ', error);
        }
    } else {
        console.log('The Queue is empty');
        process.exit(); 
    }
}, 2000);
