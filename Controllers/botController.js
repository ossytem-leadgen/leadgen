require('dotenv').config();
const rq = require('request-promise');
const Telegram = require('telegram-node-bot')
const TextCommand = Telegram.TextCommand;
const TelegramBaseController = Telegram.TelegramBaseController;
const API_KEY = process.env.BOT_TOKEN;
const CHAT_ID = process.env.ADMIN_CHAT_ID;

const killNodeListener = () => {
    const bot = new Telegram.Telegram(API_KEY, {
        workers: 1,
        webAdmin: {
            port: 8087,
            host: '127.0.0.1'
        }
    });
    
    
    class KillerController extends TelegramBaseController {
        /**
         * @param {Scope} $
         */
        killHandler($) {
            const { exec } = require('child_process');
            
            $.sendMessage('killed');
            
            exec('pkill node');
        }
    
        get routes() {
            return {
                'killCommand': 'killHandler'
            }
        }
    }
    
    class OtherwiseController extends TelegramBaseController {
        /**
         * @param {Scope} $
         */
        async handle($) {
            const inbox = $.message.text || "nothing";
            $.sendMessage(`You said ${inbox}`);

            // $.sendPhoto(`http://ui-avatars.com/api/?name=${inbox}&size=460`);
        }
    }
    
    bot.router
        .when(new TextCommand('/kill', 'killCommand'), new KillerController())
        .otherwise(new OtherwiseController())
};

const sendMsgToAdmin = msg => {
    return new Promise(async resolve => {
        try {
            const intro = '<b>Hello Best</b>\n';
            const text = msg.length ? `${intro}${msg}` : 'No message to send';
            
            const options = {
                uri: `https://api.telegram.org/bot${API_KEY}/sendMessage`,
                qs: {
                    chat_id: CHAT_ID,
                    text: text,
                    parse_mode: 'HTML'
                },
                headers: {
                    'User-Agent': 'Request-Promise'
                },
                json: true,
            };
            
            const result = await rq(options);

            console.log(result.ok);

            resolve('');
        } catch (error) {
            console.error(error);
            resolve('');
        }
    });
}

module.exports = {
    sendMsgToAdmin, killNodeListener
};