const emojiLog = require('console-emoji');
const { getCompanyDataFromQueue, getEmployeeDataFromQueue, getQueueSize } = require('../Db/Queues/getFromQueue');
const { addCompanyDataToQueue } = require('../Db/Queues/addToQueue');
const { date, time, log, emojis, sleep, len } = require('../modules');
const { CONSTANTS } = require('../helpers/constants');
const { addCompany, getCompany, deleteCompany, updateCompany } = require('../Db/Companies');
const { addEmployee, getEmployee, updateEmployee } = require('../Db/Employees');
const { addGeneratedLeads, getGeneratedLeads } = require('../Db/GeneratedLeads');
const dbConnection = require('../helpers/dbConnection');
const { COMPANY, EMPLOYEE } = CONSTANTS.DB.COLLECTIONS.QUEUE;

console.log(`\n\n--------------------------THIS IS THE LOG FOR SAVING TO MONGO DB SCRIPT [FOURTH SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);

const saveEmployeesFromQueueToDb = async () => {
    const empSize = await getQueueSize(EMPLOYEE);
    console.log('Emp size', empSize);

    for (let i = 0; i < empSize; i++) {
        const empdata = await getEmployeeDataFromQueue();
        const employee = await getEmployee({ fullname: empdata.fullname, position: empdata.position });
        console.log('Queue Employee', empdata);
        console.log('Db Employee', employee);

        if (!employee) {
            log('\n\nEmployee does not exist+++++++++++++++++++++++++++++++++++++++++++++++++++');

            await addEmployee({
                "fullname" : empdata.fullname,
                "position" : empdata.position,
                "company" : empdata.company,
                "company_url" : empdata.company_url,
                "country" : empdata.country,
                "linkedin_url" : empdata.linkedin_url,
                "email" : empdata.email,
                "phone" : empdata.phone,
                "created_at" : date(),
                "merged" : false
            });
            
        } else {
            log('\n\nEmployee exists--------------------------------------------------');
        }
        await sleep(1);
    }
    dbConnection.close();
    process.exit();
};

const removeDoubledCompaniesFromQueue = async () => {
    const cmpSize = await getQueueSize(COMPANY);
    console.log('Company size', cmpSize);

    const compArr = [];

    for (let i = 0; i < cmpSize; i++) {
        const company = await getCompanyDataFromQueue();
        const { name } = company;
        console.log(name)
        if (compArr.includes(name)) {
            //don't add just remove it from queue
        } else {
            compArr.push(name);
            await addCompanyDataToQueue(company);
            log('Added');
        }
    }

    dbConnection.close();
    process.exit();
};

const removeDoubledCompany = async () => {
    const companies = await getCompany({country: "Belarus", employees_link: {$ne: ""}});
    console.log('General number of companies', len(companies));

    const compArr = [];

    for (let company of companies) {
        const { _id, name } = company;
        if (compArr.includes(name)) {
            log('Company already exists', name);
            await deleteCompany({_id})
        } else {
            compArr.push(name);
            log('Added', name);
        }
    }
    console.log('Unique number of companies', len(compArr));
    dbConnection.close();
    process.exit();
};
const removeDoubledCompanies = async () => {
    const companies = await getCompany({country: "Belarus"});
    // const companiesProcessed = await getCompany({country: "Belarus", employees_link: {$ne: ""}});
    console.log('General number of companies', len(companies));
    // console.log('Processed number of companies', len(companiesProcessed));

    const compLinks = [];
    let done = 0,
        undone = 0;

    for (const company of companies) {
        const { linkedin_url } = company;
        compLinks.push(linkedin_url);
    }

    for (let linkedin_url of compLinks) {
        const comp = await getCompany({linkedin_url});

        if (len(comp) === 2) {
            done += 1;
        //    await updateCompany({linkedin_url}, {took_all_employees: true})
        } else {
            undone += 1;
           await updateCompany({linkedin_url}, {took_all_employees: false})            
        }
    }

    console.log(`Done: ${done}\nUndone: ${undone}`);
    dbConnection.close();
    process.exit();
};

const saveCompanyFromQueueToDb = async () => {
    const cmpSize = await getQueueSize(COMPANY);
    console.log('Company size', cmpSize);

    for (let i = 0; i < cmpSize; i++) {
        const company = await getCompanyDataFromQueue();
        const { name } = company;
        const employee = await getEmployee({company: name});
        const companyExists = await getCompany({name});
        
        console.log('Company', name);

        if (employee && !len(companyExists)) {
            log('We found someone with this company', employee.length);
            
            await addCompany({...company});
        } else {
            log('No one had this company so we are not saving this company');

            console.log('Company', name);
        }
    }
    
    dbConnection.close();
    process.exit();
};

const saveToGeneratedLeads = async () => {
    const employees = await getEmployee({ merged: false });
    let [{ batch_number }] = await getGeneratedLeads({batch_number: {$type:"number"}}, 0, 1, true, {batch_number: -1});
    batch_number = batch_number + 1;

    if (employees.length) {
        for (let i=0; i < employees.length; i++) {
            let companyData = [{name: ''}];

            if (employees[i].company !== companyData[0].name) {
                companyData = await getCompany({ name: employees[i].company });
            }

            log(`\nEmployee data:\nName: ${employees[i].fullname}\nCompany: ${companyData}`);

            const sheetData = {
                "company" : companyData[0] !== undefined ? companyData[0].name : employees[i].company,
                "country" : companyData[0] !== undefined ? companyData[0].country : "",
                "headcount" : 0,
                "description" : companyData[0] !== undefined ? companyData[0].description : "",
                "linkedin_url" : companyData[0] !== undefined ? companyData[0].linkedin_url : "",
                "company_url" : companyData[0] !== undefined ? companyData[0].company_url : "",
                "category" : "",
                "lead_name" : employees[i].fullname,
                "lead_linkedin_url" : employees[i].linkedin_url,
                "position" : employees[i].position,
                "lead_title_ranking" : "",
                "lead_email" : employees[i].email,
                "date_connect_sent" : "",
                "date_connect_expire" : "",
                "date_connect_accepted" : "",
                "status" : "",
                "date_last_interaction" : "",
                "found_by_script1" : "Y",
                "found_by_script2" : "N",
                "found_by_script3" : "N",
                "date_found_by_script1" : employees[i].created_at,
                "date_found_by_script2" : "",
                "date_found_by_script3" : "",
                "script_status" : "Not Connected",
                "sheetrange" : "",
                "created_at" : employees[i].created_at,
                "uploaded" : false,
                batch_number,
                "check_before_sending_msg" : true,
                "date_script_ran": ""
            };

            await updateEmployee({ _id: employees[i]._id }, { merged: true });
            await addGeneratedLeads(sheetData);
        }
    } else {
        emojiLog(`All employees & companies are already merged ${emojis.smile}`, 'warn')
        log(`All employees & companies are already merged ${emojis.smile}`)
    }
};

module.exports = {
    saveFromQueueToDb: saveEmployeesFromQueueToDb,
    saveToGeneratedLeads: saveToGeneratedLeads
};