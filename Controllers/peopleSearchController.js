const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
require('dotenv').config();

const { CONSTANTS, URLS } = require('../helpers/constants');
const { addCompanyLinkToQueue, addPeopleLinkToQueue } = require('../Db/Queues/addToQueue');
const { getPersonLinkFromQueue, getQueueSize } = require('../Db/Queues/getFromQueue');
const { genRandNum, date, time, log, getCookie, checkIfUrlIsValidCompany } = require('../modules');
const { scrapPeopleUrls } = require('../modules/scrappers');

const startTime = new Date().getTime();
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { PERSON_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;
const searchUrl = URLS.peopleSearch.url;
const pagination = URLS.peopleSearch.pagination;
const linkedin = URLS.home;

let randNo;

const generateUrls = (pageUrl, pageAmount) => {
    let urlArr = [pageUrl];

    for (let i = 2; i <= pageAmount; i++) {
        // this url is for Greater boston area
        let url = `${pageUrl}&page=${i}`;
        urlArr.push({ url })
    }
    return urlArr;
}

const getPeopleUrls = async () => {
    const pagesUrl = generateUrls(searchUrl, pagination);

    const browser = await puppeteer.launch({
        headless: false
    });

    const page = await browser.newPage();
    
    try {
        const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);

        await page.setCookie(...cookie);
    } catch (error) {
        console.error(error);
    }
    await page.setViewport({ width: 1366, height: 868 });

    for (let pageUrl of pagesUrl) {

        const pageResponse = await page.goto(pageUrl.url, {
            timeout: 120000,
            waitUntil: 'networkidle2',
        });

        if (pageResponse._status < 400) {
            await page.evaluate(async () => {
                await new Promise((resolve, reject) => {
                    try {
                        const maxScroll = Number.MAX_SAFE_INTEGER;
                        let lastScroll = 0;
                        
                        const interval = setInterval(() => {
                            window.scrollBy(0, 100);
                            const scrollTop = document.documentElement.scrollTop;
                            
                            if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                clearInterval(interval);
                                resolve();
                            } else {
                                lastScroll = scrollTop;
                            }
                        }, 100);
                    } catch (err) {
                        console.log(err);
                        reject(err.toString());
                    }
                });
            });
            
            const pageHandle = await page.evaluateHandle(() => document.body);
            const resultpageHandle = await page.evaluateHandle(body => body.innerHTML, pageHandle);
            const html = await resultpageHandle.jsonValue();
            let peopleUrls = scrapPeopleUrls(html);

            for(let url of peopleUrls){
                log(url);
                await addPeopleLinkToQueue(url);
            }
            
            await pageHandle.dispose();
        }
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);
        await page.waitFor(randNo);

        let lastUrl = pagesUrl[pagesUrl.length - 1];
        if (lastUrl === pageUrl) {
          await browser.close();
        }
    }

    process.on("unhandledRejection", (reason, p) => {
        console.error("ERROR", p, "reason:", reason);
        browser.close();
    }); 

}

const getPeopleCompanies = async () => {
    let peopleQueueSize = await getQueueSize(PERSON_LINK);

    if (peopleQueueSize) {
        log('The size of the queue is: ', peopleQueueSize);

        const browser = await puppeteer.launch({
            headless: false
        });

        const page = await browser.newPage();
        
        try {
            const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
    
            await page.setCookie(...cookie);
        } catch (error) {
            console.error(error);
        }
        await page.setViewport({ width: 1366, height: 868 });

        for (let i = 0; i < peopleQueueSize; i++) {
            let personUrl = await getPersonLinkFromQueue();

            if (personUrl) {
                log("\n\n1. Let's make request to employees url");
                
                const personResponse = await page.goto(personUrl, {
                    timeout: 120000,
                    waitUntil: 'networkidle2'
                });
                await page.waitFor(2000);
                if (personResponse._status < 400) {
                    await page.evaluate(async () => {
                        await new Promise((resolve, reject) => {
                            try {
                                const maxScroll = Number.MAX_SAFE_INTEGER;
                                let lastScroll = 0;
                                const interval = setInterval(() => {
                                    window.scrollBy(0, 100);
                                    const scrollTop = document.documentElement.scrollTop;
                                    if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                        clearInterval(interval);
                                        resolve();
                                    } else {
                                    lastScroll = scrollTop;
                                    }
                                }, 100);
                            } catch (err) {
                                console.log(err);
                                reject(err.toString());
                            }
                        });
                    });
                    
                    await page.waitFor(1000);

                    const personHandle = await page.evaluateHandle(() => document.body);
                    const resultpersonHandle = await page.evaluateHandle(body => body.innerHTML, personHandle);
                    const html = await resultpersonHandle.jsonValue();
                    const $ = cheerio.load(html);

                    let link = $('li.pv-profile-section__card-item-v2 a').attr('href');
                    console.log('Company Link: ',link);
                    
                    if (checkIfUrlIsValidCompany(link)) {
                        let companyUrl = `${linkedin}${link}`;
                        await addCompanyLinkToQueue(companyUrl);
                        log('Added company link to queue');
                    } else {
                        log('Not a valid company, this is the link', link);
                    }

                    await personHandle.dispose();
                }
            } else {
                log('I did not get this url for the person');
            }
            randNo = genRandNum(MIN_TIME, MAX_TIME);
            log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);
            await page.waitFor(randNo);
        }
    } else {
        log('The queue is now empty');
    }
}

module.exports = {
    getPeopleUrls : getPeopleUrls,
    getPeopleCompanies : getPeopleCompanies
}