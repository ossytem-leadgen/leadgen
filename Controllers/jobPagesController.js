const puppeteer = require('puppeteer');
const { URLS, CONSTANTS } = require('../helpers/constants');
const { addCompanyLinkToQueue } = require('../Db/Queues/addToQueue');
const { getCompany } = require('../Db/Companies');
const { genRandNum, date, time, log, getCookie, pageScroller } = require('../modules');
const { getUrlFromSearchRes } = require('../modules/scrappers');
const { getRecruitingCompany } = require('../Db/RecruitingCompanies');

const startTime = new Date().getTime();
let randNo;
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE.PAGE_LOADING;

console.log(`\n\n--------------------------THIS IS THE LOG FOR JOBS CRAWLER SCRIPT [FIRST SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);
// let country,
//     technology;
// process.argv.forEach(function (val, index, array) {
//   country = array[2]
//   technology = array[3]
// });
let country = 'usa', technology = 'php';

const getSearchUrl = (country, technology, url) => {
  log('In get search url');
  let pagination = 25;
  let urlArr = []
  // let paginationArr = []

  if (url) {
    for (let i=0; i <= 51; i++) {
      if (i === 0) {
        urlArr.push(url)
      } else {
        urlArr.push(url.concat(`&start=${pagination}`))
        pagination = pagination + 25;
      } 
    }
    return urlArr;
  } else {
    // console.log(URLS["techAndCountry"][0]["tech"][0]["name"])
    URLS.techAndCountry.some((val) => {
      if (val.country == country){
        val.tech.some( (tech_val) => {
            if (tech_val.name == technology){
              for(let i=0; i<10; i++) {
                if (i === 0) {
                  urlArr.push(tech_val.url)
                } else {
                  urlArr.push(tech_val.url.concat(`&start=${pagination}`))
                  pagination = pagination + 25;
                }
              }
            }
        });
      }
    });
  }
  return urlArr;
}

// let searchUrl = getSearchUrl(0, 0, 'https://www.linkedin.com/search/results/people/?facetGeoRegion=%5B%22us%3A14%22%5D&keywords=node%20react&origin=FACETED_SEARCH&page=1');
// let searchUrl = getSearchUrl(country, technology);

try{
  (async () => {
    let browser = await puppeteer.launch({
      headless: false
    });
    let page = await browser.newPage();
    
    try {
      const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.BEST);
      await page.setCookie(...cookie);
    } catch (error) {
      console.error(error)
    } 
    for(url of searchUrl) {

      console.log("1. Make search request\nAnd searching for", country, technology, "\nMaking search to this page", url);
      const searchResponse = await page.goto(url, {
        timeout: 120000,
        waitUntil: 'networkidle2',
      });
      await page.setViewport({ width: 1366, height: 768 });

      if (searchResponse._status < 400) {
        // let navresponse = page.waitForNavigation(['networkidle0', 'load', 'domcontentloaded']);
        await page.click('div.dropdown.jobs-search-dropdown--view-switcher');
        await page.click('button.jobs-search-dropdown__option-button.jobs-search-dropdown__option-button--single');

        await pageScroller(page);

        randNo = genRandNum(MIN_TIME, MAX_TIME);
        console.log(`\n\n Waiting for ${randNo} sec for lazy loading elements on the page`);
        await page.waitFor(randNo);

        const searchHandle = await page.evaluateHandle(() => document.body);
        const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
        let html = await resultHandle.jsonValue();
        // console.log(html)

        let searchOutput = getUrlFromSearchRes(html);
        console.log("\n\nGotten search html now waiting for 5.000 seconds then begin processing the html");
        await page.waitFor(5000);
        // console.log('\n\nThis are the companies on this page:', searchOutput);
        let total = 0;
        let existingCompany = [];
        if(Array.isArray(searchOutput) && searchOutput.length){
            for(let search_res of searchOutput) {
              console.log('---------------------------------------------------------------------------------------');
              let doesCompanyExist = await getCompany({ name: search_res.company_name });
              let recruitingCompany = await getRecruitingCompany({ name: search_res.company_name });
              if (!doesCompanyExist && !recruitingCompany) {
                if(!doesCompanyExist) console.log('We dont have this companies data', search_res.company_name)
                // console.log('New Company and maybe not a recruiting company', search_res.company_name)
                addCompanyLinkToQueue(search_res.company_url);
                total++;
              }
              if(doesCompanyExist) existingCompany.push(search_res.company_name)
              if(recruitingCompany) console.log('\nIt is a recruiting company', search_res.company_name)
              console.log('---------------------------------------------------------------------------------------');
            }
        }
        console.log('Existing Companies:', existingCompany)
        console.log(`No of companies on page: ${searchOutput.length}`);
        console.log(`No of Companies taken: ${total}`);
        console.log(`No of Companies rejected: ${searchOutput.length - total}`);

        let last = searchUrl[searchUrl.length - 1]
        if(last === url) {
          let endTimeMilli =  new Date().getTime() - startTime;
          let endTimeMin = Math.floor(endTimeMilli / 60000);
          let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);
          if (endTimeSec == 60) {
            endTimeMin = endTimeMin + 1;
            endTimeSec = 0;
          }
          console.log(`Script 1 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);
          process.exit();
        }
        console.log("\n\nDone with this page, lets go to the next but wait for 5 secs");
        await page.waitFor(5000);
        await searchHandle.dispose();
      }
    }

  process.on("unhandledRejection", (reason, p) => {
    console.error("ERROR", p, "reason:", reason);
    browser.close();
  });
    await browser.close();
  })(); 

} catch(error){
  console.error('Error with puppetter: ',error)
}
