const { MongoClient } = require('mongodb');
require('dotenv').config();
const ENV = process.env;
const URL = `${ENV.DB_PROVIDER}://${ENV.DB_USER}:${encodeURIComponent(ENV.DB_PWORD)}@${ENV.DB_HOST}:${ENV.DB_PORT}/${ENV.DB_NAME}`;

let connection = null;

module.exports = {
    get () {
        return new Promise((resolve, reject) => {
            if (!connection) {
                MongoClient.connect(URL, (err, _connection) => {
                    if (err) {
                       return reject(`Error while connecting to DB ${err}`);
                    }

                    connection = _connection;

                    console.log('Connection has been opened');

                    resolve(connection);
                });
            } else {
                resolve(connection);
            }
        });
    },
    close () {
        if (connection) {
            connection.close();
            connection = null;
            console.log('Connection has been closed');
        } else {
            console.log('Connection has already been closed');
        }
        
    }
}