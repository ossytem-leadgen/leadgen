module.exports = {
    CREDENTIALS: {
        OLEG: {
            NAME: 'smitt.job@gmail.com',
            PWD: 'oss2London'
        },
        KORZHENKO: {
            NAME: 'k.korzhenko@ossystem.com.ua',
            PWD: 'Palermo10'
        },
        LEVITSKAYA: {
            NAME: 'v.levitskaya@ossystem.com.ua',
            PWD: 'Palermo10'
        },
        LOGIN: {
            EMAIL: 'leadgen@gmail.com',
            PWD: 'admin'
        }
    },
    COOKIES_OWNER: {
        OLEG: 'oleg_sergevich',
        KORZHENKO: "korzhenko",
        LEVITSKAYA: "levitskaya",
        BEST: "best"
    },
    MESSAGE_STATUS: {
        NOT_CONNECTED: 0,
        IN_PROCESS: 2
    },
    ALPHABETS: {
        UPPERCASE: ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
        LOWERCASE: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    },
    REG_EXPRESSION: {
        GUID:"/[0-9]{8}-[0-9]{4}-.../"
    },
    EMAIL: {
        CONTACTS: {
            OLEG: "oleg.seroochenko@gmail.com",
            DENIS: "stdv.ossystem@gmail.com",
            BEST: {
                COMPANY: "irb.ossystem@gmail.com",
                PERSONAL: "rotimiibitoyeemma@gmail.com"
            },
            SCRIPT: "leadgen.ossystem@gmail.com"
        },
        REPLY: {
            SUBJECT: {
                TEXT: "LeadGen Script: Someone Replied Our Message On LinkedIn"
            }
        },
        ERROR: {
            SUBJECT: {
                TEXT : "LeadGen Script: There was an error in the script"
            }
        },
    },
    OLEG_MSG: {
        CONDITIONS: {
            CHATED: "empty_inbox",
            NOT_CHATED: "not_empty_inbox",
            IN_USA: "country_located_in_usa",
            NOT_USA: "country_located_not_in_usa",
        },
        "SCRIPT_STATUS" : {
            "NOT_STARTED" : "lead_not_in_process",
            "ON_GOING" : "lead_in_process",
            DONE: "lead_done",
            RESPONDED: "lead_responded"
        },
        "SEND_MSG_INTERVALS" : {
            "BEFORE_LAUNCH" : {
                BEGIN: "08:00:00",
                END: "12:00:00"
            },
            "AFTER_LAUNCH" : {
                BEGIN: "13:00:00",
                END: "18:00:00"
            }
        },
        // "MESSAGE_DATES_FIELDS" : {
        //     SENT: "",
        //     RECEIVED: "",
        //     READ: ""
        // }
    },
    ACCEPTED_CONNECTS_STATS: {
        EARLY_MORNING: {
            START: "00:00:00",
            END: "05:59:59"
        },
        LATE_MORNING: {
            START: "06:00:00",
            END: "11:59:59"
        },
        AFTERNOON: {
            START: "12:00:00",
            END: "17:59:59"
        },
        NIGHT: {
            START: "18:00:00",
            END: "23:59:59"
        }
    },
    DAYS: {
        LONG: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    },
    MONTHS: {
        SHORT: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        LONG: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    },
    SPREAD_SHEET_PARAMS: {
        LEADGEN: {
            ID: "15jLBGFJ_4-tFqVZlzQmhjgXoOw3hvf9wO8Km5NTuuOw",
            MAIN_SHEETS: {
                BATCH_ZERO: {
                    ID: "294519863",
                    NAME: "0_TEST"
                },
                BATCH_ONE: {
                    ID: "330064668",
                    NAME: "1_BOSTON"
                },
                BATCH_TWO: {
                    ID: "532753731",
                    NAME: "2_CANADA"
                },
                BATCH_THREE: {
                    ID: "1765758869",
                    NAME: "3_BOSTON"
                },
                BATCH_FOUR: {
                    ID: "1412895120",
                    NAME: "4_ISREAL"
                },
                DEAD_LEADS: {
                    ID: "1308383630",
                    NAME: "DeadLeads"
                },
                BATCH_FIVE: {
                    ID: "221452576",
                    NAME: "5_RUSSIA"
                },
                BATCH_SIX: {
                    ID: "28967106",
                    NAME: "6_CHICAGO"
                },
                MOBILE_LEADS: {
                    ID: "1946828371",
                    NAME: "Mobile Leads Structure",
                    TEXT: "Mobile Leads"
                },
                OLEG_CONTACTS: {
                    ID: "2078022814",
                    NAME: "Oleg Contacts"
                },
                OLEG_USA_CONTACTS: {
                    ID: "252042663",
                    NAME: "Oleg USA Contacts"
                }
            }
        },
        LEADGEN_STATISTICS: {
            ID: "1_k0nRwfh4U_WUnTUDx5NdhsUGUQgq4lSuGl4vGKRnCQ",
            STATS_SHEETS: {
                BATCH_ONE: {
                    ID: "0",
                    NAME: "1_BOSTON",
                },
                BATCH_TWO: {
                    ID: "1070079690",
                    NAME: "2_CANADA"
                },
                BATCH_THREE: {
                    ID: "267425692",
                    NAME: "3_BOSTON"
                },
                BATCH_FOUR: {
                    ID: "1506355981",
                    NAME: "4_ISREAL"
                },
                DEAD_LEADS: {
                    ID : "820403716",
                    NAME : "DeadLeads"
                },
                BATCH_FIVE: {
                    ID: "710441409",
                    NAME: "5_RUSSIA"
                },
                BATCH_SIX: {
                    ID: "438215972",
                    NAME: "6_CHICAGO"
                },
                INBOX: {
                    ID : "1224336545",
                    NAME : "Inbox"
                },
                GENERAL: {
                    ID : "2038746111",
                    NAME : "General"
                },
                MOBILE_LEADS: {
                    ID: "1574102684",
                    NAME: "Mobile Leads"
                },
                ACCEPTED_CONNECTS: {
                    ID: "1237117911",
                    NAME: "Accepted Connects"
                }
            }
        },
        THREE_REGION_CAMPAIGN: {
            ID: "1q6pSyn0IS0BTG4lBhHrXL-RgTvtHYmWzTcOI7wWAt0A",
            STATS_SHEETS: {
                NORTHERN_EUROPE: {
                    ID: "1316582872",
                    NAME: "Northern Europe",
                },
                UK: {
                    ID: "2108842613",
                    NAME: "UK"
                },
                GERMANY: {
                    ID: "708355164",
                    NAME: "Germany"
                }
            }
        },
        LEADGEN_DRIVE: {
            FOLDERS: {
                ERROR: {
                    ID: "1GZHsC9PzjAYUuTL_lLRiJ9WwmWmEa4wz",
                    NAME: "Script Error",
                    KIND: "drive#file",
                    mimeType: 'application/vnd.google-apps.folder'
                }
            }
        }
    },
    SCRIPT_PROCESS: {
        GET_ACCEPTED_CONNECTS: 1,
        SEND_GENRATED_CONNECT: 2,
        SEND_SALES_CONNECT: 3,
        SEND_DEADLEADS_ONE_FOLLOWUP: 4,
        SEND_DEADLEADS_ALL_FOLLOWUP: 5,
        SEND_NEWLEADS_FOLLOWUP: 6,
        UPDATE_SHEET_WITH_STATISTICS: 7,
        WATCH_REPLIES: 8,
        EMAIL_REPLIES: 9,
        SEARCH_BY_PEOPLE: 10,
        SEARCH_BY_JOBS: 11,
        MOBILE_LEADS: 12,
        LOGIN: 13,
        STAY_ON_HOME_PAGE: 14,
        MESSAGE_MOBILE: 15,
        KILL_NODE_LISTENER: 16,
        UPDATE_ACCEPTED_CONNECT_STATS: 17,
        GET_OLEG_LEAD_PROFILE: 18,
        GET_COMPANY_DATA: 19
    },
    SCRIPT_NAMES: {
        GET_ACCEPTED_CONNECTS: 1,
        SEND_CONNECT_MESSAGES: 2,
    },
    SCRIPT_PAUSE: {
        MIN_TIME: 30000,
        MAX_TIME: 50000,
        SEND_CONNECTS: {
            MIN_TIME: 180000,
            MAX_TIME: 300000,
        },
        PAGE_LOADING: {
            MIN_TIME: 60000,
            MAX_TIME: 80000,
        },
        MESSAGE_URL: {
            MIN_TIME: 20000,
            MAX_TIME: 35000,
        }
    },
    DB: {
        COLLECTIONS: {
            SCRIPTS: {
                NAME: "scripts",
                FIELDS: {
                    pid: "",
                    name: "",
                    time_started: "",
                    status: {
                        RUNNING: "running",
                        PAUSED: "stopped",
                        STOPPED: "stopped",
                    },
                    description: {
                        WATCH_REPLIES: "This script is watching linkedin to get any incoming message",
                        EMAIL_REPLIES: "This script is sending emails of the inbox for the previous day",
                        GET_ACCEPTED_CONNECTS: "This script is checking to see the new connects that we have",
                        GET_PROFILE: "This script is getting the profile of olegs contacts",
                        SEND_MESSAGE: "This script is sending messages to leads",
                        SEND_CONNECTS: "This script is sending connect messages",
                        GET_COMPANY_DATA: "This script is getting details about certain companies",
                        FIND_LEADS: "This script is finding people on Linkedin",
                        LOGIN: "This script is logging into linkedin",
                        KILL_NODE_LISTENER: "This script is waiting for the admin to kill all running processes on his computer"
                    },
                },
            },
            COMPANY: {
                NAME: "companies",
                FIELDS: []
            },
            EMPLOYEE: {
                NAME: "employees",
                FIELDS: []
            },
            GENERATED_LEADS: {
                NAME: "generated_leads",
                FIELDS: []
            },
            LEADS: {
                NAME: "leads",
                FIELDS: []
            },
            MOBILE_LEADS: {
                NAME: "mobile_leads",
                FIELDS: []
            },
            OLEG_LEADS: {
                NAME: "oleg_contacts",
                FIELDS: []
            },
            RECRUITING_COMPANY: {
                NAME: "recruiting_companies",
                FIELDS: []
            },
            STATISTICS: {
                NAME: "statistics",
                FIELDS: []
            },
            INBOXES: {
                NAME: "inboxes",
                FIELDS: ["_id", "name","linkedin_url","time","date","message", "email_sent"]
            },
            ACCEPTED_CONNECTS: {
                NAME: "accepted_connects",
            },
            USERS: {
                NAME: "users",
            },
            SALESLEADS: {
                NAME: "sales_leads",
                FIELDS: ["_id", "firstname","lastname","url","email","connect_message_sent","date_connect_sent", "message_one_sent", "date_message_one_sent", "already_connected", "email_req", "pending"]
            },
            MOBILE_LEADS_STATISTICS: {
                NAME: 'mobile_leads_stats',
                FIELDS: ["_id", "connects_sent","connects_pending","connects_accepted","uploaded","week"]
            },
            COOKIES: {
                NAME: 'cookies',
                FIELDS: ["_id", "owner", "content", "updatedAt"]
            },
            QUEUE: {
                COMPANY_LINK: "companies-searchres-queue",
                EMPLOYEE_LINK: "employees-pages-url-queue",
                PERSON_LINK: "people-url-queue",
                COMPANY: "companies-queue",
                EMPLOYEE: "employees-queue",
                OLEG_CONTACTS: "oleg-contact-queue"
            }
        }
    }
};