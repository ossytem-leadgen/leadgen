const best = [ 
    {
        "name": "JSESSIONID",
        "value": "ajax:3610481649524194015",
        "domain": ".www.linkedin.com",
    },
    {
      "name": "_lipt",
      "value": "CwEAAAFm1Hkef1XdT-SvKqd_Qx6q0eVI5nCUHv2wMiT2qui8HodiC7-mdEUKVqnW_IcS_ctNKdp_HMgRUDfVSQ7tPUDstaeB4PFgeqlcLR72SnI44ySBebsY3bxLLMrPmoAsJMmS9GpKUvCwe6zHu7I9iV9f1NvwcgnBRusoib1zFj3ubBZk0vGM7vYVmEW7JtZAwUvnK8Bn9X4kZpO-OB7yA1aG6pMueJBU6Er0ZkUNYAp77oJ-A7wnlYyP4uPeFH-z2G6AmHGunLlhml_7uB-LROlE3Il0u5tjprNVHbG5n7ChapL5nQERF6zx4kOtklOTgcVg0BedSnGNbuwAcBXKIWdoGh7iFL52_aZmHgqxpym1AjKdLz_oidZmEaF1JKDx",
      "domain": ".linkedin.com",
    },
    {
      "name": "bcookie",
      "value": "v=2&84e60978-7a7a-4acd-8b87-73d5395affda",
      "domain": ".linkedin.com",
    },
    {
      "name": "bscookie",
      "value": "v=1&20181102124945a10f5e17-2395-46ab-884c-8de519db9843AQELl_vnyifcByVTD8fvIMKG1uiCtgCP",
      "domain": ".www.linkedin.com",
    },
    {
      "name": "lang",
      "value": "v=2&lang=en-us",
      "domain": ".www.linkedin.com",
    },
    {
      "name": "li_at",
      "value": "AQEDARG80KgFkB2RAAABZtR5F7gAAAFm-IWbuE0Avl3knK0fgFgKB6r19Uva_xHCaZI_QTXwc9DuY8zYX0w9FMEEMlxZ9LSWAqwr_6B3GNwSoKGwy1dWEwU3F6O9HldRn174izi0q7a-Xe3yzhPp9JV9",
      "domain": ".linkedin.com",
    },
    {
      "name": "liap",
      "value": "true",
      "domain": ".linkedin.com",
    },
    {
      "name": "lidc",
      "value": "b=VB56:g=1762:u=69:i=1541163007:t=1541249398:s=AQEfnXbX4KEWxwsuNCf8FGiXoDtF4nRK",
      "domain": ".linkedin.com",
    },
    // {
    //   "name": "spectroscopyId",
    //   "value": "5984edfe-eb4a-409d-81e9-204916ab0eb9",
    //   "domain": ".www.linkedin.com",
    // },
    {
      "name": "sl",
      "value": "v=1&zorrJ",
      "domain": ".www.linkedin.com",
    },
    {
      "name": "visit",
      "value": "v=1&G",
      "domain": "www.linkedin.com",
    },
];

// k.korzhenko@ossystem.com.ua
// Palermo10
const account1 = [
  {
      "name": "JSESSIONID",
      "value": "ajax:7368908623781582840",
      "domain": ".www.linkedin.com",
  },
  {
    "name": "_lipt",
    "value": "CwEAAAFmvszSnbiD19YGEgrMQmrMMV5g0YEFCRBiORGd_6vOUIUvq_LCVhUzInIFPJ6VrXKHzhBJSoTTN8ZoKCefgy_U1tSwRLn4HW3b02ekULrl1hfKh-RLg146xOrriDUztXMHLgBHBublWtFZKjL8hqY92GRZfwlegXguQeySDMJnaF8RJ3SfG27jRX3LfQzLhnUfWMWUD98S_tKkdXzJixmUMngrvx9xZSQFUEDu2M43bDPlVh3ZrW2NMb7heHNWGM-q_KhyducPopjgLc78RO1eLjDrVt0GpUblrtbFa8AXr-0",
    "domain": ".linkedin.com",
  },
  {
    "name": "bcookie",
    "value": "v=2&334e39ef-400c-4101-8f0e-8abadcedd4fc",
    "domain": ".linkedin.com",
  },
  {
    "name": "bscookie",
    "value": "v=1&20181029074916acf9a33b-0e28-41d7-89db-8664710b5b1cAQEA141VlGNBsW8EkhmxYBoiKWmoSOT0",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "lang",
    "value": "v=2&lang=en-us",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "li_at",
    "value": "AQEDAR_yPMkAVxfOAAABZr7MzQ4AAAFm4tlRDlYAwISYvIf2zJA3mh-BEclDzo62rTn6k7zeLK_PC5G8jm5sE3tJycK9gC4KS9OVmJDV4I8gAH70m-zLXGwZL2Yo62ePp5lytji5edHC8FwK5n59DWFB",
    "domain": ".linkedin.com",
  },
  {
    "name": "liap",
    "value": "true",
    "domain": ".linkedin.com",
  },
  {
    "name": "lidc",
    "value": "b=VB69:g=1537:u=38:i=1540799385:t=1540885701:s=AQEVqFRLHBUcsMhIf0YCRdoN9Mi5RvWF",
    "domain": ".linkedin.com",
  },
  // {
  //   "name": "spectroscopyId",
  //   "value": "5984edfe-eb4a-409d-81e9-204916ab0eb9",
  //   "domain": ".www.linkedin.com",
  // },
  {
    "name": "sl",
    "value": "v=1&34vww",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "visit",
    "value": "v=1&G",
    "domain": "www.linkedin.com",
  },
];

// v.levitskaya@ossystem.com.ua
// Palermo10
const account2 = [ 
  {
      "name": "JSESSIONID",
      "value": "ajax:9167733463668429666",
      "domain": ".www.linkedin.com"
  },
  {
    "name": "_lipt",
    "value": "CwEAAAFmvsgaYvO6Jv3biE_57t3Ve-u9j4qONRTUYfmeBhIFWuqMHY1xrwbYwrzI6fqoLtqNBoVrg4oXAQRecUs6ncCko8s6B2V_LTiCj_MFo4zYXOJodZx6rJGOrtyJHHZ25t4LDWzIQa85lFXRxXVsFTIOMpk8xGo_f8-AptXGtvVeRIEdG1orIwLSmV0HyA96qF398cez8OOWfrVBFaVUqbiTk8IhYPIBThkiF27Wj7S7Cr3LxmwLvXBUUhiRp6uuu8LIJZnlqLGRBKiNRcyh-JOV2Der2AmLvgNE0M3ml7M3nAKj_BMstTMUgTFScDZnozYctoL0Zf_8mjC80FWC0MdQAWWOeIzyJ934WHvegU2mg0CKAXOc0mFTBZri",
    "domain": ".linkedin.com"
  },
  {
    "name": "bcookie",
    "value": "v=2&e2e3f1b6-efb5-4b22-85a1-260e691a19f7",
    "domain": ".linkedin.com"
  },
  {
    "name": "bscookie",
    "value": "v=1&201810290732276b08082b-588f-441d-896c-10bbeb9c1781AQE3jkU36N14eKCivg-bTULZlASSeK11",
    "domain": ".www.linkedin.com"
  },
  {
    "name": "lang",
    "value": "v=2&lang=en-us",
    "domain": ".www.linkedin.com"
  },
  {
    "name": "li_at",
    "value": "AQEDARvGdqcEtxqSAAABZr7IEsEAAAFm4tSWwU4AeldnxWd7_DiTyqTe5mIXo1EenPlHS6I1AekR1lEMH7Sn9AYT6tR4uEeaCSAo_q5zm2JQ2UPDVfE5FpXq6MdNPuPj1aAZ2YivXVwPKqIhAQz-b7yN",
    "domain": ".linkedin.com"
  },
  {
    "name": "liap",
    "value": "true",
    "domain": ".linkedin.com"
  },
  {
    "name": "lidc",
    "value": "b=VB35:g=1796:u=22:i=1540799114:t=1540885475:s=AQH5v4L3IYvyUw9qEMdzQvQqYvfXMgS7",
    "domain": ".linkedin.com"
  },
  {
    "name": "sl",
    "value": "v=1&xvjQh",
    "domain": ".www.linkedin.com"
  },
  {
    "name": "visit",
    "value": "v=1&G",
    "domain": "www.linkedin.com"
  }
];

// smitt.job@gmail.com
// oss2London
const boss = [
  {
      "name": "JSESSIONID",
      "value": "ajax:0589707246192604002",
      "domain": ".www.linkedin.com",
  },
  {
    "name": "_lipt",
    "value": "CwEAAAFnWZkodKvwAQ1OpG7h6ua5sV0TLKYnLZE0aNjyCora-LyaobPH3nECGfugl1Z0rT8W9yhmmJBImO-PrjVtLguSdEn-E7Hi_D7Y-_8aYfQ_VM_Tr1K1bu5tlguvPiyZLJd7ghhgk_WDV7DFllG_99wr_kZtUSisL3Qvvh6uCI2O2hbfmlamV7rd_2fZW_dPuNu6D5l08bW1FnQfW2pLsIlcpNU2T1djksEDcp9Wd3kgAzsBFzAnZiGK2eOST5Q674EDb2inpLYUmTAzKsl7hfuGzzVZYX9zyUE9zlmJxsR1hxupOZya2aFe_qjJ75qiDBpJKFNDTd5mbAIwNidTUWlTX4Swa-CD8XcDsHmSLCTeLzlr0ME_fuHCbfdoq7m6iBQPnNIx_Pq8Q8Hp7uKNBEiuP2XSSgnn2UHda7PkPGcmc5Oe_5pKkIn8zE7Vvn4eF-tnpfAqTRwawDS861N9gjgHPsnLTENdT6GCysz8GcfCZLB_4R5KmdFoENWr5TS_M4c3DjWvV_I",
    "domain": ".linkedin.com",
  },
  {
    "name": "bcookie",
    "value": "v=2&021c6e5c-e8b6-4f0c-85e4-8a306cc81b0f",
    "domain": ".linkedin.com",
  },
  {
    "name": "bscookie",
    "value": "v=1&201808311539523e2d42d5-080c-4776-812c-e83078caabf0AQFov6D93iY4eNpPSGzw512a5Lb0LAYC",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "lang",
    "value": "v=2&lang=en-us",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "li_at",
    "value": "AQEDAQKBJh0DFZKmAAABZ1mYKFAAAAFnfaSsUFYAhl9qIlKG7hhsQ9d3eIsCRq8TyvoGN9Cj0m1OkK_tXkSFGB4RemhNgbi0Lt4cCtCPy8zTCaKER8kZZp5lVgF8AHw4KajGKinL1ClKs1U4nUO-jJv7",
    "domain": ".linkedin.com",
  },
  {
    "name": "liap",
    "value": "true",
    "domain": ".linkedin.com",
  },
  {
    "name": "lidc",
    "value": "b=VB33:g=1957:u=765:i=1543396749:t=1543465664:s=AQFrD1Qv3xOZqw7R02RAa-1-RGWo4ZVm",
    "domain": ".linkedin.com",
  },
  // {
  //   "name": "spectroscopyId",
  //   "value": "5984edfe-eb4a-409d-81e9-204916ab0eb9",
  //   "domain": ".www.linkedin.com",
  // },
  {
    "name": "sl",
    "value": "v=1&LVChZ",
    "domain": ".www.linkedin.com",
  },
  {
    "name": "visit",
    "value": "v=1&M",
    "domain": "www.linkedin.com",
  }
];

module.exports = {
  best: best,
  acc1: account1,
  acc2: account2,
  boss: boss
};