module.exports = {
    "techAndCountry": [
        {
            "country" : "usa",
            "tech" : [
                {
                    "name" : "node",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Node.js&location=United%20States&locationId=us%3A0"
                },
                {
                    "name" : "react",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React.js&location=United%20States&locationId=us%3A0"
                },
                {
                    "name" : "reactNative",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React%20Native&location=United%20States&locationId=us%3A0"
                },
                {
                    "name" : "php",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=PHP&location=United%20States&locationId=us%3A0"
                },
                {
                    "name" : "symfony",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Symphony&location=United%20States&locationId=us%3A0"
                },
                {
                    "name" : "laravel",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Laravel&location=United%20States&locationId=us%3A0"
                }
            ],
            "nextPage" : "&start=50"
        },
        {
            "country" : "uk",
            "tech" : [
                {
                    "name" : "node",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Node.js&location=United%20Kingdom&locationId=gb%3A0"
                },
                {
                    "name" : "react",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React.js&location=United%20Kingdom&locationId=gb%3A0"
                },
                {
                    "name" : "reactNative",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React%20Native&location=United%20Kingdom&locationId=gb%3A0"
                },
                {
                    "name" : "php",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=PHP&location=United%20Kingdom&locationId=gb%3A0"
                },
                {
                    "name" : "symfony",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Symphony&location=United%20Kingdom&locationId=gb%3A0"
                },
                {
                    "name" : "laravel",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Laravel&location=United%20Kingdom&locationId=gb%3A0"
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "norway",
            "tech" : [
                {
                    "name" : "node",
                    "url" : ""
                },
                {
                    "name" : "react",
                    "url" : ""
                },
                {
                    "name" : "reactNative",
                    "url" : ""
                },
                {
                    "name" : "php",
                    "url" : ""
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "denmark",
            "tech" : [
                {
                    "name" : "node",
                    "url" : ""
                },
                {
                    "name" : "react",
                    "url" : ""
                },
                {
                    "name" : "reactNative",
                    "url" : ""
                },
                {
                    "name" : "php",
                    "url" : ""
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "finland",
            "tech" : [
                {
                    "name" : "node",
                    "url" : ""
                },
                {
                    "name" : "react",
                    "url" : ""
                },
                {
                    "name" : "reactNative",
                    "url" : ""
                },
                {
                    "name" : "php",
                    "url" : ""
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "luxembourg",
            "tech" : [
                {
                    "name" : "node",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Node.js&location=Luxembourg&locationId=lu%3A0"
                },
                {
                    "name" : "react",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React.js&location=Luxembourg&locationId=lu%3A0"
                },
                {
                    "name" : "reactNative",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React%20Native&location=Luxembourg&locationId=lu%3A0"
                },
                {
                    "name" : "php",
                    "url" : ""
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "belgium",
            "tech" : [
                {
                    "name" : "node",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Node.js&location=Belgium&locationId=be%3A0"
                },
                {
                    "name" : "react",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React.js&location=Belgium&locationId=be%3A0"
                },
                {
                    "name" : "reactNative",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React%20Native&location=Belgium&locationId=be%3A0"
                },
                {
                    "name" : "php",
                    "url" : ""
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        },
        {
            "country" : "netherlands",
            "tech" : [
                {
                    "name" : "node",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=Node.js&location=Netherlands&locationId=nl%3A0"
                },
                {
                    "name" : "react",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React.js&location=Netherlands&locationId=nl%3A0"
                },
                {
                    "name" : "reactNative",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=React%20Native&location=Netherlands&locationId=nl%3A0"
                },
                {
                    "name" : "php",
                    "url" : "https://www.linkedin.com/jobs/search/?keywords=php&location=Netherlands&locationId=nl%3A0"
                },
                {
                    "name" : "symfony",
                    "url" : ""
                },
                {
                    "name" : "laravel",
                    "url" : ""
                }
            ],
            "nextPage" : "&start=25"
        }
    ],
    "employeeByCountry" : [
        {
            "country" : "usa",
            "filter" : "&facetGeoRegion=%5B%22us%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "uk",
            "filter" : "&facetGeoRegion=%5B%22gb%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "norway",
            "filter" : "&facetGeoRegion=%5B%22no%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "denmark",
            "filter" : "&facetGeoRegion=%5B%22dk%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "finland",
            "filter" : "&facetGeoRegion=%5B%22fi%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "sweden",
            "filter" : "&facetGeoRegion=%5B%22se%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "germany",
            "filter" : "&facetGeoRegion=%5B%22de%3A0%22%5D&origin=FACETED_SEARCH"
        },
        {
            "country" : "netherlands",
            "filter" : "&facetGeoRegion=%5B%22nl%3A0%22%5D&origin=FACETED_SEARCH"
        }
    ],
    "connections" : "https://www.linkedin.com/mynetwork/invite-connect/connections/",
    "home" : "https://www.linkedin.com",
    jobs: "https://www.linkedin.com/jobs",
    "peopleSearch": {
        url: "https://www.linkedin.com/search/results/people/?facetGeoRegion=%5B%22us%3A14%22%5D&keywords=node%20react&origin=FACETED_SEARCH",
        pagination: 50
    },
    "salesNavigator": {
        search: "https://www.linkedin.com/sales/search/company?keywords=startup&page=1&searchSessionId=6fqqBExxRDKuj1JcHGgYww%3D%3D",
        pagination: 40
    },
    "messages": "https://www.linkedin.com/messaging/",
    "sent": "https://www.linkedin.com/mynetwork/invitation-manager/sent"
}