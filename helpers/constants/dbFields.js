'use strict'
//: [0-9 "[a-z A-Z / \s ( :., 0-9 -_]+"
//====================================== Existing leads =======================


const old_oleg_contacts_old = {
    "lead_name" : "",
    "position" : "",
    "country" : "",
    "linkedin_url" : "",
    "ifChatted" : false,
    "num_msg_sent" : "",
    "date_msg_one_sent" : "",
    "date_msg_one_read" : "",
    "date_msg_one_responded" : "",
    "date_msg_two_sent" : "",
    "date_msg_two_read" : "",
    "date_msg_two_responded" : "",
    "date_msg_three_sent" : "",
    "date_msg_three_read" : "",
    "date_msg_three_responded" : "",
    "date_msg_four_sent" : "",
    "date_msg_four_read" : "",
    "date_msg_four_responded" : "",
    "date_msg_five_sent" : "",
    "date_msg_five_read" : "",
    "date_msg_five_responded" : "",
    "sendMessage" : false,
    "inUSA" : false,
    "date_of_last_interaction" : "",
    "batch_number" : "",
    "lead_status" : "",
    "time_diff" : "",
    "date_last_interaction" : ""
};

const new_oleg_contacts = {
    firstname: "",
    lastname: "",
    lead_name: "",
    position: "",
    country: "",
    company: "",
    company_url: "",
    company_founded: "",
    company_headcount: "",
    company_description: "",
    linkedin_url: "",
    website: "",
    phone: "",
    address: "",
    email: "",
    twitter: "",
    skype: "",
    birthday: "",
    timezone: "", 
    time_diff: "",
    sheetrange: "",
    connected_date: "",
    removed_connect: false
};

//====================================== LEADS =======================
// A: FOUND BY SCRIPT
const generated_leads = {
    "company" : "",
    "country" : "",
    "headcount" : "",
    "description" : "",
    "linkedin_url" : "",
    "company_url" : "",
    "category" : "",
    "lead_name" : "",
    "lead_linkedin_url" : "",
    "position" : "",
    "lead_title_ranking" : "",
    "lead_email" : "",
    "date_connect_sent" : "",
    "date_connect_expire" : "",
    "date_connect_accepted" : "",
    "status" : "",
    "date_last_interaction" : "",
    "found_by_script1" : "",
    "found_by_script2" : "",
    "found_by_script3" : "",
    "date_found_by_script1" : "",
    "date_found_by_script2" : "",
    "date_found_by_script3" : "",
    "script_status" : "",
    "sheetrange" : "",
    "created_at" : "",
    "uploaded" : false,
    "check_before_sending_msg" : false,
    "date_script_ran" : "",
    "time_diff" : "",
    "timezone" : ""
};

// B: FOUND BY OLEG
const mobile_leads = {
    "lead_name" : "",
    "linkedin_url" : "",
    "connected_date" : "",
    "connected" : true,
    "chatted" : false,
    "checked" : true,
    "batch" : 0,
    "week" : "",
    "time_diff" : "",
    "company" : "",
    "company_url" : "",
    "country" : "",
    "position" : "",
    "sheetrange" : "",
    "uploaded" : true,
    "completed" : false,
    "date_last_interaction" : "",
    "max_msgs" : 0,
    "num_msg_sent" : 0,
    "replied" : "",
    "check_before_sending" : true,
    "timezone" : ""
};

// C: FOUND BY SALES DEPARTMENT
const sales_leads = {
    "firstname" : "",
    "lastname" : "",
    "lead_name" : "",
    "url" : "",
    "email" : "",
    "connect_message_sent" : false,
    "date_connect_sent" : "",
    "message_one_sent" : false,
    "date_message_one_sent" : "",
    "already_connected" : false,
    "pending" : false,
    "email_req" : false,
    "error" : 0.0,
    "connect_message_accepted" : false,
    "lead_name" : ""
};

const leads = {
    "company" : "",
    "country" : "",
    "linkedin_url" : "",
    "company_url" : "",
    "category" : "",
    "lead_name" : "",
    "lead_linkedin_url" : "",
    "position" : "",
    "lead_title_ranking" : "",
    "lead_email" : "",
    "date_connect_sent" : "",
    "date_connect_expire" : "",
    "date_connect_accepted" : "",
    "status" : "",
    "date_last_interaction" : "",
    "found_by_script1" : "",
    "found_by_script2" : "",
    "found_by_script3" : "",
    "date_found_by_script1" : "",
    "date_found_by_script2" : "",
    "date_found_by_script3" : "",
    "script_status" : "",
    "sheetrange" : "",
    "created_at" : "",
    "uploaded" : false,
    "check_before_sending_msg" : false,
    "date_script_ran" : "",
    "time_diff" : "",
    "timezone" : ""
};

//====================================== FOUND BY SCRIPT ON SEARCH =======================

const employees = {
    "fullname" : "",
    "position" : "",
    "company" : "",
    "company_url" : "",
    "country" : "",
    "linkedin_url" : "",
    "email" : "",
    "phone" : "",
    "created_at" : "",
    "merged" : false
};

const companies = {
    "name" : "",
    "linkedin_url" : "",
    "technologies" : [],
    "employees_link" : "",
    "country" : "",
    "company_url" : "",
    "created_at" : "",
    "took_all_employees" : true,
    "description" : "",
    "sheet_range" : "",
    "sales_url" : "",
    "type" : "",
    "headquarters" : "",
    "industry" : "",
    "company_size" : "",
    "founded" : ""
};


//====================================== UPDATES EVERYDAY =======================
const inboxes = {
    "name" : "",
    "linkedin_url" : "",
    "time" : "",
    "date" : "",
    "message" : "",
    "email_sent" : false
};

const accepted_connect = {
    name: "",
    message: "",
    date: "",
    time: "",
};


//=============================SCRIPT COLLECTION=========================
const scripts = {
    pid: "",
    name: "",
    time_started: "",
    status: "",
    description: ""
};

module.exports = {
    scripts, old_oleg_contacts_old, new_oleg_contacts, generated_leads, mobile_leads, 
    sales_leads, leads, employees, companies, inboxes, accepted_connect
};