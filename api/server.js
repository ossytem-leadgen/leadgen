const express = require('express');
const bodyParser = require('body-parser');

const contacts = require('./routes/api/contacts');
const users = require('./routes/api/users');

const app = express();
const port = 5000;

// Bodyparser Middleware
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Welcome to leadgen');
});

app.use('/api/contacts', contacts);

app.use('/api/users', users);

app.listen(port, () => {
    console.log(`Leadgen is listening on port ${port}`);
});
