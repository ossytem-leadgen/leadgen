const express = require('express');
const isEmpty = require('lodash.isempty');
const Validator = require('validator');
const { CONSTANTS } = require('../../../helpers/constants');
const router = express.Router();

// @route GET api/users/login
// @route Login user
// @access Private
router.post('/login', async (req, res) => {
    const { password="" } = req.body;
    const cred = CONSTANTS.CREDENTIALS.LOGIN;

    // Check if password are corrects
    const { errors, isValid } = validateInput(req.body);

    if (!isValid) {
        res.status(400).json({success: false, ...errors});
    } else if (password !== cred.PWD) {
        res.status(400).json({success: false, message: 'Wrong authentication details'});
    } else {
        res.send({success: true});
    }
});

function validateInput(data) {
    const { password="" } = data;
    let errors = {};

    // if (Validator.isEmpty(email)) {
    //     errors.email = 'This field is required';
    // }

    // if (!Validator.isEmail(email)) {
    //     errors.email = 'Email is invalid';
    // }

    if (Validator.isEmpty(password)) {
        errors.password = 'This field is required';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}

module.exports = router;