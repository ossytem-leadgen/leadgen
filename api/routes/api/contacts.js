const express = require('express');

const router = express.Router();
const { getOlegContacts, updateOlegContacts, addOlegContact } = require('../../../Db/OlegContacts');

// @route GET api/contacts
// @route Get All Contacts
// @access Private
router.get('/', async (req, res) => {
    res.send('All your contacts')
});


module.exports = router;