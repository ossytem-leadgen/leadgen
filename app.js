'use strict';

const { log } = console;
const { sendMsgsToGl, sendGlConnectMsg, getGlAcceptedConnects, updateGlNotAcceptConnects } = require('./Controllers/generatedLeadsController');
const { sendSalesConnectMsg } = require('./Controllers/salesLeadsController');
const {killNodeListener} = require('./Controllers/botController');
const { date, time, emojis, sleep, closeConnectionOnExit } = require('./modules');
const { CONSTANTS, dbFields } = require('./helpers/constants');
const { sendAllMsgsToDeadLeads, sendFirstMsgToAllDeadLeads, errorHandler, getOlegLeadsProfile } = require('./Controllers/olegLeadsController');
const { getPeopleUrls, getPeopleCompanies } = require('./Controllers/peopleSearchController');
const { updateAcceptedConnectStats } = require('./Controllers/statisticsController');
const { monitorPage, watchForNewMessage, watchForAcceptedConnects, emailStatistics } = require('./Controllers/inboxController');
const { getOlegContacts } = require('./Db/OlegContacts');
const { addScripts } = require('./Db/Scripts');
const login = require('./Controllers/authorization');
const { checkIfChatted, getAcceptedConnections, getPendingConnects, sendMlMessages } = require('./Controllers/mobileLeadsController');
const getCompanyData = require('./Controllers/companyPagesController');
const { 
    WATCH_REPLIES, SEND_SALES_CONNECT, EMAIL_REPLIES, SEND_GENRATED_CONNECT, KILL_NODE_LISTENER,
    SEND_DEADLEADS_ONE_FOLLOWUP, GET_ACCEPTED_CONNECTS, SEND_DEADLEADS_ALL_FOLLOWUP, UPDATE_ACCEPTED_CONNECT_STATS,
    SEND_NEWLEADS_FOLLOWUP, SEARCH_BY_PEOPLE, MOBILE_LEADS, LOGIN, STAY_ON_HOME_PAGE, MESSAGE_MOBILE, GET_OLEG_LEAD_PROFILE,
    GET_COMPANY_DATA
} = CONSTANTS.SCRIPT_PROCESS;

const scriptRunner = async () => {
    log('[', date(),'||', time(), '] Ready? Lets go....', emojis.oneEye, '\n');

    const params = process.argv.slice(2).reduce((acc, item) => {
        const splitElements = item.split('=');

        acc[splitElements[0]] = Number(splitElements[1]);

        return acc;
    }, {});

    try {
        switch (params.SCRIPT) {
            case GET_ACCEPTED_CONNECTS:
                await recordScriptStarted('GET_ACCEPTED_CONNECTS');
                await getGlAcceptedConnects();
                break;
        
            case SEND_SALES_CONNECT:
                await recordScriptStarted('SEND_SALES_CONNECT');
                await sendSalesConnectMsg();
                break;
        
            case SEND_DEADLEADS_ALL_FOLLOWUP:
                await recordScriptStarted('SEND_DEADLEADS_ALL_FOLLOWUP');
                await sendAllMsgsToDeadLeads();
                break;
        
            case SEND_DEADLEADS_ONE_FOLLOWUP:
                await recordScriptStarted('SEND_DEADLEADS_ONE_FOLLOWUP');
                await sendFirstMsgToDeadLeads();
                break;
        
            case SEND_NEWLEADS_FOLLOWUP:
                await recordScriptStarted('SEND_NEWLEADS_FOLLOWUP');
                await sendMsgsToGl();
                break;
        
            case EMAIL_REPLIES:
                await recordScriptStarted('EMAIL_REPLIES');
                await emailStatistics();
                break;
        
            case WATCH_REPLIES:
                await recordScriptStarted('WATCH_REPLIES');
                await monitorPage(watchForNewMessage);
                break;
                
            case STAY_ON_HOME_PAGE:
                await recordScriptStarted('STAY_ON_HOME_PAGE');
                await stayOnHomePage();
                break;
           
            case SEARCH_BY_PEOPLE:
                await recordScriptStarted('SEARCH_BY_PEOPLE');
                await searchByPeople();
                break;
           
            case MOBILE_LEADS:
                await recordScriptStarted('MOBILE_LEADS');
                await getMobileLeads();
                break;

            case LOGIN:
                await recordScriptStarted('LOGIN');
                await login(CONSTANTS.CREDENTIALS.OLEG.NAME,
                            CONSTANTS.CREDENTIALS.OLEG.PWD);
                break;
           
            case SEND_GENRATED_CONNECT:
                await sendGeneratedConnect();
                break;

            case MESSAGE_MOBILE:
                await recordScriptStarted('MESSAGE_MOBILE');
                const num_msg_sent = Number(params.MSG);
                await sendMlMessages(num_msg_sent);
                break;

            case KILL_NODE_LISTENER:
                await recordScriptStarted('KILL_NODE_LISTENER');
                killNodeListener();
                break;

            case UPDATE_ACCEPTED_CONNECT_STATS:
                await updateAcceptedConnectStats();
                break;

            case GET_OLEG_LEAD_PROFILE:
                await recordScriptStarted('GET_OLEG_LEAD_PROFILE');
                await getOlegLeadsProfile();
                break;

            case GET_COMPANY_DATA:
                await recordScriptStarted('GET_COMPANY_DATA');
                await getCompanyData("acc1", false);
                break;
            default:
                log('No script was selected to run', emojis.sad);
        }
    } catch (error) {
        await errorHandler('', error, 'app.js', `Has to do with a script ${params.SCRIPT}`);
    }
};

const stayOnHomePage = async () => {
    const messageWatcher = [watchForNewMessage, WATCH_REPLIES];
    const acceptWatcher = [watchForAcceptedConnects, GET_ACCEPTED_CONNECTS];
    
    await monitorPage([messageWatcher, acceptWatcher]);
};

const sendFirstMsgToDeadLeads = async () => {
    const olegsLeads = await getOlegContacts({ sendMessage: true, num_msg_sent: 0 });
    
    while (olegsLeads.length) {
        console.log('Time to work, we have:',olegsLeads.length, 'number of leads');
        await sendFirstMsgToAllDeadLeads();

        log('Done with first iteration now let me sleep for 900 seconds')
        await sleep(900);
    }
};

const searchByPeople = async () => {
    log('\n\nGetting links to people');
    await getPeopleUrls();
    
    log('\n\nGotten people URLS, Sleeping for 20 seconds');
    await sleep(20);
    
    log('\n\nLets get urls to companies from each person');
    await getPeopleCompanies();

    log('\n\nGotten urls from companies, Sleeping for 20 seconds');
    await sleep(20);

    log('\n\nLets get Companies data');
    require('./Controllers/companyPagesController');

    log('\n\nGotten companies data, Sleeping for 20 seconds');
    await sleep(20);

    log('\n\nLets get employees data');
    require('./Controllers/employeePagesController');
};

const getMobileLeads = async () => {
    //Get all the connections in d pst week & save in db
    await getAcceptedConnections();
    await sleep(53.25);
    //Go through each each one and check if there was no message sent && add to sheet
    await checkIfChatted();
    await sleep(60);
    
    await getPendingConnects();
};

const sendGeneratedConnect = async () => {
    // Update not accepted
    // await updateGlNotAcceptConnects();
    // log('Sleeping for 267 seconds, then send connect');
    // await sleep(267);

    // // Send connects
    // await sendGlConnectMsg();
    // log('Sleeping for 1867 seconds, send follow ups');
    // await sleep(1867);
    
    await sendMsgsToGl();
};

const recordScriptStarted = async scriptName => {
    const {description, status} = CONSTANTS.DB.COLLECTIONS.SCRIPTS.FIELDS
    
    const scriptField = dbFields.scripts;

    scriptField.pid = process.pid;
    scriptField.name = scriptName;
    scriptField.time_started = `${date()} ${time()}`;
    scriptField.status = status.RUNNING;
    scriptField.description = description[scriptName] || "No description is available for this script running";
    

    await addScripts({...scriptField});
};

scriptRunner();

closeConnectionOnExit();
