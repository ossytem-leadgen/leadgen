'use strict';

const cheerio = require('cheerio');
const { log } = require('./common');

const repliedMsg = (html, leadname) => {
    const $ = cheerio.load(html);
    // let oleg = 'Oleh Sieroochenko';
    let replyStatus = {};
    let nameClass = 'li.msg-s-message-list__event .msg-s-message-group__name';
    let bodyClass = 'li.msg-s-message-list__event .msg-s-event-listitem__body';
    let len = $(nameClass).get().length;
    let names = [];
    let msgs = [];
  
    for (let i=0; i<len; i++) {
      let name = $(nameClass).eq(i).text();
      let msg = $(bodyClass).eq(i).text();
      
      console.log('msg ', i, ' Length: ', msg.length);

      names.push(name);
      msgs.push(msg);
    }

    if (!names.includes(leadname)) {

      log('\nNo reply yet');

      replyStatus.replied = false;

      return replyStatus;

    } else {
        let msgIndex;

        log('\nReplied');

        names.forEach((val, i) => {

            if (val == leadname) {
                msgIndex = i;
            } //if statement
        }); //forEach

        replyStatus.message = msgs[msgIndex];
        replyStatus.replied = true;

        return replyStatus;
    }
};

const seenMsg = html => {
    const $ = cheerio.load(html);
    let seen = false;
    let arr = $('li.msg-s-message-list__event').last().find($('.msg-s-event-listitem__seen-receipts')).html();

    if (!arr) {
        log('The user hasnt seen your message');

        return seen;
    } else {
        log('The user has seen your message');

        seen = true;

        return seen;
    }
};

const getOnlyBoss = position => {
    let positionFilter = ['CEO', 'CTO','COO', 'President','Vice-President', 'CIO','CBDO', 'CCO', 'CMO','Chief Marketing Officer','Sales manager','Marketing manager','Business development manager','Product marketing manager','General manager', 'Director','Board member', 'Co founder', 'Product manager', 'Chief Information Officer', 'Chief Executive Officer', 'Chief Technology Officer','Vice President', 'VP', 'Co-Founder', 'Head of Technology', 'Head of IT', 'Advisor', 'Founder', 'Head of Engineering', 'Director of Engineering', 'Director of IT', 'Director of Technology']
    let found = false
    positionFilter.some((elem) => {
        let regex = new RegExp(elem, 'i');
        if (regex.test(position)){
            return found = true;
        }
    });
    
    return found;
};

module.exports = {
    repliedMsg, seenMsg, getOnlyBoss
};