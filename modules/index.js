'use strict';

const { repliedMsg, seenMsg, getOnlyBoss } = require('./messages');
const { date, time, increaseDay, reduceDay } = require('./dateAndTime');
const sheetAuthentication = require('./authSheets');
const mailAuthentication = require('./authMail');
const getTimeDiffAndTimeZone = require('./timezone');
const { len, isNumber, genRandNum, getAuthenticatedSheet, handleUncaughtError, arrayIncludesInObj,
    closeConnectionOnExit, emojis, log, testIfNameHasDot, testIfNameIsWord, sleep, checkIfUrlIsValidCompany, convertCsvToJson } = require('./common');
const { pageScroller, errorHandler, closeAndWait, getCookie, getBrowser } = require('./puppeteer');
const { statisticsToSheet, drawTableBorder, getNextColumnNum, getNextAlphRange } = require('./spreadsheet');


module.exports = {
    len, log, time, date, sleep, emojis, seenMsg, getTimeDiffAndTimeZone, closeAndWait,
    reduceDay, repliedMsg, genRandNum, increaseDay, getOnlyBoss, isNumber,
    pageScroller, testIfNameHasDot, testIfNameIsWord, sheetAuthentication, errorHandler,
    closeConnectionOnExit, handleUncaughtError, getCookie, getBrowser, getAuthenticatedSheet,
    statisticsToSheet, drawTableBorder, getNextColumnNum, getNextAlphRange, mailAuthentication,
    checkIfUrlIsValidCompany, convertCsvToJson, arrayIncludesInObj
};