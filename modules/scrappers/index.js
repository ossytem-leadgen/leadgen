const fs = require('fs');
const cheerio = require('cheerio');
const {time, isNumber, date, reduceDay, getOnlyBoss, log, len } = require('../index');
const { URLS } = require('../../helpers/constants');
const { getRecruitingCompany } = require('../../Db/RecruitingCompanies');
const linkedin = URLS.home;

/* ------------------------THIS IS THE 1ST FUNCTION --------------------------------*/
const getUrlFromSearchRes = (searchResFile) => {
    let titleAndCompanyUrl = [];

    /* Read search result from file */
    try {
        // let html = fs.readFileSync(searchResFile, 'utf8'); 
            
        const $ = cheerio.load(searchResFile);

        /* Array of Job title from html page */
        // let jobTitle = []
        // $('.job-card-search__title').each((i, elem) => {
        //     let res = $(elem).attr('class', '.lt-line-clamp .lt-line-clamp--multi-line .ember-view').text()
        //     jobTitle[i] = res.trim()
        // })
        // jobTitle.join(', ')

        /* Array of Company Names from search result */
        let companyName = []
        $('.job-card-search__company-name').each(function(i, elem) {
            companyName[i] = $(elem).text()
        });
        companyName.join(', '); 

        /* Array of Company Urls from search result */
        let companyUrl = []
        $('.job-card-search__company-name-link').each(function(i, elem) {
            companyUrl[i] = `${linkedin}${$(elem).attr('href')}`
        });
        companyUrl.join(', '); 

        let nextText = $('.next-text').text().trim();


        
        /* Return an array of title and url */
        if (companyName.length === companyUrl.length) {
            let notCmpanyUrl = 'https://www.linkedin.com#';
            for(let i = 0; i < companyName.length; i++){
                //if company has already been added to array then don't add to it !titleAndCompanyUrl.includes(companyName[i])
                if(companyUrl[i] !== notCmpanyUrl){
                    titleAndCompanyUrl.push({
                        // "title" : jobTitle[i],
                        "company_name" : companyName[i],
                        "company_url" : companyUrl[i]
                    });
                }
            }
            // Check if it is recruitment company from its name 
            titleAndCompanyUrl = titleAndCompanyUrl
                .filter( (elem) => {
                    return recruimentFilterFunc(elem.company_name) === false;
                });
            //Remove duplicate data in array
            titleAndCompanyUrl = removeDuplicate(titleAndCompanyUrl)

        } else {
            console.log('The job title and company url do not match. Please check getUrlFromSearchRes()')
            console.log('companyName',companyName)
            console.log('companyUrl',companyUrl)
        }
    } catch(err){
        console.log('Error reading file in getUrlFromSearchRes()', err)
    }  
    return titleAndCompanyUrl;
};

const removeDuplicate = (arr) => {
    let names= [], 
        urls= [], 
        unique_names= [], 
        unique_urls= [], 
        result = [];

    arr.filter(function(elem) {
        names.push(elem.company_name)
    });

    arr.filter(function(elem) {
        urls.push(elem.company_url)
    });

    names.filter(function(elem, index, self) {
        if(index == self.indexOf(elem)) unique_names.push(elem);
    });

    urls.filter(function(elem, index, self) {
        if(index == self.indexOf(elem)) unique_urls.push(elem);
    });

    for(let i = 0; i < unique_names.length; i++){
        result.push({
            "company_name" : unique_names[i],
            "company_url" : unique_urls[i]
        });
    }
    return result;
};

/* ------------------------THIS IS THE 2ND FUNCTION --------------------------------*/
const checkIfRecruiting = (companyHtmlPage) => {
    //try catch for readFileSync
    let ifRecruiting = false;
    try{   
        // let html = fs.readFileSync(companyHtmlPage, 'utf8');
        const $ = cheerio.load(companyHtmlPage);

        // Get the company name 
        let name = $('.org-top-card-module__name').text().trim()
        const checkDbIfRecruitingExists = async (name) => {
            let ifRecruitingComp = await getRecruitingCompany({ name: name });
            if (ifRecruitingComp) {
                ifRecruiting = true;
                return ifRecruiting;
            }
        }
        checkDbIfRecruitingExists(name)
        // Get industry
        let industry = $('.company-industries').text().trim()

        // Get the about us text
        let about = $('.org-about-us-organization-description').text().trim()
        
        //Get the specialties text
        let specialty = $('.org-about-company-module__specialities').text().trim()
        
        // Check if recruting exisits
        let aboutChecker = recruimentFilterFunc(about);
        let nameChecker = recruimentFilterFunc(name);
        let industryChecker = recruimentFilterFunc(industry);
        let specialtyChecker = recruimentFilterFunc(specialty);
        if(aboutChecker == true || specialtyChecker == true || industryChecker == true || nameChecker == true){
            ifRecruiting = true;
        }
              
    } catch(err){
        console.log('Error reading file in checkIfRecruiting()', err)
    }
    return ifRecruiting; 
};

const recruimentFilterFunc = (text) => {
    let filterWord = ['recruitment','recruiting', 'recruit', 'recruiters', 'recruiter', 'recruits']
    let found = false
    filterWord.some((elem) => {
        let regex = new RegExp(elem, 'i');
        if (regex.test(text)){
            return found = true;
        }
    })
    return found;
};

/* ------------------------THIS IS THE 3RD FUNCTION --------------------------------*/
const getCompanyPageData = (companyHtmlPage, linkedin_url) => {
    let company = {};

    try {   
        // const html = fs.readFileSync(process.cwd() + companyHtmlPage, 'utf8');
        const $ = cheerio.load(companyHtmlPage);
        
        // Get company name 
        company.name = $('h1.org-top-card-primary-content__title span').text().trim();
        
        company.linkedin_url = linkedin_url;

        // Loop through the overview section because they have the say class name
        // Which are d values of: Website, Industry, Company size, Headquarters, Type, founded, specialties
        const companyValues = [];

        $('.org-page-details__definition-text').each((i, el) => {
            const value = $(el).text().trim();

            companyValues.push(value);
        });

        $('.org-page-details__definition-term').each((i, el) => {
            let key = $(el).text().trim().toLowerCase().split(' ').join('_');
            
            if (key.includes("specialties")) {
                key = "technologies";

            }
            
            if (key.includes("website")) {
                key = "company_url";

            }

            company[key] = companyValues[i];
        });
            
        const employees_url = $('.org-top-card__right-col a').attr('href')
        company.employees_link = employees_url ? `${linkedin}${employees_url}` : null;

        company.sales_url = "";
        
        company.country = $('div.org-top-card-primary-content__info-item org-top-card-primary-content__headquarter').text().trim()
    
        company.created_at = date();

        company.took_all_employees =  true;

        company.description = $('.container-with-shadow p.white-space-pre-wrap').text().trim();
  
    } catch(err) {
        console.log('Error reading file in checkIfRecruiting()', err)
    }

    return company;
};

/* ------------------------THIS IS THE 4TH FUNCTION --------------------------------*/
const getAllEmployeeList = (employeesHtmlPage) => {
    const empSearchData = [];

    try {   
        // let html = fs.readFileSync(employeesHtmlPage, 'utf8');
        const $ = cheerio.load(employeesHtmlPage);
        const pagination = $('ol.results-paginator li.page-list ol li').last().find('button').text();

        //Get name, employee_url, position and country
        $('.search-result__info').each((i, el) => {
            const name = $(el)
                .find('.actor-name')
                .text()
                .replace(/\s\s+/g, '');

            let url = $(el)
                .find('a')
                .attr('href');
            let employee_url = url ? `${linkedin}${url}` : 'null'
            

            let position = $(el)
                .find('.subline-level-1')
                .text()
                .replace(/\s\s+/g, '');

            let country = $(el)
                .find('.subline-level-2')
                .text()
                .replace(/\s\s+/g, '');

            
            if (getOnlyBoss(position)){
                empSearchData.push({
                    "name" : name,
                    "employee_url" : employee_url,
                    "position" : position,
                    "country" : country
                });
            } 
        });

        return [empSearchData, pagination];
    } catch(err){
        console.log('Error reading file in getAllEmployeeList()', err)
    }
};

/* ------------------------THIS IS THE 5TH FUNCTION --------------------------------*/
const getAllConnections = (mobileLeadFields, days = 0) => {
    const { page, batch, week } = mobileLeadFields || { page: null, batch: "", week: "" };

    if (page === null) {
        return [];
    }

    const leadAndConnectTime = [];

    try {
        // const html = fs.readFileSync(process.cwd() + page, 'utf8');
        const $ = cheerio.load(page);

        // Array of  names
        const leadName = [];
        $('.mn-connection-card__name').each(function(i, elem) {
            leadName[i] = $(elem).text().trim()
        });
        leadName.join(', '); 

        // Array of leads urls
        const linkedin_url = [];
        $('.mn-connection-card__details a.mn-connection-card__link').each(function(i, elem) {
            linkedin_url[i] = `${linkedin}${$(elem).attr('href')}`;
        });
        linkedin_url.join(', '); 

        /* Array of the time of connection */
        const timeConnected = [];
        $('.time-badge').each(function(i, elem) {
            const time = $(elem).text().trim();
            //Normally it looks like this Connected 2 days ago 
            //but we are removing ~Connected and ~ago so the result is 2 days
            //this will help us work with the specific amount of days
            timeConnected[i] = time.replace(/(Connected\s|ago)/g, "").trim()
        });
        timeConnected.join(', ');

        // Array of positions
        const position = [];
        $('.mn-connection-card__occupation').each(function(i, elem) {
            position[i] = $(elem).text().trim()
        });
        position.join(', '); 

        if (leadName.length === timeConnected.length) {
            for (let i = 0; i < leadName.length; i++) {
                const { connected_date, connected_time } = choosenTimeRange(timeConnected[i], days);

                if (connected_date) {
                    leadAndConnectTime.push({
                        lead_name: leadName[i],
                        linkedin_url: linkedin_url[i],
                        position: position[i] || "",
                        connected_time,
                        connected_date,
                        connected: true,
                        chatted: false,
                        checked: false,
                        batch, week
                    }); 
                }
            }

            return leadAndConnectTime;
        } else {
            console.log('The connections name and their timing don"t match');
        }
        
    } catch(err) {
        console.log('Error while getting all connections getAllCollections()', err)
    }
};

const choosenTimeRange = (time, daysAgo) => {
    const todayArr = ["seconds", "second", "minute", "minutes", "hour", "hours"];
    const connectedDaysAgo = [];

    let connected_date = null;
    let connected_time = null;
    
    if (todayArr.includes(time.replace(/\d/g, "").trim())) {
        connected_date = date();
        connected_time = getConnectedTime(time);
    }

    if (daysAgo) {
        for (let i = 1; i <= daysAgo; i++) {
            const connectedDayAgo = i < 8 ? (i === 1 ? '1 day' : `${i} days`) : "1 week";
            connectedDaysAgo.push(connectedDayAgo);
        }

        if (len(connectedDaysAgo) && connectedDaysAgo.includes(time)) {
            const index = connectedDaysAgo.indexOf(time) + 1;
            connected_date = reduceDay(index);
            connected_time = "14:00:00";
        }
    }

    const timeRange = { connected_date, connected_time };

    return timeRange;
};

const getConnectedTime = rawTimeTxt => {
    const timeUnits = [
        { str: "second", num: 1000}, 
        { str: "minute", num: 60000}, 
        {str: "hour", num: 3600000}
    ];

    let connectedTime = "";

    for (const timeUnit of timeUnits) {
        const { str, num } = timeUnit;
        const timeTest = RegExp(str).test(rawTimeTxt);

        if (timeTest) {
            //const [1, hour] = ("1 hour").split(' ');
            const [timeAgo, ...rest] = rawTimeTxt.split(' ');
            const numOfTimeBack = Number(timeAgo);

            if (isNumber(numOfTimeBack)) {
                const dateAgo = new Date(Date.now() - timeAgo * num);
                connectedTime = time("", "", dateAgo);
            }
        }
    }

    return connectedTime;
};

const scrapPeopleUrls = (peopleHtmlPage) => {
    const peopleUrls = []
    try{   
        // let html = fs.readFileSync(peopleHtmlPage, 'utf8');
        const $ = cheerio.load(peopleHtmlPage);

        //Get name, employee_url, position and country
        $('.search-result__info').each((i, el) => {
            let url = $(el)
                .find('a')
                .attr('href');
            let personUrl = url ? `${linkedin}${url}` : 'null';
            peopleUrls.push(personUrl);
        });
        
        return peopleUrls;
    } catch(err){
        console.log('Error reading file in getAllEmployeeList()', err)
    }
};

module.exports = {
    getUrlFromSearchRes, checkIfRecruiting, getCompanyPageData,
    getAllEmployeeList, getAllConnections, scrapPeopleUrls
};
