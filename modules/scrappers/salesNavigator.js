const cheerio = require('cheerio');
const fs = require('fs');
const { getOnlyBoss, getTimeDiffAndTimeZone } = require('../index');
const { URLS } = require('../../helpers/constants');
const linkedin = URLS.home;

const search = '/resources/pages/salesNavigator/search_navigator_search.html';
const company = '/resources/pages/salesNavigator/search_navigator_company2.html'
const employees = '/resources/pages/salesNavigator/search_navigator_employee.html';

const getCompanyLinks = (htmlPage) => {
    // const html = fs.readFileSync(process.cwd() + htmlPage, 'utf8');
    const $ = cheerio.load(htmlPage);

    let companyUrls = [];

    $('.result-lockup .result-lockup__name').each((i, elem) => {
        const url = $(elem).find('a').attr('href');
        const companyUrl = url ? `${linkedin}${url}` : 'null';
        companyUrls.push(companyUrl);
    });
    
    return companyUrls;
}


const getCompanyData = (htmlPage) => {
    // const html = fs.readFileSync(process.cwd() + htmlPage, 'utf8');
    const $ = cheerio.load(htmlPage);

    //Get company name 
    const nameArr = $('.content-container h1').text().trim().split('  ');
    const name = nameArr[nameArr.length - 1].trim()

    //Get company country
    const countryArr = $('.topcard-hovercard-meta-links .hovercard a').text().trim().split('  ');
    const country = countryArr[countryArr.length - 1];

    //Get company website
    const company_url = $('#hovercard-web-link a').attr('href');

    // Get company employees_url
    const url = $('.content-container .cta-link a').attr('href');
    const employees_link = url ? `${linkedin}${url}` : 'null';

    // Get the about us text
    const description = $('artdeco-modal-content p').text().trim();

    const companyData = {
        name, employees_link, country,
        company_url, description
    };

    return companyData;
}


const getEmployeeData = htmlPage => {
    // const html = fs.readFileSync(process.cwd() + htmlPage, 'utf8');
    const $ = cheerio.load(htmlPage);
    const empSearchData = [];

    const pagination = $('ol.search-results__pagination-list li').last().text();
    
    $('ol.search-results__result-list li').each((i, el) => {
        const name = $(el)
            .find('.result-lockup__name a')
            .text().trim();

        if (name) {
    
            const url = $(el)
                .find('.result-lockup__name a')
                .attr('href');
    
                const employee_url = url ? `${linkedin}${url}` : 'null';
        
    
            let position = $(el)
                .find('.result-lockup__highlight-keyword')
                .text()
                .replace(/\n+/g, '').trim().split('   ');
            position = position.filter(p => p.length);
            position = position[0].trim();

            const country = $(el)
                .find('.result-lockup__misc-item')
                .text()
                .replace(/\s\s+/g, '');

            const { time_diff, timezone } = getTimeDiffAndTimeZone(country);
            
            if (getOnlyBoss(position)) {
                empSearchData.push({
                    name, employee_url,
                    position, country, 
                    timezone, time_diff
                });
            } 
        }
    });

    const employeeObj = {
        pagination: pagination ? Number(pagination) : 0,
        employees: empSearchData
    };
    
    return employeeObj;
};


module.exports = {
    getCompanyLinks: getCompanyLinks,
    getCompanyData: getCompanyData,
    getEmployeeData: getEmployeeData
}