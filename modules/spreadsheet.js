


const {google} = require('googleapis');
const { emojis, log } = require('./common');
const sheetAuthentication = require('./authSheets');
const { 
    CONSTANTS: { SPREAD_SHEET_PARAMS: { LEADGEN_STATISTICS: { ID, STATS_SHEETS  } }, ALPHABETS },
    } = require('../helpers/constants');

const leadgen_spreadsheet_id = ID;

const statisticsToSheet = async (range, values) => {
    return new Promise((res, rej) => {
        sheetAuthentication(auth => {
            const sheets = google.sheets({ version: 'v4', auth });
            
            sheets.spreadsheets.values.batchUpdate({
                spreadsheetId: leadgen_spreadsheet_id,
                resource: { 
                    valueInputOption: 'USER_ENTERED',
                    "data": [
                        {
                            range: range,
                            majorDimension: "COLUMNS",
                            values: [values]
                        }
                    ]
                }
            }, async (err, result) => {
                if (err) rej('companyspreadsheet The API returned an error: ' + err);
                res('Pushed into sheet');
            });
        });
    });
};

const drawTableBorder = async (sheetId, startColumnIndex, endColumnIndex) => {
    return new Promise((resolve, reject) => {
        sheetAuthentication(auth => {
            const sheets = google.sheets({ version: 'v4', auth });
            const number = 0;
            const requests = [];
    
            requests.push({
                "updateBorders": {
                    "range": {
                    "sheetId": sheetId,
                    "startRowIndex": 0,
                    "endRowIndex": 4,
                    "startColumnIndex": startColumnIndex,
                    "endColumnIndex": endColumnIndex,
                    },
                    "top": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    },
                    "bottom": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    },
                    "left": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    },
                    "right": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    },
                    "innerHorizontal": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    },
                    "innerVertical": {
                    "style": "SOLID",
                    "width": 1,
                    "color": {
                        "red": number,
                        "green": number,
                        "blue": number,
                    },
                    }
                }
            });
    
            sheets.spreadsheets.batchUpdate({
                spreadsheetId: leadgen_spreadsheet_id,
                resource: {requests},
            }, (err, res) => {
                if (err) {
                    console.log(err);
                    rej(err);
                } else {
                    log('The table was successfully drawn on the sheet', emojis.coolGlasses);
                    log(res.data);
                    resolve('');
                }
            });

        }); 
    });
};

const getNextColumnNum = sheetName => {
    return new Promise((res, rej) => {
        sheetAuthentication(auth => {
            const sheets = google.sheets({ version: 'v4', auth });
            sheets.spreadsheets.values.get({
                spreadsheetId: leadgen_spreadsheet_id,
                range: sheetName,
            }, (err, result) => {
                if (err) rej('The API returned an error: ' + err);
                const rows = result.data.values;
                const obj = { start: rows[0].length, end: rows[0].length + 1};
                res(obj);
            });
        }); 
    });
};

const getNextAlphRange = columnNo => {
    const upperCaseAlp = ALPHABETS.UPPERCASE;
    let nextCol = '';

    if (columnNo >= 1 && columnNo <= 26) {
        nextCol = '!' + upperCaseAlp[columnNo] + '1';
    } else if (columnNo >= 27 && columnNo <= 52) {
        nextCol = '!A' + upperCaseAlp[columnNo] + '1';
    } else if (columnNo >= 53 && columnNo <= 78) {
        nextCol = '!B' + upperCaseAlp[columnNo] + '1';
    } else if (columnNo >= 79 && columnNo <= 104) {
        nextCol = '!C' + upperCaseAlp[columnNo] + '1';
    }
    
    return nextCol;
};

module.exports = {
    statisticsToSheet, drawTableBorder, getNextColumnNum, getNextAlphRange
};