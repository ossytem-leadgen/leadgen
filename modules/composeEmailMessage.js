
/**
 * @param {array} messages Array of inbox messages
 * @param {number} yesterdaysAcceptedConnects No of accepted connects yesterday
 * 
 * const messages = [{name: "",linkedin_url: "",date: "",message: ""}];
 */
const inboxMsgsTemplate = (messages, yesterdaysAcceptedConnects) => {
    let connects = "";

    if (!yesterdaysAcceptedConnects) {
        connects = '</table><br><h2>Side note: Unfortunately no one accepted our connects yesterday, I am sure they will today. 😉</h2>'
    } else {
        connects = `</table><br><h2>Side note: ${yesterdaysAcceptedConnects} people accepted our connects yesterday. 🎉</h2>`
    }

    const style = {
        body: "font-family:Roboto,Arial,Helvetica,sans-serif;font-size: 1em;line-height: 1.3;font-style: normal; font-weight: 300;",
        table: "border-collapse: collapse; width: 70%; margin: 1rem auto;",
        p: "padding-bottom: 10px;",
        th: "height: 30px;border-bottom: 1px solid #ddd;text-align: center;word-wrap:break-word; overflow-wrap:break-word;background-color: #DB4E00;color: white;text-align: center;",
        td: "border-bottom: 1px solid #ddd;text-align: center;word-wrap:break-word; overflow-wrap:break-word;width: 90%;padding-bottom: 20px;",
        button: "-webkit-appearance: button;-moz-appearance: button;appearance: button;text-decoration: none;color: initial;display: inline-block;padding: 6px 6px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;background-color: #DB4E00;border-color: #DB4E00;"
    };

    const header = `<html><body style="${style.body}">
                    <h2>Good morning Boss, I got some new messages for you 📝</h2> <table style="${style.table}">
                    <tr>
                        <th style="${style.th}"><h3>Messages</h3></th>
                    </tr>`;
    
    let body = '';

    if (Array.isArray(messages) && messages.length) {
        for (let inbox of messages) {
            const {name, message, linkedin_url } = inbox;
            
            body += `<tr>
                        <td style="${style.td}"><h3>${name}</h3><p style="${style.p}">${message}</p><a href="${linkedin_url}" style="${style.button}" target="_blank">Reply ${name.split(' ')[0]} Now</a></td>
                    </tr>`;
        }
    }

    const footer = `<p>Sincerely,</p><p><strong>LeadGen Script. 🙂</strong></p></body></html>`;

    const email = header.concat(body, connects, footer);

    return email;
}

/**
 * @param {Object} dateObj Contains an object with day and full_date fields
 * const dateObj = {day: "Monday", full_date: "12.11.2018" } 
 */
const titleForInboxMsgs = dateObj => {
    return `LI Responses ${dateObj.day} ${dateObj.full_date}`;
}

/**
 * Template for email when an error occurs in the script
 * @param {String} errorMsg The error message from Node
 * @param {String} errorLocation Location of where the error occured
 */
const composeErrorMessage = (errorMsg, errorLocation, extraInfo) => {
    return `<html><body style="font-family:'Lato','Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 17px;color: #202124;line-height: 1.3;"><p><strong>There is an error in ${errorLocation}. The error message is:</strong></p><div style="border: 1px solid #a0a0a2; border-radius: 8px; padding: 40px; text-align:center; border-collapse: separate;"><strong>${errorMsg}</strong></div><p>Extra Information about error: ${extraInfo}</p><br><p>Sincerely,</p><p><strong>LeadGen Script. 🙂</strong></p></body></html>`
}

module.exports = {
    composeErrorMessage,
    inboxMsgsTemplate,
    titleForInboxMsgs
}