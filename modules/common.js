'use strict';

const { google } = require('googleapis');
const csv = require('csvtojson');
const jsonfile = require('jsonfile');
const path = require('path');
const dbConnection = require('../helpers/dbConnection');
const { sendMsgToAdmin } = require('../Controllers/botController');
const emojiLogger = require('console-emoji');
const { date, time } = require('./dateAndTime');
const sheetAuthentication = require('./authSheets');

/**
 * 
 * Calculate the length of a based in value
 * 
 * @param {Any} n Any type of value
 */
const len = n => n.length;

/**
 * 
 * Check if the passed value is a number
 * 
 * @param {Number} n Any number
 * @returns {Boolean}
 */
const isNumber = n => !isNaN(parseFloat(n)) && isFinite(n);

/**
 * 
 * Check if the given url is a valid company url on Linkedin
 * 
 * @param {URL} link Company url
 * @returns {Boolean}
 */
const checkIfUrlIsValidCompany = link => link.match(/company/g);


/**
 * Generate a random number from a range of 2 numbers
 * @param {Number} min The minimum number for the random number
 * @param {Number} max The maximum number for the random number
 */
const genRandNum = (min, max) => Math.floor(Math.random() * (1 + max - min)) + min;


/**
 * 
 * Authenticate the spreadsheet
 * 
 * @returns {Autheticated Sheet}
 */
const getAuthenticatedSheet = _ => {
    return new Promise(res => {
      sheetAuthentication(auth => res(google.sheets({version: 'v4', auth})));
    });
};


const emojis = {
    'smile' : '🙂',
    'sad' : '😞',
    'coolGlasses' : '😎',
    'oneEye' : '😉',
    'typing' : '📝',
    'help' : '🔑',
    'byAlphabet' : '🔤',
    'search' : '🔎',
    'synonym' : '📚',
    'chat' : '🗣👂',
    'fingerRight' : '👉',
    'save' : '💾',
    'fingerDown' : '👇',
    'byNumber' : '🔢'
};


/**
 * Easiest way to log to the console instead of writing console.log()
 * @param {Any} args Any argument of your choice but should be seperated by a comma
 */
const log = (...args) => {
    let logRes = `${emojis.fingerRight} [${date()} || ${time()}] -`;
    args.forEach(arg => {
        if (typeof arg === 'object') console.log(`${emojis.fingerRight} [${date()} || ${time()}] -`, arg);
        else if (typeof arg === 'array') console.log(`${emojis.fingerRight} [${date()} || ${time()}] -`, arg);
        else if (typeof arg === 'function') console.log(`${emojis.fingerRight} [${date()} || ${time()}] -`, arg);
        else logRes += arg;
    });
    emojiLogger(logRes, 'ok');
};

/**
 * Check if there is a dot in the leads firstname
 * @param {String} name The name of the lead
 * Like this `R. Michael Hendrix`
 */
const testIfNameHasDot = name => new RegExp("\\.").test(name);

/**
 * 
 * Check if firstname is a letter
 * 
 * @param {String} name The name of the lead
 * let name = `☁️ Greg Kelleher ☁️` // false 
 * @returns {Boolean}
 */
const testIfNameIsWord = name => new RegExp("[а-я А-Я A-Z a-z]").test(name);

/**
 * 
 * Make script sleep for x amount of seconds
 * 
 * @param {Number} sec Seconds to pause
 */
const sleep = sec => new Promise(res => setTimeout(res, (sec * 1000)));

/**
 * 
 * Close connection on error
 *
 * @param {*} _ 
 * @returns {Null}
 */
const closeConnectionOnExit = _ => {
    [`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `SIGTERM`].forEach(eventType => {
        process.on(eventType, async () => {
            const pid = process.pid;

            await sendMsgToAdmin(`Closing a process ${pid}`);

            dbConnection.close();
            process.exit(1);
        });
    });

    return true;
};

const handleUncaughtError = errParams => {
    process.on('uncaughtException', async error => {
        const { errorLocation, extraInfo } = errParams || {errorLocation: '', extraInfo: ''};

        await sendMsgToAdmin(`An Uncaught error occured: ${error}, in ${errorLocation}.\nMore info: ${extraInfo}`);
        // await errorHandler('', error, errorLocation, extraInfo);
        process.exit();
    });
};

/**
 * Convert connections csv file to json file
 */
const convertCsvToJson = () => {
    const csv_path = path.format({
        root: '/ignored',
        dir: `${process.cwd()}/resources/files`,
        base: 'connections.csv'
    });

    const jsonPath = path.format({
        root: '/ignored',
        dir: `${process.cwd()}/resources/files`,
        base: 'connections.json'
    });

    csv()
    .fromFile(csv_path)
    .then( (jsonObj) => {
        jsonfile.writeFile(jsonPath, jsonObj, function (err) { 
           if (err) console.error(err); 
        });
    });
};

/**
 * 
 * Check if array of objects contains value of a key
 * 
 * @param {array} arr Array of objects
 * @param {string} key Key of object to check in
 * @param {Any} valueToCheck Value of the key to check for
 */
const arrayIncludesInObj = (arr, key, valueToCheck) => {
    let found = false;
  
    arr.some(value => {
      if (value[key] === valueToCheck) {
        found = true;
        return true; // this will break the loop once found
      }
    });
  
    return found;
};

module.exports = {
    len, isNumber, genRandNum, getAuthenticatedSheet, closeConnectionOnExit, convertCsvToJson, arrayIncludesInObj,
    emojis, log, testIfNameHasDot, testIfNameIsWord, sleep, handleUncaughtError, checkIfUrlIsValidCompany
};