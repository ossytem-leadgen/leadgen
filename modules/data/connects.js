﻿exports.connects = [
  {
    "firstname": "Zi",
    "lastname": "Zhang",
    "email": "",
    "company": "Center for Health Information and Analysis",
    "position": "Research Director",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Leon",
    "lastname": "Ormes",
    "email": "",
    "company": "Tessian",
    "position": "Node Developer",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Irina",
    "lastname": "Derkach",
    "email": "",
    "company": "Texode",
    "position": "HRM",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Mark",
    "lastname": "Mckeeganey",
    "email": "",
    "company": "Only Solutions Digital",
    "position": "Director/Senior Consultant",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Christoffer",
    "lastname": "Sandell",
    "email": "",
    "company": "Inrego AB",
    "position": "CEO - Inrego Global",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Terry",
    "lastname": "Phillips",
    "email": "",
    "company": "NextCrew",
    "position": "CTO",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Maximilian",
    "lastname": "Gerer",
    "email": "",
    "company": "e-bot7 GmbH",
    "position": "Chief Technology Officer / Co-Founder",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Rajesh",
    "lastname": "Thakrar",
    "email": "",
    "company": "OliveJar Digital",
    "position": "Co-Founder Director",
    "connect_date": "16 Jan 2019"
  },
  {
    "firstname": "Utsab",
    "lastname": "Wagle",
    "email": "",
    "company": "Outix Group",
    "position": "Co Founder/ Chief Technology Officer",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Dr Debra",
    "lastname": "Williams",
    "email": "",
    "company": "Motokiki",
    "position": "CEO",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Roman",
    "lastname": "Svichar",
    "email": "",
    "company": "Vidazoo",
    "position": "CTO",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Andrew",
    "lastname": "O'Harney",
    "email": "",
    "company": "Context Scout",
    "position": "CTO and Co-Founder",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Carlo",
    "lastname": "Szelinsky",
    "email": "",
    "company": "adjoe GmbH",
    "position": "CTO & Co-Founder",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Jonas",
    "lastname": "Karlsson",
    "email": "",
    "company": "Inrego AB",
    "position": "CEO",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Georg",
    "lastname": "Torjusen",
    "email": "",
    "company": "Con Moto",
    "position": "CEO and Partner",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Anvarzhon",
    "lastname": "Zhurajev",
    "email": "",
    "company": "University of Zurich",
    "position": "Software Engineer",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Nikolai",
    "lastname": "Korniyuk, MPA",
    "email": "",
    "company": "RigER: Digital Oilfield Platform for Energy Service and Equipment Rentals",
    "position": "VP of Marketing and Business Development",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Lars A.",
    "lastname": "Rottweiler",
    "email": "",
    "company": "Mbanq",
    "position": "CTO | Co-founder",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Carl",
    "lastname": "Neuberger",
    "email": "",
    "company": "Merchants Fleet Management",
    "position": "Director -Business Development",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Bob",
    "lastname": "Gregor",
    "email": "",
    "company": "Quick Base",
    "position": "Software Engineering Manager",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Anastasiya",
    "lastname": "Zdzitavetskaya",
    "email": "",
    "company": "Salesforce",
    "position": "Technical Advisor, Customer Engagement Strategy and AI Solutions",
    "connect_date": "15 Jan 2019"
  },
  {
    "firstname": "Mike",
    "lastname": "Kronenberg",
    "email": "",
    "company": "AutoLoop",
    "position": "CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Simon Niebuhr",
    "lastname": "Agerskov",
    "email": "",
    "company": "Academondo",
    "position": "Co-Founder & CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Torsten",
    "lastname": "Stein",
    "email": "",
    "company": "authentic.network",
    "position": "CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Judy",
    "lastname": "Lewis",
    "email": "",
    "company": "Raytheon",
    "position": "Vice President Of Business Development, Integrated Defense Systems Headquarters",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Henry",
    "lastname": "Rodriguez",
    "email": "",
    "company": "Hensoft Corp",
    "position": "CEO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Oleksandr",
    "lastname": "Shamotii",
    "email": "",
    "company": "Ringostat",
    "position": "CCO Assistant",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Stuart",
    "lastname": "McCamley",
    "email": "",
    "company": "ITrex Web Services Ltd",
    "position": "Founder",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Jones",
    "lastname": "Aktan",
    "email": "",
    "company": "Business Elements",
    "position": "Co-founder, Managing Partner, COO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Igor",
    "lastname": "Zalutski",
    "email": "",
    "company": "Tictrac",
    "position": "Head Of Engineering / CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Suresh",
    "lastname": "Shinde",
    "email": "",
    "company": "Allure Software, Inc",
    "position": "CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Stefan",
    "lastname": "Tröhler",
    "email": "",
    "company": "bs4y (business solutions for you",
    "position": "Outsourcing Projekt, Remote Project Manager, CTO der Firma bs4y",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Torsten",
    "lastname": "Meister",
    "email": "",
    "company": "Touchless Biometric Systems AG",
    "position": "CTO",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Jessie",
    "lastname": "Rucker",
    "email": "",
    "company": "Self Employed",
    "position": "Independent Consultant",
    "connect_date": "14 Jan 2019"
  },
  {
    "firstname": "Victoria",
    "lastname": "Chen",
    "email": "",
    "company": "Pragmatic Ventures",
    "position": "Technical Recruiter",
    "connect_date": "13 Jan 2019"
  },
  {
    "firstname": "Andrew",
    "lastname": "Thomas",
    "email": "",
    "company": "atlastix.io",
    "position": "Co-founder and CEO",
    "connect_date": "13 Jan 2019"
  },
  {
    "firstname": "Susan",
    "lastname": "Glynn",
    "email": "",
    "company": "Arbella Insurance Group",
    "position": "Director, Strategic Initiatives",
    "connect_date": "12 Jan 2019"
  },
  {
    "firstname": "David",
    "lastname": "Dawson",
    "email": "",
    "company": "Kohab",
    "position": "Founder & CEO",
    "connect_date": "12 Jan 2019"
  },
  {
    "firstname": "Milos",
    "lastname": "Rakic",
    "email": "",
    "company": "Autoklose",
    "position": "Sales Executive",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Ben",
    "lastname": "Curren",
    "email": "",
    "company": "Green Bits Inc.",
    "position": "CEO and Founder",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Brent",
    "lastname": "Kirkland",
    "email": "",
    "company": "Bitfinex",
    "position": "Software Engineer",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Steve",
    "lastname": "Cox",
    "email": "",
    "company": "TBWA\\Media Arts Lab",
    "position": "Director of Digital",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Karianne",
    "lastname": "Hoel",
    "email": "",
    "company": "Acando Norge",
    "position": "Direktør / Senior Vice President",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Prashanth Reddy",
    "lastname": "Mamidi",
    "email": "",
    "company": "Equal Experts",
    "position": "Developer",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Yue (Richard)",
    "lastname": "Yu",
    "email": "",
    "company": "Wuhan Hi-Lead Information Technology Co., Ltd.",
    "position": "CTO, Co-founder",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Olga",
    "lastname": "Oleolenko",
    "email": "",
    "company": "Code Space, LLC",
    "position": "Client Engagement Manager",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Kurt",
    "lastname": "Kranen",
    "email": "",
    "company": "Nenark Group",
    "position": "Chief Executive Officer",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Maria",
    "lastname": "Dejneka",
    "email": "",
    "company": "Restream",
    "position": "IT Recruiter",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Mathieu",
    "lastname": "Garaud",
    "email": "",
    "company": "Pretty Simple",
    "position": "CTO",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Miriam",
    "lastname": "Mertens",
    "email": "",
    "company": "Happy Rebels",
    "position": "Founder",
    "connect_date": "11 Jan 2019"
  },
  {
    "firstname": "Mikkel",
    "lastname": "Lindhard",
    "email": "",
    "company": "Leymus Genomics",
    "position": "Partner & marketing",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Birgitte Feginn",
    "lastname": "Angelil",
    "email": "",
    "company": "Creuna",
    "position": "Board Member",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Mark",
    "lastname": "Petty",
    "email": "",
    "company": "Intrusted",
    "position": "Founder & CEO",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Vadym",
    "lastname": "Machulskyi",
    "email": "",
    "company": "Sugar.Soft.Design",
    "position": "Manager Sales",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Michal",
    "lastname": "Stipek",
    "email": "",
    "company": "SpecPage",
    "position": "CTO",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Aurélien",
    "lastname": "Vecchiato",
    "email": "",
    "company": "DeeCide",
    "position": "CTO",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Rune",
    "lastname": "Tangen",
    "email": "",
    "company": "Point Taken AS",
    "position": "CEO / Partner",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Michael",
    "lastname": "Cantu",
    "email": "",
    "company": "Accelerate",
    "position": "CEO of Accelerate | Principal Technology Architect",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Ken",
    "lastname": "Lembie",
    "email": "",
    "company": "LawLess Team, Swedish winner of Global Legal Hackathon 2018",
    "position": "Co Founder",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Joanna",
    "lastname": "Apps",
    "email": "",
    "company": "Logan Startup Hub",
    "position": "Co-Founder",
    "connect_date": "10 Jan 2019"
  },
  {
    "firstname": "Julien",
    "lastname": "Balmont",
    "email": "",
    "company": "Zenchef",
    "position": "Co-founder, CTO & Product Manager",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Alex",
    "lastname": "Eckelberry",
    "email": "",
    "company": "AutoLoop",
    "position": "Chief Operating Officer",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Marks",
    "email": "",
    "company": "Quorum",
    "position": "Co-founder & CTO",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Gleb",
    "lastname": "Kobets",
    "email": "",
    "company": "RigER: Digital Oilfield Platform for Energy Service and Equipment Rentals",
    "position": "VP of Operations",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Lorcan",
    "lastname": "Kennedy",
    "email": "",
    "company": "Waterford Technologies",
    "position": "CTO",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Greg",
    "lastname": "Lind",
    "email": "",
    "company": "Humanitec",
    "position": "CTO",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Aaron",
    "lastname": "Brooks",
    "email": "",
    "company": "Vamp",
    "position": "Co-Founder/Co-CEO and Board Member",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Viatcheslav",
    "lastname": "Schwartz",
    "email": "",
    "company": "SysComData IT Solutions",
    "position": "Founder & CTO",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Sergey",
    "lastname": "Kanaschuk",
    "email": "",
    "company": "Noveo Group",
    "position": "Software Project Manager",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Gustav",
    "lastname": "Bergman",
    "email": "",
    "company": "Expektra AB",
    "position": "Co-founder and CTO",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Antonio",
    "lastname": "Merendaz do Carmo Neto",
    "email": "",
    "company": "UFF",
    "position": "Aluno de Doutorado",
    "connect_date": "09 Jan 2019"
  },
  {
    "firstname": "Roman",
    "lastname": "Tatusko",
    "email": "",
    "company": "Intelliarts",
    "position": "Software Engineer",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Michael",
    "lastname": "Erhard",
    "email": "",
    "company": "censhare AG",
    "position": "Vice President Software Development",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Yulia",
    "lastname": "Konovalova",
    "email": "",
    "company": "8allocate",
    "position": "Business Development Manager",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Marcin",
    "lastname": "Cichon",
    "email": "",
    "company": "Price f(x)",
    "position": "CEO & co-Founder",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Tom",
    "lastname": "Glanfield",
    "email": "",
    "company": "LHi Group Ltd.",
    "position": "CEO / Chief Executive Officer",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Tete",
    "lastname": "Mensa-Annan",
    "email": "",
    "company": "Cartman",
    "position": "CEO & Founder",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "William",
    "lastname": "Savage",
    "email": "",
    "company": "Fat Lama",
    "position": "Full-stack Software Engineer",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Stephan",
    "lastname": "Engelen",
    "email": "",
    "company": "SweepBright",
    "position": "COO & Late Co Founder",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Sarunas",
    "lastname": "Savicianskas",
    "email": "",
    "company": "TransferGo",
    "position": "DPO",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Mats",
    "lastname": "Grøtterud",
    "email": "",
    "company": "Heros Journey AS",
    "position": "Co-founder & CTO & Fullstack Development",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Ann Eliese",
    "lastname": "Grey",
    "email": "",
    "company": "Independent Consulting",
    "position": "Senior Technical Consultant",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Michael",
    "lastname": "Briers AO",
    "email": "",
    "company": "Food Agility",
    "position": "Founding CEO",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Leonard",
    "lastname": "Witteler",
    "email": "",
    "company": "Coffee Chat",
    "position": "Founder",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Stephan",
    "lastname": "Wedel Alsman",
    "email": "",
    "company": "Block by Block",
    "position": "Co-Founder and owner",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Paul",
    "lastname": "Buchmeier",
    "email": "",
    "company": "dizmo AG",
    "position": "Head Of Support",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Daniel",
    "lastname": "Kim",
    "email": "",
    "company": "FlowFact GmbH",
    "position": "CTO and Geschäftsführer / Managing Director",
    "connect_date": "08 Jan 2019"
  },
  {
    "firstname": "Azam",
    "lastname": "Ikramullah",
    "email": "",
    "company": "Booz Allen Hamilton",
    "position": "Lead Associate",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Nenad",
    "lastname": "Tripic",
    "email": "",
    "company": "Nodus Medical",
    "position": "CTO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Andreas",
    "lastname": "Müller",
    "email": "",
    "company": "kWIQly GmbH",
    "position": "CTO, Co - Founder",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Maximilian",
    "lastname": "Schickler",
    "email": "",
    "company": "Advanon AG",
    "position": "Head of Customer Development",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Christoph",
    "lastname": "Schwarz",
    "email": "",
    "company": "Accloud",
    "position": "CTO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "David",
    "lastname": "Giger",
    "email": "",
    "company": "Hivemind AG",
    "position": "CTO / Head of Software",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Houman",
    "lastname": "Goudarzi",
    "email": "",
    "company": "International Air Transport Association (IATA)",
    "position": "Head of BI & Industry Engagement",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Jason",
    "lastname": "Gilmore",
    "email": "",
    "company": "DreamFactory, Inc.",
    "position": "CTO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Bart",
    "lastname": "Vansevenant",
    "email": "",
    "company": "Ticto",
    "position": "Co-founder and CMO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Bill",
    "lastname": "Landers",
    "email": "",
    "company": "CompanionLabs",
    "position": "CTO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Udoka Mark",
    "lastname": "Uzoka",
    "email": "",
    "company": "Intelia",
    "position": "CEO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Malick",
    "lastname": "Maachi",
    "email": "",
    "company": "Digitrade Capital",
    "position": "Managing Partner",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Bobby",
    "lastname": "King",
    "email": "",
    "company": "Rock Paper Reality",
    "position": "COO & Founder",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Joshua",
    "lastname": "Douglas",
    "email": "",
    "company": "Bridge Connector",
    "position": "CTO",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Kosh",
    "lastname": "Podder",
    "email": "",
    "company": "Contractor",
    "position": "CTO, Technical Lead, Lead Developer and Architect",
    "connect_date": "07 Jan 2019"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Suarez",
    "email": "",
    "company": "Muniy",
    "position": "Chief Executive Officer and Co-Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Rajeev",
    "lastname": "Surati",
    "email": "",
    "company": "computation and imaging",
    "position": "President/CTO",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Noah",
    "lastname": "Gitalis",
    "email": "",
    "company": "AIFounded",
    "position": "CTO & Co-Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Mads",
    "lastname": "Thimmer",
    "email": "",
    "company": "Innovation Lab",
    "position": "co-founder, CEO",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Clive",
    "lastname": "Hubbard",
    "email": "",
    "company": "bubbleFiz Pty Ltd",
    "position": "Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Åsne",
    "lastname": "Havnelid",
    "email": "",
    "company": "Norsk Tipping",
    "position": "CEO",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Tom",
    "lastname": "Carpenter",
    "email": "",
    "company": "CWNP",
    "position": "CTO",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Will",
    "lastname": "McVay",
    "email": "",
    "company": "Culture Trip",
    "position": "Tech Lead",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Renato",
    "lastname": "Peter",
    "email": "",
    "company": "yawave",
    "position": "Founder / CEO",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Daniele",
    "lastname": "Rapisarda",
    "email": "",
    "company": "Catchme-Now",
    "position": "CEO and Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Jean-Marie",
    "lastname": "Porchet",
    "email": "",
    "company": "Mixfit Inc.",
    "position": "Frontend Engineer",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Michael",
    "lastname": "Kessler",
    "email": "",
    "company": "Tokenise",
    "position": "CEO & Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Christian",
    "lastname": "Simms",
    "email": "",
    "company": "Casenet, LLC",
    "position": "Senior Information Technology Consultant",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Christian",
    "lastname": "Gabriel",
    "email": "",
    "company": "Capdesk",
    "position": "CEO & Co-Founder",
    "connect_date": "06 Jan 2019"
  },
  {
    "firstname": "Thibaut",
    "lastname": "Delarbre",
    "email": "",
    "company": "Webestimate",
    "position": "Co-Founder",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Andrew",
    "lastname": "Tarver",
    "email": "",
    "company": "Jigsaw XYZ",
    "position": "Founder",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Callum",
    "lastname": "Rimmer",
    "email": "",
    "company": "By Miles",
    "position": "Co-Founder & CTO",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Philip",
    "lastname": "Nussbaumer",
    "email": "",
    "company": "skybow AG",
    "position": "Co-founder & CEO",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Daniel",
    "lastname": "Azbel",
    "email": "",
    "company": "i-Lance",
    "position": "Founder & Managing Director",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Howard",
    "lastname": "Falcon",
    "email": "",
    "company": "Quantitative Financial Solutions, LLC",
    "position": "Managing Partner",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Russell",
    "lastname": "Briggs",
    "email": "",
    "company": "CGI",
    "position": "Vice President Consulting Services",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Lana",
    "lastname": "Brandorn",
    "email": "lana.kaupuza@gmail.com",
    "company": "Sthlm Fintech Week",
    "position": "Organizer",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Paul",
    "lastname": "Giorgi",
    "email": "",
    "company": "DeFY Security",
    "position": "Co-Founder and CTO",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Marco",
    "lastname": "Iotti",
    "email": "",
    "company": "Mixfit",
    "position": "CTO, CSO & Co-Founder",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Dawid",
    "lastname": "Dereszewski",
    "email": "",
    "company": "Levuro AG",
    "position": "Head Of Development",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Steven",
    "lastname": "Sharman",
    "email": "",
    "company": "Royal Marketing Management",
    "position": "Vice President Sales Marketing",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Oliver",
    "lastname": "Halvorsrød",
    "email": "",
    "company": "ABB",
    "position": "VP Country Digital Lead, Startup Engangement Manager",
    "connect_date": "05 Jan 2019"
  },
  {
    "firstname": "Martin",
    "lastname": "Mangan",
    "email": "",
    "company": "Heureka Software, LLC",
    "position": "Senior Software Engineer",
    "connect_date": "04 Jan 2019"
  },
  {
    "firstname": "Bryan",
    "lastname": "Helmig",
    "email": "",
    "company": "Zapier",
    "position": "CTO, co-founder",
    "connect_date": "04 Jan 2019"
  },
  {
    "firstname": "Dominik",
    "lastname": "Grolimund",
    "email": "",
    "company": "Refind",
    "position": "Founder",
    "connect_date": "04 Jan 2019"
  },
  {
    "firstname": "Andrew",
    "lastname": "Paton-Smith BA(Hons), MBT (UNSW)",
    "email": "",
    "company": "Jazoodle Pty Ltd",
    "position": "Founder and CEO",
    "connect_date": "04 Jan 2019"
  },
  {
    "firstname": "Wendy",
    "lastname": "Cole",
    "email": "",
    "company": "EBSCO Information Services",
    "position": "Vice President of Strategic Projects",
    "connect_date": "04 Jan 2019"
  },
  {
    "firstname": "Reilly",
    "lastname": "Davis",
    "email": "",
    "company": "PeopleGrove",
    "position": "Co-founder & CTO",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Jack",
    "lastname": "Bowcott",
    "email": "",
    "company": "Tipi",
    "position": "Co-Founder/CEO",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Trygve",
    "lastname": "Haakedal",
    "email": "",
    "company": "Storebrand",
    "position": "SVP IT Strategy & Architecture",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Geir",
    "lastname": "Gautvik",
    "email": "",
    "company": "Sticos",
    "position": "IT Drift tekniker",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Christian",
    "lastname": "Richter",
    "email": "",
    "company": "YUKKA Lab AG",
    "position": "CTO",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Svend",
    "lastname": "Heier",
    "email": "",
    "company": "Informasjonskontroll AS",
    "position": "Chairman of the board",
    "connect_date": "03 Jan 2019"
  },
  {
    "firstname": "Tom",
    "lastname": "Alfoldi",
    "email": "",
    "company": "Appster",
    "position": "CEO @ Growth Factors by Appster",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Radu",
    "lastname": "Puscasu",
    "email": "",
    "company": "Cannvas MedTech Inc.",
    "position": "VP of Technology",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Maria Jose",
    "lastname": "Mendieta",
    "email": "",
    "company": "Boko Technologies",
    "position": "CEO",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Phaibion Royal",
    "lastname": "B2B Lead Generation Expert",
    "email": "",
    "company": "Royal Marketing Management",
    "position": "Chief Executive Officer | B2B Lead Generation Expert",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Alex",
    "lastname": "Barton",
    "email": "",
    "company": "mush: letsmush.com",
    "position": "CTO",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Nick",
    "lastname": "Jorens",
    "email": "",
    "company": "Qanyon",
    "position": "Founder",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Mathias",
    "lastname": "Verraes",
    "email": "",
    "company": "Domain-Driven Design Europe",
    "position": "Curator",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "James",
    "lastname": "Taylor",
    "email": "",
    "company": "FinTech Network Ltd",
    "position": "Founder",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Bruce",
    "lastname": "Muirhead",
    "email": "",
    "company": "MindHive",
    "position": "Founder and CEO, Mindhive",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Torsten",
    "lastname": "Petersen",
    "email": "",
    "company": "Woomio",
    "position": "Chief Technology Officer (CTO) and Founder",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Jørn",
    "lastname": "Skaane",
    "email": "",
    "company": "Lefdal Mine Datacenter AS",
    "position": "CEO",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Adam",
    "lastname": "Higgins",
    "email": "",
    "company": "Progressive Recruitment",
    "position": "Senior Consultant",
    "connect_date": "02 Jan 2019"
  },
  {
    "firstname": "Olivier",
    "lastname": "Kaisin",
    "email": "",
    "company": "Marker.io",
    "position": "Founder & CTO",
    "connect_date": "01 Jan 2019"
  },
  {
    "firstname": "Peter KT",
    "lastname": "Yu",
    "email": "",
    "company": "XYZ Robotics Inc.",
    "position": "CTO",
    "connect_date": "01 Jan 2019"
  },
  {
    "firstname": "Paul M.",
    "lastname": "Russell",
    "email": "",
    "company": "Digital Receipt Exchange",
    "position": "Co-Founder",
    "connect_date": "01 Jan 2019"
  },
  {
    "firstname": "Joe",
    "lastname": "Jones",
    "email": "",
    "company": "Franklin Robotics",
    "position": "Cofounder and CTO",
    "connect_date": "31 Dec 2018"
  },
  {
    "firstname": "Arnstein",
    "lastname": "Leivestad",
    "email": "",
    "company": "TradeTracker Norway AS",
    "position": "Country Manager-Partner",
    "connect_date": "31 Dec 2018"
  },
  {
    "firstname": "Angela",
    "lastname": "Stevenson, MPH BSN RN",
    "email": "",
    "company": "Great Lakes Water Authority",
    "position": "IT Manager - Administrative Services",
    "connect_date": "31 Dec 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Wilson",
    "email": "",
    "company": "Your Digital File",
    "position": "Managing Director",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Van Cauwenbergh",
    "email": "",
    "company": "CRANIUM Privacy & Security",
    "position": "Founder - CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Stuart",
    "lastname": "Rowlands",
    "email": "",
    "company": "Firecannon Pty Ltd",
    "position": "Founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Levitt ★★★★★",
    "email": "",
    "company": "ServiceSeeking Pty Ltd",
    "position": "Co-Founder and Joint CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Stefan",
    "lastname": "Meyer, PhD",
    "email": "",
    "company": "Ambrosus Technologies",
    "position": "CTO & Co-Founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Cyril",
    "lastname": "Samovskiy",
    "email": "",
    "company": "Mobilunity",
    "position": "Founder and CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Olivier",
    "lastname": "Eyries",
    "email": "",
    "company": "Alaya",
    "position": "Co-founder & Chief Of Operations",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Riham",
    "lastname": "Abu Elinin",
    "email": "",
    "company": "BznsBuilder",
    "position": "Founder, CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Hestbaek",
    "email": "",
    "company": "Lift Relations",
    "position": "Founder & CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Silvano",
    "lastname": "Trapella",
    "email": "",
    "company": "Coresultant srl",
    "position": "CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Keaton",
    "lastname": "Okkonen",
    "email": "",
    "company": "Black.ai",
    "position": "Co Founder & Chief Executive Officer",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Amrit",
    "lastname": "Rupasinghe",
    "email": "",
    "company": "Kliq Networks",
    "position": "Founder / CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Doron",
    "lastname": "Ostrin",
    "email": "",
    "company": "the urge",
    "position": "Co-Founder, CEO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Coulter",
    "email": "",
    "company": "Localift",
    "position": "Founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Javier",
    "lastname": "Coronel Orovio",
    "email": "",
    "company": "koko",
    "position": "CTO & co-founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Ajay",
    "lastname": "K",
    "email": "",
    "company": "Gigsterz Technologies",
    "position": "Founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Michael Bodekær",
    "lastname": "Jensen",
    "email": "",
    "company": "Labster",
    "position": "Founder, CTO",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Joshua",
    "lastname": "Cusens",
    "email": "",
    "company": "Intuitive Software Solutions Perth",
    "position": "Founder",
    "connect_date": "30 Dec 2018"
  },
  {
    "firstname": "Dr Don",
    "lastname": "Perugini",
    "email": "",
    "company": "Presagen",
    "position": "Co-Founder",
    "connect_date": "29 Dec 2018"
  },
  {
    "firstname": "Tiago",
    "lastname": "Maximo",
    "email": "",
    "company": "VanHack Technologies Inc.",
    "position": "CTO",
    "connect_date": "29 Dec 2018"
  },
  {
    "firstname": "Evgeny",
    "lastname": "Shilov",
    "email": "",
    "company": "IDS GeoRadar",
    "position": "Customer Support and Operations Manager",
    "connect_date": "29 Dec 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Pettit",
    "email": "",
    "company": "Astral IT Solutions",
    "position": "Enterprise Systems Architect / Senior Consultant / CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Fornander",
    "email": "",
    "company": "SENTIENTIC.io",
    "position": "CEO & Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Steven",
    "lastname": "Dunston",
    "email": "",
    "company": "Amplitude",
    "position": "Head of Growth and Digital Demand Generation",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Robbie",
    "lastname": "De Meyer",
    "email": "",
    "company": "Akti",
    "position": "CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Louis-Philippe",
    "lastname": "Kerkhove",
    "email": "",
    "company": "Crunch Analytics",
    "position": "Co-Founder & Head of Analytics",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "Dierckxsens",
    "email": "",
    "company": "Arkane Network",
    "position": "Co-founder and CSO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Nicola",
    "lastname": "Moresi",
    "email": "",
    "company": "Moresi.Com SA - The Swiss Bank of Data - La Banca Svizzera dei Dati",
    "position": "CEO & Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Nasir",
    "lastname": "Moxa",
    "email": "",
    "company": "DeepIT",
    "position": "Founder / Network & System Engineer",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Albin",
    "lastname": "CAUDERLIER",
    "email": "",
    "company": "Mubiz",
    "position": "Fondateur",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Pia Ella",
    "lastname": "Elmegård",
    "email": "",
    "company": "Growth Tribe Academy",
    "position": "Co-founder & Managing Director @ Growth Tribe Denmark",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Stepuk",
    "email": "",
    "company": "Noveo Group",
    "position": "Managing Partner",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Marie-Anne",
    "lastname": "Conac",
    "email": "",
    "company": "Admeet",
    "position": "Founder & CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Sebastian",
    "lastname": "Brandes",
    "email": "",
    "company": "Criterion AI",
    "position": "Co-Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Bremild",
    "email": "",
    "company": "Anyware Solutions",
    "position": "Co-Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Dieter",
    "lastname": "De Naeyer",
    "email": "",
    "company": "Royal Belgian Football Association",
    "position": "Project manager Video Assistant Referee",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Henrik",
    "lastname": "Hofmeister",
    "email": "",
    "company": "dexi.io",
    "position": "CEO, Co-Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Bo",
    "lastname": "Krogsgaard",
    "email": "",
    "company": "Cobiro",
    "position": "Co-founder & CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Mads",
    "lastname": "Boas",
    "email": "",
    "company": "Planway",
    "position": "Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Durak",
    "lastname": "Bahadir",
    "email": "",
    "company": "Octopus Cloud AG",
    "position": "Co-CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Dave",
    "lastname": "Van de Maele",
    "email": "",
    "company": "Cronos Interactive",
    "position": "Co-Founder & Managing Partner",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Carl",
    "lastname": "Fransman",
    "email": "",
    "company": "Baxter Planning",
    "position": "Managing Director EMEA",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Holger",
    "lastname": "Thorup",
    "email": "",
    "company": "Merittian",
    "position": "Co-founder & CTO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Bisgaard",
    "email": "",
    "company": "Players 1st",
    "position": "CEO & Co-founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Patrick",
    "lastname": "Gadd",
    "email": "",
    "company": "Lynx Sight",
    "position": "Co-Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Jes",
    "lastname": "T. S. Brinch",
    "email": "",
    "company": "Zenegy ApS",
    "position": "CEO & Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Reto",
    "lastname": "Kaeser",
    "email": "",
    "company": "astarios",
    "position": "Co-Founder, CTO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Dave",
    "lastname": "Hannam",
    "email": "",
    "company": "SharesInside Ltd",
    "position": "Co Founder & Entrepreneur",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Mickey",
    "lastname": "Soussah",
    "email": "",
    "company": "Cloud Teams ApS",
    "position": "CCO - Commercial Director / founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Sonu",
    "lastname": "Kansal",
    "email": "",
    "company": "NextHealth Technologies",
    "position": "Chief Product Officer",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Ajay",
    "lastname": "Yadav",
    "email": "",
    "company": "Roomi",
    "position": "Founder & CEO",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Marckx",
    "email": "",
    "company": "TheLedger",
    "position": "Co Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Christian Ulrik",
    "lastname": "von Scholten",
    "email": "",
    "company": "NorthQ ApS",
    "position": "Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Jorn",
    "lastname": "Jambers ☁️",
    "email": "",
    "company": "in4it",
    "position": "Co Founder",
    "connect_date": "28 Dec 2018"
  },
  {
    "firstname": "Pierre-Olivier",
    "lastname": "Danhaive",
    "email": "",
    "company": "Verbolia",
    "position": "Founder & Chief Executive Officer",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Jasper",
    "lastname": "Nuyens",
    "email": "",
    "company": "Linux Belgium",
    "position": "Founder and CEO",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Sean",
    "lastname": "Prescott",
    "email": "",
    "company": "Unity Investment AG",
    "position": "Founder & CEO",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Rasmussen",
    "email": "",
    "company": "iHeadHunt",
    "position": "CEO and Founder",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Wim",
    "lastname": "Vandamme",
    "email": "",
    "company": "The Goosebumps Factory",
    "position": "Co-Founder & CTO at the The Goosebumps Factory",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Tanguy",
    "lastname": "Vanderlinden",
    "email": "",
    "company": "Havila Partners",
    "position": "Partner",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Dizon",
    "email": "",
    "company": "M-PAYG",
    "position": "CPO & Co-founder",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Hendrickx",
    "email": "",
    "company": "Raccoons Group",
    "position": "Co-Founder",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Andersen",
    "email": "",
    "company": "Wedoio",
    "position": "CEO & Founder",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Pogorzelski",
    "email": "",
    "company": "The Riverside Company",
    "position": "Operating Partner",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Dwayne",
    "lastname": "Remekie",
    "email": "",
    "company": "Conscia",
    "position": "Founder & CTO",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Sophia",
    "lastname": "Shevchenko",
    "email": "",
    "company": "impltech",
    "position": "IT Sales Manager",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Kent Andrew",
    "lastname": "Westbye",
    "email": "",
    "company": "Lindorff",
    "position": "Liaison Advisor",
    "connect_date": "27 Dec 2018"
  },
  {
    "firstname": "Yegor",
    "lastname": "Fomin",
    "email": "",
    "company": "SoftClub",
    "position": "SMM mamager / Saler",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Jens",
    "lastname": "Grud",
    "email": "",
    "company": "Heaps Digital Ventures",
    "position": "Partner",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Backman",
    "email": "",
    "company": "Iostream Solutions",
    "position": "Founder and Senior Software Engineer",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Eldar",
    "lastname": "Dzhafarov",
    "email": "",
    "company": "cross platform solutions GmbH",
    "position": "CTO at cross platform solutions GmbH",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Oksana",
    "lastname": "Ferchuk",
    "email": "",
    "company": "EVO.company, Zakupki.prom.ua, Вчасно",
    "position": "CEO",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Simon Westh",
    "lastname": "Henriksen",
    "email": "",
    "company": "TripX Travel AB",
    "position": "CTO",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Lars",
    "lastname": "Boilesen",
    "email": "",
    "company": "Opera Software",
    "position": "CEO",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Aliya",
    "lastname": "Prokofyeva",
    "email": "",
    "company": "GALAKTIKA",
    "position": "Founder, CEO",
    "connect_date": "26 Dec 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Ulleberg",
    "email": "",
    "company": "Maindeck",
    "position": "CTO",
    "connect_date": "25 Dec 2018"
  },
  {
    "firstname": "Jake",
    "lastname": "Dunlap",
    "email": "",
    "company": "Skaled",
    "position": "CEO",
    "connect_date": "25 Dec 2018"
  },
  {
    "firstname": "Axton",
    "lastname": "Liu",
    "email": "",
    "company": "ITPROSOFT CANADA INC.",
    "position": "CTO",
    "connect_date": "25 Dec 2018"
  },
  {
    "firstname": "Nurjeta",
    "lastname": "Ismaili",
    "email": "",
    "company": "Ela AS",
    "position": "CEO (Co-founder)",
    "connect_date": "24 Dec 2018"
  },
  {
    "firstname": "Ion",
    "lastname": "Dronic",
    "email": "",
    "company": "narrafy.io",
    "position": "IT Consultant",
    "connect_date": "24 Dec 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Russell",
    "email": "",
    "company": "A4 Technologies",
    "position": "CTO",
    "connect_date": "24 Dec 2018"
  },
  {
    "firstname": "Julien",
    "lastname": "Desmarais",
    "email": "",
    "company": "DiGEiZ",
    "position": "CTO",
    "connect_date": "23 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Wendland",
    "email": "",
    "company": "Luminopia",
    "position": "Co-Founder & CTO",
    "connect_date": "23 Dec 2018"
  },
  {
    "firstname": "Jessica",
    "lastname": "Thompson",
    "email": "",
    "company": "KlearExpress Corp",
    "position": "Software Engineer",
    "connect_date": "22 Dec 2018"
  },
  {
    "firstname": "Andreas Grydeland",
    "lastname": "Sulejewski",
    "email": "",
    "company": "Neptune Software",
    "position": "Chief Executive Officer",
    "connect_date": "22 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Kurochkin",
    "email": "",
    "company": "Etalon Idea LLC",
    "position": "Founder & CEO",
    "connect_date": "22 Dec 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Lihodievsky",
    "email": "",
    "company": "System Technologies",
    "position": "Chief Operating Officer",
    "connect_date": "21 Dec 2018"
  },
  {
    "firstname": "Jeffrey",
    "lastname": "Stewart",
    "email": "",
    "company": "Asterius Media LLC",
    "position": "Owner / IT and Management Consultant / Fractional CTO CMTO",
    "connect_date": "21 Dec 2018"
  },
  {
    "firstname": "Vitalii",
    "lastname": "Vasenkov",
    "email": "",
    "company": "YayPay UA",
    "position": "Chief Business Officer",
    "connect_date": "21 Dec 2018"
  },
  {
    "firstname": "Ashwin",
    "lastname": "Rao",
    "email": "",
    "company": "Collabera Inc.",
    "position": "EVP, Chief Sales Officer (CSO) and Board Member",
    "connect_date": "21 Dec 2018"
  },
  {
    "firstname": "Ellen Susanne",
    "lastname": "Kvernmo",
    "email": "",
    "company": "Norsk Tipping",
    "position": "Leder Systemadministrasjon",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Molitor",
    "email": "",
    "company": "Improbable",
    "position": "VP of Engineering",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Roy",
    "lastname": "Loomis",
    "email": "",
    "company": "ECommerce Partners",
    "position": "Chief Executive Officer",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Atkin",
    "email": "",
    "company": "Calida Tech",
    "position": "Founder",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Tore",
    "lastname": "Foss",
    "email": "",
    "company": "Garnes Gruppen AS",
    "position": "Group CEO / co-founder",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Sablin",
    "email": "",
    "company": "Scand",
    "position": "Customer Relationship Manager",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Stevie",
    "lastname": "Ghiassi",
    "email": "",
    "company": "Legaler",
    "position": "Co-Founder | CEO",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Paul F.",
    "lastname": "Lambert",
    "email": "",
    "company": "Iozeta, LLC",
    "position": "Co-Founder",
    "connect_date": "20 Dec 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Ingledew",
    "email": "",
    "company": "FinTech Scotland",
    "position": "Chief Executive",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Igor",
    "lastname": "Royzis",
    "email": "",
    "company": "Kinect Consulting",
    "position": "SVP",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Jon Roar",
    "lastname": "Odden",
    "email": "",
    "company": "STOLT IT",
    "position": "CEO and Founder",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Ryvkin",
    "email": "",
    "company": "AssetBar",
    "position": "Founder & CEO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Serge",
    "lastname": "Helou",
    "email": "",
    "company": "FlipNpik, Worldwide",
    "position": "CTO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Bill",
    "lastname": "Coyman",
    "email": "",
    "company": "Swoon Group",
    "position": "Leadership Team",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Stian",
    "lastname": "Hauge",
    "email": "",
    "company": "SYNQ.fm",
    "position": "Founder & CEO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Yury",
    "lastname": "Baranovsky",
    "email": "",
    "company": "Instinctools Company",
    "position": "CTO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Knight",
    "email": "",
    "company": "miiFile",
    "position": "Founder & CEO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Samuel",
    "lastname": "Howse",
    "email": "",
    "company": "Crucial Group",
    "position": "Head of Telecoms & Fintech",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Alessandro",
    "lastname": "Di Lullo",
    "email": "",
    "company": "CFTE - Centre for Finance, Technology and Entrepreneurship",
    "position": "Teaching Associate",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Ragnvald",
    "lastname": "Nærø (Naero)",
    "email": "",
    "company": "Runde Environmental Centre",
    "position": "Chairman Of The Board",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Cole",
    "email": "",
    "company": "Global Integrated FinTech Solutions",
    "position": "CEO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Anastasia",
    "lastname": "Chinchuk",
    "email": "",
    "company": "EffectiveSoft Corporation",
    "position": "Senior Business Development Manager, Account Manager",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Booth",
    "email": "",
    "company": "Fintech Retail startup - details to be revealed",
    "position": "Chief Operating Officer",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Øyvind",
    "lastname": "Dyrseth",
    "email": "",
    "company": "TRIARK systems AS",
    "position": "CEO | Solutions Architect",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Lauren",
    "lastname": "Ryder",
    "email": "",
    "company": "Leading Edge Global",
    "position": "CEO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Dr Richard",
    "lastname": "Satur",
    "email": "",
    "company": "dClinic.io",
    "position": "Chief Executive Officer",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Doug",
    "lastname": "McLean",
    "email": "",
    "company": "Katipult",
    "position": "CTO",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "James",
    "lastname": "Olver",
    "email": "",
    "company": "APEXX Fintech Ltd",
    "position": "Head Of Corporate",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Clement",
    "lastname": "Kao",
    "email": "",
    "company": "Blend",
    "position": "Product Manager",
    "connect_date": "19 Dec 2018"
  },
  {
    "firstname": "Ian",
    "lastname": "Brewer",
    "email": "",
    "company": "LowerMyCharges.com",
    "position": "CEO - Founder - Lowering the cost of Financial Advice - FINTECH - REGTECH",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Elizabeth",
    "lastname": "Lumley",
    "email": "",
    "company": "VC Innovations",
    "position": "Director of Content and FinTech Ecosystem",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Arne Peder",
    "lastname": "Blix",
    "email": "",
    "company": "Friend Software Corporation AS",
    "position": "CEO and Founder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Svein",
    "lastname": "Willassen",
    "email": "",
    "company": "Confrere",
    "position": "CEO and Co-founder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Stephan",
    "lastname": "Nilsson",
    "email": "",
    "company": "Unisot.io AS",
    "position": "Founder, CEO, Blockchain Evangelist & Implementer",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Resnick",
    "email": "",
    "company": "Cureatr",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Rudi",
    "lastname": "Baumgarten",
    "email": "",
    "company": "Safety Computing AS",
    "position": "Chief Executive Officer",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "E. Jacobsen",
    "email": "",
    "company": "SAGAsystem AS",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Finn Egil",
    "lastname": "Haraldseide",
    "email": "",
    "company": "Baikingu AS",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Astreika",
    "email": "",
    "company": "ISsoft",
    "position": "Marketing Director",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Wenche",
    "lastname": "Lie-Pedersen",
    "email": "",
    "company": "Brilliant AS",
    "position": "CEO-Daglig leder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Tor Jakob",
    "lastname": "Ramsøy",
    "email": "",
    "company": "Arundo Analytics, Inc.",
    "position": "CEO and Founder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Alan",
    "lastname": "Duric",
    "email": "",
    "company": "Wire™",
    "position": "COO/CTO & Cofounder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Sergey",
    "lastname": "Zubekhin",
    "email": "",
    "company": "GP Solutions GmbH",
    "position": "Co-founder, Managing Director",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Håvard",
    "lastname": "Notøy",
    "email": "",
    "company": "FOSTECH AS",
    "position": "Owner & CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Leigh",
    "lastname": "Travers",
    "email": "",
    "company": "DigitalX Ltd",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Taras",
    "lastname": "Makh",
    "email": "",
    "company": "Sunvery",
    "position": "COO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Berman, Ph.D.",
    "email": "",
    "company": "PsyQuation",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Soederholm",
    "email": "",
    "company": "Nordic Semiconductor ASA",
    "position": "Director of Business Development",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Natalia",
    "lastname": "Tschmel",
    "email": "",
    "company": "Instinctools Company",
    "position": "Business Development Manager",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Hashi",
    "lastname": "Kaar",
    "email": "",
    "company": "PLYCODE",
    "position": "CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Erik M.",
    "lastname": "Rehn",
    "email": "",
    "company": "Flow Neuroscience",
    "position": "CTO & Co-founder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Olga",
    "lastname": "Mazyuk",
    "email": "",
    "company": "EffectiveSoft Corporation",
    "position": "Business Development Manager",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Wladislaw",
    "lastname": "Gramowicz",
    "email": "",
    "company": "System Technologies",
    "position": "Executive Director",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Sachidananda",
    "lastname": "Kini",
    "email": "",
    "company": "Unisoft Global Services, Inc.",
    "position": "CEO & CTO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Nicole",
    "lastname": "Hu",
    "email": "",
    "company": "One Concern, Inc.",
    "position": "Chief Technology Officer and Co Founder",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Alexey",
    "lastname": "Ulnirov",
    "email": "",
    "company": "LeverX",
    "position": "COO, Deputy Director",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Diego",
    "lastname": "Villegas",
    "email": "",
    "company": "Slang",
    "position": "Co-Founder & CEO",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Frank",
    "lastname": "Guerrera",
    "email": "",
    "company": "Pegasystems",
    "position": "Chief Technical Systems Officer",
    "connect_date": "18 Dec 2018"
  },
  {
    "firstname": "Adam",
    "lastname": "Martel",
    "email": "",
    "company": "Gravyty",
    "position": "CEO and Co-Founder",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Theo",
    "lastname": "Stanton",
    "email": "",
    "company": "Floom",
    "position": "CTO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Chaim",
    "lastname": "Pollak",
    "email": "",
    "company": "Solvv LLC",
    "position": "CTO - Foundeer",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "G. Ryan",
    "lastname": "Fawcett",
    "email": "",
    "company": "Amobee",
    "position": "Engineering Operations Manager",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Duleepa \"Dups\"",
    "lastname": "Wijayawardhana",
    "email": "",
    "company": "Supermetrics",
    "position": "CTO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Menashe",
    "lastname": "Haskin",
    "email": "",
    "company": "EdgyBees Ltd.",
    "position": "CTO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Nikhil",
    "lastname": "Ambekar",
    "email": "",
    "company": "CueLogic Technologies",
    "position": "CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Dr. Jill S.",
    "lastname": "Becker",
    "email": "",
    "company": "Kebotix",
    "position": "CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Bryan",
    "lastname": "Reynolds",
    "email": "",
    "company": "Docxonomy",
    "position": "Founder and CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Atul",
    "lastname": "Jain",
    "email": "",
    "company": "Vichara Technologies",
    "position": "CEO & Founder",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "✔ Daniel",
    "lastname": "Beckley",
    "email": "",
    "company": "TOD Intelligence",
    "position": "Founding Director",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Lyashok",
    "email": "",
    "company": "WorkFusion",
    "position": "CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Jessica",
    "lastname": "DE JESUS DE PINHO PINHAL",
    "email": "",
    "company": "Push! Founders",
    "position": "Entrepreneur In Residence",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "Murison",
    "email": "",
    "company": "Talos Digital",
    "position": "Blockchain Strategist & Engineer",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Petersson",
    "email": "",
    "company": "Fortnox",
    "position": "Business Development",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Trevor",
    "lastname": "Townsend",
    "email": "",
    "company": "Startupbootcamp Melbourne",
    "position": "CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Craig",
    "lastname": "Mowll",
    "email": "",
    "company": "MyState Limited",
    "position": "General Manager, Wealth",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "George",
    "lastname": "Cvetanovski- Hyperscalers",
    "email": "",
    "company": "HYPERSCALERS",
    "position": "Founder and CEO: Hyperscalers",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Dragan",
    "lastname": "Mandic",
    "email": "",
    "company": "PARts Australia",
    "position": "CTO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Babak",
    "lastname": "Pasdar",
    "email": "",
    "company": "Acreto",
    "position": "CEO & CTO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "James",
    "lastname": "Murray",
    "email": "",
    "company": "ETZ Payments",
    "position": "Group CTO and CEO Australasia",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Gavin",
    "lastname": "Evans",
    "email": "",
    "company": "ConnectiX Technologies Pty Ltd",
    "position": "Chief Executive Officer",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Pereira",
    "email": "",
    "company": "AdTorque Edge",
    "position": "Chief Executive Officer",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Karl",
    "lastname": "Norman",
    "email": "",
    "company": "eWave",
    "position": "CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "James",
    "lastname": "Gardner",
    "email": "",
    "company": "Wrethink, Inc.",
    "position": "Chief Executive Officer",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Lee",
    "email": "",
    "company": "Assembly Payments",
    "position": "Co Founder and CEO",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Such",
    "email": "",
    "company": "Reefwing Software",
    "position": "Executive Director",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Tristan",
    "lastname": "Goode",
    "email": "",
    "company": "Aptira",
    "position": "CEO, Founder and Director",
    "connect_date": "17 Dec 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Beaugeard",
    "email": "",
    "company": "HubOne",
    "position": "CEO & Founder",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Yair",
    "lastname": "Goldfinger",
    "email": "",
    "company": "AppCard",
    "position": "Co-Founder & CEO",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Kristofer",
    "lastname": "Rogers",
    "email": "",
    "company": "Split Payments",
    "position": "Chief Executive Officer (CEO)",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Brett",
    "lastname": "Burford",
    "email": "",
    "company": "Elementrex",
    "position": "CEO & Founder",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Francois",
    "lastname": "Hensley",
    "email": "",
    "company": "Outware Mobile",
    "position": "Senior Software Engineer",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Sawyer",
    "email": "",
    "company": "E3K.co",
    "position": "Associé CTO",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Forest",
    "lastname": "Handford",
    "email": "",
    "company": "Affectiva",
    "position": "Engineering Manager",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "De Leon Battista",
    "email": "",
    "company": "Taxfix",
    "position": "CTO",
    "connect_date": "16 Dec 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Shepherd",
    "email": "",
    "company": "tekFinder",
    "position": "Director / Software Engineering Recruiter",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Joe",
    "lastname": "Betts-Lacroix",
    "email": "",
    "company": "Vium",
    "position": "CTO",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Joe",
    "lastname": "Merces",
    "email": "",
    "company": "Forbes Technology Council",
    "position": "Official Member",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Sev",
    "lastname": "Onyshkevych",
    "email": "",
    "company": "ELEKS",
    "position": "Chief Customer Officer",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Ayal",
    "lastname": "Zylberman",
    "email": "",
    "company": "QualiTest Group",
    "position": "Founder and Director",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Elias",
    "lastname": "Sprengel",
    "email": "",
    "company": "Audatic",
    "position": "Co-Founder and CTO",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Mickel",
    "lastname": "Andersson",
    "email": "",
    "company": "Remente",
    "position": "CTO",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Hutch",
    "lastname": "Ozdil",
    "email": "",
    "company": "SwiftHero",
    "position": "Co-founder",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Munawar",
    "lastname": "Abadullah",
    "email": "",
    "company": "York Global Investment Group",
    "position": "President",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Abhi",
    "lastname": "Agarwal",
    "email": "",
    "company": "Consummate Technologies - Salesforce, Microsoft, Ramco & EduSuiteOnDemand Implementation Partner",
    "position": "Founder and Chief Executive Officer",
    "connect_date": "15 Dec 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Rehman",
    "email": "",
    "company": "Cognizant",
    "position": "Global CTO, Digital Engineering",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "McKinnon",
    "email": "",
    "company": "McKinnon Holdings",
    "position": "Trustee",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "George",
    "lastname": "Moore",
    "email": "",
    "company": "Cengage Learning",
    "position": "Chief Technology Officer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Mohsin",
    "lastname": "Syed",
    "email": "",
    "company": "KiwiTech",
    "position": "EVP & Chief Startup Officer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Ara",
    "lastname": "Ohanian",
    "email": "",
    "company": "Systech International",
    "position": "Chief Executive Officer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "James Allen",
    "lastname": "Regenor Col USAF(ret)",
    "email": "",
    "company": "Blockchain Resources Group (BRG), LLC",
    "position": "Co-Founder",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Halliday",
    "email": "",
    "company": "InvestX Capital Ltd.",
    "position": "CTO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Salomon",
    "email": "",
    "company": "Mobile App Fund",
    "position": "CEO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Veejay",
    "lastname": "Jadhaw",
    "email": "",
    "company": "Provenir",
    "position": "Global Chief Technology Officer (Machine Learning, Analytics, SaaS, Data Management)",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Halis Osman",
    "lastname": "Erkan",
    "email": "",
    "company": "Secure.Industries",
    "position": "CEO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Wior",
    "email": "",
    "company": "Omnivore",
    "position": "CHIEF EXECUTIVE OFFICER / CO-FOUNDER",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Raul",
    "lastname": "Mihali",
    "email": "",
    "company": "Vener8 Technologies",
    "position": "Chief Technology Officer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Eklund",
    "email": "",
    "company": "Algorex Healthcare Technologies",
    "position": "CTO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Walsh",
    "email": "",
    "company": "Saylent",
    "position": "Senior Software Developer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Joseph",
    "lastname": "Schwendt",
    "email": "",
    "company": "Sanofi",
    "position": "Lead Software Architect, Medical Device Technologies",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Sergey",
    "lastname": "Lushin",
    "email": "",
    "company": "Shark Software",
    "position": "CEO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "François",
    "lastname": "Misslin",
    "email": "",
    "company": "AyoLab",
    "position": "Cofounder and CTO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Emmanuel",
    "lastname": "Gonnet",
    "email": "",
    "company": "Univeris",
    "position": "VP Product Management and Software Engineering",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Ross",
    "lastname": "McCrossan",
    "email": "",
    "company": "TEC Partners - Technical Recruitment Specialists",
    "position": "Resourcer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Magdalena",
    "lastname": "Cieślak",
    "email": "",
    "company": "Woodrow Mercer",
    "position": "Recruitment Consultant",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Edin",
    "lastname": "Bajramovic",
    "email": "",
    "company": "App Fiction GmbH",
    "position": "Founder / CEO",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Lionel",
    "lastname": "Martin",
    "email": "",
    "company": "Wi-5",
    "position": "Chief Technology Officer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "McManus",
    "email": "",
    "company": "Oblix - Technology Recruitment",
    "position": "Senior JavaScript Recruiter",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Florian",
    "lastname": "Feichtinger",
    "email": "",
    "company": "devBuddies",
    "position": "Founder",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Carter",
    "email": "",
    "company": "DAZN",
    "position": "Technical Sourcer",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Ralph",
    "lastname": "Bragg",
    "email": "",
    "company": "Open Banking",
    "position": "Ecosystem Trust Framework Architect",
    "connect_date": "14 Dec 2018"
  },
  {
    "firstname": "Marcos",
    "lastname": "Aguayo",
    "email": "",
    "company": "Futrli",
    "position": "Senior Backend Developer",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Korshakov",
    "email": "",
    "company": "System Technologies",
    "position": "CEO",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Dylan",
    "lastname": "Foster",
    "email": "",
    "company": "TravelBank",
    "position": "Engineering Manager",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Trent",
    "lastname": "Deike",
    "email": "",
    "company": "Amazon",
    "position": "Software Development Manager",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "James",
    "lastname": "Hewett",
    "email": "",
    "company": "Sonovate",
    "position": "Head of Software Development",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Reed",
    "email": "",
    "company": "Advantage Solutions: Sales, Marketing, Technology",
    "position": "Lead Software Engineer",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Katia",
    "lastname": "Voziianova",
    "email": "",
    "company": "Red Tag Ukraine",
    "position": "Human Resources Manager",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Herbert",
    "lastname": "ten Have",
    "email": "",
    "company": "Fizyr (deep learning for vision guided robotics)",
    "position": "CEO",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Jai",
    "lastname": "Shukla",
    "email": "",
    "company": "Comtech LLC",
    "position": "Recruiting Manager",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Fulkerson",
    "email": "",
    "company": "CoStar Group",
    "position": "Vice President, Software Engineering (CoStar Suite)",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Rakovitsky",
    "email": "",
    "company": "ROKO Labs",
    "position": "Founder & CEO",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Caspar",
    "lastname": "Høegh",
    "email": "",
    "company": "Acando Norge",
    "position": "Tech Lead",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "McCord",
    "email": "",
    "company": "Mobile Posse",
    "position": "CTO",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Dag Hendrik",
    "lastname": "Lerdal",
    "email": "",
    "company": "Preseria AS",
    "position": "CEO & Co-Founder",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Barry",
    "lastname": "Edwards",
    "email": "",
    "company": "TechSelect.io",
    "position": "Founder",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Ertel",
    "email": "",
    "company": "Free2Move - The Carsharing App",
    "position": "CTO",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Sokolowski",
    "email": "",
    "company": "Facebook",
    "position": "Engineering",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Younge",
    "email": "",
    "company": "Apptio",
    "position": "Engineering Site Director",
    "connect_date": "13 Dec 2018"
  },
  {
    "firstname": "Cliff",
    "lastname": "Isaacson",
    "email": "",
    "company": "Syncron",
    "position": "Director of Pricing Solutions",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Ivo",
    "lastname": "Radulovski",
    "email": "",
    "company": "Founder Institute",
    "position": "Co-Director",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Burns",
    "email": "",
    "company": "GigLabs",
    "position": "Co-Founder, CTO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Vertume Thomas",
    "lastname": "Dufault",
    "email": "",
    "company": "Trillion Technology Solutions, Inc",
    "position": "SVP Business Development",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Luke",
    "lastname": "Norris",
    "email": "",
    "company": "Faction Inc.",
    "position": "Founder- CEO/President",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Curt",
    "lastname": "Hulbert",
    "email": "",
    "company": "Beaver Run Resort & Conference Center",
    "position": "CIO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Prasanna",
    "lastname": "Sankar",
    "email": "",
    "company": "Rippling",
    "position": "Co-Founder and CTO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Loszak",
    "email": "",
    "company": "Humi",
    "position": "CTO & Cofounder",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Franziska",
    "lastname": "Sedlak",
    "email": "",
    "company": "GoEuro",
    "position": "Senior People Partner - Tech, Product, Design",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Guy",
    "lastname": "Rombaut",
    "email": "",
    "company": "OneFit",
    "position": "CTO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Lars",
    "lastname": "Flesland",
    "email": "",
    "company": "FlowMotion Technologies",
    "position": "CEO & Co-founder",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Igor",
    "lastname": "Kravchenko",
    "email": "",
    "company": "Reksoft",
    "position": "CIO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Đỗ Binau",
    "email": "",
    "company": "Gratisal ApS",
    "position": "CTO & Partner",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Anna Holm",
    "lastname": "Heide",
    "email": "",
    "company": "No Isolation",
    "position": "Chief Communications Officer",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Max M.",
    "lastname": "Diez",
    "email": "",
    "company": "Assaia International AG",
    "position": "CEO",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Oljay",
    "lastname": "Kush",
    "email": "",
    "company": "The TJX Companies, Inc.",
    "position": "General Manager",
    "connect_date": "12 Dec 2018"
  },
  {
    "firstname": "Scott",
    "lastname": "Schecter",
    "email": "",
    "company": "NetVendor",
    "position": "CTO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Ulad",
    "lastname": "Radkevitch",
    "email": "",
    "company": "ScienceSoft Inc",
    "position": "VP marketing, sales and business development; partner",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "John",
    "lastname": "Shiple",
    "email": "",
    "company": "StartEngine",
    "position": "Chief Technology Officer",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Renny",
    "lastname": "Koshy",
    "email": "",
    "company": "Platform28",
    "position": "CTO/Architect",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Sergio",
    "lastname": "Almaguer",
    "email": "",
    "company": "Yaydoo",
    "position": "Founder, CEO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Ivan",
    "lastname": "Grakov",
    "email": "",
    "company": "SoftClub Ltd.",
    "position": "Deputy General Director, Presales and Business Development",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Jozef",
    "lastname": "Antony",
    "email": "",
    "company": "Foundation s.r.o.",
    "position": "CTO, Owner",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Sebastian",
    "lastname": "Bürgel",
    "email": "",
    "company": "Validity Labs",
    "position": "Co-Founder and CTO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Jochen",
    "lastname": "Krause",
    "email": "",
    "company": "Valdon Group GmbH",
    "position": "Head Operations & co-founder",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Fréour",
    "email": "",
    "company": "COLONIES",
    "position": "CTO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Wirz",
    "email": "",
    "company": "Cloudrexx AG",
    "position": "CEO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Halford",
    "email": "",
    "company": "O2 Solutions",
    "position": "Principal Consultant / Director",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Franziska",
    "lastname": "Palumbo-Seidel",
    "email": "",
    "company": "KennedyFitch",
    "position": "Director, Digital Practice",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Samuel",
    "lastname": "Huegli",
    "email": "",
    "company": "Tamedia",
    "position": "CTO & Head of Digital Ventures",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Tobias",
    "lastname": "Rein",
    "email": "",
    "company": "GetYourGuide",
    "position": "Co-founder & Principal Engineer",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Karsten",
    "lastname": "Samaschke",
    "email": "",
    "company": "Cloudibility GmbH",
    "position": "Co-Founder & CEO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Korotkov",
    "email": "",
    "company": "VRP Consulting",
    "position": "Head Of Department",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Lucas",
    "lastname": "Ward",
    "email": "",
    "company": "Kin Insurance",
    "position": "CoFounder & CTO",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Nikolay",
    "lastname": "Zakharov",
    "email": "",
    "company": "Benebay",
    "position": "Co Founder",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Mykhaylo",
    "lastname": "Shaforostov",
    "email": "",
    "company": "AppDynamics",
    "position": "Director of Sales Engineering - APAC",
    "connect_date": "11 Dec 2018"
  },
  {
    "firstname": "Andrei",
    "lastname": "David",
    "email": "",
    "company": "Founders Factory",
    "position": "Head Of Engineering",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Cyril",
    "lastname": "LAPINTE",
    "email": "",
    "company": "Mt Pelerin",
    "position": "Blockchain",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Javier",
    "lastname": "Ubillos",
    "email": "",
    "company": "Looklet AB",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Rick",
    "lastname": "Maré",
    "email": "",
    "company": "JXT Global",
    "position": "Founder and CEO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Praveen",
    "lastname": "Kumar",
    "email": "",
    "company": "Solidus Labs",
    "position": "Co-founder, CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Ievgen",
    "lastname": "Demchenko",
    "email": "",
    "company": "Homelike",
    "position": "Head Of Engineering",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Fabian",
    "lastname": "Baptista",
    "email": "",
    "company": "Monkop",
    "position": "Advisor To The Board",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Eric B",
    "lastname": "Sackowitz",
    "email": "",
    "company": "PeerStream, Inc.",
    "position": "Chief Technology Officer",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Jelle",
    "lastname": "Smedts",
    "email": "",
    "company": "Krypt.ly",
    "position": "Founder & CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Gene",
    "lastname": "Linetsky",
    "email": "",
    "company": "Poynt",
    "position": "Director Of Engineering",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Hannes",
    "lastname": "Gredler (HIRING)",
    "email": "",
    "company": "RtBrick Inc",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Filipe",
    "lastname": "Garcia",
    "email": "",
    "company": "Elucidate",
    "position": "Founder and CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Gordan",
    "lastname": "Bakalar",
    "email": "",
    "company": "BakalarSoftware",
    "position": "CTO/Architect and Founder",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Cédric",
    "lastname": "Laruelle",
    "email": "",
    "company": "NUMA Paris",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Alexandre",
    "lastname": "Hudavert",
    "email": "",
    "company": "Kyump",
    "position": "Cofondateur",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Junaid",
    "lastname": "Maqsood",
    "email": "",
    "company": "Upwork",
    "position": "Software Developer",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Aaron",
    "lastname": "Burciaga, CAP",
    "email": "",
    "company": "Analytics2Go",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Sergey",
    "lastname": "Shcherbanenko",
    "email": "",
    "company": "SANNACODE",
    "position": "CTO, Co-founder",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Gerardo",
    "lastname": "Colorado Diaz-Caneja",
    "email": "",
    "company": "5miles",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Ramo",
    "lastname": "Karahasan-Riechardt",
    "email": "",
    "company": "itembase, Inc.",
    "position": "Co-Founder & CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Idan",
    "lastname": "Hahn",
    "email": "",
    "company": "WayCare",
    "position": "Founder & C.T.O",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Júlio",
    "lastname": "Santos",
    "email": "",
    "company": "Fractal",
    "position": "CTO and Co-Founder",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Oksana",
    "lastname": "Pidkuyko (Bayda)",
    "email": "",
    "company": "Standard Chartered Bank",
    "position": "Executive Director, Global Co-Head Commodities Structuring",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Roni",
    "lastname": "El-Bahar",
    "email": "",
    "company": "Toga Networks",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Eran",
    "lastname": "Zinman",
    "email": "",
    "company": "monday.com",
    "position": "Co-founder & CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Krivitski",
    "email": "",
    "company": "Sberbank",
    "position": "IT Lead @Digital Channels",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Yunkai",
    "lastname": "Zhou",
    "email": "",
    "company": "Leap.ai",
    "position": "Co-founder & CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Bryan",
    "lastname": "Pirtle",
    "email": "",
    "company": "Nova Labs (YC W16)",
    "position": "CTO & Co-founder",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Jordan",
    "lastname": "Hudgens",
    "email": "",
    "company": "DevCamp Platform",
    "position": "CTO",
    "connect_date": "10 Dec 2018"
  },
  {
    "firstname": "Sean",
    "lastname": "Yao",
    "email": "",
    "company": "InterPrac Ltd",
    "position": "Software Development Manager",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Zach",
    "lastname": "Rattner",
    "email": "",
    "company": "Yembo",
    "position": "Co-Founder and CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Adam",
    "lastname": "Gernon",
    "email": "",
    "company": "CXi Software",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Kelly",
    "lastname": "Peng",
    "email": "",
    "company": "Kura Technologies",
    "position": "Founder, CEO & CTO (Electrical / Optical / Software Engineer)",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Louis",
    "lastname": "Cibot",
    "email": "",
    "company": "Zenaton",
    "position": "Co-founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "TERNOIR",
    "email": "",
    "company": "Hubware",
    "position": "Co-Founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Joel",
    "lastname": "Beasley",
    "email": "",
    "company": "LeaderBits",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Snurnikov",
    "email": "",
    "company": "Instacart",
    "position": "Software Engineer",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Valentin",
    "lastname": "Stanishevsky",
    "email": "",
    "company": "IT Rocket",
    "position": "CEO, Owner",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Karl",
    "lastname": "Skidmore",
    "email": "",
    "company": "Freelance",
    "position": "CTO-as-a-Service",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Fabian",
    "lastname": "Langlet",
    "email": "",
    "company": "BimBamJob",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Assaf",
    "lastname": "Dagan",
    "email": "",
    "company": "Tiidan",
    "position": "Co-Founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Ilya",
    "lastname": "Mikheev",
    "email": "",
    "company": "IPONWEB",
    "position": "Technical Owner, Supply",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Jonas",
    "lastname": "Bergström",
    "email": "",
    "company": "Mirado Consulting",
    "position": "Co-Founder",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Xavier",
    "lastname": "Godron",
    "email": "",
    "company": "DNA Script",
    "position": "Co-founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Shiell",
    "email": "",
    "company": "Cognitive Geology",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Chad",
    "lastname": "Slaughter",
    "email": "",
    "company": "Future • Finance",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Wilson",
    "lastname": "Pang",
    "email": "",
    "company": "Appen",
    "position": "Chief Technical Officer",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Nikolaievskyi",
    "email": "",
    "company": "Marakos",
    "position": "CEO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Pavel",
    "lastname": "Sokolov",
    "email": "",
    "company": "Visata",
    "position": "CTO/PM",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Brossard",
    "email": "",
    "company": "Monisnap",
    "position": "Co-Founder and CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Vitali",
    "lastname": "Nikulenka",
    "email": "",
    "company": "Promwad, electronics design house",
    "position": "Chief Operations Officer",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Arthus",
    "lastname": "de Saint Chaffray",
    "email": "",
    "company": "BlueCargo (YC S18)",
    "position": "Co-founder | CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Konstantin",
    "lastname": "Siomkin",
    "email": "",
    "company": "ITTAS LLC",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Shaun",
    "lastname": "Edwards",
    "email": "",
    "company": "Plus One Robotics",
    "position": "Co-Founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Marko",
    "lastname": "Mrdjenovič",
    "email": "",
    "company": "v17.si",
    "position": "CEO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Avi",
    "lastname": "Gershon",
    "email": "",
    "company": "VELOQUANT Ltd",
    "position": "Lead Quantitative Research and Development",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Yaron",
    "lastname": "Kassner",
    "email": "",
    "company": "SIlverfort",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Ambroise",
    "lastname": "Couissin",
    "email": "",
    "company": "CoverGo | Insurtech",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Greg. Startup CTO",
    "lastname": "VP of Engineering, Problem Solver, CEO",
    "email": "",
    "company": "IT4Medicine",
    "position": "Co-Founder (Director Of Engineering)",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Wim",
    "lastname": "Tas",
    "email": "",
    "company": "ThorbiQ nv",
    "position": "Co-Founder / CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Kobi",
    "lastname": "Sasson",
    "email": "",
    "company": "Bits of Gold",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Pekka",
    "lastname": "Kosonen",
    "email": "",
    "company": "Freska",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Raymond",
    "lastname": "Dyngeseth Selvik",
    "email": "",
    "company": "Experis Ciber AS",
    "position": "Tech Lead",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Varughese",
    "email": "",
    "company": "Drover Ltd",
    "position": "Founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Antoine",
    "lastname": "Baudoux",
    "email": "",
    "company": "TAKTIK sa",
    "position": "Co-founder & CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Radoslav",
    "lastname": "Georgiev",
    "email": "",
    "company": "gtmhub",
    "position": "CTO & co-founder",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Aviad",
    "lastname": "Mor",
    "email": "",
    "company": "Lumigo",
    "position": "CTO and Co-Founder",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Dennis",
    "lastname": "Xenos",
    "email": "",
    "company": "Flexciton",
    "position": "Co-founder & Chief Technology Officer",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Zhebrak",
    "email": "",
    "company": "Insilico Medicine, Inc",
    "position": "CTO",
    "connect_date": "09 Dec 2018"
  },
  {
    "firstname": "Alihan",
    "lastname": "Ozbayrak",
    "email": "",
    "company": "Socio Labs",
    "position": "CoFounder & CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Denis",
    "lastname": "Bauer",
    "email": "",
    "company": "Joblift",
    "position": "CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Xavier",
    "lastname": "Lernould",
    "email": "",
    "company": "Kolsquare",
    "position": "CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Julien",
    "lastname": "BRODIER",
    "email": "",
    "company": "TALIUM",
    "position": "co-Founder and CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Eilon",
    "lastname": "Reshef",
    "email": "",
    "company": "Gong.io",
    "position": "Co-Founder and CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Pedulla",
    "email": "",
    "company": "Responsive Consulting Inc.",
    "position": "CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Bjarke",
    "lastname": "Mønsted",
    "email": "",
    "company": "Pricebutler.dk",
    "position": "CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Abdul",
    "lastname": "Ahmed",
    "email": "",
    "company": "Simpli Compli",
    "position": "Chief Executive Officer",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Roy",
    "lastname": "Gurskevik",
    "email": "",
    "company": "Experis Ciber AS",
    "position": "Avdelingsdirektør - Systemutvikling og vedlikehold",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Veenhof",
    "email": "",
    "company": "Dropsolid NV",
    "position": "CTO",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Ken",
    "lastname": "Decanio",
    "email": "",
    "company": "41monkeys",
    "position": "consutling startup guy",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Ciaran",
    "lastname": "Foley",
    "email": "",
    "company": "Immersive Entertainment, Inc.",
    "position": "CEO & Co-Founder",
    "connect_date": "08 Dec 2018"
  },
  {
    "firstname": "Kia",
    "lastname": "McClain",
    "email": "",
    "company": "OutfitSets.com",
    "position": "Founder/CEO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Øyvind Kleppe",
    "lastname": "Asphjell",
    "email": "",
    "company": "PasientSky",
    "position": "CTO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Magnus",
    "lastname": "Nevstad",
    "email": "",
    "company": "EGGS Design",
    "position": "Creative Director of Technology",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Faisal",
    "lastname": "Naseri",
    "email": "",
    "company": "UNINETT",
    "position": "IT Konsulent",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Lindström",
    "email": "",
    "company": "Symbio Sweden",
    "position": "CTO, Teknikchef",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Lee",
    "lastname": "Tang",
    "email": "",
    "company": "Aspec Solution Inc.",
    "position": "Owner",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Dan Trygve",
    "lastname": "Olsrød",
    "email": "",
    "company": "Rada Labs AS",
    "position": "Founder / CTO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Niskanen",
    "email": "",
    "company": "Lurkit",
    "position": "CEO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Tornes",
    "email": "",
    "company": "Uni Micro Web AS",
    "position": "Systems Architect / Partner",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Burns",
    "email": "",
    "company": "Original People",
    "position": "Co-Founder & CTO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Harcus",
    "email": "",
    "company": "MonkeyHost Group",
    "position": "CTO",
    "connect_date": "07 Dec 2018"
  },
  {
    "firstname": "John",
    "lastname": "Lee",
    "email": "",
    "company": "YARDY Ventures",
    "position": "Venture Partner",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Xin (Shawn)",
    "lastname": "Liao",
    "email": "",
    "company": "GE",
    "position": "Technical Product Management",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Goodman",
    "email": "",
    "company": "Eastnine",
    "position": "Co Founder & CEO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Leconte",
    "email": "",
    "company": "Stratability",
    "position": "Cofounder and CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Kostiantyn",
    "lastname": "Sokolinskyi",
    "email": "",
    "company": "TBWA\\MOBILE",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Kelvin",
    "lastname": "Clibbon",
    "email": "",
    "company": "Ecrebo",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Oliver",
    "lastname": "Nicolini",
    "email": "",
    "company": "Aula",
    "position": "Co-founder & CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Per Henrik",
    "lastname": "Oja",
    "email": "",
    "company": "Skjerpa Web",
    "position": "Co-Founder",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Lars-Cyril",
    "lastname": "Blystad",
    "email": "",
    "company": "Next Signal AS",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Casey",
    "lastname": "Ellis",
    "email": "",
    "company": "Bugcrowd Inc",
    "position": "Founder, Chairman and CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Jin",
    "lastname": "Huang",
    "email": "",
    "company": "Aktana",
    "position": "CTO and VP of Engineering",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "David",
    "lastname": "Babayan",
    "email": "",
    "company": "EasySize",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Petter I.",
    "lastname": "Gustafson",
    "email": "",
    "company": "NHST Media Group",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Sjöstedt",
    "email": "",
    "company": "Simon Saves AB",
    "position": "Chairman Of The Board",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Mads",
    "lastname": "Rued",
    "email": "",
    "company": "Bonzer",
    "position": "Co-Founder & CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Tor",
    "lastname": "Hildrum",
    "email": "",
    "company": "OBOS",
    "position": "Enterprise Architect",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Batlin",
    "email": "",
    "company": "Trustology",
    "position": "Founder and CEO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Owen",
    "lastname": "Turner-Major",
    "email": "",
    "company": "Fat Llama",
    "position": "Co-founder & CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Evans",
    "email": "",
    "company": "Futrli",
    "position": "Chief Engineering Officer",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Rodriguez",
    "email": "",
    "company": "Data.be",
    "position": "Managing Partner",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Adham",
    "lastname": "Shawwaf",
    "email": "",
    "company": "MEDS",
    "position": "Co-founder and CTO at MEDS",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Dariya",
    "lastname": "Dolgopola",
    "email": "",
    "company": "Teemika",
    "position": "Cofounder & Business Development Director",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Per Nestor",
    "lastname": "Warp",
    "email": "",
    "company": "Axeptia Credit Intelligence AS",
    "position": "CTO & Co-founder",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Bishnu",
    "lastname": "Nayak",
    "email": "",
    "company": "Fixstream, Inc",
    "position": "CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Voloshyn",
    "email": "",
    "company": "Pilot SG",
    "position": "Co-Founder & CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Ian",
    "lastname": "Joffe",
    "email": "",
    "company": "Sydney Professionals - Sydpro Pty Ltd",
    "position": "Co-Founder, Director - Remote Staffing & Outsourcing",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Erhan",
    "lastname": "Kartaltepe, PMP, CISSP",
    "email": "",
    "company": "VeriLedger",
    "position": "Co-Founder and CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "John",
    "lastname": "Richards",
    "email": "",
    "company": "Journey Church of Hopkinsville",
    "position": "Online Campus Pastor",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Katrin",
    "lastname": "Suess",
    "email": "",
    "company": "Katrin Suess",
    "position": "UX for Startups",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Carlos",
    "lastname": "Arteaga",
    "email": "",
    "company": "AMTech (Activity Monitoring Technologies)",
    "position": "Co-founder and CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Alan",
    "lastname": "Mosca",
    "email": "",
    "company": "nPlan",
    "position": "Co-Founder & CTO",
    "connect_date": "06 Dec 2018"
  },
  {
    "firstname": "Joao",
    "lastname": "Martins",
    "email": "",
    "company": "Yapily",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "A. Richill",
    "lastname": "ARTLoe",
    "email": "",
    "company": "Spefz",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Charles",
    "lastname": "Sutton",
    "email": "",
    "company": "Datascientest.com",
    "position": "Founder",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Hooman",
    "lastname": "Kashef",
    "email": "",
    "company": "Libre Wireless Technologies",
    "position": "CEO, Founder",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Mohamed",
    "lastname": "Meabed",
    "email": "",
    "company": "Stealth Mode Startup",
    "position": "Co-Founder & CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Al",
    "lastname": "Brown",
    "email": "",
    "company": "Veritone, Inc.",
    "position": "SVP, Engineering",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Aharon",
    "lastname": "Twizer",
    "email": "",
    "company": "Spotinst",
    "position": "Co-Founder & CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Eikenes",
    "email": "",
    "company": "Carving Five",
    "position": "CEO and founder",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Nima",
    "lastname": "Badrbeigi",
    "email": "",
    "company": "Digitaldanmark",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Arentsen",
    "email": "",
    "company": "Nubera",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Dragsbæk",
    "email": "",
    "company": "introDus",
    "position": "CTO & Co-Founder",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Vanderstraeten",
    "email": "",
    "company": "Law Is Code",
    "position": "Executive Tech Coach",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Bjorn",
    "lastname": "Bjercke",
    "email": "",
    "company": "BitGate",
    "position": "CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Vasylchenko, PhD",
    "email": "",
    "company": "Sofitto",
    "position": "Co-Founder, CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Kirsch",
    "email": "",
    "company": "IrishCompany.EU",
    "position": "Head of Customer Succes",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Jeroen",
    "lastname": "Keppens",
    "email": "",
    "company": "Amazium",
    "position": "Building amazing teams",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Noman",
    "lastname": "Ahmed",
    "email": "",
    "company": "SmartEnds",
    "position": "Co-Founder and CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Bert",
    "lastname": "Vandenberghe",
    "email": "",
    "company": "Skyline Communications",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Zac",
    "lastname": "Glenn",
    "email": "",
    "company": "Kalos, Inc.",
    "position": "Software Director",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Darnell",
    "email": "",
    "company": "Altair",
    "position": "Senior VP Model Based Embedded Tools",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Dick",
    "email": "",
    "company": "HIRING HUB",
    "position": "CTO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Geoff",
    "lastname": "Watts",
    "email": "",
    "company": "EDITED",
    "position": "Co-founder and CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Fred",
    "lastname": "Dimmel II",
    "email": "",
    "company": "Power I.T. LLC",
    "position": "President & CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Sharples",
    "email": "",
    "company": "TriCom Technical Services",
    "position": "Founder/Owner/CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Kristina",
    "lastname": "Kotyk",
    "email": "",
    "company": "Flextronics",
    "position": "Program Management and Business Development Manager",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Gaute",
    "lastname": "Hæreid",
    "email": "",
    "company": "Webstep AS",
    "position": "Partner",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Waller",
    "email": "",
    "company": "BORN Group",
    "position": "Partner & COO, Commerce",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Love",
    "email": "",
    "company": "LETS GO SURFING",
    "position": "Instructor",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Kjell Magne",
    "lastname": "Aase",
    "email": "",
    "company": "Branncompaniet AS",
    "position": "Styreleder og medeier",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Justin",
    "lastname": "Zambo",
    "email": "",
    "company": "BalancePoint Corporation",
    "position": "Chief Sales Officer",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Terri",
    "lastname": "Foudray",
    "email": "",
    "company": "Rumble Now",
    "position": "Founder and CEO",
    "connect_date": "05 Dec 2018"
  },
  {
    "firstname": "Jack",
    "lastname": "Crowley",
    "email": "",
    "company": "Forge.AI",
    "position": "AI/ML Director of Science and Technology",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Steven",
    "lastname": "Woods",
    "email": "",
    "company": "Nudge Software Inc.",
    "position": "CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Boban",
    "lastname": "Davidovic",
    "email": "",
    "company": "Netmaking AS",
    "position": "Senior webutvikler",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Hamilton",
    "email": "",
    "company": "Huddol",
    "position": "CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Valerii",
    "lastname": "Shypunov",
    "email": "",
    "company": "Innocode AS",
    "position": "CTO, shareholder",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Paulo",
    "lastname": "Capani",
    "email": "",
    "company": "HCL Technologies",
    "position": "Business Development Executive - Latam",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Igor",
    "lastname": "Gaydut",
    "email": "",
    "company": "ADMIRAL Container Lines Inc. Ltd.",
    "position": "Sales Executive",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Per Olav",
    "lastname": "Monseth",
    "email": "",
    "company": "Norsk Tipping",
    "position": "Deputy Chairman of the Board",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Gianluca",
    "lastname": "Bisceglie",
    "email": "",
    "company": "Visyond",
    "position": "Founder & CEO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Jose",
    "lastname": "Cartro",
    "email": "",
    "company": "Lendify",
    "position": "CTO, Co-Founder",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Matias",
    "lastname": "Murad",
    "email": "",
    "company": "BYON8",
    "position": "CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Mikkel",
    "lastname": "Lindblom",
    "email": "",
    "company": "GoKarakter ApS",
    "position": "IT Business Partner",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Victor",
    "lastname": "Kovalev",
    "email": "",
    "company": "Redbubble",
    "position": "CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Janus Klok",
    "lastname": "Matthesen",
    "email": "",
    "company": "Pixelz Inc",
    "position": "Co-founder and CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "John",
    "lastname": "MacKinnon",
    "email": "",
    "company": "Virwire",
    "position": "CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Bijan",
    "lastname": "Vaez",
    "email": "",
    "company": "FractionalCTO.ca",
    "position": "Part-Time CTO, Product Development Consultant",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Dwayne",
    "lastname": "Rudy",
    "email": "",
    "company": "Evodant Interactive Inc.",
    "position": "CEO / CTO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Sydney, PMP, ITIL",
    "email": "",
    "company": "Royk Enterprise Solutions Inc.",
    "position": "CTO, Project Manager",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Mathieu",
    "lastname": "Johnson, B.Ing",
    "email": "",
    "company": "Services Tangea inc.",
    "position": "Co CEO",
    "connect_date": "04 Dec 2018"
  },
  {
    "firstname": "Dominick",
    "lastname": "Rivard",
    "email": "",
    "company": "JD LABS TECHNOLOGIES INC.",
    "position": "CTO & Co-Founder",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Yves",
    "lastname": "Desgagne",
    "email": "",
    "company": "Cloud Officer",
    "position": "CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Sandesh",
    "lastname": "Parulekar",
    "email": "",
    "company": "Voya Financial",
    "position": "Director",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Jens",
    "lastname": "Hinrichs",
    "email": "",
    "company": "Carnect",
    "position": "Head Of Technology / CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Thornton",
    "email": "",
    "company": "Sparqa Legal",
    "position": "CEO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Klaus",
    "lastname": "Andersen",
    "email": "",
    "company": "Basware",
    "position": "CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Sampson",
    "email": "",
    "company": "Tiliter Technology",
    "position": "CTO & Co-Founder",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Mansour",
    "lastname": "Ahmadian",
    "email": "",
    "company": "NCTech - Reality Imaging Systems",
    "position": "Director Of Engineering",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Dovydas",
    "lastname": "Bulanavičius",
    "email": "",
    "company": "UAB \"Aciety\"",
    "position": "Managing Director",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Joan",
    "lastname": "Figuerola Hurtado",
    "email": "",
    "company": "SpecifiedBy",
    "position": "Co-founder & CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Detlef",
    "lastname": "Rupp",
    "email": "",
    "company": "TouchingCode",
    "position": "CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Stig",
    "lastname": "Hansen",
    "email": "",
    "company": "Evry Økonomitjenester",
    "position": "Senior rådgiver",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Berntsson",
    "email": "",
    "company": "iZettle",
    "position": "Tech manager (consultant)",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Tomas",
    "lastname": "Øen",
    "email": "",
    "company": "Conax",
    "position": "CFO & EVP",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Rob",
    "lastname": "Grosche",
    "email": "",
    "company": "I-optic Computing",
    "position": "CTO",
    "connect_date": "03 Dec 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Heyns",
    "email": "",
    "company": "brytlyt",
    "position": "Founder and CEO",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Rick",
    "lastname": "Brownlow",
    "email": "",
    "company": "Geektastic",
    "position": "CEO & Co Founder",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Lene",
    "lastname": "Finstad",
    "email": "",
    "company": "Norsk Tipping",
    "position": "Executive Vice President Product & Brands",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Benjamin",
    "lastname": "Wootton",
    "email": "",
    "company": "Contino - Global Enterprise DevOps and Cloud Transformation Consultancy",
    "position": "Co-Founder & CTO",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Nukri",
    "lastname": "Basharuli",
    "email": "",
    "company": "Aggregion Ltd.",
    "position": "Chief Executive Officer",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Halpin",
    "email": "",
    "company": "CloudCoCo",
    "position": "Chief Executive Officer",
    "connect_date": "02 Dec 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Frost",
    "email": "",
    "company": "Zonal Retail Data Systems",
    "position": "Group Operations Director",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "James",
    "lastname": "Whelan",
    "email": "",
    "company": "LevelBlox (OTCQB: LVBX)",
    "position": "Chief Technology Officer",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "Bruce",
    "lastname": "Vernon",
    "email": "",
    "company": "Calcivis Limited",
    "position": "CTO",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "Dag",
    "lastname": "Krohn",
    "email": "",
    "company": "Retired",
    "position": "My own boss",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Quiroga",
    "email": "",
    "company": "beyondr",
    "position": "Managing Director",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Marshall",
    "email": "",
    "company": "Concept Gap",
    "position": "Principal",
    "connect_date": "01 Dec 2018"
  },
  {
    "firstname": "John",
    "lastname": "Johansson",
    "email": "",
    "company": "Devies",
    "position": "CTO",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Vadim",
    "lastname": "Tushev",
    "email": "",
    "company": "ProCoders.TECH",
    "position": "Co-Head of Development",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Roy",
    "lastname": "Popiolek",
    "email": "",
    "company": "cloudeo AG",
    "position": "cloudeo expert partner",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Marcos",
    "lastname": "Jimenez",
    "email": "",
    "company": "Private Investor",
    "position": "Private Investor",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Antonio Gabriel",
    "lastname": "Bertolino",
    "email": "",
    "company": "Folder IT",
    "position": "CEO at Folder IT",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Viktor",
    "lastname": "Shcherban",
    "email": "",
    "company": "HotelForce",
    "position": "CTO & co-founder",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Terje",
    "lastname": "Ovreberg",
    "email": "",
    "company": "NAGRA",
    "position": "Head Of Services Key Accounts",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Zelenskiy",
    "email": "",
    "company": "Interbranche Internetagentur München GmbH",
    "position": "CTO / Software Architect / Engineer",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Albert",
    "lastname": "Santalo",
    "email": "",
    "company": "8base",
    "position": "Founder and CEO",
    "connect_date": "30 Nov 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Rouke",
    "email": "",
    "company": "PRWD",
    "position": "Founder & CEO",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Bjørn Erik B",
    "lastname": "Helgeland",
    "email": "",
    "company": "ABAX",
    "position": "COO & Founding Partner",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Helle",
    "lastname": "Moen",
    "email": "",
    "company": "EGGS Design",
    "position": "Regional Director",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Thor",
    "lastname": "Holm",
    "email": "",
    "company": "Frende Forsikring",
    "position": "Enterprise Architect",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Olga",
    "lastname": "Yakuba",
    "email": "",
    "company": "Zfort Group",
    "position": "Recruiter",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Vincent",
    "lastname": "Hagen P.E.",
    "email": "",
    "company": "Nordic Semiconductor ASA",
    "position": "Global Business Development Manager",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Amit",
    "lastname": "Jaiswal",
    "email": "",
    "company": "stratejos.ai",
    "position": "Product Manager",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "David",
    "lastname": "Jeusette",
    "email": "",
    "company": "Smartwinkle",
    "position": "Founder",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Marcus",
    "lastname": "Kneen",
    "email": "",
    "company": "Actively seeking new opportunities",
    "position": "CEO, CFO",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Axbey",
    "email": "",
    "company": "BUILD",
    "position": "Member Of The Board Of Advisors",
    "connect_date": "29 Nov 2018"
  },
  {
    "firstname": "Pontus",
    "lastname": "Tjernberg",
    "email": "",
    "company": "Sendify",
    "position": "Business Development Lead",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Johan",
    "lastname": "Pontin",
    "email": "",
    "company": "The RNA Medicines Company",
    "position": "Founder + CEO",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Izyumskiy",
    "email": "",
    "company": "Omnicomm",
    "position": "CTO",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Esteban",
    "lastname": "Tognini",
    "email": "",
    "company": "InteraxLink",
    "position": "CEO / Founder",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Alfredo",
    "lastname": "Peralta",
    "email": "",
    "company": "Laerdal Medical",
    "position": "IT Consultant",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Benjamin",
    "lastname": "Bachhuber",
    "email": "",
    "company": "Freework GmbH",
    "position": "CTO & Co-Founder",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Rune",
    "lastname": "Larsen",
    "email": "",
    "company": "Acando Norge",
    "position": "Vice President Industry 4.0",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Anderson",
    "email": "",
    "company": "The DOTTT",
    "position": "Head boy",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "John",
    "lastname": "Hardy",
    "email": "",
    "company": "OUTSOURCE CONSULTANTS, LLC",
    "position": "Vice President",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Jesper",
    "lastname": "Svensson",
    "email": "",
    "company": "Fortnox",
    "position": "CTO",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Sara",
    "lastname": "Arildsson",
    "email": "",
    "company": "iZettle",
    "position": "Chief Operating Officer",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Donnelly",
    "email": "",
    "company": "Tictrac",
    "position": "Chief Operating Officer",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Paula",
    "lastname": "Doyle",
    "email": "",
    "company": "Cognite AS",
    "position": "Vice President of Industry Solutions",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "James",
    "lastname": "Rushton",
    "email": "",
    "company": "DAZN",
    "position": "Chief Executive Officer",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Kristian",
    "lastname": "Gjerding",
    "email": "",
    "company": "CellPoint Mobile",
    "position": "CEO",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Jan Erik",
    "lastname": "Kjerpeseth",
    "email": "",
    "company": "Sparebanken Vest",
    "position": "CEO / Konsernsjef",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Magnus",
    "lastname": "Pernvik",
    "email": "",
    "company": "Stratsys AB",
    "position": "Founder",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Strachan",
    "email": "",
    "company": "3 SIDED CUBE",
    "position": "Managing Director",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Natalie",
    "lastname": "Weishuhn",
    "email": "",
    "company": "Biosight",
    "position": "Senior Program Manager Lead",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Cody",
    "lastname": "Sklar",
    "email": "",
    "company": "Synapse International",
    "position": "Founder, Locator of Humans",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Christo",
    "lastname": "Peev",
    "email": "",
    "company": "Motion Software",
    "position": "Founder and CEO",
    "connect_date": "28 Nov 2018"
  },
  {
    "firstname": "Magnus",
    "lastname": "Kongsli Hillestad",
    "email": "",
    "company": "Sanity.io",
    "position": "CEO & Co-founder",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Dimopoulos",
    "email": "",
    "company": "Gavl",
    "position": "Head of Software Development",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Mihai",
    "lastname": "Cimpoesu, PhD",
    "email": "",
    "company": "crowdx.io",
    "position": "Founder and CEO",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Nasima",
    "lastname": "Thomassen",
    "email": "",
    "company": "Mobica",
    "position": "Head Of Research And Development",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Goran",
    "lastname": "Jordanov",
    "email": "",
    "company": "Cygni",
    "position": "IT consultant",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Rune",
    "lastname": "Kiowsky",
    "email": "",
    "company": "Blackwood Seven",
    "position": "Partner, Head of Customer Experience",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Allardice",
    "email": "",
    "company": "BORN Group",
    "position": "Chief Creative Officer",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Georg",
    "lastname": "Petzinka",
    "email": "",
    "company": "42dp Labs GmbH",
    "position": "Business Development",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Bliss",
    "email": "",
    "company": "Icon Solutions (UK) Ltd",
    "position": "Business Development Director",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Hoge",
    "email": "",
    "company": "HolidayCheck AG",
    "position": "IT-System Architect",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Barry",
    "lastname": "Day",
    "email": "",
    "company": "Packt",
    "position": "Chief Operating Officer",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Askman",
    "email": "",
    "company": "Item Consulting as",
    "position": "Senior IT Consultant",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Dr. Martin L.",
    "lastname": "Brückner",
    "email": "",
    "company": "Euronet Worldwide",
    "position": "CTO",
    "connect_date": "27 Nov 2018"
  },
  {
    "firstname": "Farid",
    "lastname": "Kendoul",
    "email": "",
    "company": "Emesent",
    "position": "CTO / Co-Founder",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Hjalmarsson",
    "email": "",
    "company": "Stratsys AB",
    "position": "Chief Product Owner",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Ove",
    "lastname": "Bastiansen",
    "email": "",
    "company": "Teleplan Globe",
    "position": "Chief Software Architect",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Langvatn",
    "email": "",
    "company": "Sticos AS",
    "position": "Business Developer",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Maria",
    "lastname": "Börjesson",
    "email": "",
    "company": "Knowit",
    "position": "CEO Knowit Development AB",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Pozdniakov",
    "email": "",
    "company": "First Line Software, Inc",
    "position": "CEO",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Sureya",
    "lastname": "Agirman",
    "email": "",
    "company": "Botkyrka kommun",
    "position": "Enhetschef IT-enheten",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Leonid",
    "lastname": "Kazartsev",
    "email": "",
    "company": "Reksoft",
    "position": "Head Of Software Development Center",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Obermüller",
    "email": "",
    "company": "WorkpathHQ",
    "position": "CTO",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Vitaly",
    "lastname": "Domrachev",
    "email": "",
    "company": "Emergn Limited",
    "position": "Delivery Manager",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Gideon",
    "lastname": "Carroll",
    "email": "",
    "company": "Edge Petrol Ltd",
    "position": "Founder & CEO",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Darren",
    "lastname": "Clark",
    "email": "",
    "company": "Kohab",
    "position": "Co-founder and CTO",
    "connect_date": "26 Nov 2018"
  },
  {
    "firstname": "Dario",
    "lastname": "Valenza",
    "email": "",
    "company": "Carbonix",
    "position": "Founder and CTO",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Cornale",
    "email": "",
    "company": "Impulse Digital Pty Ltd",
    "position": "Founder & CTO",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Van",
    "email": "",
    "company": "hoolah",
    "position": "CTO",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Malcolm",
    "lastname": "Fitzgerald",
    "email": "",
    "company": "Zip Co Limited",
    "position": "Chief Technology Officer",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Müller",
    "email": "",
    "company": "bytabo - Digital Crew",
    "position": "Head of Startup as a Service & UX-Designer",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Blake",
    "lastname": "Harrold",
    "email": "",
    "company": "Westpac",
    "position": "Associate Analyst - CTO Enterprise Architecture",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Thorfinn",
    "lastname": "Hansen",
    "email": "",
    "company": "Netcompetence",
    "position": "Chairman of the Board",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Rebecca",
    "lastname": "Enonchong",
    "email": "",
    "company": "AppsTech",
    "position": "Founder and CEO",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Nicholas",
    "lastname": "Bransby-Williams",
    "email": "",
    "company": "Laundrapp",
    "position": "Co-Founder & CTO",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Prof. Dr. Burkhard",
    "lastname": "Schwenker",
    "email": "",
    "company": "Wissensfabrik - Unternehmen für Deutschland e.V.",
    "position": "Member of the Steering Committee",
    "connect_date": "25 Nov 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Hamnes",
    "email": "",
    "company": "Oneflow AB",
    "position": "CEO & Founder",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Clark",
    "email": "",
    "company": "fusesport, Inc.",
    "position": "Founder / Chair",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Jan Inge",
    "lastname": "Bergseth",
    "email": "",
    "company": "Cognite AS",
    "position": "Principal Solution Architect",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Kristian",
    "lastname": "Hellquist",
    "email": "",
    "company": "Mynewsdesk",
    "position": "CTO",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Marlene",
    "lastname": "do Vale",
    "email": "",
    "company": "Startup Guide",
    "position": "Global Partnerships Lead",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Drougge",
    "email": "",
    "company": "Jetshop",
    "position": "CTO",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Glenn Øystein",
    "lastname": "Bergan",
    "email": "",
    "company": "ABAX",
    "position": "Director of Data Capture",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Stein",
    "lastname": "Onsrud",
    "email": "",
    "company": "Buypass AS",
    "position": "Vice President of Strategy and Business Development",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Stian",
    "lastname": "Rustad",
    "email": "",
    "company": "24SevenOffice_US",
    "position": "Founder and CEO",
    "connect_date": "24 Nov 2018"
  },
  {
    "firstname": "Ingeborg",
    "lastname": "Molden Hegstad",
    "email": "",
    "company": "Q-Free ASA",
    "position": "Board of Directors Member",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Per Christian",
    "lastname": "Andersen",
    "email": "",
    "company": "ABAX",
    "position": "Director Nordic Partnerships",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Jan",
    "lastname": "Webering",
    "email": "",
    "company": "Sevenval Technologies GmbH",
    "position": "CEO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Lukas",
    "lastname": "Weber",
    "email": "",
    "company": "Vimcar",
    "position": "Co-Founder and CTO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Bowen",
    "email": "",
    "company": "Opus Recruitment Solutions",
    "position": "Team Lead - Open Source Specialist",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Anette",
    "lastname": "Mellbye",
    "email": "",
    "company": "Rekode",
    "position": "Consultant / Advisor / Board member",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Snorre",
    "lastname": "Lie",
    "email": "",
    "company": "Experis Ciber AS",
    "position": "Senior Business Consultant",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Björn",
    "lastname": "Ivroth",
    "email": "",
    "company": "EVRY",
    "position": "CEO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Erik",
    "lastname": "Sundet",
    "email": "",
    "company": "Innit AS",
    "position": "Avdelingsleder drift",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Jacob",
    "lastname": "Osborne",
    "email": "",
    "company": "Adaptavist",
    "position": "Internal Recruiter",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Wilhelm",
    "email": "",
    "company": "Coming soon",
    "position": "Startup Stealth Mode",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Freiman",
    "email": "",
    "company": "Exicom Software AB",
    "position": "VD",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Kirill",
    "lastname": "Churilin",
    "email": "",
    "company": "Lanit-Tercom",
    "position": "Director of Information Systems Department",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Lucas",
    "lastname": "Habrich",
    "email": "",
    "company": "insoro",
    "position": "CTO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Arjan",
    "lastname": "van der Meer",
    "email": "",
    "company": "Dreamlines GmbH",
    "position": "Managing Director & CTO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Per",
    "lastname": "Bäckman",
    "email": "",
    "company": "Presento",
    "position": "Co-Founder",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Juha",
    "lastname": "Ristolainen",
    "email": "",
    "company": "DSTOQ",
    "position": "Blockchain and Technology Advisor",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Dahlgren",
    "email": "",
    "company": "Familjen Dahlgren Invest AB",
    "position": "Investor",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Roderik",
    "lastname": "van der Veer",
    "email": "",
    "company": "SettleMint",
    "position": "Founder & CTO",
    "connect_date": "23 Nov 2018"
  },
  {
    "firstname": "Jasmin",
    "lastname": "Steinhardt",
    "email": "",
    "company": "LMU Entrepreneurship Center",
    "position": "Startup Operations Management",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Søren",
    "lastname": "Hveisel Hansen",
    "email": "",
    "company": "PasientSky",
    "position": "Chief Operations Officer",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Norman",
    "lastname": "Wankowski",
    "email": "",
    "company": "Palmetto Gruppe",
    "position": "IT Leitung Auftragsmanagement, CTO",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "David",
    "lastname": "Sundström",
    "email": "",
    "company": "IBM",
    "position": "Junior Consultant - Internship",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Eirik Lilland",
    "lastname": "Nerdal",
    "email": "",
    "company": "Angel Challenge AS",
    "position": "Startup Manager",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Phoebe",
    "lastname": "Lara",
    "email": "",
    "company": "All American Facility Maintenance",
    "position": "Vice President Business",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Carlos Omar",
    "lastname": "Delgado",
    "email": "",
    "company": "Self-employed",
    "position": "Business Development",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Casper",
    "lastname": "Lindberg",
    "email": "",
    "company": "InnoTact Software AB",
    "position": "Co-Founder & CTO",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Enrique",
    "lastname": "Fernández Casado",
    "email": "",
    "company": "Derivco Spain",
    "position": "Head of Development (SWIX)",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Spiik",
    "email": "",
    "company": "Cetrez",
    "position": "Head Of Operations",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Box",
    "email": "",
    "company": "Berry Technologies",
    "position": "Head Of Sales",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Roman",
    "lastname": "Potekhin",
    "email": "",
    "company": "Holda",
    "position": "CTO",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "J. Israel",
    "lastname": "Greene, LSSGB",
    "email": "",
    "company": "Israel Greene, LLC",
    "position": "Keynote Speaker| Leadership Teacher|Coach",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Duka",
    "email": "",
    "company": "Emergn Limited",
    "position": "Head of Engineering Excellence",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Maxim",
    "lastname": "Pavlukhin",
    "email": "",
    "company": "Omnicomm",
    "position": "Technical Director",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Sasan",
    "lastname": "Mameghani",
    "email": "",
    "company": "Maindeck AS",
    "position": "Founder",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Marko",
    "lastname": "Salonen",
    "email": "",
    "company": "Remit Ltd",
    "position": "Partner and competence lead hybris",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Matthew",
    "lastname": "Nicholl",
    "email": "",
    "company": "Berklee College of Music",
    "position": "Associate Vice President of Global Initiatives",
    "connect_date": "22 Nov 2018"
  },
  {
    "firstname": "Greg",
    "lastname": "Spaulding",
    "email": "",
    "company": "ezCater",
    "position": "Director of Talent Acquisition",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Iain",
    "lastname": "Niven-Bowling",
    "email": "",
    "company": "Essence",
    "position": "SVP Technology",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Bruno",
    "lastname": "Tavares",
    "email": "",
    "company": "DAZN",
    "position": "Head Of Software Development",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Julien",
    "lastname": "Cartwright",
    "email": "",
    "company": "UENI Ltd",
    "position": "QC Ops Manager",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Yan",
    "lastname": "Cui",
    "email": "",
    "company": "THEBURNINGMONK LTD",
    "position": "Director",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Bullock",
    "email": "",
    "company": "Amplience",
    "position": "Vice President, Business Development",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Piper",
    "email": "",
    "company": "TAB (The App Business)",
    "position": "IT Manager",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "King",
    "email": "",
    "company": "Black Swan Data",
    "position": "CEO",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "James",
    "lastname": "Brooke",
    "email": "",
    "company": "Amplience",
    "position": "Founder & CEO",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Matthew",
    "lastname": "Riddle",
    "email": "",
    "company": "RocketShoes Pty Ltd",
    "position": "Founder and CEO",
    "connect_date": "21 Nov 2018"
  },
  {
    "firstname": "Kelly",
    "lastname": "Waters",
    "email": "",
    "company": "101 Ways",
    "position": "Founder & CEO",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Karolina",
    "lastname": "Kiwak",
    "email": "",
    "company": "MedTouch",
    "position": "Digitial Project Coordinator",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Eamon",
    "lastname": "Pielow",
    "email": "",
    "company": "PT BIZ",
    "position": "Co-Founder and Chief Executive Officer",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Fredrik Martini",
    "lastname": "Andersson",
    "email": "",
    "company": "Tailify",
    "position": "Founder and Client Service Director",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Ben",
    "lastname": "Farzam",
    "email": "",
    "company": "Stealth Mode Fintech Startup",
    "position": "Founder & CEO",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Grzegorz",
    "lastname": "Bigos",
    "email": "",
    "company": "Fourteen33",
    "position": "CTO",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Ellen Sofie",
    "lastname": "Øgaard",
    "email": "",
    "company": "Circulab network",
    "position": "Certified Circulab Consultant",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Igor",
    "lastname": "Shtelmakh",
    "email": "",
    "company": "TechFunder - Building Startups with Marketing Experts",
    "position": "CTO",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Tayfun",
    "lastname": "Samli",
    "email": "",
    "company": "BMW Group",
    "position": "BMW World IT Manager",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Viktor",
    "lastname": "Löfgren",
    "email": "",
    "company": "DigiExam AB",
    "position": "Chief Executive Officer",
    "connect_date": "20 Nov 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Fogh",
    "email": "",
    "company": "Startup central dk",
    "position": "CEO-Founder & Owner",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Carlos",
    "lastname": "Floen Alnes",
    "email": "",
    "company": "Vendu.ai",
    "position": "CEO & Founder",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Arif",
    "lastname": "Samaletdin",
    "email": "",
    "company": "Mirum Agency",
    "position": "Managing Director",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Filippo",
    "email": "",
    "company": "ezCater",
    "position": "Senior Caterer Partnerships",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Eivind",
    "lastname": "Rognan",
    "email": "",
    "company": "Evry Financial Services",
    "position": "Chief Consultant",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Dylan",
    "lastname": "Smith",
    "email": "",
    "company": "Rubix Consulting Pty Ltd",
    "position": "Founder",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "Rockson",
    "lastname": "Chan",
    "email": "",
    "company": "flobox.io",
    "position": "Co-Founder",
    "connect_date": "19 Nov 2018"
  },
  {
    "firstname": "David",
    "lastname": "Rawk",
    "email": "",
    "company": "Bootstrap Dragon",
    "position": "Senior Software Developer",
    "connect_date": "18 Nov 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Wirth",
    "email": "",
    "company": "Quorum",
    "position": "Cofounder & CEO",
    "connect_date": "18 Nov 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Khabe",
    "email": "",
    "company": "PRIME BPM",
    "position": "Co Founder",
    "connect_date": "18 Nov 2018"
  },
  {
    "firstname": "Reza",
    "lastname": "Keshavarzi",
    "email": "",
    "company": "WipeHero",
    "position": "Founder - CEO",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Williams",
    "email": "",
    "company": "BIRDI Pty Ltd",
    "position": "CTO",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Evan",
    "lastname": "Clark",
    "email": "",
    "company": "ClickView",
    "position": "Co-Founder and Non-Executive Director",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Pavel",
    "lastname": "Matveev",
    "email": "",
    "company": "Wirex",
    "position": "CEO",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Mehrad",
    "lastname": "Sarbaz",
    "email": "",
    "company": "CanYou AS",
    "position": "CEO",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Kristian",
    "lastname": "Feldborg",
    "email": "",
    "company": "Vesuvio",
    "position": "Founder",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Douglas",
    "lastname": "English",
    "email": "",
    "company": "Culture Amp",
    "position": "Founder & CTO",
    "connect_date": "17 Nov 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Gisaeus",
    "email": "",
    "company": "Ocast",
    "position": "Founder & Business Development",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Anthony",
    "lastname": "Kalinde",
    "email": "",
    "company": "Red Ocean Technology AS",
    "position": "Senior Consultant",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Tunold-Hanssen",
    "email": "",
    "company": "Flow Insights",
    "position": "Business Development/Commercial Director",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Taavi",
    "lastname": "Rehemägi",
    "email": "",
    "company": "Dashbird",
    "position": "Co-Founder",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Markus",
    "lastname": "Schranner",
    "email": "",
    "company": "s2m - Innovationsmanagement & IT GmbH",
    "position": "Geschäftsführender Inhaber",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Marcia",
    "lastname": "Schranner",
    "email": "",
    "company": "s2m - Innovationsmanagement & IT GmbH",
    "position": "Geschäftsführender Gesellschafter",
    "connect_date": "16 Nov 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Harrison",
    "email": "",
    "company": "4DI",
    "position": "Building & Recruiting Technical Leaders",
    "connect_date": "15 Nov 2018"
  },
  {
    "firstname": "Jake",
    "lastname": "Foley",
    "email": "",
    "company": "X-Team",
    "position": "Chief Growth Officer (CGO)",
    "connect_date": "15 Nov 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Lennon",
    "email": "",
    "company": "Hanover Recruitment",
    "position": "Director",
    "connect_date": "15 Nov 2018"
  },
  {
    "firstname": "Lyubomyr",
    "lastname": "Reverchuk",
    "email": "",
    "company": "EchoUA: Hire Ukrainian Developers",
    "position": "Founder & CEO",
    "connect_date": "15 Nov 2018"
  },
  {
    "firstname": "Klaus",
    "lastname": "Akselsen",
    "email": "",
    "company": "MIRSK",
    "position": "CTO",
    "connect_date": "14 Nov 2018"
  },
  {
    "firstname": "Mary",
    "lastname": "Ryzhyk",
    "email": "",
    "company": "EchoUA",
    "position": "Recruiter",
    "connect_date": "14 Nov 2018"
  },
  {
    "firstname": "Hanna Kristine",
    "lastname": "Markeng Edland",
    "email": "",
    "company": "Marketer Technologies",
    "position": "Campaign Manager",
    "connect_date": "13 Nov 2018"
  },
  {
    "firstname": "Joeri",
    "lastname": "Lieten",
    "email": "",
    "company": "Payconiq Belgium",
    "position": "CEO",
    "connect_date": "13 Nov 2018"
  },
  {
    "firstname": "James",
    "lastname": "Brooks",
    "email": "",
    "company": "Rue Gilt Groupe",
    "position": "Sr. UX Designer",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Brendan P.",
    "lastname": "Keegan",
    "email": "",
    "company": "Merchants Fleet Management",
    "position": "Chief Executive Officer",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Doug",
    "lastname": "Wallace",
    "email": "",
    "company": "Voya Financial",
    "position": "Business Development Director",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Александрина",
    "lastname": "Сайко",
    "email": "",
    "company": "КА ШАГ Одесса",
    "position": "Менеджер по персоналу",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Claudia Raffay",
    "lastname": "MBA, PHR",
    "email": "",
    "company": "Sussex Rural Electric Cooperative",
    "position": "Director of Marketing and Member Services",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Andre",
    "lastname": "Holdschick",
    "email": "",
    "company": "Stealth-Startup",
    "position": "Founder",
    "connect_date": "12 Nov 2018"
  },
  {
    "firstname": "Linda",
    "lastname": "Winfield",
    "email": "",
    "company": "HealthcareSource",
    "position": "Group Product Manager",
    "connect_date": "11 Nov 2018"
  },
  {
    "firstname": "Maryna",
    "lastname": "Molchan",
    "email": "",
    "company": "Trembit",
    "position": "Chief HR/People Officer",
    "connect_date": "10 Nov 2018"
  },
  {
    "firstname": "Sumith",
    "lastname": "Damodaran",
    "email": "",
    "company": "Sitecore",
    "position": "Sr. Product Manager - Experience Platform",
    "connect_date": "10 Nov 2018"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Moran",
    "email": "",
    "company": "Chroma Recruitment Limited",
    "position": "Recruiter",
    "connect_date": "09 Nov 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Trela",
    "email": "",
    "company": "Iron Mountain",
    "position": "Director, Cloud Product Management",
    "connect_date": "09 Nov 2018"
  },
  {
    "firstname": "Oksana",
    "lastname": "Lesnik",
    "email": "",
    "company": "ING Bank N.V, London Branch",
    "position": "ING PtopTech Lab",
    "connect_date": "09 Nov 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Hazlewood",
    "email": "",
    "company": "SCIEX",
    "position": "Sr Director, Software Strategy and Business Development",
    "connect_date": "09 Nov 2018"
  },
  {
    "firstname": "Grant",
    "lastname": "Bartel",
    "email": "",
    "company": "X-Team",
    "position": "Ambassador, Vetting Team",
    "connect_date": "09 Nov 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Foreshew-Cain",
    "email": "",
    "company": "ThoughtWorks",
    "position": "Chief Operating Officer (UK)",
    "connect_date": "08 Nov 2018"
  },
  {
    "firstname": "David S",
    "lastname": "Josef",
    "email": "",
    "company": "Startup in the Telecommunication domain.",
    "position": "Founder / CEO- Startup.",
    "connect_date": "08 Nov 2018"
  },
  {
    "firstname": "Asif Shatil",
    "lastname": "Hossain",
    "email": "",
    "company": "AMD",
    "position": "Business Development Manager",
    "connect_date": "08 Nov 2018"
  },
  {
    "firstname": "Roberto",
    "lastname": "Brandão",
    "email": "",
    "company": "AMD",
    "position": "General Director and Country Manager",
    "connect_date": "08 Nov 2018"
  },
  {
    "firstname": "Jordan",
    "lastname": "Levy",
    "email": "",
    "company": "CapSource (Capstone Source)",
    "position": "Executive Director",
    "connect_date": "07 Nov 2018"
  },
  {
    "firstname": "Radek",
    "lastname": "Zaleski",
    "email": "",
    "company": "netguru",
    "position": "Head of Growth",
    "connect_date": "06 Nov 2018"
  },
  {
    "firstname": "David",
    "lastname": "Mackay",
    "email": "",
    "company": "TeamSnap",
    "position": "Sr. Manager of Digital Media - Advertising",
    "connect_date": "06 Nov 2018"
  },
  {
    "firstname": "Wiktor",
    "lastname": "Schmidt",
    "email": "",
    "company": "Netguru",
    "position": "CEO",
    "connect_date": "06 Nov 2018"
  },
  {
    "firstname": "Becky",
    "lastname": "Teague",
    "email": "",
    "company": "Progressive Recruitment",
    "position": "Senior Consultant",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Jane",
    "lastname": "Cowie",
    "email": "",
    "company": "Opus Recruitment Solutions",
    "position": "Contract Recruitment Consultant (PHP / JavaScript)",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Gabriela",
    "lastname": "Cinal",
    "email": "",
    "company": "iBasis",
    "position": "Business Development Representative – Americas",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Rahimah Lailah",
    "lastname": "Ahmad",
    "email": "",
    "company": "MEDITECH",
    "position": "Clinical Informatics Product Manager",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Ruben",
    "lastname": "Miessen",
    "email": "",
    "company": "ZetaPulse",
    "position": "Co-Founder & Managing Director",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Rocco",
    "lastname": "Bortone",
    "email": "",
    "company": "Rave Mobile Safety",
    "position": "Director of IT Operations",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Allen",
    "lastname": "Dickerson",
    "email": "",
    "company": "ASP Technologies, Inc.",
    "position": "President Owner Founder CEO",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "O'Hare",
    "email": "",
    "company": "MobileMonkey",
    "position": "Chief Operations Officer",
    "connect_date": "05 Nov 2018"
  },
  {
    "firstname": "Joy",
    "lastname": "Banerjee",
    "email": "",
    "company": "Liberty General Insurance",
    "position": "Senior Vice President and Head Learning & Development",
    "connect_date": "04 Nov 2018"
  },
  {
    "firstname": "Kevin",
    "lastname": "Merisanu",
    "email": "",
    "company": "Creators Never Die Inc.",
    "position": "Product Manager",
    "connect_date": "03 Nov 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Carney",
    "email": "",
    "company": "Globus Medical",
    "position": "Spine Specialist",
    "connect_date": "03 Nov 2018"
  },
  {
    "firstname": "J.",
    "lastname": "R.",
    "email": "",
    "company": "Genesis Capital",
    "position": "Chief Investment Officer",
    "connect_date": "02 Nov 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Pütz",
    "email": "",
    "company": "Tyin Arkitekter AS og Tyin Totalentreprenør AS",
    "position": "Styreleder",
    "connect_date": "02 Nov 2018"
  },
  {
    "firstname": "Larry",
    "lastname": "Foster",
    "email": "",
    "company": "The TJX Companies, Inc.",
    "position": "Senior Vice President, Infrastructure and Operations",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Antony",
    "lastname": "Rafter",
    "email": "",
    "company": "Центр Рафтингу",
    "position": "Sales and events generalist",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Ashok",
    "lastname": "Balasubramanian",
    "email": "",
    "company": "Syntel",
    "position": "Vice President and Head - Services Transformation Group",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Luke",
    "lastname": "Hollis",
    "email": "",
    "company": "Archimedes Digital",
    "position": "Founder / CEO",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Akash",
    "lastname": "Chopra",
    "email": "",
    "company": "Syntel",
    "position": "Engagement Director",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Conrad",
    "lastname": "Meneide",
    "email": "",
    "company": "Aetna, a CVS Health Company",
    "position": "Executive Director, Affiliate Infrastructure",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Bassett",
    "email": "",
    "company": "Linedata",
    "position": "Senior Vice President, Global Head of Client Services",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Solly",
    "lastname": "Solomou",
    "email": "",
    "company": "LADbible Group",
    "position": "Founder & CEO",
    "connect_date": "01 Nov 2018"
  },
  {
    "firstname": "Boris",
    "lastname": "Eibelman",
    "email": "",
    "company": "Data Pro Software Solutions",
    "position": "CEO – Custom Software Development That Deliver Results",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Hans",
    "lastname": "Massie",
    "email": "",
    "company": "ecoText, Inc",
    "position": "Chief Information Officer",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Zutz",
    "email": "",
    "company": "Cognex Corporation",
    "position": "Mobile Sales Engineer Europe",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Jordan",
    "lastname": "Pasternak",
    "email": "",
    "company": "Upright NYC",
    "position": "Co-founder, Director Of Business Development",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Jodi",
    "lastname": "Gornick",
    "email": "",
    "company": "CGI",
    "position": "Director, Product Development & Support",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Mircea Mihai",
    "lastname": "Costache",
    "email": "",
    "company": "Orange",
    "position": "DevOps",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Enzo",
    "lastname": "Micali",
    "email": "",
    "company": "The TJX Companies, Inc.",
    "position": "Senior Vice President, Digital Solution Delivery",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Jennifer",
    "lastname": "Moran",
    "email": "",
    "company": "MEDITECH",
    "position": "Project Coordinator",
    "connect_date": "31 Oct 2018"
  },
  {
    "firstname": "Rogier",
    "lastname": "Warnawa",
    "email": "",
    "company": "Warnax",
    "position": "Software Development & Project Management",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Lucious",
    "lastname": "McDaniel III",
    "email": "",
    "company": "TJX",
    "position": "Regional Vice President",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Bezzubenkov",
    "email": "",
    "company": "Dev Team Inc",
    "position": "Co Founder and Team Lead",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Calvin",
    "lastname": "Smith",
    "email": "",
    "company": "Wipro Digital",
    "position": "Director & Global Head of IoT Partner Engineering",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Rusha",
    "lastname": "Sopariwala",
    "email": "",
    "company": "EnergySage",
    "position": "Director, User Experience & Design • Founding member",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Ellie",
    "lastname": "Starr",
    "email": "",
    "company": "Museum of Science, Boston",
    "position": "Senior Vice President, Advancement",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "Kathleen",
    "lastname": "Warner, PhD, CPM",
    "email": "",
    "company": "RCM Technologies",
    "position": "Vice President",
    "connect_date": "30 Oct 2018"
  },
  {
    "firstname": "John",
    "lastname": "Crissey",
    "email": "",
    "company": "Sussex Rural Electric Cooperative",
    "position": "Manager of Information Technology",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Hill",
    "email": "",
    "company": "MathWorks",
    "position": "Senior Technical Recruiter",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Austin",
    "lastname": "Harvey",
    "email": "",
    "company": "Soft Robotics Inc.",
    "position": "Sr. Product Manager",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Patrick",
    "lastname": "Walsh",
    "email": "",
    "company": "National Grid",
    "position": "Director, National Grid Partners",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Tomas",
    "lastname": "Gorny",
    "email": "",
    "company": "Nextiva",
    "position": "CEO & Chairman",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Ron",
    "lastname": "Trentini, CPA",
    "email": "",
    "company": "Startup Financial Solutions",
    "position": "Principal",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Skip",
    "lastname": "Maloney",
    "email": "",
    "company": "Aspen Technology",
    "position": "SVP & Chief Human Resource Officer",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Bill",
    "lastname": "C. Dargie, MS",
    "email": "",
    "company": "The TJX Companies, Inc.",
    "position": "Assistant Vice President, Director of Data & Analytics",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "James",
    "lastname": "Zensky",
    "email": "",
    "company": "HCA",
    "position": "Director of Business Growth",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Jay",
    "lastname": "Compton",
    "email": "",
    "company": "Aetna",
    "position": "Principal Digital and Cloud Architect",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Evgeniy",
    "lastname": "Orehov",
    "email": "",
    "company": "ООО Управдом",
    "position": "CEO",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Hans",
    "lastname": "Dahlström",
    "email": "",
    "company": "Favro",
    "position": "Co-Founder and Chief product officer",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Swafford",
    "email": "",
    "company": "XLR8 Athletix",
    "position": "Owner",
    "connect_date": "29 Oct 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Flanigan",
    "email": "",
    "company": "Datto, Inc.",
    "position": "Vice President Of Product Management",
    "connect_date": "28 Oct 2018"
  },
  {
    "firstname": "Jan-Christian",
    "lastname": "Skjaeggerud",
    "email": "",
    "company": "Playces",
    "position": "Chief Executive Officer",
    "connect_date": "28 Oct 2018"
  },
  {
    "firstname": "Chirag",
    "lastname": "Suthar",
    "email": "",
    "company": "Syntel Inc. Ltd, Pune.",
    "position": "Delivery Director",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Volodymyr",
    "lastname": "Bryskin",
    "email": "",
    "company": "CourseYard GmbH",
    "position": "Managing Partner",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Ferri",
    "email": "",
    "company": "National Grid",
    "position": "Director - External Innovation (Tech & Venture Scouting)",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Evangelos",
    "lastname": "PAPADOPOULOS",
    "email": "",
    "company": "TheMarketsTrust",
    "position": "Founder & CEO",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Øwre",
    "email": "",
    "company": "TECHPROS AS",
    "position": "Co-founder & CEO",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Mads",
    "lastname": "Viborg Jørgensen",
    "email": "",
    "company": "Company42",
    "position": "CTO, Co-founder & Head of product",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Shell",
    "lastname": "Riehlin",
    "email": "",
    "company": "Chewy",
    "position": "Technical Product Manager",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Guowei",
    "lastname": "Wang",
    "email": "",
    "company": "AspenTech China",
    "position": "Country Manager",
    "connect_date": "27 Oct 2018"
  },
  {
    "firstname": "Mariah",
    "lastname": "Obiedzinski",
    "email": "",
    "company": "Stamats",
    "position": "Director of Content Services",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Alessandro",
    "lastname": "Petroni",
    "email": "",
    "company": "Red Hat",
    "position": "Director - Global Head of Strategy and Solutions for Financials Services",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Sabastine",
    "lastname": "Okeke (PhD, MBA, FCMI)",
    "email": "",
    "company": "The Predictive Index",
    "position": "Certified Partner",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Mohamad",
    "lastname": "Ali",
    "email": "",
    "company": "iRobot",
    "position": "Member Board of Directors",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Philipp",
    "lastname": "Winkmann",
    "email": "",
    "company": "Liberty Acoustics UG (haftungsbeschränkt)",
    "position": "Co-Founder & CEO",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Aaron",
    "lastname": "Hess",
    "email": "",
    "company": "The TJX Companies, Inc.",
    "position": "Vice President Mountain West Region TJ Maxx",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "Mahoney",
    "email": "",
    "company": "Payfactors",
    "position": "Business Development Representative",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Gregory",
    "lastname": "Stoner",
    "email": "",
    "company": "Intel Corporation",
    "position": "CTO Software Visual Computing",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Corey",
    "lastname": "Snow ☁️ ☄",
    "email": "",
    "company": "Harvard University",
    "position": "CRM Strategist | Solution Architect",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Beletsky",
    "email": "",
    "company": "Strathmore Advisors",
    "position": "Principal",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "JOSEPH GORDON",
    "lastname": "CLEVELAND",
    "email": "",
    "company": "Rue Gilt Groupe",
    "position": "Art Director",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Sven",
    "lastname": "Klockmann",
    "email": "",
    "company": "Cognex Corporation",
    "position": "EMEA Sales Manager - ID Mobile",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "John",
    "lastname": "Biggs",
    "email": "",
    "company": "Jaywalk",
    "position": "Co-Founder, CMO",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Renaud",
    "lastname": "Teasdale",
    "email": "",
    "company": "MyCustomizer",
    "position": "Co-founder and CEO at MyCustomizer",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Kate",
    "lastname": "Hurd",
    "email": "",
    "company": "SilverCloud",
    "position": "Product Manager",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Sverdlik",
    "email": "",
    "company": "Xenoss",
    "position": "CEO and Founder",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Stas",
    "lastname": "Borukhoff",
    "email": "",
    "company": "MobiLine, Inc",
    "position": "CEO & Founder",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Vinod",
    "lastname": "Parthasarathy",
    "email": "",
    "company": "Medtronic",
    "position": "Sr Director, Global Monitoring Operations (Clinical research)",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Rasmus",
    "lastname": "Hjulskov Larsen",
    "email": "",
    "company": "Peliba",
    "position": "Co-founder",
    "connect_date": "26 Oct 2018"
  },
  {
    "firstname": "Adebayo",
    "lastname": "Alonge",
    "email": "",
    "company": "RxAll Inc.",
    "position": "Co-Founder/CEO",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Joseph",
    "lastname": "Cenatiempo",
    "email": "",
    "company": "Cena Technology",
    "position": "Chief Executive Officer",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Beaumont",
    "email": "",
    "company": "Social Magnetics",
    "position": "Co-founder, CEO and Chief Software Architect",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Geary",
    "email": "",
    "company": "WordStream",
    "position": "Senior Account Executive",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Max",
    "lastname": "Tokarsky",
    "email": "",
    "company": "InvestAcure",
    "position": "Founder & CEO",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Greg",
    "lastname": "Barnett, Ph.D.",
    "email": "",
    "company": "The Predictive Index",
    "position": "Senior Vice President, Science",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Suzanne",
    "lastname": "Ellis",
    "email": "",
    "company": "Private Jet Services",
    "position": "Senior Dir. Bus. Development",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Cooke",
    "email": "",
    "company": "Linedata",
    "position": "Director, Client Solutions",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Jitesh",
    "lastname": "Midha",
    "email": "",
    "company": "Confidential",
    "position": "Entrepreneur / CIO / Technology Leader | IT Head | Business Enabler",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Diamantopoulos",
    "email": "",
    "company": "UL",
    "position": "General Manager Global Accounts",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Amr",
    "lastname": "Hafez",
    "email": "",
    "company": "Rudolph Technologies",
    "position": "Director of Software Engineering, Lithography Systems Group",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Bob",
    "lastname": "Hallahan",
    "email": "",
    "company": "Comcast Cable",
    "position": "Sr. Director, Nextgen Architecture",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Candice",
    "lastname": "Groth",
    "email": "",
    "company": "Merchants Fleet Management",
    "position": "Director of Operations - Purchasing",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Kathrine",
    "lastname": "Gorman",
    "email": "",
    "company": "EIMS",
    "position": "Team Manager",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Alexandra",
    "lastname": "Ryan",
    "email": "",
    "company": "Rue Gilt Groupe",
    "position": "Senior Director, Styling and Brand",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Lana",
    "lastname": "Holmen",
    "email": "",
    "company": "Reputation Transfer",
    "position": "Co-Founder and CPO",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Elliot",
    "lastname": "Jones",
    "email": "",
    "company": "Venturi Ltd",
    "position": "Senior Recruitment Consultant",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Thach",
    "lastname": "Nguyen",
    "email": "",
    "company": "Care/of",
    "position": "Head of People",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Iggy",
    "lastname": "Gullstrand",
    "email": "",
    "company": "Shiftkey People",
    "position": "Co-Founder and CEO",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Christensen",
    "email": "",
    "company": "Upodi",
    "position": "CTO, CoFounder",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Bill",
    "lastname": "Welch",
    "email": "",
    "company": "iBasis",
    "position": "Product Manager Mobile Innovations",
    "connect_date": "25 Oct 2018"
  },
  {
    "firstname": "Wagner Paiva",
    "lastname": "Pinto",
    "email": "",
    "company": "Comcast",
    "position": "Technical Product Manager | Business Solution Architect",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "John",
    "lastname": "Hague",
    "email": "",
    "company": "Aspen Technology",
    "position": "Sr. Vice President and GM, APM Business Unit",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Chad",
    "lastname": "Fowler",
    "email": "",
    "company": "Microsoft",
    "position": "General Manager, Developer Advocacy",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Dagfinn",
    "lastname": "Kolstad",
    "email": "",
    "company": "Teleplan Globe",
    "position": "CEO",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Curt",
    "lastname": "Raffi",
    "email": "",
    "company": "Endurance International Group",
    "position": "SVP & General Manager, Innovation Products",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Kira",
    "lastname": "McKelvey",
    "email": "",
    "company": "Datto, Inc.",
    "position": "Partner Marketing Coordinator",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Gerber",
    "email": "",
    "company": "Agilent Technologies",
    "position": "Senior Vice President, Strategy and Corporate Development",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Usama",
    "lastname": "Masood",
    "email": "",
    "company": "Payfactors",
    "position": "Senior Business Development Representative",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Henrik",
    "lastname": "Fabrin",
    "email": "",
    "company": "BotXO | Chatbot platform for businesses",
    "position": "Founder & CEO",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Brad",
    "lastname": "Shipp",
    "email": "",
    "company": "Voya Financial",
    "position": "VP of Information Techolgy @Voya® | IT Operations | Infrastructure | End User | Network",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Sergio",
    "lastname": "Bastos",
    "email": "",
    "company": "RedRabbit Project - Open Source  / Boston Scientific",
    "position": "Agile Technical Product Manager – Feature management.",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Godden",
    "email": "",
    "company": "Foundation Medicine",
    "position": "Chief Information Officer",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Schmücker",
    "email": "",
    "company": "56°North Software (56N Software AB)",
    "position": "Co Founder, Partner",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Berg",
    "lastname": "Moe",
    "email": "",
    "company": "EntraHouse AS",
    "position": "Partner & CEO",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Maksymilian",
    "lastname": "Majer",
    "email": "",
    "company": "ITCraftship",
    "position": "CEO",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Varun",
    "lastname": "Nair",
    "email": "",
    "company": "Syntel Inc",
    "position": "Specialist - Advisor and Analyst Relations",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Sean",
    "lastname": "Drummy",
    "email": "",
    "company": "Vee24",
    "position": "Director, Client Success",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Sajag",
    "lastname": "'Kumar",
    "email": "",
    "company": "HCL Technologies",
    "position": "Senior Director of Business Development- HiTech Industry",
    "connect_date": "24 Oct 2018"
  },
  {
    "firstname": "Charlotte",
    "lastname": "Hamill",
    "email": "",
    "company": "Bracebridge Capital, LLC",
    "position": "Managing Director",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Scott",
    "lastname": "Brook",
    "email": "",
    "company": "Foundation Medicine",
    "position": "Director, Technology Platforms",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Nancy",
    "lastname": "Drees",
    "email": "",
    "company": "Vacaré Group",
    "position": "CEO & Founder @ Vacare Group",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Danielle",
    "lastname": "Ferreira",
    "email": "",
    "company": "Wilson Language Training",
    "position": "Director of Plan Implementation",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Christensen",
    "email": "",
    "company": "Hitachi Vantara",
    "position": "CTO and Customer Advocacy, Northern EMEA",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Lynn",
    "lastname": "Robbins",
    "email": "",
    "company": "HCA",
    "position": "Director of Marketing and Public Relations, Portsmouth Regional Hospital",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Tyrrell",
    "email": "",
    "company": "AcadiaSoft",
    "position": "Information Security Manager",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Rickard",
    "lastname": "Norregårdh",
    "email": "",
    "company": "Hyper One AB",
    "position": "Founder",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Jørgen",
    "lastname": "Svennevik Notland",
    "email": "",
    "company": "Flare",
    "position": "Co-Founder & CTO",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Serfaty",
    "email": "",
    "company": "Aptima, Inc.",
    "position": "Chairman & CEO",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Edmund",
    "lastname": "Zaloga",
    "email": "",
    "company": "Responsify",
    "position": "Founder & CEO",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Marichal",
    "lastname": "MacDonald",
    "email": "",
    "company": "Care/of",
    "position": "Recruiting Lead",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Josef",
    "lastname": "Falk",
    "email": "",
    "company": "Echo State AB",
    "position": "CEO / VD",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Johan Philip",
    "lastname": "Düfke",
    "email": "",
    "company": "Younicon Studio",
    "position": "Co-Founder & Partner",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Navjit",
    "lastname": "Dhaliwal",
    "email": "",
    "company": "Iagon",
    "position": "Founder, CEO",
    "connect_date": "23 Oct 2018"
  },
  {
    "firstname": "Zachary",
    "lastname": "Samiljan",
    "email": "",
    "company": "Datto, Inc.",
    "position": "Enterprise Account Executive | East Coast",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Gang",
    "lastname": "Wu",
    "email": "",
    "company": "Medtronic",
    "position": "Director of Software and System Engineering",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Frank",
    "lastname": "Moberg",
    "email": "",
    "company": "Octaos AS",
    "position": "CEO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Benjamin",
    "lastname": "Mohlie",
    "email": "",
    "company": "Mendix",
    "position": "Director of Strategic Alliances",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Till A.",
    "lastname": "Kaestner",
    "email": "",
    "company": "Artisense Corporation",
    "position": "Geschäftsführer; COO of Corporation",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Torgeir",
    "lastname": "Letting",
    "email": "",
    "company": "Compello",
    "position": "CEO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Erland",
    "lastname": "Bakke",
    "email": "",
    "company": "TL Salonger AS",
    "position": "Administrerende Direktør",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Nessler",
    "email": "",
    "company": "Solv",
    "position": "CEO at Solv AS",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Saponar",
    "email": "",
    "company": "iBasis, Inc",
    "position": "Chief Information Officer",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Roman",
    "lastname": "Rodomansky",
    "email": "",
    "company": "Ralabs",
    "position": "Co-Founder, Chief Technology Officer",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Ove",
    "lastname": "Vik",
    "email": "",
    "company": "Erate AS",
    "position": "CEO / Founder",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Henry",
    "lastname": "Knoblock",
    "email": "",
    "company": "HITECHLAW Group",
    "position": "Member",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Dyring-Andersen",
    "email": "",
    "company": "e-Money A/S",
    "position": "CEO & Founder",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Ashok",
    "lastname": "Dudhat",
    "email": "",
    "company": "Tech Jobs Fair",
    "position": "Founder & CTO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Franc",
    "lastname": "Bozic",
    "email": "",
    "company": "ORBIRAMA",
    "position": "Co-founder, CTO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Denisa",
    "lastname": "Pantilimon",
    "email": "",
    "company": "Stelfox",
    "position": "IT Recruitment Consultant",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Kenneth",
    "lastname": "Wagenius",
    "email": "",
    "company": "BankBridge",
    "position": "CTO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Sebastian",
    "lastname": "Nause-Blueml",
    "email": "",
    "company": "Scoutbase",
    "position": "Co-Founder & Service Designer",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Sofie",
    "lastname": "Woge",
    "email": "",
    "company": "Tendo AB",
    "position": "Founder & CEO",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Marcel",
    "lastname": "Krehbiel",
    "email": "",
    "company": "SAP",
    "position": "Business Development SAP Innovation Center",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Johannes",
    "lastname": "Marseth",
    "email": "",
    "company": "Vainu.io",
    "position": "Account Manager",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Hovaldt",
    "email": "",
    "company": "Nordea",
    "position": "Business Partner at Startup & Growth",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Narve",
    "lastname": "Hansen",
    "email": "",
    "company": "Vipps",
    "position": "Chief Innovation Officer - International Department",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Hermann",
    "lastname": "Kudlich",
    "email": "",
    "company": "Startup, Edtech",
    "position": "Co-Founder",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Ignatz",
    "lastname": "Schatz",
    "email": "",
    "company": "SAP",
    "position": "Director Startup Engagement, SAP IoT Accelerator",
    "connect_date": "22 Oct 2018"
  },
  {
    "firstname": "Bjorn-Arild",
    "lastname": "Woll",
    "email": "",
    "company": "Foreningen Et Hjerte av gull",
    "position": "Grunnlegger",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Ricardo",
    "lastname": "Parro",
    "email": "",
    "company": "Tradeshift",
    "position": "Director Of Engineering",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Ross",
    "lastname": "Cusack",
    "email": "",
    "company": "The KCS Academy - the Only Authorized Certifying Body for KCS",
    "position": "Salesforce Administrator (Contracted)",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Sanjay",
    "lastname": "Jaiman",
    "email": "",
    "company": "Abside Networks, Inc.",
    "position": "Member Of Technical Staff",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Stian",
    "lastname": "Broen",
    "email": "",
    "company": "Broentech Solutions AS",
    "position": "CEO",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Stephan",
    "lastname": "Heller",
    "email": "",
    "company": "FinCompare GmbH",
    "position": "Founder / CEO",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Sveum",
    "email": "",
    "company": "Startupmatcher",
    "position": "CEO & Co-founder",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Nikolai",
    "lastname": "Erland",
    "email": "",
    "company": "Techchat",
    "position": "CEO and Founder",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "James",
    "lastname": "Walker",
    "email": "",
    "company": "C-LevelClone/Total Startup Solutions",
    "position": "Chairman/Co-Founder",
    "connect_date": "21 Oct 2018"
  },
  {
    "firstname": "Francesco",
    "lastname": "Guidara",
    "email": "",
    "company": "Università degli Studi di Roma 'La Sapienza'",
    "position": "Contract Professor of Governance Disclosure and Capital Markets",
    "connect_date": "20 Oct 2018"
  },
  {
    "firstname": "Madhukar",
    "lastname": "Rao",
    "email": "",
    "company": "Epsilon",
    "position": "Senior Director - Digital Technology",
    "connect_date": "20 Oct 2018"
  },
  {
    "firstname": "Sten Roger",
    "lastname": "Sandvik",
    "email": "",
    "company": "Payr",
    "position": "CTO",
    "connect_date": "19 Oct 2018"
  },
  {
    "firstname": "Luke",
    "lastname": "Mannion",
    "email": "",
    "company": "ITECCO",
    "position": "Senior Recruitment Consultant at ITECCO",
    "connect_date": "19 Oct 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Love",
    "email": "",
    "company": "UiPath",
    "position": "Presales Technical Consultant",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Ryan",
    "lastname": "Donovan",
    "email": "",
    "company": "Edgy Couture",
    "position": "Owner/CEO",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Joe",
    "lastname": "Madsen",
    "email": "",
    "company": "Nordstrom",
    "position": "Technical Sourcer",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Preston",
    "lastname": "Junger",
    "email": "pjunger@gmail.com",
    "company": "Mile Square Labs",
    "position": "Co-Founder, Managing Partner",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Catherine",
    "lastname": "Seiferth",
    "email": "",
    "company": "Odyssey Systems Consulting",
    "position": "CTO",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Bojan",
    "lastname": "Eric",
    "email": "",
    "company": "SkyHighGrowth",
    "position": "B2B Sales Consultant",
    "connect_date": "18 Oct 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Monaghan",
    "email": "",
    "company": "Datadog",
    "position": "Technical Recruiting Lead",
    "connect_date": "17 Oct 2018"
  },
  {
    "firstname": "Mithlesh",
    "lastname": "mithlesh@inficaretech.com",
    "email": "",
    "company": "InfiCare Technologies",
    "position": "Director - IT Staffing / Consulting Services",
    "connect_date": "17 Oct 2018"
  },
  {
    "firstname": "Diana",
    "lastname": "Rajkumari",
    "email": "",
    "company": "Dilato Infotech - Software Testing Services",
    "position": "Account Manager",
    "connect_date": "17 Oct 2018"
  },
  {
    "firstname": "Krzysztof",
    "lastname": "Danielewicz",
    "email": "",
    "company": "HubSpot",
    "position": "Senior Software Engineer",
    "connect_date": "17 Oct 2018"
  },
  {
    "firstname": "Alexandra",
    "lastname": "Piccirilli",
    "email": "",
    "company": "Madavor Media",
    "position": "Senior Media Solutions Manager",
    "connect_date": "17 Oct 2018"
  },
  {
    "firstname": "David",
    "lastname": "Vorderer",
    "email": "",
    "company": "Arrowstreet Capital, Limited Partnership",
    "position": "Account Service Associate",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Sasidhar",
    "lastname": "Nayudu",
    "email": "",
    "company": "CGI",
    "position": "Vice President",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Michelle",
    "lastname": "Smith",
    "email": "",
    "company": "SilverCloud, Inc.",
    "position": "Strategic Account Executive",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Brandon",
    "lastname": "Apodaca",
    "email": "",
    "company": "ANSYS, Inc.",
    "position": "Account Manager",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Rita",
    "lastname": "Horner",
    "email": "ritahorner@yahoo.com",
    "company": "Synopsys",
    "position": "Senior Technical Marketing Manager, Analog/Mixed Signal IP",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "DiGeronimo",
    "email": "",
    "company": "Sharecare",
    "position": "Sr. Inside Sales Representative",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Govind",
    "lastname": "Seshadri",
    "email": "",
    "company": "Epsilon",
    "position": "Vice President Of Technology",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Olav",
    "lastname": "Madland",
    "email": "",
    "company": "Applied Autonomy AS",
    "position": "CEO",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Hazem",
    "lastname": "Kanafani",
    "email": "",
    "company": "KldEra",
    "position": "Business Development Manager  MENA ( Database, SaaS, E-learning )",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Mads Ragnvald",
    "lastname": "Nielsen",
    "email": "",
    "company": "Scoutbase",
    "position": "Human Factors & Systems Safety specialist & Co-Founder",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Matthew",
    "lastname": "Frasier",
    "email": "",
    "company": "Prepp",
    "position": "Sales Consultant - North America",
    "connect_date": "16 Oct 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "O'Donnell",
    "email": "",
    "company": "Cognex Corporation",
    "position": "Senior Director Global IT",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Kevin",
    "lastname": "Roy",
    "email": "",
    "company": "Green Banana SEO",
    "position": "Co Founder",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Adriana Eugenia",
    "lastname": "Aguilar Ordonez",
    "email": "",
    "company": "Huddol",
    "position": "Director of Partner Developement",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Yariv",
    "lastname": "Tabac",
    "email": "",
    "company": "DBmaestro",
    "position": "Co-Founder & CEO",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Maria",
    "lastname": "Östlund",
    "email": "",
    "company": "Evoque Media",
    "position": "Redaktör",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Fredrik",
    "lastname": "Johansson",
    "email": "",
    "company": "Nordea",
    "position": "Relationship Manager",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Raymond",
    "lastname": "Øverlie",
    "email": "",
    "company": "Bull & Bear Trading AS",
    "position": "CIO",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Madhuri",
    "lastname": "Nainani",
    "email": "",
    "company": "Vertex Solutions International Ltd",
    "position": "Technical Consultant",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Patrik",
    "lastname": "Sjölander",
    "email": "",
    "company": "Aefe StartUp Media AB",
    "position": "Co-founder",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Schörner",
    "email": "",
    "company": "SAP",
    "position": "eXperience Center Technologist & Ambassador",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Zappala",
    "email": "",
    "company": "Infinity Consulting Solutions",
    "position": "IT Account Manager",
    "connect_date": "15 Oct 2018"
  },
  {
    "firstname": "Carsten Brinch",
    "lastname": "Larsen",
    "email": "",
    "company": "Startup, EdTech",
    "position": "Co-Founder",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "André",
    "lastname": "Hellström",
    "email": "",
    "company": "Northvolt",
    "position": "Talent Acquisition Manager",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Rowland",
    "lastname": "Northe",
    "email": "",
    "company": "Capital One",
    "position": "Technical Recruiter",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Joanna",
    "lastname": "De La Via",
    "email": "",
    "company": "Solution Street LLC",
    "position": "Hiring Manager",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Richard (Rik)",
    "lastname": "Goodwin, Ph.D.",
    "email": "",
    "company": "impact42",
    "position": "Founder,  Managing Director",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Kogut",
    "email": "",
    "company": "Fidelity Investments",
    "position": "Director Of Development, Fidelity Clearing Canada",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Rowland",
    "lastname": "Hobbs",
    "email": "",
    "company": "Stake",
    "position": "Co-Founder",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Janine",
    "lastname": "Lacey",
    "email": "",
    "company": "Rangle.io",
    "position": "Business Development Associate",
    "connect_date": "14 Oct 2018"
  },
  {
    "firstname": "Jorge",
    "lastname": "Barnaby",
    "email": "",
    "company": "Medallia",
    "position": "Staff Software Engineer",
    "connect_date": "13 Oct 2018"
  },
  {
    "firstname": "Jim",
    "lastname": "Nolan",
    "email": "",
    "company": "Interactions Corporation",
    "position": "VP, Professional Services",
    "connect_date": "13 Oct 2018"
  },
  {
    "firstname": "Shawn",
    "lastname": "Mazza",
    "email": "",
    "company": "Spruce Technology, Inc.",
    "position": "Business Development - New England Market",
    "connect_date": "13 Oct 2018"
  },
  {
    "firstname": "Aurélie",
    "lastname": "JEAN, Ph.D.",
    "email": "",
    "company": "In Silico Veritas (ISV)",
    "position": "Founder and CEO",
    "connect_date": "13 Oct 2018"
  },
  {
    "firstname": "Liang",
    "lastname": "Mou",
    "email": "",
    "company": "Lyft",
    "position": "Software Engineer",
    "connect_date": "13 Oct 2018"
  },
  {
    "firstname": "Wilson",
    "lastname": "Ho",
    "email": "",
    "company": "Kenetic",
    "position": "Vice President, Operations",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Tyler",
    "lastname": "Bainbridge",
    "email": "",
    "company": "Conduit Analytics",
    "position": "Director of Product Engineering",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "David",
    "lastname": "Spicer",
    "email": "",
    "company": "IdentityJS",
    "position": "Founder",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Sung Yoon",
    "lastname": "Whang",
    "email": "",
    "company": "Microsoft",
    "position": "Software Engineer",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Chiranjeev",
    "lastname": "Lenka",
    "email": "",
    "company": "Net2Source Inc.",
    "position": "Talent Acquisition Specialist",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Nikki",
    "lastname": "Bost",
    "email": "",
    "company": "Velcro Companies",
    "position": "Inside Sales Manager",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Selvan",
    "lastname": "Rajan",
    "email": "",
    "company": "SRS Consulting, LLC",
    "position": "CTO/Architect/Engineer",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Walter",
    "email": "",
    "company": "Aspen Technology",
    "position": "Vice President, Global Partners",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Lappin",
    "email": "",
    "company": "New England Safety Partners",
    "position": "Director Security Services",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Drahun",
    "email": "",
    "company": "Flex",
    "position": "Director Program Management",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Eduard",
    "lastname": "de Vries, MBA",
    "email": "",
    "company": "McKinsey & Company",
    "position": "Senior advisor",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Emily",
    "lastname": "Viana",
    "email": "",
    "company": "California Pizza Kitchen",
    "position": "Customer Service Representative",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Nikhil",
    "lastname": "Rai",
    "email": "",
    "company": "The MathWorks",
    "position": "Senior Product Marketing Manager",
    "connect_date": "12 Oct 2018"
  },
  {
    "firstname": "Shuchi",
    "lastname": "Agarwal",
    "email": "",
    "company": "Iron Mountain",
    "position": "Director Of Software Development",
    "connect_date": "11 Oct 2018"
  },
  {
    "firstname": "Frank",
    "lastname": "Natale",
    "email": "",
    "company": "VADAR Systems, Inc.",
    "position": "CEO",
    "connect_date": "11 Oct 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Manning",
    "email": "",
    "company": "Cosē Technologies, LLC",
    "position": "CEO and Co-Founder",
    "connect_date": "11 Oct 2018"
  },
  {
    "firstname": "Sandeep",
    "lastname": "Tyagi",
    "email": "",
    "company": "Atos Syntel",
    "position": "Engagement Director -  The Home Depot",
    "connect_date": "11 Oct 2018"
  },
  {
    "firstname": "Markus",
    "lastname": "Nispel",
    "email": "",
    "company": "Extreme Networks",
    "position": "Vice President Software and Solutions Engineering",
    "connect_date": "11 Oct 2018"
  },
  {
    "firstname": "Justin",
    "lastname": "Handley",
    "email": "",
    "company": "Web Mission Control, Inc",
    "position": "President",
    "connect_date": "10 Oct 2018"
  },
  {
    "firstname": "Paul J",
    "lastname": "Elisii",
    "email": "",
    "company": "ALTEN Calsoft Labs",
    "position": "Vice President of Business Intelligence and Blockchain Technologies",
    "connect_date": "10 Oct 2018"
  },
  {
    "firstname": "Евгений",
    "lastname": "Шамрай",
    "email": "",
    "company": "OSSystem",
    "position": "Full Stack Web Developer",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Nigel",
    "lastname": "Jacob",
    "email": "",
    "company": "City of Boston",
    "position": "Co-Founder, Mayor's Office of New Urban Mechanics",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Ryan",
    "lastname": "Walsh",
    "email": "",
    "company": "RepVue",
    "position": "Founder and CEO",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Ana",
    "lastname": "Mihelić",
    "email": "",
    "company": "Infomediji d.o.o.",
    "position": "HR Communications Manager",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Alan",
    "lastname": "Flower",
    "email": "",
    "company": "HCL Technologies",
    "position": "Senior Vice President - CTO EMEA & Global Head, Cloud Native Labs",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Mykola",
    "lastname": "Bubelich",
    "email": "",
    "company": "greentec development gmbh",
    "position": "Scrum Master",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Preetham",
    "lastname": "Manjunath",
    "email": "",
    "company": "Epsilon",
    "position": "Sr. Director",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Jeff",
    "lastname": "Pacheco",
    "email": "",
    "company": "BNY Mellon | Eagle Investment Systems",
    "position": "Head of Client and Technology Operations",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Amanda",
    "lastname": "Hinds",
    "email": "",
    "company": "Boston Scientific",
    "position": "Manager of Digital Creative, Product Design & UX",
    "connect_date": "09 Oct 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Stolow",
    "email": "",
    "company": "Huddol",
    "position": "CEO",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Sophie",
    "lastname": "Bembridge",
    "email": "",
    "company": "Administrate",
    "position": "Business Development",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "McNulty",
    "email": "",
    "company": "National Preparedness Leadership Initiative (NPLI)",
    "position": "Associate Director",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Bill",
    "lastname": "Schmarzo",
    "email": "",
    "company": "Hitachi Vantara",
    "position": "CTO, IoT and Analytics",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Marius",
    "lastname": "Minca",
    "email": "",
    "company": "AIR Worldwide (A Verisk Analytics Business)",
    "position": "Director, Software Research and Development",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Jordi",
    "lastname": "Cenzano",
    "email": "",
    "company": "Brightcove",
    "position": "Director of Engineering for live services",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Funk",
    "email": "",
    "company": "Ascend Learning",
    "position": "Director Software Engineering",
    "connect_date": "08 Oct 2018"
  },
  {
    "firstname": "Андрей",
    "lastname": "Бушанский",
    "email": "",
    "company": "CQR",
    "position": "Project Development Manager",
    "connect_date": "07 Oct 2018"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Coombe",
    "email": "",
    "company": "Apxium",
    "position": "Managing Director",
    "connect_date": "06 Oct 2018"
  },
  {
    "firstname": "Sean",
    "lastname": "Armstrong",
    "email": "",
    "company": "Comcast",
    "position": "Director of Research",
    "connect_date": "05 Oct 2018"
  },
  {
    "firstname": "Russell",
    "lastname": "Watts MBA",
    "email": "",
    "company": "SCIEX",
    "position": "Director - Business Development & Marketing, EMEAI",
    "connect_date": "05 Oct 2018"
  },
  {
    "firstname": "Igor",
    "lastname": "Chterental",
    "email": "",
    "company": "Bracebridge Capital, LLC",
    "position": "Director of R&D",
    "connect_date": "05 Oct 2018"
  },
  {
    "firstname": "Josh",
    "lastname": "Nazarian",
    "email": "",
    "company": "Eliassen Group",
    "position": "President",
    "connect_date": "05 Oct 2018"
  },
  {
    "firstname": "Max M.",
    "lastname": "Mansoubi, Ph.D.",
    "email": "",
    "company": "Mansoubian and Associates - Technology Startup Advisory",
    "position": "Founder and Principal",
    "connect_date": "04 Oct 2018"
  },
  {
    "firstname": "Jessica",
    "lastname": "Carroll",
    "email": "",
    "company": "Pluto TV",
    "position": "Director of Human Resources",
    "connect_date": "04 Oct 2018"
  },
  {
    "firstname": "Andra",
    "lastname": "Tanase",
    "email": "",
    "company": "EUROPEAN DYNAMICS",
    "position": "Resources and Development Officer",
    "connect_date": "03 Oct 2018"
  },
  {
    "firstname": "Gabrielle",
    "lastname": "Charland",
    "email": "",
    "company": "Behaviour Interactive",
    "position": "Conseillère Senior Acquisition de talents",
    "connect_date": "03 Oct 2018"
  },
  {
    "firstname": "Rich",
    "lastname": "Surgener",
    "email": "",
    "company": "Aetna, a CVS Health Company",
    "position": "Senior Director, IT Program Management",
    "connect_date": "02 Oct 2018"
  },
  {
    "firstname": "Luke",
    "lastname": "Wilson-Mawer",
    "email": "",
    "company": "KYND",
    "position": "Chief Technical Officer",
    "connect_date": "30 Sep 2018"
  },
  {
    "firstname": "Lee",
    "lastname": "Pickavance",
    "email": "",
    "company": "ConsenSys",
    "position": "Global Managing Director",
    "connect_date": "30 Sep 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "Jarvis",
    "email": "",
    "company": "eVision Industry Software",
    "position": "Director, Business Development",
    "connect_date": "26 Sep 2018"
  },
  {
    "firstname": "Best",
    "lastname": "Rotimi",
    "email": "",
    "company": "OSSystem",
    "position": "Developer",
    "connect_date": "25 Sep 2018"
  },
  {
    "firstname": "Kirsten",
    "lastname": "Snyder",
    "email": "",
    "company": "Camden Kelly Corporation",
    "position": "IT Recruiter",
    "connect_date": "25 Sep 2018"
  },
  {
    "firstname": "Victoria",
    "lastname": "Frank",
    "email": "",
    "company": "Kuchinate",
    "position": "Intern",
    "connect_date": "24 Sep 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Revsbech",
    "email": "",
    "company": "DI - Dansk Industri",
    "position": "Board Member of DI Digital",
    "connect_date": "24 Sep 2018"
  },
  {
    "firstname": "Ksenia",
    "lastname": "Niechaieva",
    "email": "",
    "company": "IT DEV GROUP",
    "position": "Recruiter",
    "connect_date": "23 Sep 2018"
  },
  {
    "firstname": "Ирина",
    "lastname": "Кныж",
    "email": "",
    "company": "OSSystem",
    "position": "junior back-end developer",
    "connect_date": "23 Sep 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Toms",
    "email": "",
    "company": "Salt",
    "position": "Principal Consultant - Software Engineering",
    "connect_date": "23 Sep 2018"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Samborskyi",
    "email": "",
    "company": "Temabit Software Development",
    "position": "Javascript Developer",
    "connect_date": "23 Sep 2018"
  },
  {
    "firstname": "Victoria",
    "lastname": "Gerukh",
    "email": "",
    "company": "uTrigg",
    "position": "CEO",
    "connect_date": "21 Sep 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Torell",
    "email": "",
    "company": "Loop Industries",
    "position": "CEO",
    "connect_date": "21 Sep 2018"
  },
  {
    "firstname": "Ioannis",
    "lastname": "Karamanlakis",
    "email": "",
    "company": "Unibuddy",
    "position": "Head of Data Science",
    "connect_date": "21 Sep 2018"
  },
  {
    "firstname": "dariusz",
    "lastname": "nowakowski",
    "email": "",
    "company": "freePBX",
    "position": "dyrektor",
    "connect_date": "20 Sep 2018"
  },
  {
    "firstname": "Kajal",
    "lastname": "Gondaliya",
    "email": "",
    "company": "Self-Employed",
    "position": "Android, IOS, WordPress, Woocommerce, PHP, Laravel developer",
    "connect_date": "20 Sep 2018"
  },
  {
    "firstname": "Tino",
    "lastname": "Kyre",
    "email": "",
    "company": "IBM iX",
    "position": "Local Practice Group Leader Digital Business Design & Reinvention",
    "connect_date": "18 Sep 2018"
  },
  {
    "firstname": "Елена",
    "lastname": "Немеря",
    "email": "",
    "company": "Афина МДМ Corporation",
    "position": "Бухгалтер ГСМ учета",
    "connect_date": "16 Sep 2018"
  },
  {
    "firstname": "Mykyta",
    "lastname": "Semenov",
    "email": "",
    "company": "IT CEO Club",
    "position": "Coordinator",
    "connect_date": "14 Sep 2018"
  },
  {
    "firstname": "Wael",
    "lastname": "Jammal",
    "email": "",
    "company": "Black Swan Data",
    "position": "Principal Software Architect",
    "connect_date": "12 Sep 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Makedonskiy 🔟К",
    "email": "",
    "company": "DEBILL SERVICES LTD",
    "position": "Java Developer",
    "connect_date": "11 Sep 2018"
  },
  {
    "firstname": "Alexey",
    "lastname": "Bozhenko",
    "email": "",
    "company": "EduNav",
    "position": "QA Manager",
    "connect_date": "10 Sep 2018"
  },
  {
    "firstname": "Wisdom",
    "lastname": "Oparaocha",
    "email": "",
    "company": "Populous World",
    "position": "CTO",
    "connect_date": "10 Sep 2018"
  },
  {
    "firstname": "Kenneth",
    "lastname": "Wu",
    "email": "",
    "company": "naviHealth",
    "position": "VP Software Engineering",
    "connect_date": "08 Sep 2018"
  },
  {
    "firstname": "Charles",
    "lastname": "Baccanello",
    "email": "",
    "company": "Nicholas Humphreys",
    "position": "Lettings Manager",
    "connect_date": "08 Sep 2018"
  },
  {
    "firstname": "Сергей",
    "lastname": "Николаев",
    "email": "",
    "company": "ABBYY",
    "position": "User Retention Specialist",
    "connect_date": "07 Sep 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Pavlishyn",
    "email": "",
    "company": "Dennis",
    "position": "React Developer",
    "connect_date": "06 Sep 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Elswood",
    "email": "",
    "company": "Planixs",
    "position": "COO",
    "connect_date": "05 Sep 2018"
  },
  {
    "firstname": "Piotr",
    "lastname": "Vladimir",
    "email": "",
    "company": "iMationoSoft Inc",
    "position": "Web Developer",
    "connect_date": "04 Sep 2018"
  },
  {
    "firstname": "Gary",
    "lastname": "Higham",
    "email": "",
    "company": "WonderBill",
    "position": "VP of Engineering",
    "connect_date": "03 Sep 2018"
  },
  {
    "firstname": "Denise",
    "lastname": "Neves Santos",
    "email": "",
    "company": "Expedia Group",
    "position": "Director, Engineering",
    "connect_date": "03 Sep 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Sylvester",
    "email": "",
    "company": "MindGeek",
    "position": "Data Analyst: Payments, Risk & Business Analytics",
    "connect_date": "02 Sep 2018"
  },
  {
    "firstname": "Julia",
    "lastname": "Yurchak",
    "email": "",
    "company": "PrivatBank",
    "position": "Marketing Specialist at PrivatBank",
    "connect_date": "01 Sep 2018"
  },
  {
    "firstname": "Kiryl",
    "lastname": "Chykeyuk",
    "email": "",
    "company": "HYPERVSN",
    "position": "Founder at HYPERVSN",
    "connect_date": "31 Aug 2018"
  },
  {
    "firstname": "Andrea",
    "lastname": "Pace",
    "email": "",
    "company": "Runpath",
    "position": "Head of Web Development",
    "connect_date": "31 Aug 2018"
  },
  {
    "firstname": "Maria",
    "lastname": "Tolkacheva",
    "email": "",
    "company": "CHI Software",
    "position": "Global Sales Representative",
    "connect_date": "29 Aug 2018"
  },
  {
    "firstname": "Huw",
    "lastname": "Owen",
    "email": "",
    "company": "TravelLocal",
    "position": "Co Founder",
    "connect_date": "29 Aug 2018"
  },
  {
    "firstname": "Rohit",
    "lastname": "Changediya",
    "email": "",
    "company": "Genx Technologies",
    "position": "Founder",
    "connect_date": "28 Aug 2018"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Prasolov",
    "email": "",
    "company": "Point-X",
    "position": "Co-Founder",
    "connect_date": "28 Aug 2018"
  },
  {
    "firstname": "Joey",
    "lastname": "Krug",
    "email": "",
    "company": "Pantera Capital",
    "position": "Co-Chief Investment Officer",
    "connect_date": "28 Aug 2018"
  },
  {
    "firstname": "Matus",
    "lastname": "Maar",
    "email": "",
    "company": "Talis Capital",
    "position": "Managing Partner Ventures & Co-Founder",
    "connect_date": "27 Aug 2018"
  },
  {
    "firstname": "René",
    "lastname": "Jensen Nystrup",
    "email": "",
    "company": "Vestas",
    "position": "Vice President, COO Finance",
    "connect_date": "26 Aug 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Kelz",
    "email": "",
    "company": "BMW Group",
    "position": "Head of Engineering Munich",
    "connect_date": "25 Aug 2018"
  },
  {
    "firstname": "Junichi",
    "lastname": "Hasegawa",
    "email": "",
    "company": "Kea Institute",
    "position": "President and Co-Founder",
    "connect_date": "24 Aug 2018"
  },
  {
    "firstname": "Thomas Hal",
    "lastname": "Robson-Kanu",
    "email": "",
    "company": "Sports Ledger",
    "position": "Co-Founder",
    "connect_date": "23 Aug 2018"
  },
  {
    "firstname": "Oded",
    "lastname": "Keinan",
    "email": "",
    "company": "Lindar Media",
    "position": "Founder",
    "connect_date": "22 Aug 2018"
  },
  {
    "firstname": "Максим",
    "lastname": "Белов",
    "email": "",
    "company": "Chatbullet.com",
    "position": "CEO",
    "connect_date": "21 Aug 2018"
  },
  {
    "firstname": "Yurii",
    "lastname": "Maksymchuk",
    "email": "",
    "company": "Upwork",
    "position": "Talent Agent Admin",
    "connect_date": "21 Aug 2018"
  },
  {
    "firstname": "Oleg",
    "lastname": "Magaletsky",
    "email": "",
    "company": "CornCopi",
    "position": "Co Founder & CEO",
    "connect_date": "21 Aug 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Obelitz Høgsbro-Rode",
    "email": "",
    "company": "IMPACT Extend A/S",
    "position": "Partner, co-CEO",
    "connect_date": "21 Aug 2018"
  },
  {
    "firstname": "Владимир",
    "lastname": "Петров",
    "email": "",
    "company": "Red Moon IT",
    "position": "Sales Manager",
    "connect_date": "21 Aug 2018"
  },
  {
    "firstname": "Edward",
    "lastname": "Poland",
    "email": "",
    "company": "Hire Space",
    "position": "Co-Founder",
    "connect_date": "20 Aug 2018"
  },
  {
    "firstname": "Robert-Lee",
    "lastname": "Griffith",
    "email": "",
    "company": "ZooPay Limited",
    "position": "Founder/Managing Director",
    "connect_date": "20 Aug 2018"
  },
  {
    "firstname": "Kyryl",
    "lastname": "Mamatkazin",
    "email": "",
    "company": "Star Shine Shipping LLC",
    "position": "Lawyer",
    "connect_date": "19 Aug 2018"
  },
  {
    "firstname": "Roman",
    "lastname": "Roik",
    "email": "",
    "company": "Upwork",
    "position": "Freelance Web/Mobile Developer",
    "connect_date": "19 Aug 2018"
  },
  {
    "firstname": "Nikolai",
    "lastname": "Kasilev",
    "email": "",
    "company": "Media-blog platform",
    "position": "CEO Hashtap",
    "connect_date": "18 Aug 2018"
  },
  {
    "firstname": "Fernando",
    "lastname": "Martinho",
    "email": "",
    "company": "Naoris Cyber Blockchain",
    "position": "Co-Founder",
    "connect_date": "18 Aug 2018"
  },
  {
    "firstname": "Joe",
    "lastname": "Manna",
    "email": "",
    "company": "Experian Consumer Services",
    "position": "CPO/CTO Experian",
    "connect_date": "18 Aug 2018"
  },
  {
    "firstname": "Kevin",
    "lastname": "North",
    "email": "",
    "company": "Plentific",
    "position": "Director Of Business Development",
    "connect_date": "17 Aug 2018"
  },
  {
    "firstname": "Isa",
    "lastname": "Goksu",
    "email": "",
    "company": "ThoughtWorks",
    "position": "Director of Technology, Financial Services",
    "connect_date": "16 Aug 2018"
  },
  {
    "firstname": "Josh",
    "lastname": "Nesbitt",
    "email": "",
    "company": "LADbible Group",
    "position": "Interim Head of Software Engineering",
    "connect_date": "16 Aug 2018"
  },
  {
    "firstname": "Ian",
    "lastname": "Richardson",
    "email": "",
    "company": "TheLADbible Group",
    "position": "Managing Director, Joyride",
    "connect_date": "16 Aug 2018"
  },
  {
    "firstname": "Kristina",
    "lastname": "Rabecaite",
    "email": "",
    "company": "Limejump Ltd",
    "position": "Business Development Manager - Power",
    "connect_date": "15 Aug 2018"
  },
  {
    "firstname": "Antoine",
    "lastname": "Rabanes",
    "email": "",
    "company": "Applicaster",
    "position": "Head of Technology Europe",
    "connect_date": "15 Aug 2018"
  },
  {
    "firstname": "Enrico",
    "lastname": "Kufahl",
    "email": "",
    "company": "ImmobilienScout24 - Scout24",
    "position": "Head of Technology",
    "connect_date": "15 Aug 2018"
  },
  {
    "firstname": "Quirino",
    "lastname": "Zagarese",
    "email": "",
    "company": "Yoti Ltd",
    "position": "Principal Engineer",
    "connect_date": "15 Aug 2018"
  },
  {
    "firstname": "Joe",
    "lastname": "Bishop",
    "email": "",
    "company": "LoyaltyLion",
    "position": "Senior Loyalty Consultant",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Stehno",
    "email": "",
    "company": "Token0x",
    "position": "Token0x Platform + Ethnamed Protocol",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Ruth",
    "lastname": "Harrison",
    "email": "",
    "company": "ThoughtWorks",
    "position": "Managing Director",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "David",
    "lastname": "Elliman",
    "email": "",
    "company": "Forbes Technology Council",
    "position": "Official Member",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Declan",
    "lastname": "Kane",
    "email": "",
    "company": "Ocado Technology",
    "position": "Head Of Infrastructure",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Bruno",
    "lastname": "Beuzelin",
    "email": "",
    "company": "Making Waves",
    "position": "Client Partner / Senior Strategic Advisor",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Will",
    "lastname": "Swannell",
    "email": "",
    "company": "Hire Space",
    "position": "Chief Executive",
    "connect_date": "14 Aug 2018"
  },
  {
    "firstname": "Marina",
    "lastname": "Rabtsevych",
    "email": "",
    "company": "Marina Rabtsevych Consulting services",
    "position": "Pre Sale Services",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "Amanda",
    "lastname": "Terry",
    "email": "",
    "company": "Acxiom",
    "position": "Vice President, Global Partner Development",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "soebuejo",
    "lastname": "argo",
    "email": "",
    "company": "jogorogo",
    "position": "entrepreneur",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "Stefan",
    "lastname": "Stallmann",
    "email": "",
    "company": "Aperto - An IBM Company",
    "position": "Director Mobile & Digital Service Development",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "Emily",
    "lastname": "Nguyen",
    "email": "",
    "company": "Zalando SE",
    "position": "Tech Recruitment Specialist",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "Neil",
    "lastname": "Catton",
    "email": "",
    "company": "Wipro Digital",
    "position": "Chief Digital Architect",
    "connect_date": "13 Aug 2018"
  },
  {
    "firstname": "Hadiputra",
    "lastname": "Munandar",
    "email": "",
    "company": "Cryptocurrency",
    "position": "Specialist Marketing",
    "connect_date": "12 Aug 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "van Doorn",
    "email": "",
    "company": "TradeTracker.com",
    "position": "Chief Executive Officer",
    "connect_date": "11 Aug 2018"
  },
  {
    "firstname": "Keano",
    "lastname": "Lane",
    "email": "",
    "company": "POD Point Ltd.",
    "position": "Head of Software Engineering",
    "connect_date": "10 Aug 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Forrest",
    "email": "",
    "company": "Forrest Tech",
    "position": "Consultant",
    "connect_date": "10 Aug 2018"
  },
  {
    "firstname": "Finbarr",
    "lastname": "Murphy",
    "email": "",
    "company": "Modular",
    "position": "Co-Founder",
    "connect_date": "09 Aug 2018"
  },
  {
    "firstname": "David",
    "lastname": "Strober",
    "email": "",
    "company": "MovMobile",
    "position": "Director of Product Development and Emerging Technology",
    "connect_date": "08 Aug 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Diamond",
    "email": "",
    "company": "Technology Risk & Digital Insights Ltd",
    "position": "Director",
    "connect_date": "08 Aug 2018"
  },
  {
    "firstname": "Øivind",
    "lastname": "Dahl",
    "email": "",
    "company": "KONGSBERG",
    "position": "Group Vice President Business Development",
    "connect_date": "07 Aug 2018"
  },
  {
    "firstname": "Julia",
    "lastname": "Kulyk",
    "email": "",
    "company": "Ralabs",
    "position": "Chief Business Development Officer",
    "connect_date": "07 Aug 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Crummie",
    "email": "",
    "company": "Too Good To Go",
    "position": "Co Founder",
    "connect_date": "06 Aug 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "van Pappelendam",
    "email": "",
    "company": "Maester",
    "position": "Co-Founder / CEO",
    "connect_date": "06 Aug 2018"
  },
  {
    "firstname": "Geir",
    "lastname": "Faremo",
    "email": "",
    "company": "eSmart Systems",
    "position": "Head Of Development",
    "connect_date": "05 Aug 2018"
  },
  {
    "firstname": "Mike J.",
    "lastname": "Johnson",
    "email": "",
    "company": "High Impact Technologies",
    "position": "Owner/Manager of Technical Operations",
    "connect_date": "05 Aug 2018"
  },
  {
    "firstname": "Knut H. H.",
    "lastname": "Johansen",
    "email": "",
    "company": "eSmart Systems",
    "position": "CEO",
    "connect_date": "05 Aug 2018"
  },
  {
    "firstname": "Bjoern",
    "lastname": "Frank",
    "email": "",
    "company": "Kaiser X labs, a company of Allianz",
    "position": "Managing Director, SVP",
    "connect_date": "03 Aug 2018"
  },
  {
    "firstname": "Jasmine",
    "lastname": "Daniels",
    "email": "",
    "company": "Software-Nation.com",
    "position": "Chief Technology Officer",
    "connect_date": "03 Aug 2018"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Blokhin",
    "email": "",
    "company": "Puratos",
    "position": "Key Account Manager",
    "connect_date": "02 Aug 2018"
  },
  {
    "firstname": "Lorenz",
    "lastname": "Lehmann",
    "email": "",
    "company": "Veeva Systems",
    "position": "Director Software Development",
    "connect_date": "02 Aug 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Peak",
    "email": "",
    "company": "Asyncy",
    "position": "Founder and CEO",
    "connect_date": "02 Aug 2018"
  },
  {
    "firstname": "Mariam",
    "lastname": "Hakobyan",
    "email": "",
    "company": "Softr",
    "position": "Co-Founder & CEO",
    "connect_date": "01 Aug 2018"
  },
  {
    "firstname": "Kate",
    "lastname": "Burns",
    "email": "",
    "company": "Hambro Perks",
    "position": "Venture Partner, Hambro Perks",
    "connect_date": "01 Aug 2018"
  },
  {
    "firstname": "Pavlo",
    "lastname": "Voznenko",
    "email": "",
    "company": "Fineway",
    "position": "Head of Software Engineering",
    "connect_date": "01 Aug 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Lowe",
    "email": "",
    "company": "Gambit Technologies",
    "position": "Technology Recruiter",
    "connect_date": "01 Aug 2018"
  },
  {
    "firstname": "Евгений",
    "lastname": "Вовк",
    "email": "",
    "company": "Freshmart",
    "position": "Руководитель HR-отдела",
    "connect_date": "01 Aug 2018"
  },
  {
    "firstname": "Robin",
    "lastname": "Skidmore",
    "email": "",
    "company": "Journey Further",
    "position": "Founder & CEO",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Christoph",
    "lastname": "Kronast",
    "email": "",
    "company": "HolidayPirates Group",
    "position": "CTO",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Sebastian",
    "lastname": "Reckzeh",
    "email": "",
    "company": "zeuz.io",
    "position": "COO & Founder",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Mikhail",
    "lastname": "Larchanka",
    "email": "",
    "company": "Sytac IT Consulting",
    "position": "Technical Lead",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Markus",
    "lastname": "Schneider",
    "email": "",
    "company": "zeuz.io",
    "position": "Managing Director & Founder",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Williams",
    "email": "",
    "company": "TORI Global",
    "position": "Head of Business Transformation",
    "connect_date": "31 Jul 2018"
  },
  {
    "firstname": "Witali",
    "lastname": "Schwetz",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "30 Jul 2018"
  },
  {
    "firstname": "Boris",
    "lastname": "Krumrey",
    "email": "",
    "company": "UiPath - Robotic Process Automation",
    "position": "Chief Robotics Officer",
    "connect_date": "30 Jul 2018"
  },
  {
    "firstname": "Ruhan",
    "lastname": "Ahmed",
    "email": "",
    "company": "Crowdcube",
    "position": "Investor",
    "connect_date": "29 Jul 2018"
  },
  {
    "firstname": "Adeeb",
    "lastname": "Rahman",
    "email": "",
    "company": "ReactDOM",
    "position": "Editor In Chief",
    "connect_date": "29 Jul 2018"
  },
  {
    "firstname": "Taras",
    "lastname": "Zakharkiv",
    "email": "",
    "company": "ELEKS Software",
    "position": "Business Analyst",
    "connect_date": "26 Jul 2018"
  },
  {
    "firstname": "Adam",
    "lastname": "Finzel",
    "email": "",
    "company": "Green Man Gaming",
    "position": "SVP of Engineering",
    "connect_date": "26 Jul 2018"
  },
  {
    "firstname": "Nitin",
    "lastname": "Deshmukh",
    "email": "",
    "company": "Payconiq",
    "position": "Chief Architect",
    "connect_date": "24 Jul 2018"
  },
  {
    "firstname": "Mathew",
    "lastname": "Munro",
    "email": "",
    "company": "Conversocial",
    "position": "Chief Product Officer",
    "connect_date": "24 Jul 2018"
  },
  {
    "firstname": "Jonas",
    "lastname": "Brock",
    "email": "",
    "company": "Netcompany A/S",
    "position": "Managing Architect, IM Division",
    "connect_date": "24 Jul 2018"
  },
  {
    "firstname": "Ryan",
    "lastname": "Gruss",
    "email": "",
    "company": "Diversis Capital LLC",
    "position": "Private Equity Analyst",
    "connect_date": "23 Jul 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Gatrell",
    "email": "",
    "company": "Nutmeg",
    "position": "CTO",
    "connect_date": "23 Jul 2018"
  },
  {
    "firstname": "Lars",
    "lastname": "Gustavsson",
    "email": "",
    "company": "TORI Global",
    "position": "Head of the Systems Integrator business",
    "connect_date": "23 Jul 2018"
  },
  {
    "firstname": "Lennart",
    "lastname": "Schroer",
    "email": "",
    "company": "Undagrid",
    "position": "Co-Founder | Marketing & Operations",
    "connect_date": "22 Jul 2018"
  },
  {
    "firstname": "Klaus",
    "lastname": "Straub",
    "email": "",
    "company": "BMW Group",
    "position": "CIO and Senior Vice President Information Management BMW Group",
    "connect_date": "22 Jul 2018"
  },
  {
    "firstname": "Tomas",
    "lastname": "Vocetka",
    "email": "",
    "company": "GoEuro",
    "position": "VP Engineering",
    "connect_date": "21 Jul 2018"
  },
  {
    "firstname": "Casper",
    "lastname": "Rasmussen",
    "email": "",
    "company": "Nodes",
    "position": "CTO & Partner",
    "connect_date": "21 Jul 2018"
  },
  {
    "firstname": "Rob",
    "lastname": "Crossley",
    "email": "",
    "company": "PlayStack",
    "position": "Head of Developer Partnerships",
    "connect_date": "20 Jul 2018"
  },
  {
    "firstname": "Helle",
    "lastname": "Huss",
    "email": "",
    "company": "KMD",
    "position": "Executive Vice President, KMD Business",
    "connect_date": "20 Jul 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Rawles",
    "email": "",
    "company": "Bright Blue Day Ltd",
    "position": "Chief Innovation Officer",
    "connect_date": "20 Jul 2018"
  },
  {
    "firstname": "Raj",
    "lastname": "Naik",
    "email": "",
    "company": "Conversocial",
    "position": "VP of Engineering",
    "connect_date": "20 Jul 2018"
  },
  {
    "firstname": "Cody",
    "lastname": "Lawrence",
    "email": "",
    "company": "Cody Lawrence Consultancy",
    "position": "Executive Search & Investment Banking",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "James",
    "lastname": "O'Vens",
    "email": "",
    "company": "News UK",
    "position": "Software Developer",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Elbanna",
    "email": "",
    "company": "199Creative",
    "position": "Senior Developer",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Gorenko",
    "email": "",
    "company": "Independent",
    "position": "Talent Acquisition Consultant",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "Pragati",
    "lastname": "Mathur",
    "email": "",
    "company": "Staples",
    "position": "EVP - CTO/CIO Digital solutions",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "Kenneth",
    "lastname": "Dahlberg",
    "email": "",
    "company": "Banqsoft",
    "position": "Senior System Consultant, Business Analytics",
    "connect_date": "19 Jul 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Kupisz-Cichosz",
    "email": "",
    "company": "Softway Ltd",
    "position": "Co-Owner & Managing Director",
    "connect_date": "18 Jul 2018"
  },
  {
    "firstname": "Iryna",
    "lastname": "Bolibok",
    "email": "",
    "company": "Little Voice",
    "position": "Senior UI Designer",
    "connect_date": "18 Jul 2018"
  },
  {
    "firstname": "Gianni",
    "lastname": "Pignataro",
    "email": "",
    "company": "Computer Futures",
    "position": "Recruitment Consultant",
    "connect_date": "18 Jul 2018"
  },
  {
    "firstname": "Nataliia",
    "lastname": "Hunter",
    "email": "",
    "company": "Sykes Enterprises, Incorporated",
    "position": "Customer Service Specialist",
    "connect_date": "18 Jul 2018"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Zaleski",
    "email": "",
    "company": "Applause",
    "position": "Senior Director Of Engineering",
    "connect_date": "17 Jul 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Roeder",
    "email": "",
    "company": "Walter Roeder GmbH",
    "position": "Geschäftsführer",
    "connect_date": "17 Jul 2018"
  },
  {
    "firstname": "Jakob",
    "lastname": "Bjerg-Heise",
    "email": "jakobbheise@gmail.com",
    "company": "SCALES A/S",
    "position": "Principal - BI and Analytics",
    "connect_date": "15 Jul 2018"
  },
  {
    "firstname": "Claus",
    "lastname": "Jørgensen",
    "email": "",
    "company": "Netcompany",
    "position": "Partner and Owner",
    "connect_date": "12 Jul 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Munch-Andersen",
    "email": "",
    "company": "Clue by Biowink",
    "position": "VP of Engineering",
    "connect_date": "12 Jul 2018"
  },
  {
    "firstname": "Tural",
    "lastname": "Mamedov",
    "email": "",
    "company": "Artelogic",
    "position": "CEO and Co-Founder",
    "connect_date": "11 Jul 2018"
  },
  {
    "firstname": "Anatoly",
    "lastname": "Rysich",
    "email": "",
    "company": "vivendo.agency",
    "position": "Founder",
    "connect_date": "10 Jul 2018"
  },
  {
    "firstname": "Edgar",
    "lastname": "Kraaikamp",
    "email": "",
    "company": "Sytac IT Consulting",
    "position": "Operations Manager",
    "connect_date": "10 Jul 2018"
  },
  {
    "firstname": "Pavel",
    "lastname": "Zavada",
    "email": "",
    "company": "NetForce Ukraine LLC",
    "position": "Founder, CEO",
    "connect_date": "10 Jul 2018"
  },
  {
    "firstname": "Brett",
    "lastname": "Beck",
    "email": "",
    "company": "NEXTFLY Web Design",
    "position": "C.E.O.",
    "connect_date": "09 Jul 2018"
  },
  {
    "firstname": "Maksim",
    "lastname": "Naumov",
    "email": "",
    "company": "solarisBank AG",
    "position": "Staff Engineer",
    "connect_date": "07 Jul 2018"
  },
  {
    "firstname": "Wolfgang",
    "lastname": "Huennekens",
    "email": "",
    "company": "von Neuem GmbH",
    "position": "Founder and Partner",
    "connect_date": "06 Jul 2018"
  },
  {
    "firstname": "Manolis",
    "lastname": "Sera ☁",
    "email": "",
    "company": "ikubINFO",
    "position": "International Sales Manager",
    "connect_date": "06 Jul 2018"
  },
  {
    "firstname": "Wouter",
    "lastname": "Meeuwisse",
    "email": "",
    "company": "Stampwallet",
    "position": "CEO & Founder",
    "connect_date": "05 Jul 2018"
  },
  {
    "firstname": "Marcin",
    "lastname": "Balinski",
    "email": "",
    "company": "WATTx",
    "position": "Head of Engineering",
    "connect_date": "05 Jul 2018"
  },
  {
    "firstname": "Bernard",
    "lastname": "Kochen",
    "email": "",
    "company": "Convious",
    "position": "VP of Sales",
    "connect_date": "05 Jul 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Beer",
    "email": "",
    "company": "TEC Partners - Technical Recruitment Specialists",
    "position": "Recruitment Consultant",
    "connect_date": "04 Jul 2018"
  },
  {
    "firstname": "Philipp",
    "lastname": "Gaerte",
    "email": "",
    "company": "Media4Care",
    "position": "Co-Founder - CEO",
    "connect_date": "04 Jul 2018"
  },
  {
    "firstname": "Ronny",
    "lastname": "Rentner",
    "email": "",
    "company": "Rocket Internet SE",
    "position": "CTO",
    "connect_date": "03 Jul 2018"
  },
  {
    "firstname": "Niki",
    "lastname": "Fuller",
    "email": "",
    "company": "IDBS",
    "position": "GPS and Education Consultant",
    "connect_date": "03 Jul 2018"
  },
  {
    "firstname": "Nicola",
    "lastname": "Di Marco",
    "email": "",
    "company": "Dexlab.io",
    "position": "Co-Founder / Product Designer",
    "connect_date": "03 Jul 2018"
  },
  {
    "firstname": "Viktoriia",
    "lastname": "Semerneva",
    "email": "",
    "company": "MYCO.com",
    "position": "Project Manager",
    "connect_date": "02 Jul 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Cooper",
    "email": "",
    "company": "Wildix",
    "position": "Managing Director",
    "connect_date": "01 Jul 2018"
  },
  {
    "firstname": "Jannik",
    "lastname": "Dam Kehlet",
    "email": "",
    "company": "Praqo",
    "position": "Daglig leder",
    "connect_date": "30 Jun 2018"
  },
  {
    "firstname": "Doug",
    "lastname": "Adams, PE",
    "email": "",
    "company": "IoT Systems, Inc.",
    "position": "President, CEO & Co-Founder",
    "connect_date": "29 Jun 2018"
  },
  {
    "firstname": "Benjamin",
    "lastname": "Libor",
    "email": "",
    "company": "Self-employed",
    "position": "Advisor",
    "connect_date": "29 Jun 2018"
  },
  {
    "firstname": "Willemijn",
    "lastname": "Schneyder RM",
    "email": "",
    "company": "SwipeGuide",
    "position": "CEO & Proud Founder",
    "connect_date": "29 Jun 2018"
  },
  {
    "firstname": "Maxime",
    "lastname": "Pruvost",
    "email": "",
    "company": "Carts Guru",
    "position": "Co-Founder & CTO",
    "connect_date": "28 Jun 2018"
  },
  {
    "firstname": "Deborah",
    "lastname": "Darcis",
    "email": "",
    "company": "hatch IT",
    "position": "Engineering Talent Director",
    "connect_date": "28 Jun 2018"
  },
  {
    "firstname": "Юрий",
    "lastname": "Будилов",
    "email": "",
    "company": "Netinum | Effective marketing",
    "position": "CEO, Founder",
    "connect_date": "27 Jun 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "DiStasi",
    "email": "",
    "company": "Levvel.io",
    "position": "Director Of Recruiting",
    "connect_date": "27 Jun 2018"
  },
  {
    "firstname": "Jan",
    "lastname": "Donselaar",
    "email": "",
    "company": "Donselaar Management & Advies (DMA bv)",
    "position": "Owner/manager",
    "connect_date": "27 Jun 2018"
  },
  {
    "firstname": "Oksana",
    "lastname": "Zhyrukha",
    "email": "",
    "company": "8allocate",
    "position": "IT Resource Manager",
    "connect_date": "25 Jun 2018"
  },
  {
    "firstname": "Roy",
    "lastname": "van Beek",
    "email": "",
    "company": "bagpoint",
    "position": "Chief Operating Officer",
    "connect_date": "25 Jun 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Bramsen",
    "email": "",
    "company": "Tele2 IoT",
    "position": "Sales Manager - IOT",
    "connect_date": "23 Jun 2018"
  },
  {
    "firstname": "Anton",
    "lastname": "de Bode",
    "email": "",
    "company": "Xeelas - IoT for Humanity",
    "position": "Front-end Developer",
    "connect_date": "22 Jun 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Brouwer",
    "email": "",
    "company": "VIVE",
    "position": "Founder",
    "connect_date": "21 Jun 2018"
  },
  {
    "firstname": "Volodymyr",
    "lastname": "Ilianoi",
    "email": "",
    "company": "all world",
    "position": "freelancer",
    "connect_date": "21 Jun 2018"
  },
  {
    "firstname": "Justin",
    "lastname": "Scott",
    "email": "",
    "company": "Workbridge Associates",
    "position": "Practice Manager",
    "connect_date": "21 Jun 2018"
  },
  {
    "firstname": "Sara",
    "lastname": "Ponsford",
    "email": "",
    "company": "Original Software",
    "position": "Head of Product Management",
    "connect_date": "21 Jun 2018"
  },
  {
    "firstname": "Edin",
    "lastname": "Memisevic",
    "email": "",
    "company": "Foodpanda / Delivery Hero",
    "position": "Head of Mobile Applications",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Jan Øystein",
    "lastname": "Thorsnæs",
    "email": "",
    "company": "TOPdesk Norge",
    "position": "Bid Manager",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Rob",
    "lastname": "de Vries",
    "email": "",
    "company": "Ciwit B.V.",
    "position": "Co-Founder",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "C. Serkan",
    "lastname": "Baydin",
    "email": "",
    "company": "Rocket Internet SE",
    "position": "Head Of Search",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Volosenko",
    "email": "",
    "company": "Zalando SE",
    "position": "Head of Enterprise Applications & Architectures",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "George",
    "lastname": "Pretty",
    "email": "",
    "company": "Zalando SE",
    "position": "Dedicated Recruiter - Digital Foundation",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Artur",
    "lastname": "Mkrtchyan",
    "email": "",
    "company": "Collabary by Zalando",
    "position": "CTO/CPO",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Gordon",
    "lastname": "Skinner",
    "email": "",
    "company": "Hummingbird Technologies Ltd",
    "position": "CTO",
    "connect_date": "20 Jun 2018"
  },
  {
    "firstname": "Derk",
    "lastname": "Roodhuyzen de Vries",
    "email": "",
    "company": "Fixico B.V.",
    "position": "Founder & CEO",
    "connect_date": "19 Jun 2018"
  },
  {
    "firstname": "Steven",
    "lastname": "Deurloo",
    "email": "",
    "company": "Experty.io",
    "position": "Expert",
    "connect_date": "19 Jun 2018"
  },
  {
    "firstname": "Jouri",
    "lastname": "Schoemaker",
    "email": "",
    "company": "Puurbezorgd",
    "position": "Founder",
    "connect_date": "19 Jun 2018"
  },
  {
    "firstname": "Babak",
    "lastname": "Heydari",
    "email": "",
    "company": "Shake-on",
    "position": "Founder & Owner",
    "connect_date": "19 Jun 2018"
  },
  {
    "firstname": "Dominic",
    "lastname": "Garabedian",
    "email": "",
    "company": "Trustpilot",
    "position": "Vice President of Sales, North America",
    "connect_date": "18 Jun 2018"
  },
  {
    "firstname": "Colin B.",
    "lastname": "Solberg",
    "email": "",
    "company": "Mähren Digital GmbH",
    "position": "Director Of Operations",
    "connect_date": "18 Jun 2018"
  },
  {
    "firstname": "Lucie",
    "lastname": "Cömert",
    "email": "",
    "company": "Cookswood",
    "position": "Head Of Sales Operations",
    "connect_date": "18 Jun 2018"
  },
  {
    "firstname": "Mario",
    "lastname": "Szirniks",
    "email": "",
    "company": "TBA",
    "position": "And Now for Something Completely Different",
    "connect_date": "18 Jun 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Bertelsen",
    "email": "",
    "company": "Blackwood Seven",
    "position": "Co-founder & Managing Partner",
    "connect_date": "17 Jun 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Kuruc",
    "email": "",
    "company": "Abrigo",
    "position": "Recruiting",
    "connect_date": "16 Jun 2018"
  },
  {
    "firstname": "Will",
    "lastname": "Wells",
    "email": "",
    "company": "Hummingbird Technologies Ltd",
    "position": "Founder & CEO",
    "connect_date": "15 Jun 2018"
  },
  {
    "firstname": "James",
    "lastname": "McIntyre",
    "email": "",
    "company": "Progressive Recruitment",
    "position": "Recruitment Consultant",
    "connect_date": "15 Jun 2018"
  },
  {
    "firstname": "Howard",
    "lastname": "Lerman",
    "email": "",
    "company": "Yext",
    "position": "Co-Founder & CEO",
    "connect_date": "15 Jun 2018"
  },
  {
    "firstname": "Reece",
    "lastname": "Warren",
    "email": "",
    "company": "Knowit",
    "position": "Senior Technical Recruiter",
    "connect_date": "14 Jun 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Nordgaard",
    "email": "",
    "company": "Middlesex University",
    "position": "Associate Professor Supply Chain Management/Production Management",
    "connect_date": "14 Jun 2018"
  },
  {
    "firstname": "Tom Ragnar",
    "lastname": "Aulesjord",
    "email": "",
    "company": "Digiprom",
    "position": "VP Sales and Marketing",
    "connect_date": "14 Jun 2018"
  },
  {
    "firstname": "Daniel-Constantin",
    "lastname": "Mierla",
    "email": "",
    "company": "Kamailio World Conference",
    "position": "Co-Organizer",
    "connect_date": "14 Jun 2018"
  },
  {
    "firstname": "Nic",
    "lastname": "Comrie",
    "email": "",
    "company": "NB Ventures Ltd",
    "position": "Co-Founder",
    "connect_date": "13 Jun 2018"
  },
  {
    "firstname": "Michele",
    "lastname": "Sofio",
    "email": "",
    "company": "Real Garant",
    "position": "Sales Area Manager North&Central Italy / Sales Trainer",
    "connect_date": "13 Jun 2018"
  },
  {
    "firstname": "Nicholas",
    "lastname": "Aiello",
    "email": "",
    "company": "ForeFront Web: Get In Front",
    "position": "Project Manager",
    "connect_date": "13 Jun 2018"
  },
  {
    "firstname": "Graham",
    "lastname": "Collie",
    "email": "",
    "company": "TrafficJunky",
    "position": "Director of Sales",
    "connect_date": "13 Jun 2018"
  },
  {
    "firstname": "Ksenia",
    "lastname": "Sulkovskaya",
    "email": "",
    "company": "Direct Line LLC",
    "position": "Manager Sales",
    "connect_date": "13 Jun 2018"
  },
  {
    "firstname": "Alicja",
    "lastname": "Niedbalski",
    "email": "",
    "company": "Ready Education",
    "position": "Technical Project Manager",
    "connect_date": "12 Jun 2018"
  },
  {
    "firstname": "Vidar",
    "lastname": "Gunnerud",
    "email": "",
    "company": "Solution Seeker",
    "position": "CEO",
    "connect_date": "12 Jun 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Ruud",
    "email": "",
    "company": "Pentia",
    "position": "CCO, partner",
    "connect_date": "11 Jun 2018"
  },
  {
    "firstname": "Jeppe",
    "lastname": "Rasmussen",
    "email": "",
    "company": "Codespace ApS",
    "position": "Partner & Udvikler",
    "connect_date": "09 Jun 2018"
  },
  {
    "firstname": "Jasper",
    "lastname": "Snijder",
    "email": "",
    "company": "e-office",
    "position": "CEO",
    "connect_date": "08 Jun 2018"
  },
  {
    "firstname": "Marcel",
    "lastname": "Dumont",
    "email": "",
    "company": "FitForMe",
    "position": "CTO",
    "connect_date": "08 Jun 2018"
  },
  {
    "firstname": "Oleg",
    "lastname": "Mizov",
    "email": "",
    "company": "SoftServe",
    "position": "Delivery Manager",
    "connect_date": "05 Jun 2018"
  },
  {
    "firstname": "Johan",
    "lastname": "Liljeros",
    "email": "",
    "company": "Avensia, Inc",
    "position": "General Manager North Americas",
    "connect_date": "05 Jun 2018"
  },
  {
    "firstname": "Suzanne",
    "lastname": "Lauritzen",
    "email": "",
    "company": "raffle.ai",
    "position": "CEO",
    "connect_date": "04 Jun 2018"
  },
  {
    "firstname": "Bruce",
    "lastname": "Barron",
    "email": "",
    "company": "Digi-BNC",
    "position": "Chief Executive Officer",
    "connect_date": "04 Jun 2018"
  },
  {
    "firstname": "Santiago",
    "lastname": "Dei",
    "email": "",
    "company": "DSB",
    "position": "Socio fundador (Founder partner)",
    "connect_date": "04 Jun 2018"
  },
  {
    "firstname": "Ricky",
    "lastname": "Lee",
    "email": "",
    "company": "sync.money",
    "position": "Founder",
    "connect_date": "01 Jun 2018"
  },
  {
    "firstname": "Attila",
    "lastname": "Sükösd",
    "email": "",
    "company": "AIRTAME",
    "position": "CTO & Co-founder",
    "connect_date": "01 Jun 2018"
  },
  {
    "firstname": "Carl Erik",
    "lastname": "Kjærsgaard",
    "email": "",
    "company": "Blackwood Seven A/S",
    "position": "CEO and Co-Founder",
    "connect_date": "31 May 2018"
  },
  {
    "firstname": "Bodil",
    "lastname": "Biering",
    "email": "",
    "company": "Blackwood Seven",
    "position": "Head Of Development",
    "connect_date": "31 May 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Melanchik",
    "email": "",
    "company": "Bank of Memories",
    "position": "CEO",
    "connect_date": "30 May 2018"
  },
  {
    "firstname": "Kasper",
    "lastname": "Ebsen",
    "email": "",
    "company": "KYNETIC",
    "position": "CEO, Partner & Senior Strategy Consultant",
    "connect_date": "27 May 2018"
  },
  {
    "firstname": "Michelle",
    "lastname": "Gorry",
    "email": "",
    "company": "erwin, Inc.",
    "position": "Executive Assistant",
    "connect_date": "26 May 2018"
  },
  {
    "firstname": "James",
    "lastname": "McParlane",
    "email": "",
    "company": "Massive Interactive",
    "position": "CTO",
    "connect_date": "26 May 2018"
  },
  {
    "firstname": "Hannah",
    "lastname": "Evans",
    "email": "",
    "company": "Hanover Recruitment",
    "position": "Senior Recruitment Consultant",
    "connect_date": "24 May 2018"
  },
  {
    "firstname": "Sal",
    "lastname": "Necholy",
    "email": "",
    "company": "i am happy where i am working at",
    "position": "i am not looking for Job",
    "connect_date": "24 May 2018"
  },
  {
    "firstname": "Vincent",
    "lastname": "Clerc",
    "email": "",
    "company": "A.P. Moller - Maersk",
    "position": "Executive Vice President, Chief Commercial Officer",
    "connect_date": "24 May 2018"
  },
  {
    "firstname": "Anil",
    "lastname": "Bharath",
    "email": "",
    "company": "Imperial College London",
    "position": "Reader in Image Analysis",
    "connect_date": "23 May 2018"
  },
  {
    "firstname": "Borys",
    "lastname": "Konstantynov",
    "email": "",
    "company": "GREEN LIGHT-CS",
    "position": "Business Development Director",
    "connect_date": "21 May 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Wounlund",
    "email": "",
    "company": "Peopleway A/S",
    "position": "Chairman Of The Board",
    "connect_date": "21 May 2018"
  },
  {
    "firstname": "Laurence",
    "lastname": "Forcione",
    "email": "",
    "company": "1stdibs.com",
    "position": "Founder - Board Director",
    "connect_date": "18 May 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Wyke",
    "email": "",
    "company": "Hambro Perks",
    "position": "Partner at Hambro Perks",
    "connect_date": "18 May 2018"
  },
  {
    "firstname": "Anastasiia",
    "lastname": "Iling",
    "email": "",
    "company": "Kindle Vision (KIVI)",
    "position": "Head of Business Development KIVI TV",
    "connect_date": "17 May 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Gaston",
    "email": "",
    "company": "Calmer Solutions",
    "position": "Director of Business Development & Marketing",
    "connect_date": "16 May 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Schacht",
    "email": "",
    "company": "Resultpartner",
    "position": "Partner",
    "connect_date": "15 May 2018"
  },
  {
    "firstname": "Magnus",
    "lastname": "Andersson",
    "email": "",
    "company": "Kamstrup AB",
    "position": "Head Of Development",
    "connect_date": "15 May 2018"
  },
  {
    "firstname": "Brandon",
    "lastname": "Brotsky",
    "email": "",
    "company": "Payclub",
    "position": "CTO / Co-founder",
    "connect_date": "14 May 2018"
  },
  {
    "firstname": "Charlie",
    "lastname": "Casey",
    "email": "",
    "company": "LoyaltyLion",
    "position": "CEO",
    "connect_date": "12 May 2018"
  },
  {
    "firstname": "Dmitriy",
    "lastname": "Cherveniuk",
    "email": "",
    "company": "BRONA",
    "position": "Юрист",
    "connect_date": "10 May 2018"
  },
  {
    "firstname": "Milad",
    "lastname": "Safi",
    "email": "",
    "company": "Goveeto.com",
    "position": "Co-founder and CEO",
    "connect_date": "10 May 2018"
  },
  {
    "firstname": "Michael Alexander",
    "lastname": "Trolle",
    "email": "",
    "company": "FDC",
    "position": "Data Protection Officer at FDC",
    "connect_date": "08 May 2018"
  },
  {
    "firstname": "Christian Østergård",
    "lastname": "Kristensen",
    "email": "",
    "company": "Vestas",
    "position": "Vice President",
    "connect_date": "08 May 2018"
  },
  {
    "firstname": "Jesper",
    "lastname": "Holm Joensen",
    "email": "",
    "company": "iPaper A/S",
    "position": "Chairman of Board",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Steen",
    "lastname": "Møller Jensen",
    "email": "",
    "company": "Topdanmark",
    "position": "Underdirektør",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Henrik",
    "lastname": "Svensson",
    "email": "",
    "company": "Henrik Svensson",
    "position": "Managing Partner",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Kim",
    "lastname": "Løwert",
    "email": "",
    "company": "appstract",
    "position": "CEO, Founder and Partner",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Erling",
    "lastname": "Jensen",
    "email": "",
    "company": "Hansen Toft A/S",
    "position": "Partner hos Hansen Toft A/S, Executive Search & Selection - Leadership development",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Charlie",
    "lastname": "Smith",
    "email": "",
    "company": "Blis",
    "position": "Managing Director UK",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Toni",
    "lastname": "Hohlbein",
    "email": "",
    "company": "Falcon.io",
    "position": "COO",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Klitgaard",
    "email": "",
    "company": "dunenhof",
    "position": "Founder",
    "connect_date": "07 May 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Gledden",
    "email": "",
    "company": "aiPod",
    "position": "Co-founder, Chief Revenue Officer and EVP Corporate Strategy",
    "connect_date": "06 May 2018"
  },
  {
    "firstname": "Jörgen",
    "lastname": "Bertilsson",
    "email": "",
    "company": "Avensia",
    "position": "Executive Vice President global business development",
    "connect_date": "06 May 2018"
  },
  {
    "firstname": "Martin Brinch",
    "lastname": "Petersen",
    "email": "",
    "company": "Kruso",
    "position": "Chief Operating Officer",
    "connect_date": "05 May 2018"
  },
  {
    "firstname": "Mads",
    "lastname": "Jensen",
    "email": "",
    "company": "Sostrene Grene",
    "position": "CDO - Chief Digital and Marketing Officer",
    "connect_date": "05 May 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Sørensen",
    "email": "",
    "company": "EG A/S",
    "position": "Director",
    "connect_date": "04 May 2018"
  },
  {
    "firstname": "Roman",
    "lastname": "Shklyar",
    "email": "",
    "company": "EY Academy of Business",
    "position": "BDM",
    "connect_date": "04 May 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Walker",
    "email": "",
    "company": "Instrat AB",
    "position": "Partner",
    "connect_date": "04 May 2018"
  },
  {
    "firstname": "Steen",
    "lastname": "Wienke",
    "email": "",
    "company": "Cloudeon A/S - Innovative Cloud Solutions",
    "position": "Partner /  Vice-president Delivery",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Jan",
    "lastname": "Harding Gliemann",
    "email": "",
    "company": "GRUNDFOS",
    "position": "Senior R&D Director",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Søren",
    "lastname": "Dandanell Nielsen",
    "email": "",
    "company": "Cloudeon - Technology Changes Business",
    "position": "Co-Founder, Chief Business Development Officer",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Frank",
    "lastname": "Mogensen",
    "email": "",
    "company": "Cloudeon A/S",
    "position": "Founder",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Lasse",
    "lastname": "Berzanth Legarth",
    "email": "",
    "company": "Novicell",
    "position": "CTO / Udviklingschef",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Solomon",
    "lastname": "Rajput",
    "email": "",
    "company": "University of Michigan",
    "position": "Medical Student",
    "connect_date": "03 May 2018"
  },
  {
    "firstname": "Adam Peter",
    "lastname": "Nielsen",
    "email": "",
    "company": "Novicell",
    "position": "Teknologichef / CTO",
    "connect_date": "02 May 2018"
  },
  {
    "firstname": "David",
    "lastname": "de-Vilder",
    "email": "",
    "company": "Assetz Capital",
    "position": "Head Of Engineering",
    "connect_date": "01 May 2018"
  },
  {
    "firstname": "Денис",
    "lastname": "Москалюк",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "30 Apr 2018"
  },
  {
    "firstname": "Kjersti",
    "lastname": "Lund",
    "email": "",
    "company": "Designit",
    "position": "Global Executive Director",
    "connect_date": "30 Apr 2018"
  },
  {
    "firstname": "Frank",
    "lastname": "Jepsen",
    "email": "",
    "company": "Parabolic",
    "position": "Co-Founder",
    "connect_date": "30 Apr 2018"
  },
  {
    "firstname": "Anders",
    "lastname": "Mayntzhusen",
    "email": "",
    "company": "Dixa",
    "position": "CCO",
    "connect_date": "29 Apr 2018"
  },
  {
    "firstname": "Troels",
    "lastname": "Petersen",
    "email": "",
    "company": "Danfoss",
    "position": "Senior Vice President Danfoss M&A",
    "connect_date": "29 Apr 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Højgaard",
    "email": "mho@coinify.com",
    "company": "Coinify",
    "position": "CEO and Co-Founder",
    "connect_date": "27 Apr 2018"
  },
  {
    "firstname": "James",
    "lastname": "Johnson",
    "email": "",
    "company": "Align",
    "position": "Director",
    "connect_date": "27 Apr 2018"
  },
  {
    "firstname": "Jonas",
    "lastname": "Tempel",
    "email": "",
    "company": "Opopop",
    "position": "Founding Partner",
    "connect_date": "25 Apr 2018"
  },
  {
    "firstname": "Andy",
    "lastname": "Chen",
    "email": "",
    "company": "Weorder",
    "position": "CEO",
    "connect_date": "25 Apr 2018"
  },
  {
    "firstname": "Ole Jakob",
    "lastname": "Thorsen",
    "email": "",
    "company": "TAXA 4x35",
    "position": "Deputy Chairman of the Board of Directors",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Asger",
    "lastname": "Rasmussen",
    "email": "",
    "company": "Klipworks",
    "position": "CEO & Founder",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Jacob",
    "lastname": "Lindborg",
    "email": "",
    "company": "Klipworks",
    "position": "CTO & Creator",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Bohdan",
    "lastname": "Veretelnik",
    "email": "",
    "company": "BPO company",
    "position": "Бухгалтер",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Mads",
    "lastname": "Fosselius",
    "email": "",
    "company": "Dixa",
    "position": "CEO & Co-founder",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Jakob Nederby",
    "lastname": "Nielsen",
    "email": "",
    "company": "Dixa",
    "position": "Lead Front-end Developer and Co-founder",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Jesper",
    "lastname": "Goos",
    "email": "",
    "company": "Peytz & Co",
    "position": "Direktør og Partner",
    "connect_date": "24 Apr 2018"
  },
  {
    "firstname": "Adam",
    "lastname": "Newton",
    "email": "",
    "company": "Maersk Oil",
    "position": "Director of External Relations and Communications",
    "connect_date": "23 Apr 2018"
  },
  {
    "firstname": "Anna",
    "lastname": "Holte Thorius",
    "email": "",
    "company": "KMD",
    "position": "Head of Release Management",
    "connect_date": "23 Apr 2018"
  },
  {
    "firstname": "Pascal-Alexander",
    "lastname": "Felder",
    "email": "",
    "company": "InVision",
    "position": "Customer Success Manager",
    "connect_date": "23 Apr 2018"
  },
  {
    "firstname": "Bjarne",
    "lastname": "Aarup",
    "email": "",
    "company": "Danski A/S",
    "position": "Chairman of the Board",
    "connect_date": "22 Apr 2018"
  },
  {
    "firstname": "Fredrik",
    "lastname": "Östbye",
    "email": "",
    "company": "GRUNDFOS",
    "position": "Vice President, Head of  Digital Transformation",
    "connect_date": "21 Apr 2018"
  },
  {
    "firstname": "Ilya",
    "lastname": "Pozin",
    "email": "",
    "company": "Pluto TV",
    "position": "Chief Growth Officer & Co-Founder",
    "connect_date": "21 Apr 2018"
  },
  {
    "firstname": "Evgeniya",
    "lastname": "Potapova",
    "email": "",
    "company": "Luxoft",
    "position": "Researcher",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "David",
    "lastname": "McMillan",
    "email": "",
    "company": "GRUNDFOS",
    "position": "Executive Vice President - Building Services",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "Kenny",
    "lastname": "Bain",
    "email": "",
    "company": "Rant & Rave",
    "position": "CEO",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "Juha",
    "lastname": "Christensen",
    "email": "",
    "company": "CloudMade",
    "position": "Co-Founder, Chairman & CEO",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "Lars Bjørn",
    "lastname": "Falkenberg",
    "email": "",
    "company": "Charlie Tango",
    "position": "Vice President & Chief Executive Officer | Charlie Tango A/S - a KMD Company",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "Cini",
    "lastname": "Sathyavan",
    "email": "",
    "company": "Pluto TV",
    "position": "Head of Data Science, Analytics and Data Engineering",
    "connect_date": "20 Apr 2018"
  },
  {
    "firstname": "Jon Robert",
    "lastname": "Bradford",
    "email": "",
    "company": "fuego",
    "position": "Co-Founder/President",
    "connect_date": "19 Apr 2018"
  },
  {
    "firstname": "Nikita",
    "lastname": "Alferov",
    "email": "",
    "company": "Doc.ua",
    "position": "Mentor",
    "connect_date": "19 Apr 2018"
  },
  {
    "firstname": "Mehdi",
    "lastname": "Motaghiani",
    "email": "",
    "company": "Netcompany",
    "position": "Principal",
    "connect_date": "18 Apr 2018"
  },
  {
    "firstname": "Signe",
    "lastname": "Jarlov",
    "email": "",
    "company": "Netcompany",
    "position": "Principal",
    "connect_date": "17 Apr 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Sepstrup",
    "email": "",
    "company": "Ordbogen A/S",
    "position": "Head of IT",
    "connect_date": "17 Apr 2018"
  },
  {
    "firstname": "Christian",
    "lastname": "Poulsen",
    "email": "",
    "company": "COMM2IG A/S",
    "position": "Direktør",
    "connect_date": "17 Apr 2018"
  },
  {
    "firstname": "André",
    "lastname": "Rogaczewski",
    "email": "",
    "company": "Netcompany A/S",
    "position": "Adm Dir. / CEO",
    "connect_date": "16 Apr 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Edgar",
    "email": "",
    "company": "BESTSELLER",
    "position": "President USA / Canada",
    "connect_date": "16 Apr 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "McCarthy-Stott",
    "email": "",
    "company": "PushON",
    "position": "Head Of Development",
    "connect_date": "16 Apr 2018"
  },
  {
    "firstname": "Mathias",
    "lastname": "Mønsted",
    "email": "",
    "company": "SimplyJob.dk",
    "position": "Co-Founder",
    "connect_date": "16 Apr 2018"
  },
  {
    "firstname": "David",
    "lastname": "Helgason",
    "email": "",
    "company": "Unity Technologies",
    "position": "Founder & Board Member",
    "connect_date": "16 Apr 2018"
  },
  {
    "firstname": "Klaus",
    "lastname": "Holse",
    "email": "",
    "company": "SimCorp",
    "position": "CEO",
    "connect_date": "14 Apr 2018"
  },
  {
    "firstname": "Jeff",
    "lastname": "Ransdell",
    "email": "",
    "company": "Rokk3rFuel EXO",
    "position": "Founding Partner - Managing Director at Rokk3r Fuel",
    "connect_date": "14 Apr 2018"
  },
  {
    "firstname": "Matthew",
    "lastname": "Eames",
    "email": "",
    "company": "Eames Partnership - Executive Search & Leadership Consulting",
    "position": "Managing Partner/Founder",
    "connect_date": "14 Apr 2018"
  },
  {
    "firstname": "Per Nymann",
    "lastname": "Jørgensen",
    "email": "",
    "company": "KMD",
    "position": "Head Of Development",
    "connect_date": "13 Apr 2018"
  },
  {
    "firstname": "Villy",
    "lastname": "Gravengaard",
    "email": "",
    "company": "EG A/S",
    "position": "Vice President, Digital Solutions Scandinavia",
    "connect_date": "13 Apr 2018"
  },
  {
    "firstname": "Ulrik",
    "lastname": "Falkner Thagesen",
    "email": "",
    "company": "e-Boks A/S",
    "position": "CEO",
    "connect_date": "13 Apr 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Coburn",
    "email": "",
    "company": "Jebbit",
    "position": "Co-Founder, CEO",
    "connect_date": "13 Apr 2018"
  },
  {
    "firstname": "Peter G.",
    "lastname": "Jørgensen",
    "email": "",
    "company": "Boozt Fashion AB",
    "position": "CMO",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Kolster",
    "email": "",
    "company": "Danfoss",
    "position": "Head of Service Innovation",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "André",
    "lastname": "Borouchaki",
    "email": "",
    "company": "Danfoss",
    "position": "Chief Technology Officer",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Ulas",
    "lastname": "Karademir",
    "email": "",
    "company": "Unity Technologies",
    "position": "Vice President - Core Technology",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Jesus",
    "lastname": "Requena",
    "email": "",
    "company": "Unity Technologies",
    "position": "Director, Growth Marketing",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Vlad",
    "lastname": "Kapichulin",
    "email": "",
    "company": "Freelance",
    "position": "Recruiter",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Lars",
    "lastname": "Berntsen",
    "email": "",
    "company": "Dwarf",
    "position": "CEO",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Håkan",
    "email": "",
    "company": "Clio Online",
    "position": "CFO",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Na'Tosha",
    "lastname": "Bard",
    "email": "",
    "company": "Unity Technologies",
    "position": "Technical Director, Core R&D",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Juan",
    "lastname": "Montoya",
    "email": "",
    "company": "Rokk3r Labs",
    "position": "COO",
    "connect_date": "12 Apr 2018"
  },
  {
    "firstname": "Dana",
    "lastname": "Kosychenko",
    "email": "",
    "company": "Codemotion Ninjas",
    "position": "Business Development Manager",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Nicolai Schou",
    "lastname": "Jensen",
    "email": "",
    "company": "IT Minds",
    "position": "Director",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Landaiche",
    "email": "paul@rokk3rfuel.com",
    "company": "Rokk3rFuel EXO",
    "position": "General Partner - Chief Operating Officer",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Kristian Wichmand",
    "lastname": "Larsen",
    "email": "",
    "company": "CodersTrust",
    "position": "Investor and advisor",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Rene",
    "lastname": "Kristensen",
    "email": "",
    "company": "Fullrate",
    "position": "Director of Marketing, Channel Mgmt & Digital",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Alexandre",
    "lastname": "Likin",
    "email": "",
    "company": "FSM Group",
    "position": "Recruitment Consultant ⎜ IT Contract Freelance ⎜ Software Development",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Serhii",
    "lastname": "Piatnytskyi",
    "email": "",
    "company": "ReDWall",
    "position": "CEO",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Julia",
    "lastname": "Kolomiytseva",
    "email": "",
    "company": "Freelancer",
    "position": "IT Sales coach",
    "connect_date": "11 Apr 2018"
  },
  {
    "firstname": "Karen",
    "lastname": "Murphy",
    "email": "",
    "company": "TimeTrade Systems",
    "position": "Executive Assistant to CEO",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Kullmann",
    "email": "",
    "company": "beryl.cc",
    "position": "Head Of Operations",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Rahul",
    "lastname": "Yadav",
    "email": "",
    "company": "KMD",
    "position": "Chief R&D Officer, Vice President",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Drozdova",
    "lastname": "Valeria",
    "email": "",
    "company": "AB Soft/BVG Software Group",
    "position": "Sales manager",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Frederik",
    "lastname": "Scholten",
    "email": "",
    "company": "3 Danmark",
    "position": "Head of Marketing and CRM",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Poppy",
    "lastname": "Allen",
    "email": "",
    "company": "Parallel Consulting",
    "position": "Digital Recruitment Consultant",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Guy",
    "lastname": "Hipwell",
    "email": "",
    "company": "International Ecommerce Expert Consultant and Advisor",
    "position": "Advisor / Consultant / Mentor / Investor / Speaker for numerous entities",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Barnett",
    "email": "",
    "company": "Quantcast",
    "position": "Chief Product Officer",
    "connect_date": "10 Apr 2018"
  },
  {
    "firstname": "Denis",
    "lastname": "Peterson",
    "email": "",
    "company": "IBM",
    "position": "Chief Operating Officer - Financial Services Sector, Global Business Services",
    "connect_date": "09 Apr 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Tavaria",
    "email": "",
    "company": "EasyPost",
    "position": "Chief Operating Officer (Interim)",
    "connect_date": "09 Apr 2018"
  },
  {
    "firstname": "Damilola",
    "lastname": "Samuel",
    "email": "",
    "company": "Kozak Group",
    "position": "Sales Manager",
    "connect_date": "09 Apr 2018"
  },
  {
    "firstname": "Job",
    "lastname": "den Hamer",
    "email": "",
    "company": "Coorp.ID",
    "position": "Initiative lead at Coorp.ID",
    "connect_date": "09 Apr 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Shaw",
    "email": "",
    "company": "RealityMine",
    "position": "COO",
    "connect_date": "08 Apr 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Goodwin",
    "email": "",
    "company": "LANDMARK INFORMATION GROUP LIMITED",
    "position": "Board Of Directors",
    "connect_date": "08 Apr 2018"
  },
  {
    "firstname": "Otto",
    "lastname": "Vos",
    "email": "",
    "company": "Payconiq",
    "position": "Product Owner",
    "connect_date": "08 Apr 2018"
  },
  {
    "firstname": "Ricardo",
    "lastname": "De Jong",
    "email": "",
    "company": "FUGA",
    "position": "Product Owner",
    "connect_date": "07 Apr 2018"
  },
  {
    "firstname": "Andrey",
    "lastname": "Koretsky",
    "email": "",
    "company": "EasyPay",
    "position": "Product manager",
    "connect_date": "07 Apr 2018"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Zamura",
    "email": "",
    "company": "SYTOSS",
    "position": "Director of Sales and Marketing",
    "connect_date": "07 Apr 2018"
  },
  {
    "firstname": "Eugene",
    "lastname": "Kravchenko",
    "email": "",
    "company": "Independent projects",
    "position": "Head of sales and business development",
    "connect_date": "07 Apr 2018"
  },
  {
    "firstname": "Bogdan",
    "lastname": "Dushutin",
    "email": "",
    "company": "Freelance",
    "position": "Sales Manager",
    "connect_date": "07 Apr 2018"
  },
  {
    "firstname": "Mehmet",
    "lastname": "Cakmak",
    "email": "",
    "company": "YouSee",
    "position": "Developer",
    "connect_date": "06 Apr 2018"
  },
  {
    "firstname": "Rowan",
    "lastname": "Milwid",
    "email": "",
    "company": "Castor EDC",
    "position": "Head of Product",
    "connect_date": "06 Apr 2018"
  },
  {
    "firstname": "Federico",
    "lastname": "Guglielmino",
    "email": "",
    "company": "Vivacity Labs",
    "position": "Internal Operations",
    "connect_date": "06 Apr 2018"
  },
  {
    "firstname": "Joshua",
    "lastname": "Nathan",
    "email": "",
    "company": "Runpath",
    "position": "IT Manager",
    "connect_date": "05 Apr 2018"
  },
  {
    "firstname": "Antoine",
    "lastname": "Larmanjat",
    "email": "",
    "company": "Euler Hermes",
    "position": "Group CIO",
    "connect_date": "05 Apr 2018"
  },
  {
    "firstname": "Niels",
    "lastname": "Abildskov",
    "email": "",
    "company": "Nordea",
    "position": "Chief IT Infrastructure Specialist",
    "connect_date": "05 Apr 2018"
  },
  {
    "firstname": "Sacha",
    "lastname": "Manson-Smith",
    "email": "",
    "company": "beryl.cc",
    "position": "Senior Software Engineer",
    "connect_date": "05 Apr 2018"
  },
  {
    "firstname": "Jon",
    "lastname": "Oslowski",
    "email": "",
    "company": "H2O.ai",
    "position": "Chief Financial Officer",
    "connect_date": "05 Apr 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Havenaar",
    "email": "",
    "company": "TechEquipt Pty Ltd",
    "position": "Technical Director",
    "connect_date": "04 Apr 2018"
  },
  {
    "firstname": "Ray",
    "lastname": "Cotton",
    "email": "",
    "company": "Brown University",
    "position": "Graduate Teaching Associate- EMCS 2210, Privacy and Personal Data Protection",
    "connect_date": "04 Apr 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Lynch",
    "email": "",
    "company": "AtScale",
    "position": "Executive Chairman & CEO",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Greg",
    "lastname": "Michaelson",
    "email": "",
    "company": "DataRobot",
    "position": "General Manager for Banking",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Sri Satish",
    "lastname": "Ambati",
    "email": "",
    "company": "H2O (0xdata)",
    "position": "CEO & Co-founder",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Briffett",
    "email": "",
    "company": "Wagestream",
    "position": "CEO, Co-Founder",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Arno",
    "lastname": "Candel",
    "email": "",
    "company": "H2O.ai",
    "position": "CTO",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Emily",
    "lastname": "Brooke MBE",
    "email": "",
    "company": "beryl.cc",
    "position": "CEO & Founder",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Karen",
    "lastname": "McCormick",
    "email": "",
    "company": "Beringea LLP",
    "position": "Chief Investment Officer",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Joseph",
    "lastname": "Galea",
    "email": "",
    "company": "Wolters Kluwer ELM Solutions",
    "position": "Global Sales Director",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Ephgrave",
    "email": "",
    "company": "Festicket",
    "position": "COO",
    "connect_date": "03 Apr 2018"
  },
  {
    "firstname": "Timothy",
    "lastname": "Young",
    "email": "",
    "company": "ENIAC Ventures",
    "position": "Founding General Partner",
    "connect_date": "02 Apr 2018"
  },
  {
    "firstname": "Artur",
    "lastname": "Shpakovskyi",
    "email": "",
    "company": "Red Tag Ukraine",
    "position": "Salesforce Developer",
    "connect_date": "02 Apr 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Rous",
    "email": "",
    "company": "Various",
    "position": "Venture Investor",
    "connect_date": "02 Apr 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Hennessy",
    "email": "",
    "company": "SmashFly Technologies",
    "position": "Founder",
    "connect_date": "02 Apr 2018"
  },
  {
    "firstname": "Kilian",
    "lastname": "Kämpfen",
    "email": "",
    "company": "Ringier AG",
    "position": "Chief Business Development Officer / Head of International Marketplace Unit",
    "connect_date": "02 Apr 2018"
  },
  {
    "firstname": "Erik",
    "lastname": "Schluntz",
    "email": "",
    "company": "Cobalt Robotics",
    "position": "Cofounder & CTO",
    "connect_date": "01 Apr 2018"
  },
  {
    "firstname": "Anthony",
    "lastname": "Rose",
    "email": "",
    "company": "SeedLegals",
    "position": "Founder and CEO",
    "connect_date": "31 Mar 2018"
  },
  {
    "firstname": "Matt",
    "lastname": "Bennett-Blacklock",
    "email": "",
    "company": "Beamly",
    "position": "SVP Engineering",
    "connect_date": "31 Mar 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Alvey",
    "email": "",
    "company": "Clipisode",
    "position": "CEO",
    "connect_date": "30 Mar 2018"
  },
  {
    "firstname": "Pierre",
    "lastname": "Socha",
    "email": "",
    "company": "Amadeus Capital Partners",
    "position": "Principal",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Oli",
    "lastname": "Brown",
    "email": "",
    "company": "Paymentsense",
    "position": "Partnership Manager",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Martin",
    "lastname": "Peniak",
    "email": "",
    "company": "Cortexica Vision Systems",
    "position": "Head of Innovation",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Watson",
    "email": "",
    "company": "Synthace Limited",
    "position": "Chief Commercial Officer",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Amy",
    "lastname": "Kean",
    "email": "",
    "company": "Starcom",
    "position": "Head of Strategic Innovation, Starcom Global Clients",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Raz",
    "lastname": "Qazalbash",
    "email": "",
    "company": "Beamly",
    "position": "Programmatic Director",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Alesia",
    "lastname": "Sorokina",
    "email": "",
    "company": "AltaClub ( Extension of Altair VC )",
    "position": "Investor relations manager for East Europe in AltaClub vc",
    "connect_date": "29 Mar 2018"
  },
  {
    "firstname": "Alberto",
    "lastname": "Janza",
    "email": "",
    "company": "GrayHair Software, Inc.",
    "position": "CTO",
    "connect_date": "28 Mar 2018"
  },
  {
    "firstname": "Provide Site",
    "lastname": "Studio",
    "email": "",
    "company": "Provide Site Studio",
    "position": "Команда",
    "connect_date": "28 Mar 2018"
  },
  {
    "firstname": "Azahara",
    "lastname": "Egea",
    "email": "",
    "company": "sync.",
    "position": "Head Of Operations",
    "connect_date": "28 Mar 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Qiu",
    "email": "",
    "company": "Udemy",
    "position": "SVP Global Business Development",
    "connect_date": "27 Mar 2018"
  },
  {
    "firstname": "Ed",
    "lastname": "Wal",
    "email": "",
    "company": "BWD Partnership Ltd",
    "position": "Managing Director",
    "connect_date": "27 Mar 2018"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Meredith",
    "email": "",
    "company": "Webrevolve",
    "position": "SEO & SMM Technician",
    "connect_date": "27 Mar 2018"
  },
  {
    "firstname": "Larby S.",
    "lastname": "Amirouche",
    "email": "",
    "company": "Starlight Group",
    "position": "Founder & CEO",
    "connect_date": "26 Mar 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "McCourt",
    "email": "",
    "company": "Cobalt Robotics",
    "position": "Head of Commercialization",
    "connect_date": "26 Mar 2018"
  },
  {
    "firstname": "Dave",
    "lastname": "Kerpen",
    "email": "",
    "company": "Normal",
    "position": "Author",
    "connect_date": "26 Mar 2018"
  },
  {
    "firstname": "Francesco",
    "lastname": "Palazzo",
    "email": "",
    "company": "Sky",
    "position": "Group Security Consultant",
    "connect_date": "25 Mar 2018"
  },
  {
    "firstname": "Open",
    "lastname": "World",
    "email": "",
    "company": "Open World",
    "position": "Директор проекта",
    "connect_date": "25 Mar 2018"
  },
  {
    "firstname": "Barny",
    "lastname": "Pinner",
    "email": "",
    "company": "DigitalGenius",
    "position": "Senior Sales Engineer",
    "connect_date": "23 Mar 2018"
  },
  {
    "firstname": "Yoram",
    "lastname": "Bachrach",
    "email": "",
    "company": "DeepMind",
    "position": "Staff Research Scientist",
    "connect_date": "23 Mar 2018"
  },
  {
    "firstname": "Kate",
    "lastname": "Cooper",
    "email": "",
    "company": "YMCA Norfolk",
    "position": "Communications and Marketing Officer",
    "connect_date": "23 Mar 2018"
  },
  {
    "firstname": "Johannes Bambang",
    "lastname": "Wirawan",
    "email": "",
    "company": "Entry.Money",
    "position": "Fintech Advisor/Board member",
    "connect_date": "23 Mar 2018"
  },
  {
    "firstname": "Art",
    "lastname": "Stavenka",
    "email": "",
    "company": "Kino-mo",
    "position": "Founder",
    "connect_date": "23 Mar 2018"
  },
  {
    "firstname": "Alla",
    "lastname": "Demidova",
    "email": "",
    "company": "Kino-mo",
    "position": "COO",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Carl",
    "lastname": "Rodrigues",
    "email": "",
    "company": "Streamr",
    "position": "Advisor & Partnerships Ambassador",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Grey",
    "lastname": "Chen",
    "email": "",
    "company": "Skylight Investment",
    "position": "Founding Partner",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Jaffe",
    "email": "",
    "company": "ChowNow",
    "position": "Co-founder and COO",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Anton",
    "lastname": "Macius",
    "email": "",
    "company": "Gett",
    "position": "UK Head of Product and Development & Global Head of Professional Services",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Vance",
    "lastname": "Loiselle",
    "email": "",
    "company": "Boston Logic",
    "position": "President & CEO",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Taylor",
    "lastname": "Allen",
    "email": "",
    "company": "Oliver Bernard",
    "position": "Team Lead",
    "connect_date": "22 Mar 2018"
  },
  {
    "firstname": "Pete",
    "lastname": "Steggles",
    "email": "",
    "company": "Ubisense",
    "position": "Founder and Chief Software Architect",
    "connect_date": "21 Mar 2018"
  },
  {
    "firstname": "Ryan",
    "lastname": "Watson",
    "email": "",
    "company": "Synergy Interactive",
    "position": "President | Partner",
    "connect_date": "19 Mar 2018"
  },
  {
    "firstname": "Kielty",
    "lastname": "Hughes",
    "email": "",
    "company": "AIB",
    "position": "Head of Analytics Services",
    "connect_date": "18 Mar 2018"
  },
  {
    "firstname": "Gemma",
    "lastname": "Connolly",
    "email": "",
    "company": "Runpath",
    "position": "Head Of Operations",
    "connect_date": "18 Mar 2018"
  },
  {
    "firstname": "Danny",
    "lastname": "Gavin",
    "email": "",
    "company": "Brian Gavin Diamonds",
    "position": "VP, Director of Marketing",
    "connect_date": "16 Mar 2018"
  },
  {
    "firstname": "Marcelo",
    "lastname": "Pio",
    "email": "",
    "company": "weLOVE.education",
    "position": "Founder",
    "connect_date": "16 Mar 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Daly",
    "email": "",
    "company": "Vesta Property",
    "position": "Head of Engineering",
    "connect_date": "15 Mar 2018"
  },
  {
    "firstname": "Radboud",
    "lastname": "Vlaar",
    "email": "",
    "company": "Robeco",
    "position": "Board Member",
    "connect_date": "15 Mar 2018"
  },
  {
    "firstname": "Joshua",
    "lastname": "Sims",
    "email": "",
    "company": "Dynatrace",
    "position": "Director of Brand",
    "connect_date": "15 Mar 2018"
  },
  {
    "firstname": "Mukul",
    "lastname": "Goyal",
    "email": "",
    "company": "TimeTrade",
    "position": "Chief Technology Officer",
    "connect_date": "15 Mar 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Webb",
    "email": "",
    "company": "ChowNow",
    "position": "CEO / Co-Founder",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Keith",
    "lastname": "Doe",
    "email": "",
    "company": "Karhoo",
    "position": "Head Of Operations",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Leah",
    "lastname": "Debes",
    "email": "",
    "company": "ChowNow",
    "position": "Director of Partner Experience",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Sukhdeep",
    "lastname": "Singh",
    "email": "",
    "company": "Orro",
    "position": "Strategic Operations Lead",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Damian",
    "lastname": "Kimmelman",
    "email": "",
    "company": "DueDil",
    "position": "Co-Founder and CEO",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Jason",
    "lastname": "Hsiao",
    "email": "",
    "company": "Animoto",
    "position": "President & Co-Founder",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "John",
    "lastname": "Trobough",
    "email": "",
    "company": "JLA Advisors",
    "position": "Managing Director",
    "connect_date": "14 Mar 2018"
  },
  {
    "firstname": "Skip",
    "lastname": "Fidura",
    "email": "",
    "company": "DMA UK",
    "position": "Non Executive Director",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "von Eckartsberg",
    "email": "",
    "company": "Vricon",
    "position": "Senior Vice President, Government - Chief Revenue Officer",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Tink",
    "lastname": "Taylor",
    "email": "",
    "company": "dotMailer",
    "position": "Founder & President",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Tim",
    "lastname": "Winkler",
    "email": "",
    "company": "hatch IT",
    "position": "Founder & CEO",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Kathy",
    "lastname": "Perrotte",
    "email": "",
    "company": "ActiveViam",
    "position": "Managing Director",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Vincent",
    "lastname": "Fletcher",
    "email": "",
    "company": "CartonCloud",
    "position": "Founder",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Damian",
    "lastname": "Saunders",
    "email": "",
    "company": "Synopsys Inc",
    "position": "Director, EMEA, Software Composition Analysis",
    "connect_date": "13 Mar 2018"
  },
  {
    "firstname": "Gary",
    "lastname": "Ambrosino",
    "email": "",
    "company": "TimeTrade",
    "position": "Chief Executive Officer",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Brown",
    "email": "",
    "company": "Basis Technology",
    "position": "VP International",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Kieran",
    "lastname": "Flanagan",
    "email": "",
    "company": "HubSpot",
    "position": "VP Marketing/Growth",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Andy",
    "lastname": "Ward",
    "email": "",
    "company": "Ubisense",
    "position": "Chief Technology Officer and Founder",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Darren",
    "lastname": "Sabet",
    "email": "",
    "company": "Ed-admin",
    "position": "Director of Product Development",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Lu",
    "email": "",
    "company": "Cvent",
    "position": "Vice President of Product Development",
    "connect_date": "12 Mar 2018"
  },
  {
    "firstname": "Georgie",
    "lastname": "Mack",
    "email": "",
    "company": "Made by Many",
    "position": "Managing Partner",
    "connect_date": "11 Mar 2018"
  },
  {
    "firstname": "Timothy",
    "lastname": "Kasbe",
    "email": "",
    "company": "The Warehouse Group",
    "position": "Chief Information and Digital Officer",
    "connect_date": "11 Mar 2018"
  },
  {
    "firstname": "Darren",
    "lastname": "Frlan",
    "email": "",
    "company": "Paxus - Technology + Digital Talent",
    "position": "Victorian State Manager / Recruitment Leader",
    "connect_date": "11 Mar 2018"
  },
  {
    "firstname": "Sirish",
    "lastname": "Thapa",
    "email": "",
    "company": "RainFocus",
    "position": "Product Owner",
    "connect_date": "09 Mar 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "Herzog",
    "email": "",
    "company": "IBM - The World's #2 Storage Company in Revenue",
    "position": "Chief Marketing Officer and VP of Worldwide Storage Channels - IBM Storage Division",
    "connect_date": "09 Mar 2018"
  },
  {
    "firstname": "Roi",
    "lastname": "Zahut",
    "email": "",
    "company": "IBM",
    "position": "Chief Technology Officer - Global Advanced Analytics CoC",
    "connect_date": "09 Mar 2018"
  },
  {
    "firstname": "James",
    "lastname": "Markarian",
    "email": "",
    "company": "SnapLogic",
    "position": "CTO",
    "connect_date": "09 Mar 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Zellem",
    "email": "",
    "company": "Chef Software",
    "position": "Regional Sales Director",
    "connect_date": "09 Mar 2018"
  },
  {
    "firstname": "Rob",
    "lastname": "Williams",
    "email": "",
    "company": "American Eagle Institute - China",
    "position": "TEFL",
    "connect_date": "08 Mar 2018"
  },
  {
    "firstname": "Katie",
    "lastname": "Fry ✰",
    "email": "",
    "company": "Sydney UI/UX Meetup",
    "position": "Co-Founder",
    "connect_date": "08 Mar 2018"
  },
  {
    "firstname": "Jon",
    "lastname": "Holloway",
    "email": "",
    "company": "Zuper Superannuation",
    "position": "Co-Founder & Chief Product Officer",
    "connect_date": "08 Mar 2018"
  },
  {
    "firstname": "Cristina",
    "lastname": "Cozma, PMP, CSPO",
    "email": "",
    "company": "Expedia Group",
    "position": "PM / Product Owner / Sr. Process Analyst / Sr. Systems Analyst",
    "connect_date": "07 Mar 2018"
  },
  {
    "firstname": "Stijn",
    "lastname": "Paumen",
    "email": "",
    "company": "Wandera, Inc.",
    "position": "VP Sales & BD",
    "connect_date": "07 Mar 2018"
  },
  {
    "firstname": "Manuel",
    "lastname": "Noirfalise",
    "email": "",
    "company": "Independent",
    "position": "Digital Transformation Specialist",
    "connect_date": "07 Mar 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Fahey",
    "email": "",
    "company": "Brookson",
    "position": "Managing Director - Brookson One",
    "connect_date": "06 Mar 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Fenner",
    "email": "",
    "company": "Clearvision",
    "position": "Technical Consultant",
    "connect_date": "06 Mar 2018"
  },
  {
    "firstname": "Gerald",
    "lastname": "Tombs",
    "email": "",
    "company": "Clearvision",
    "position": "CEO",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Jake",
    "lastname": "Churcher",
    "email": "",
    "company": "Clearvision",
    "position": "IT Services & Operations Manager",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Adam",
    "lastname": "Dodds",
    "email": "",
    "company": "Freetrade",
    "position": "CEO & Founder",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Joshua",
    "lastname": "Pierson",
    "email": "",
    "company": "Ometria",
    "position": "Business Development Director",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Davide",
    "lastname": "Fioranelli",
    "email": "",
    "company": "Lumen Ventures",
    "position": "Founder & Managing Partner",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Zein",
    "lastname": "Jaber",
    "email": "",
    "company": "Cloudflare, Inc.",
    "position": "Technical Support Engineer",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Hannah",
    "lastname": "Waller",
    "email": "",
    "company": "Clearvision",
    "position": "Events Manager",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Yoma James",
    "lastname": "Kukor",
    "email": "",
    "company": "WiseAlpha",
    "position": "Investor",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "J-P",
    "lastname": "Wilkins",
    "email": "",
    "company": "Runpath",
    "position": "Managing Director",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Andy",
    "lastname": "Carmichael",
    "email": "",
    "company": "Huge IO (UK & Ireland) Ltd",
    "position": "Managing Director and Principal Trainer",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Morris",
    "email": "",
    "company": "Expedia, Inc.",
    "position": "Director of Customer Operations - Brand Expedia EMEA",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Neil",
    "lastname": "Butler",
    "email": "",
    "company": "Digital Dimensions",
    "position": "Managing and delivering complex digital transformation programmes",
    "connect_date": "05 Mar 2018"
  },
  {
    "firstname": "Rory",
    "lastname": "Sutherland",
    "email": "",
    "company": "Ogilvy",
    "position": "Vice Chairman",
    "connect_date": "04 Mar 2018"
  },
  {
    "firstname": "Thais",
    "lastname": "Solano Langarica",
    "email": "",
    "company": "Valtech Netherlands",
    "position": "Senior Digital Project Manager",
    "connect_date": "04 Mar 2018"
  },
  {
    "firstname": "Dominic",
    "lastname": "Perks",
    "email": "",
    "company": "Hambro Perks",
    "position": "CEO / Co-founder",
    "connect_date": "03 Mar 2018"
  },
  {
    "firstname": "Duncan",
    "lastname": "Barrigan",
    "email": "",
    "company": "GoCardless",
    "position": "VP Product",
    "connect_date": "03 Mar 2018"
  },
  {
    "firstname": "Shaan",
    "lastname": "Kodituwakku",
    "email": "",
    "company": "Agenor Technology",
    "position": "Senior IT Project Manager",
    "connect_date": "03 Mar 2018"
  },
  {
    "firstname": "Hiroki",
    "lastname": "Takeuchi",
    "email": "",
    "company": "GoCardless",
    "position": "CEO",
    "connect_date": "02 Mar 2018"
  },
  {
    "firstname": "Steve",
    "lastname": "Harper",
    "email": "",
    "company": "Adaptavist",
    "position": "Principal Consultant",
    "connect_date": "01 Mar 2018"
  },
  {
    "firstname": "Nataliia",
    "lastname": "Cheliada, ACCA",
    "email": "",
    "company": "SoftServe",
    "position": "Senior Manager of Global Talent Management | HR CoE",
    "connect_date": "01 Mar 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Kovach",
    "email": "",
    "company": "VIHOR",
    "position": "Co-Founder & COO",
    "connect_date": "28 Feb 2018"
  },
  {
    "firstname": "Jeff",
    "lastname": "Rick",
    "email": "",
    "company": "Deerwalk Inc",
    "position": "Chief Operating Officer",
    "connect_date": "27 Feb 2018"
  },
  {
    "firstname": "Carl",
    "lastname": "Gough",
    "email": "",
    "company": "GAMESTARS LTD",
    "position": "Owner",
    "connect_date": "27 Feb 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Beck",
    "email": "",
    "company": "Darktrace",
    "position": "Global Head of Threat Analysis",
    "connect_date": "27 Feb 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Dines",
    "email": "",
    "company": "UiPath - Robotic Process Automation",
    "position": "CEO & Founder",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Hjelming",
    "email": "",
    "company": "YouInWeb",
    "position": "CTO Board Advisor & Chief Engineer",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Hank",
    "lastname": "Dorkin",
    "email": "",
    "company": "Massachusetts Medical Society",
    "position": "President",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Dr",
    "lastname": "Karim",
    "email": "",
    "company": "MiRAK-ICO",
    "position": "Founder - CEO",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "McCann",
    "email": "",
    "company": "YOMA",
    "position": "Director/Owner",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Nelson",
    "lastname": "Wootton",
    "email": "",
    "company": "Saescada Limited",
    "position": "CEO",
    "connect_date": "26 Feb 2018"
  },
  {
    "firstname": "Meelan",
    "lastname": "Radia",
    "email": "",
    "company": "Yieldify",
    "position": "Co-Founder / Chief Strategy Officer",
    "connect_date": "25 Feb 2018"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Ward",
    "email": "",
    "company": "Cloudreach",
    "position": "Cloud Strategist",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Heidi",
    "lastname": "Herre",
    "email": "",
    "company": "Yoyo",
    "position": "VP of CX - Customer Experience",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Vasile",
    "lastname": "Foca",
    "email": "",
    "company": "Talis Capital Limited",
    "position": "Co-Founder / Managing Partner",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Devika",
    "lastname": "Wood",
    "email": "",
    "company": "Vida Care",
    "position": "Founder",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Brandon",
    "lastname": "Forrest",
    "email": "",
    "company": "American Technologies, Inc.",
    "position": "Senior Corporate Recruiter",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Светлана",
    "lastname": "Устина",
    "email": "",
    "company": "Белсистемтехнологии",
    "position": "Инженер-конструктор",
    "connect_date": "23 Feb 2018"
  },
  {
    "firstname": "Tom",
    "lastname": "Fowler",
    "email": "",
    "company": "Yext",
    "position": "Director, Enterprise Sales",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "John",
    "lastname": "Rosenberg",
    "email": "",
    "company": "tastyworks",
    "position": "Investor",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Strang",
    "email": "",
    "company": "EventsTag",
    "position": "CEO and Co-Founder",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Andreou",
    "email": "",
    "company": "LifeWorks",
    "position": "Chief Commercial Officer",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Les",
    "lastname": "Jeal",
    "email": "",
    "company": "Yoyo",
    "position": "Business Development Manager",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Vyacheslav",
    "lastname": "Fabrichnikov",
    "email": "",
    "company": "ООО Маслозавод \"Пензенский\"",
    "position": "Главный энергетик",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Faye",
    "lastname": "Whitlock",
    "email": "",
    "company": "Culture Trip",
    "position": "Director of Recruitment",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Andrew",
    "lastname": "Anderson",
    "email": "",
    "company": "Liftshare",
    "position": "Business Engagement",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Daniel",
    "lastname": "Mooney",
    "email": "",
    "company": "GoCardless",
    "position": "Head of Customer Operations",
    "connect_date": "22 Feb 2018"
  },
  {
    "firstname": "Sander",
    "lastname": "van O",
    "email": "",
    "company": "Awin Global",
    "position": "Head Of Product - London",
    "connect_date": "21 Feb 2018"
  },
  {
    "firstname": "Ross",
    "lastname": "Menghini",
    "email": "",
    "company": "Hear & Tell",
    "position": "Co-Founder",
    "connect_date": "21 Feb 2018"
  },
  {
    "firstname": "Anh",
    "lastname": "Pham Vu",
    "email": "",
    "company": "ueni ltd",
    "position": "co-founder",
    "connect_date": "20 Feb 2018"
  },
  {
    "firstname": "Phil",
    "lastname": "Soltau",
    "email": "",
    "company": "Silobreaker Ltd",
    "position": "HR & Talent Acquisition Manager",
    "connect_date": "20 Feb 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Stephany",
    "email": "",
    "company": "Beam",
    "position": "Founder & CEO",
    "connect_date": "20 Feb 2018"
  },
  {
    "firstname": "Rafael",
    "lastname": "Franco",
    "email": "",
    "company": "Devbridge Group",
    "position": "Senior Product Manager",
    "connect_date": "18 Feb 2018"
  },
  {
    "firstname": "Paul",
    "lastname": "Sulyok",
    "email": "",
    "company": "Green Man Gaming",
    "position": "Founder & CEO",
    "connect_date": "18 Feb 2018"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Voloshynskyy",
    "email": "",
    "company": "N-iX",
    "position": "Delivery Manager",
    "connect_date": "17 Feb 2018"
  },
  {
    "firstname": "Lewis",
    "lastname": "Kite",
    "email": "",
    "company": "FAXI LTD",
    "position": "Chief Community Officer",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Terrell",
    "email": "",
    "company": "JustPark",
    "position": "Head of Data & Analytics",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Jade",
    "lastname": "Bickerstaff",
    "email": "",
    "company": "Adaptavist",
    "position": "Head of Internal Systems",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Joel",
    "lastname": "Windels",
    "email": "",
    "company": "NetMotion Software",
    "position": "VP Global Marketing",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "James",
    "lastname": "Morris",
    "email": "",
    "company": "JustPark",
    "position": "HR Administrator & Recruitment",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "OLEG",
    "lastname": "POLIANCHUK",
    "email": "",
    "company": "VRobot",
    "position": "CEO/Fonder",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Yana",
    "lastname": "Ananevych",
    "email": "",
    "company": "AgiliWay Group, Inc.",
    "position": "HR/Recruiter",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Dan",
    "lastname": "Baylis",
    "email": "",
    "company": "JWT",
    "position": "Integrated Project Director",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Алексей",
    "lastname": "Обоянский",
    "email": "",
    "company": "Freelancer",
    "position": "HTML coder",
    "connect_date": "16 Feb 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Greeno",
    "email": "",
    "company": "Cuvva",
    "position": "CTO",
    "connect_date": "15 Feb 2018"
  },
  {
    "firstname": "Louis",
    "lastname": "Georgiou",
    "email": "",
    "company": "Code Computerlove",
    "position": "Founder and Director",
    "connect_date": "15 Feb 2018"
  },
  {
    "firstname": "Jonas",
    "lastname": "Partner",
    "email": "",
    "company": "Habiplace",
    "position": "CTO",
    "connect_date": "15 Feb 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Phillips",
    "email": "",
    "company": "Afill",
    "position": "Founder, CEO - We're hiring!",
    "connect_date": "14 Feb 2018"
  },
  {
    "firstname": "Christopher",
    "lastname": "Daniells",
    "email": "",
    "company": "Ascend Diagnostics",
    "position": "Contract Senior Javascript Engineer",
    "connect_date": "14 Feb 2018"
  },
  {
    "firstname": "Kristin",
    "lastname": "Mauck",
    "email": "",
    "company": "Nordstrom",
    "position": "Director, HR Technology",
    "connect_date": "14 Feb 2018"
  },
  {
    "firstname": "Андрей",
    "lastname": "Мазеин",
    "email": "",
    "company": "A-Soft",
    "position": "Front End Developer",
    "connect_date": "13 Feb 2018"
  },
  {
    "firstname": "Luca",
    "lastname": "Pagano",
    "email": "",
    "company": "BeMyEye",
    "position": "Group CEO",
    "connect_date": "13 Feb 2018"
  },
  {
    "firstname": "Andy",
    "lastname": "Feinberg",
    "email": "",
    "company": "Not Announced Yet",
    "position": "Stealth Mode",
    "connect_date": "13 Feb 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Tsuchiya",
    "email": "",
    "company": "Softsales",
    "position": "Software Sales Consultant- Founder",
    "connect_date": "13 Feb 2018"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Tsuprik",
    "email": "",
    "company": "Freelance/Contract",
    "position": "3D Modeler Hard surface",
    "connect_date": "13 Feb 2018"
  },
  {
    "firstname": "Jon",
    "lastname": "Woodall",
    "email": "",
    "company": "Space 48",
    "position": "Founder & Managing Director",
    "connect_date": "10 Feb 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Stewart",
    "email": "",
    "company": "Mubaloo Limited",
    "position": "CTO",
    "connect_date": "10 Feb 2018"
  },
  {
    "firstname": "Stein Thore",
    "lastname": "Haga",
    "email": "",
    "company": "Multimedia Nordic",
    "position": "CEO",
    "connect_date": "09 Feb 2018"
  },
  {
    "firstname": "Aaron",
    "lastname": "Roy",
    "email": "",
    "company": "Bond Inc",
    "position": "Chief Product Officer",
    "connect_date": "09 Feb 2018"
  },
  {
    "firstname": "Abhishek",
    "lastname": "Srivastava",
    "email": "",
    "company": "DATA Inc UK",
    "position": "Senior Talent Acquisition Consultant",
    "connect_date": "08 Feb 2018"
  },
  {
    "firstname": "Kevin",
    "lastname": "Riches",
    "email": "",
    "company": "Momenta Operations",
    "position": "Non Executive Director",
    "connect_date": "08 Feb 2018"
  },
  {
    "firstname": "Phil",
    "lastname": "Brady",
    "email": "",
    "company": "Sportpesa",
    "position": "DevOps Engineer",
    "connect_date": "08 Feb 2018"
  },
  {
    "firstname": "David",
    "lastname": "Killick",
    "email": "",
    "company": "Karmarama",
    "position": "Planning Director",
    "connect_date": "08 Feb 2018"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Trufin",
    "email": "",
    "company": "Carlsberg Group",
    "position": "National Legal Manager",
    "connect_date": "08 Feb 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "O'Loan",
    "email": "",
    "company": "Smarte",
    "position": "CEO",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Rutley",
    "email": "",
    "company": "PushON",
    "position": "Managing Director",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Sheen",
    "email": "",
    "company": "Sideways 6",
    "position": "CMO",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Nigel",
    "lastname": "Crutchley",
    "email": "",
    "company": "Actualize1",
    "position": "CEO",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Jon",
    "lastname": "Wiltshire",
    "email": "",
    "company": "Space66",
    "position": "Founder & CEO",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "James",
    "lastname": "Naylor",
    "email": "",
    "company": "James Naylor",
    "position": "Freelance Product Consultant",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Mathew",
    "lastname": "Marchant",
    "email": "",
    "company": "Al Tayer Group",
    "position": "Lead Magento Developer",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Barber (PGDipM MCIM)",
    "email": "",
    "company": "Praesto Consulting",
    "position": "Marketing Director",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Cheyne",
    "email": "",
    "company": "Block Matrix",
    "position": "Founder",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Nick",
    "lastname": "Rhind",
    "email": "",
    "company": "CTI Digital",
    "position": "CEO",
    "connect_date": "07 Feb 2018"
  },
  {
    "firstname": "Karl",
    "lastname": "Barker",
    "email": "",
    "company": "Cube3",
    "position": "CEO & Founder",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Laurence",
    "lastname": "Parkes",
    "email": "",
    "company": "Rufus Leonard",
    "position": "Chief Strategy Officer",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Brian",
    "lastname": "Robinson",
    "email": "",
    "company": "Dept Agency",
    "position": "Managing Director Design and Technology - UK and US",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Lawrence",
    "lastname": "Weber",
    "email": "",
    "company": "Curve",
    "position": "Partner",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Stewart",
    "lastname": "Hart",
    "email": "",
    "company": "Waracle",
    "position": "Lead iOS Software Developer",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Vaghish",
    "lastname": "Salimath",
    "email": "",
    "company": "Rapha Racing Limited",
    "position": "Senior QA Engineer",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Alistair",
    "lastname": "Wyse",
    "email": "",
    "company": "Drum Cussac Group",
    "position": "CTO",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Wolff",
    "email": "",
    "company": "Softwire",
    "position": "Head of Digital Labs",
    "connect_date": "06 Feb 2018"
  },
  {
    "firstname": "Ashley",
    "lastname": "Hewins",
    "email": "",
    "company": "EventsTag",
    "position": "Product Manager",
    "connect_date": "05 Feb 2018"
  },
  {
    "firstname": "Giovanni",
    "lastname": "Daprà",
    "email": "",
    "company": "Moneyfarm",
    "position": "Co-Founder & CEO",
    "connect_date": "05 Feb 2018"
  },
  {
    "firstname": "David",
    "lastname": "Ensor",
    "email": "",
    "company": "Awbury Group",
    "position": "Active Underwriter",
    "connect_date": "05 Feb 2018"
  },
  {
    "firstname": "Richard",
    "lastname": "Johnston",
    "email": "",
    "company": "Push Doctor",
    "position": "API Team Lead",
    "connect_date": "05 Feb 2018"
  },
  {
    "firstname": "Gerard",
    "lastname": "Frith",
    "email": "",
    "company": "KL Innovation",
    "position": "Innovation",
    "connect_date": "05 Feb 2018"
  },
  {
    "firstname": "Gert",
    "lastname": "Horne",
    "email": "",
    "company": "Kashing Limited",
    "position": "Chief Security Officer",
    "connect_date": "04 Feb 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Sohal",
    "email": "",
    "company": "West 4th Strategy, LLC",
    "position": "President & CEO",
    "connect_date": "04 Feb 2018"
  },
  {
    "firstname": "Laura",
    "lastname": "Peakall",
    "email": "",
    "company": "Adthena",
    "position": "Head of Talent Acquisition",
    "connect_date": "04 Feb 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Pearson 🚀",
    "email": "",
    "company": "Fuel Ventures",
    "position": "Founder & Managing Partner",
    "connect_date": "03 Feb 2018"
  },
  {
    "firstname": "Paul Razvan",
    "lastname": "Berg",
    "email": "",
    "company": "AZTEC Protocol",
    "position": "Software Engineer",
    "connect_date": "02 Feb 2018"
  },
  {
    "firstname": "Lemi",
    "lastname": "Wright",
    "email": "",
    "company": "Skillsoft",
    "position": "Executive Assistant to Chief Revenue Officer",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Bearman",
    "email": "",
    "company": "Vecna Robotics",
    "position": "VP, General Counsel",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "Vicky",
    "lastname": "Sheth",
    "email": "",
    "company": "Ness Digital Engineering",
    "position": "AVP Business Development - Global Financial Services",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "George",
    "lastname": "Mason",
    "email": "",
    "company": "Reco Group",
    "position": "Principal C# Recruitment Consultant",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "Paolo",
    "lastname": "Galvani",
    "email": "",
    "company": "Moneyfarm",
    "position": "Co-Founder and Chairman",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "Patrick",
    "lastname": "Bradshaw",
    "email": "",
    "company": "Lawrence Harvey",
    "position": "Recruitment Consultant",
    "connect_date": "01 Feb 2018"
  },
  {
    "firstname": "Catherine",
    "lastname": "Burn",
    "email": "",
    "company": "LT Harper - Cybersecurity Recruitment",
    "position": "Cyber Security - Application Security",
    "connect_date": "31 Jan 2018"
  },
  {
    "firstname": "Ali",
    "lastname": "Sarshogh",
    "email": "",
    "company": "Filed",
    "position": "Senior Full Stack Web Developer( .Net, Azure, Angular)",
    "connect_date": "30 Jan 2018"
  },
  {
    "firstname": "Morten",
    "lastname": "Brøgger",
    "email": "",
    "company": "Wire™",
    "position": "CEO",
    "connect_date": "30 Jan 2018"
  },
  {
    "firstname": "Brett",
    "lastname": "Ansley",
    "email": "",
    "company": "Brett Ansley Consulting",
    "position": "♦♦ Executive Coach & Mentor ► Creating Agility ► Increasing Revenue ♦♦",
    "connect_date": "30 Jan 2018"
  },
  {
    "firstname": "Frederic",
    "lastname": "Nze",
    "email": "",
    "company": "Oakam Ltd",
    "position": "CEO and Founder",
    "connect_date": "29 Jan 2018"
  },
  {
    "firstname": "Alasdair",
    "lastname": "Keyes",
    "email": "",
    "company": "Cloudee LTD",
    "position": "Director",
    "connect_date": "29 Jan 2018"
  },
  {
    "firstname": "Simon",
    "lastname": "Mortimer",
    "email": "",
    "company": "Talent Point",
    "position": "Director/Co-Founder",
    "connect_date": "29 Jan 2018"
  },
  {
    "firstname": "Joshua",
    "lastname": "Oldrid",
    "email": "",
    "company": "The Estée Lauder Companies Inc.",
    "position": "Senior Executive Team",
    "connect_date": "27 Jan 2018"
  },
  {
    "firstname": "Hakon",
    "lastname": "Martinsen",
    "email": "",
    "company": "Hive | Centrica Connected Home",
    "position": "Engineering Manager",
    "connect_date": "27 Jan 2018"
  },
  {
    "firstname": "Matthew",
    "lastname": "Goldstein",
    "email": "",
    "company": "True.",
    "position": "Managing Director, North America",
    "connect_date": "27 Jan 2018"
  },
  {
    "firstname": "Mark",
    "lastname": "Hartley",
    "email": "",
    "company": "Silicon Rhino",
    "position": "COO",
    "connect_date": "26 Jan 2018"
  },
  {
    "firstname": "Roderick",
    "lastname": "Smit",
    "email": "",
    "company": "Madison Black",
    "position": "Manager Madison Black, Netherlands, Belgium, Luxembourg & France",
    "connect_date": "26 Jan 2018"
  },
  {
    "firstname": "Andy",
    "lastname": "Russell",
    "email": "",
    "company": "Empact Collaboration Platform",
    "position": "Founder and Chairman",
    "connect_date": "26 Jan 2018"
  },
  {
    "firstname": "Hadley",
    "lastname": "Wickham",
    "email": "",
    "company": "RStudio, Inc.",
    "position": "Chief Scientist",
    "connect_date": "25 Jan 2018"
  },
  {
    "firstname": "Evgen",
    "lastname": "Agapov",
    "email": "",
    "company": "Sigma",
    "position": "Hardware Engineer – Sigma",
    "connect_date": "25 Jan 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Kormushoff",
    "email": "",
    "company": "TiE Boston",
    "position": "Charter Member",
    "connect_date": "24 Jan 2018"
  },
  {
    "firstname": "Romey",
    "lastname": "Thakur",
    "email": "",
    "company": "Entertainment Industry",
    "position": "Professional Freelancer",
    "connect_date": "24 Jan 2018"
  },
  {
    "firstname": "Jeff",
    "lastname": "Weiner",
    "email": "",
    "company": "Mersoft Corporation",
    "position": "Vice President, Product, Marketing, Sales",
    "connect_date": "24 Jan 2018"
  },
  {
    "firstname": "Imran",
    "lastname": "Arshed",
    "email": "",
    "company": "The Plum Guide",
    "position": "Co Founder and Head of Technology",
    "connect_date": "23 Jan 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Hart",
    "email": "",
    "company": "Levvel, LLC",
    "position": "Co-Founder and CEO",
    "connect_date": "22 Jan 2018"
  },
  {
    "firstname": "Sam",
    "lastname": "Sweeney",
    "email": "",
    "company": "AppNeta",
    "position": "Associate Sales Operations Specialist",
    "connect_date": "22 Jan 2018"
  },
  {
    "firstname": "Vincent",
    "lastname": "Carbonie",
    "email": "",
    "company": "Utelly",
    "position": "CTO and Co-founder",
    "connect_date": "22 Jan 2018"
  },
  {
    "firstname": "John",
    "lastname": "Kahan",
    "email": "",
    "company": "Microsoft",
    "position": "Chief Data Analytics Officer, Microsoft, Corporate External and Legal Affairs",
    "connect_date": "22 Jan 2018"
  },
  {
    "firstname": "Razvan",
    "lastname": "Ranca",
    "email": "",
    "company": "Tractable",
    "position": "Cofounder",
    "connect_date": "21 Jan 2018"
  },
  {
    "firstname": "Eric",
    "lastname": "McRae",
    "email": "",
    "company": "My ByWard Office",
    "position": "Founder",
    "connect_date": "20 Jan 2018"
  },
  {
    "firstname": "Evan",
    "lastname": "Harmon",
    "email": "",
    "company": "iZotope, Inc.",
    "position": "Senior Cloud Engineer",
    "connect_date": "19 Jan 2018"
  },
  {
    "firstname": "Eamon",
    "lastname": "Leonard",
    "email": "",
    "company": "AAIR.AI",
    "position": "Managing Partner",
    "connect_date": "19 Jan 2018"
  },
  {
    "firstname": "Tatyana",
    "lastname": "Lobachova",
    "email": "",
    "company": "Uvoteam",
    "position": "Business Development Representative",
    "connect_date": "19 Jan 2018"
  },
  {
    "firstname": "Jay",
    "lastname": "Johnson",
    "email": "",
    "company": "Levvel, LLC",
    "position": "Principal Consultant",
    "connect_date": "19 Jan 2018"
  },
  {
    "firstname": "Robert",
    "lastname": "Swanwick",
    "email": "",
    "company": "Management Concepts Inc.",
    "position": "VP, Strategy & Product Innovation",
    "connect_date": "19 Jan 2018"
  },
  {
    "firstname": "Rob",
    "lastname": "Cohen",
    "email": "me@robertlcohen.com",
    "company": "2U",
    "position": "Strategic Advisor, 2UGrad",
    "connect_date": "18 Jan 2018"
  },
  {
    "firstname": "Theresa",
    "lastname": "Holder",
    "email": "",
    "company": "Optum (formerly Alere Wellbeing)",
    "position": "Vice President - Veterans & Military Health  BD",
    "connect_date": "18 Jan 2018"
  },
  {
    "firstname": "John",
    "lastname": "Elton",
    "email": "",
    "company": "Greycroft",
    "position": "Partner",
    "connect_date": "17 Jan 2018"
  },
  {
    "firstname": "Stephen",
    "lastname": "Ferrara",
    "email": "",
    "company": "Compass /",
    "position": "Co-Founder, Hudson Advisory Team",
    "connect_date": "17 Jan 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Farrell",
    "email": "",
    "company": "Trilogy Education",
    "position": "Chief Revenue Officer, Founding Chief Operating Officer",
    "connect_date": "17 Jan 2018"
  },
  {
    "firstname": "Aleksey",
    "lastname": "Bilous",
    "email": "",
    "company": "Digitally Inspired Ltd",
    "position": "Middle Software engineer",
    "connect_date": "17 Jan 2018"
  },
  {
    "firstname": "Konrad",
    "lastname": "Nowak",
    "email": "",
    "company": "OSRAM",
    "position": "Senior Talent Sourcing Expert",
    "connect_date": "17 Jan 2018"
  },
  {
    "firstname": "David",
    "lastname": "Berger",
    "email": "",
    "company": "Trilogy Education",
    "position": "President & Co-Founder",
    "connect_date": "16 Jan 2018"
  },
  {
    "firstname": "Karim",
    "lastname": "El Gammal",
    "email": "",
    "company": "DemystData",
    "position": "Director, Enterprise Sales",
    "connect_date": "16 Jan 2018"
  },
  {
    "firstname": "Darko",
    "lastname": "Avramovski",
    "email": "",
    "company": "STRV",
    "position": "Head Of Business Development",
    "connect_date": "15 Jan 2018"
  },
  {
    "firstname": "JoAnne",
    "lastname": "Kennedy",
    "email": "",
    "company": "Nordstrom",
    "position": "Vice President Technology",
    "connect_date": "13 Jan 2018"
  },
  {
    "firstname": "Nella",
    "lastname": "Winklaar",
    "email": "",
    "company": "Motion10",
    "position": "Talentwerving* Corporate recruiter* Cloud",
    "connect_date": "13 Jan 2018"
  },
  {
    "firstname": "Taylor",
    "lastname": "Donnell",
    "email": "",
    "company": "Jebbit",
    "position": "Vice President, Partnerships & Strategic Alliances",
    "connect_date": "12 Jan 2018"
  },
  {
    "firstname": "Natalie",
    "lastname": "Turner",
    "email": "",
    "company": "Aspire",
    "position": "PHP AND Javascript Consultant",
    "connect_date": "12 Jan 2018"
  },
  {
    "firstname": "Dylan",
    "lastname": "Jaffe",
    "email": "",
    "company": "Bernstein Management Corporation",
    "position": "Leasing Associate",
    "connect_date": "11 Jan 2018"
  },
  {
    "firstname": "Noel",
    "lastname": "McKenna",
    "email": "",
    "company": "Titan IC Systems",
    "position": "CEO",
    "connect_date": "11 Jan 2018"
  },
  {
    "firstname": "Melissa",
    "lastname": "Koskovich",
    "email": "",
    "company": "Georgetown University School of Continuing Studies",
    "position": "Adjunct Lecturer",
    "connect_date": "11 Jan 2018"
  },
  {
    "firstname": "Jose",
    "lastname": "Ferreira",
    "email": "",
    "company": "Bakpax",
    "position": "Co-Founder & CEO",
    "connect_date": "10 Jan 2018"
  },
  {
    "firstname": "Marshall",
    "lastname": "DeWitt",
    "email": "",
    "company": "DeWitt Guitar Incorporated",
    "position": "Owner and President",
    "connect_date": "10 Jan 2018"
  },
  {
    "firstname": "Joseph",
    "lastname": "Brightwell",
    "email": "",
    "company": "Venquis",
    "position": "Senior Consultant - Insurance",
    "connect_date": "10 Jan 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Sack",
    "email": "",
    "company": "TeraCode",
    "position": "President & CEO",
    "connect_date": "10 Jan 2018"
  },
  {
    "firstname": "Jake",
    "lastname": "Hall",
    "email": "",
    "company": "Infinity Works",
    "position": "Senior Consultant",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Heather",
    "lastname": "Kofoed",
    "email": "",
    "company": "InVision",
    "position": "Executive Assistant",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Allen",
    "lastname": "Whipple",
    "email": "",
    "company": "ActiveViam",
    "position": "Managing Director",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Jamie",
    "lastname": "Peloquin",
    "email": "",
    "company": "Newforma",
    "position": "Director of DevOps & Quality",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Rotem",
    "lastname": "Hayoun-Meidav",
    "email": "",
    "company": "Vault",
    "position": "Co-Founder & CTO",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Medvedev",
    "email": "",
    "company": "\"Ukrburgas\" Drilling Company",
    "position": "Deputy CEO of HR",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Robert J.",
    "lastname": "Gibbons Jr.",
    "email": "",
    "company": "Datto, Inc.",
    "position": "Chief Technology Officer",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Rushi",
    "lastname": "Luhar",
    "email": "",
    "company": "Jeavio",
    "position": "Strategy, Technology and Architecture",
    "connect_date": "09 Jan 2018"
  },
  {
    "firstname": "Sarah",
    "lastname": "Hilton",
    "email": "",
    "company": "Paradigm Talks",
    "position": "Conference Coordinator",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "Jon",
    "lastname": "Oestermeyer",
    "email": "",
    "company": "Porcaro Stolarek Mete Partners, LLC",
    "position": "Practice Manager",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "Michael",
    "lastname": "Berard",
    "email": "",
    "company": "Experian Data Quality",
    "position": "Strategic Manager, Data Management",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "James",
    "lastname": "Friedman",
    "email": "",
    "company": "Quick Base",
    "position": "Senior Account Executive, Enterprise",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "Billy",
    "lastname": "Luetchford",
    "email": "",
    "company": "Noir Consulting",
    "position": "Recruitment Consultant",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "Franziska",
    "lastname": "Hempel",
    "email": "",
    "company": "ultimate.ai",
    "position": "Senior Frontend Engineer",
    "connect_date": "08 Jan 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "Coburn",
    "email": "",
    "company": "Partners HealthCare",
    "position": "Chief Innovation Officer; President, Partners HealthCare International",
    "connect_date": "07 Jan 2018"
  },
  {
    "firstname": "Andreas",
    "lastname": "Heiberg",
    "email": "",
    "company": "DueDil",
    "position": "Web Developer",
    "connect_date": "06 Jan 2018"
  },
  {
    "firstname": "Cindy",
    "lastname": "Russo",
    "email": "",
    "company": "PAR Technology Corporation (NYSE: PAR)",
    "position": "Chair of the Audit Committee and member of Compensation & Nominating/Corporate Governance committees",
    "connect_date": "05 Jan 2018"
  },
  {
    "firstname": "Grant",
    "lastname": "Garton",
    "email": "",
    "company": "Polecat - where reputation is no longer a risk",
    "position": "Client Partner",
    "connect_date": "05 Jan 2018"
  },
  {
    "firstname": "Chris",
    "lastname": "White",
    "email": "",
    "company": "CAST",
    "position": "VP Sales Enablement",
    "connect_date": "05 Jan 2018"
  },
  {
    "firstname": "Nitin",
    "lastname": "Malhotra",
    "email": "",
    "company": "Cvent, Inc.",
    "position": "Senior Vice President, Corporate Development",
    "connect_date": "05 Jan 2018"
  },
  {
    "firstname": "Dominic",
    "lastname": "Kaiser",
    "email": "",
    "company": "Zmags",
    "position": "Senior Devops Engineer",
    "connect_date": "05 Jan 2018"
  },
  {
    "firstname": "Bates",
    "lastname": "Turpen",
    "email": "",
    "company": "First Data Corporation",
    "position": "CTO Global Digital Commerce",
    "connect_date": "04 Jan 2018"
  },
  {
    "firstname": "Alex",
    "lastname": "Beebe",
    "email": "",
    "company": "Toast, Inc.",
    "position": "Senior Project Coordinator",
    "connect_date": "04 Jan 2018"
  },
  {
    "firstname": "Jeff",
    "lastname": "Vincent",
    "email": "",
    "company": "Appcues",
    "position": "Senior Product Manager",
    "connect_date": "04 Jan 2018"
  },
  {
    "firstname": "Harry",
    "lastname": "Ulrich",
    "email": "",
    "company": "Celerity",
    "position": "Vice President of Technology Services",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Thomas",
    "lastname": "Schutz",
    "email": "",
    "company": "Experian",
    "position": "Senior Vice President, Sales and Marketing Strategy, North America",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Peter",
    "lastname": "Klein",
    "email": "",
    "company": "Provide IT Consulting",
    "position": "Looking for PHP & .Net developers",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Manoj Kumar",
    "lastname": "Goyal",
    "email": "",
    "company": "Oracle",
    "position": "Group Vice President",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Sean",
    "lastname": "Trigony",
    "email": "",
    "company": "Brella",
    "position": "Angel Investor",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Maria",
    "lastname": "Cirino",
    "email": "",
    "company": ".406 Ventures",
    "position": "Founding Managing Partner",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Nikolay",
    "lastname": "Rodionov (we're hiring at Streamroot)",
    "email": "",
    "company": "Streamroot",
    "position": "Co-Founder (we're hiring!)",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Steven",
    "lastname": "Ruggieri",
    "email": "",
    "company": "RapidMiner, Inc.",
    "position": "Head Of Sales",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Sophie",
    "lastname": "Slowe",
    "email": "",
    "company": "Kitewheel",
    "position": "VP Strategy",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Angela",
    "lastname": "Cirrone",
    "email": "",
    "company": "Upland Software",
    "position": "Director of Marketing Operations",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Heidi",
    "lastname": "Rawding",
    "email": "",
    "company": "RapidMiner, Inc.",
    "position": "Senior Director of Field Operations (Sales, Channel, and Customer Success)",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Mike",
    "lastname": "Mattei",
    "email": "",
    "company": "AppNeta",
    "position": "Senior Director of Sales",
    "connect_date": "03 Jan 2018"
  },
  {
    "firstname": "Gautier",
    "lastname": "DEMOND",
    "email": "",
    "company": "Streamroot",
    "position": "Vice President Global Sales",
    "connect_date": "02 Jan 2018"
  },
  {
    "firstname": "Bob",
    "lastname": "Ranaldi",
    "email": "",
    "company": "FTV Capital",
    "position": "Senior Growth Advisor",
    "connect_date": "02 Jan 2018"
  },
  {
    "firstname": "Edward",
    "lastname": "Davis",
    "email": "",
    "company": "Edward Davis, LLC",
    "position": "Chief Executive Officer",
    "connect_date": "01 Jan 2018"
  },
  {
    "firstname": "Alexander",
    "lastname": "Perkov",
    "email": "",
    "company": "Remote Consulting",
    "position": "Marketing Specialist",
    "connect_date": "01 Jan 2018"
  },
  {
    "firstname": "Maksim",
    "lastname": "Kurinskiy",
    "email": "",
    "company": "Gamarus",
    "position": "Owner of the Company",
    "connect_date": "29 Dec 2017"
  },
  {
    "firstname": "Faisal",
    "lastname": "Zafar",
    "email": "",
    "company": "EWS",
    "position": "Finance & Operations",
    "connect_date": "28 Dec 2017"
  },
  {
    "firstname": "Joe",
    "lastname": "Beazley",
    "email": "",
    "company": "Edison Hill Limited",
    "position": "JavaScript Recruiter",
    "connect_date": "28 Dec 2017"
  },
  {
    "firstname": "David",
    "lastname": "Snopek",
    "email": "",
    "company": "myDropWizard",
    "position": "Co Founder",
    "connect_date": "28 Dec 2017"
  },
  {
    "firstname": "Dr. Lars",
    "lastname": "Buttler",
    "email": "",
    "company": "AI Foundation",
    "position": "Co-Founder & CEO",
    "connect_date": "27 Dec 2017"
  },
  {
    "firstname": "Jay",
    "lastname": "Paul",
    "email": "",
    "company": "Charter Global",
    "position": "Senior Technical Recruiter",
    "connect_date": "27 Dec 2017"
  },
  {
    "firstname": "Artem",
    "lastname": "Zemlyanoy",
    "email": "",
    "company": "UTOR",
    "position": "Partner | Head of Business Development",
    "connect_date": "25 Dec 2017"
  },
  {
    "firstname": "KHLOPKOV",
    "lastname": "ARTEM",
    "email": "",
    "company": "ICO",
    "position": "Senior Quality Assurance Analyst",
    "connect_date": "25 Dec 2017"
  },
  {
    "firstname": "Clifford",
    "lastname": "Lerner",
    "email": "",
    "company": "Cliff",
    "position": "Founder:  Working On A Project To Get The Next 100 Million People To Adopt Crypto",
    "connect_date": "25 Dec 2017"
  },
  {
    "firstname": "Anna",
    "lastname": "Voichek",
    "email": "",
    "company": "Zero to One search",
    "position": "Co-Founder & Tech Recruiter",
    "connect_date": "24 Dec 2017"
  },
  {
    "firstname": "Wyatt",
    "lastname": "Depuy",
    "email": "",
    "company": "Ceros",
    "position": "Senior Sales Development Representative",
    "connect_date": "21 Dec 2017"
  },
  {
    "firstname": "Yaroslav",
    "lastname": "Kotylko",
    "email": "",
    "company": "виконавчий комітет Нововолинської міської ради",
    "position": "Начальник відділу кадрової роботи та з питань запобігання і виявлення корупції",
    "connect_date": "21 Dec 2017"
  },
  {
    "firstname": "Johan",
    "lastname": "Nordstrom",
    "email": "",
    "company": "Wallarm Inc.",
    "position": "Chief Business Development Officer (CBDO)",
    "connect_date": "21 Dec 2017"
  },
  {
    "firstname": "Julien",
    "lastname": "Hamilton",
    "email": "",
    "company": "Lab49",
    "position": "Senior Product Owner",
    "connect_date": "20 Dec 2017"
  },
  {
    "firstname": "Diana",
    "lastname": "Chavdar",
    "email": "",
    "company": "Infaned BV",
    "position": "Manager Operations",
    "connect_date": "20 Dec 2017"
  },
  {
    "firstname": "Ben",
    "lastname": "Cranham",
    "email": "",
    "company": "MTI",
    "position": "Chief Operating Officer",
    "connect_date": "19 Dec 2017"
  },
  {
    "firstname": "Micayla K.",
    "lastname": "Ryan",
    "email": "",
    "company": "Schoology",
    "position": "Executive Assistant to CEO / Co-Founder",
    "connect_date": "19 Dec 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Murray",
    "email": "",
    "company": "Skyword Inc.",
    "position": "President",
    "connect_date": "19 Dec 2017"
  },
  {
    "firstname": "Rick",
    "lastname": "Zumpano",
    "email": "",
    "company": "Boxed Wholesale",
    "position": "Vice President of Distribution",
    "connect_date": "19 Dec 2017"
  },
  {
    "firstname": "Yevhen",
    "lastname": "Bilchuk",
    "email": "",
    "company": "Institute of Engineering Thermophysics of National Academy of Sciences of Ukraine",
    "position": "Senior Research Scientist",
    "connect_date": "19 Dec 2017"
  },
  {
    "firstname": "Irina",
    "lastname": "Wulf",
    "email": "",
    "company": "Zero to One Search",
    "position": "Human Resources Manager",
    "connect_date": "17 Dec 2017"
  },
  {
    "firstname": "Ashwin",
    "lastname": "Balamohan",
    "email": "",
    "company": "Xendit",
    "position": "Director of Engineering",
    "connect_date": "17 Dec 2017"
  },
  {
    "firstname": "Kenneth",
    "lastname": "Alleyne MD",
    "email": "",
    "company": "The University of Connecticut Health Center",
    "position": "Member Board Of Directors",
    "connect_date": "17 Dec 2017"
  },
  {
    "firstname": "Judy",
    "lastname": "Krandel, CFA",
    "email": "",
    "company": "PeerStream, Inc.",
    "position": "Chief Financial Officer",
    "connect_date": "16 Dec 2017"
  },
  {
    "firstname": "Carmela Nicole",
    "lastname": "Villavicencio",
    "email": "",
    "company": "Animoto",
    "position": "Customer Operations",
    "connect_date": "15 Dec 2017"
  },
  {
    "firstname": "Kamil",
    "lastname": "Piskorz",
    "email": "k_piskorz@outlook.com",
    "company": "NextApps",
    "position": "Software / DevOps Engineer",
    "connect_date": "15 Dec 2017"
  },
  {
    "firstname": "Marc-André",
    "lastname": "Lacombe",
    "email": "",
    "company": "FX INNOVATION",
    "position": "Director Of Development",
    "connect_date": "15 Dec 2017"
  },
  {
    "firstname": "Cyrille",
    "lastname": "Quemin",
    "email": "",
    "company": "Yoti Ltd",
    "position": "Head of Mobile",
    "connect_date": "15 Dec 2017"
  },
  {
    "firstname": "Kirill",
    "lastname": "Mukhov",
    "email": "",
    "company": "Luxoft",
    "position": "Python Lead Developer",
    "connect_date": "15 Dec 2017"
  },
  {
    "firstname": "Katia",
    "lastname": "Mazga",
    "email": "",
    "company": "AllPro Solutions",
    "position": "Chief Executive Officer/IT Recruiter",
    "connect_date": "14 Dec 2017"
  },
  {
    "firstname": "Moiz",
    "lastname": "Ali",
    "email": "",
    "company": "GoEuro",
    "position": "Engineering Manager Apps",
    "connect_date": "13 Dec 2017"
  },
  {
    "firstname": "Crisi",
    "lastname": "Kabisch",
    "email": "",
    "company": "blogfoster GmbH",
    "position": "Head of CRM",
    "connect_date": "13 Dec 2017"
  },
  {
    "firstname": "Ed",
    "lastname": "Johnson",
    "email": "",
    "company": "VirtualHealth",
    "position": "Member Director",
    "connect_date": "13 Dec 2017"
  },
  {
    "firstname": "Josh",
    "lastname": "Cole",
    "email": "",
    "company": "Voxis Engineering Ltd",
    "position": "CEO & Founder",
    "connect_date": "13 Dec 2017"
  },
  {
    "firstname": "Luis",
    "lastname": "Ferreira",
    "email": "",
    "company": "Portico Property",
    "position": "Property Services",
    "connect_date": "13 Dec 2017"
  },
  {
    "firstname": "P.J.",
    "lastname": "Morreale",
    "email": "",
    "company": "YouVisit",
    "position": "Vice President, YouVisit Studios",
    "connect_date": "12 Dec 2017"
  },
  {
    "firstname": "Tim",
    "lastname": "Jordan",
    "email": "",
    "company": "Northgate Information Solutions",
    "position": "Team Leader",
    "connect_date": "12 Dec 2017"
  },
  {
    "firstname": "James",
    "lastname": "Begg",
    "email": "",
    "company": "Gartner",
    "position": "Events Attendance Specialist",
    "connect_date": "12 Dec 2017"
  },
  {
    "firstname": "Beth",
    "lastname": "Roberts",
    "email": "",
    "company": "TTEC",
    "position": "Director, Digital Consulting - Learning & Performance",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Pete",
    "lastname": "Finnecy",
    "email": "",
    "company": "YouVisit",
    "position": "VP of Client Success",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Scheckman",
    "email": "",
    "company": "StyleSage",
    "position": "VP Business Development",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Charmien",
    "lastname": "Fugelsang",
    "email": "",
    "company": "Vimeo",
    "position": "Vice President, Talent",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Tetyana",
    "lastname": "Golubyeva",
    "email": "",
    "company": "DataArt",
    "position": "Delivery Manager",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "🇺🇸 Steve",
    "lastname": "Tucker",
    "email": "",
    "company": "Unified Materials Group",
    "position": "CEO & President",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Frank",
    "lastname": "Khan",
    "email": "",
    "company": "Guidepoint Global",
    "position": "Vice President, Business Development",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Tim",
    "lastname": "Dunham",
    "email": "",
    "company": "Guidepoint",
    "position": "Vice President Business Development",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Liam",
    "lastname": "McCabe",
    "email": "",
    "company": "Code & Wander",
    "position": "Technical Director",
    "connect_date": "11 Dec 2017"
  },
  {
    "firstname": "Daniel",
    "lastname": "Lewis",
    "email": "",
    "company": "Bright Leadership Ltd.",
    "position": "Managing Partner",
    "connect_date": "09 Dec 2017"
  },
  {
    "firstname": "Nathanial",
    "lastname": "Langman",
    "email": "",
    "company": "Amazon Web Services",
    "position": "Senior Software Development Manager, AWS Migration Services",
    "connect_date": "08 Dec 2017"
  },
  {
    "firstname": "Fran",
    "lastname": "Soistman",
    "email": "",
    "company": "Aetna, a CVS Health Company",
    "position": "Executive Vice President and Head of Government Services",
    "connect_date": "08 Dec 2017"
  },
  {
    "firstname": "Amaia",
    "lastname": "Arruabarrena",
    "email": "",
    "company": "ezCater",
    "position": "Program Manager, People & Talent",
    "connect_date": "07 Dec 2017"
  },
  {
    "firstname": "Troy",
    "lastname": "Cox",
    "email": "",
    "company": "Foundation Medicine",
    "position": "CEO",
    "connect_date": "07 Dec 2017"
  },
  {
    "firstname": "Ricardo",
    "lastname": "Da Silva Horta",
    "email": "",
    "company": "ECOM",
    "position": "Account Executive",
    "connect_date": "06 Dec 2017"
  },
  {
    "firstname": "Derek",
    "lastname": "Heintz",
    "email": "",
    "company": "Rapid7",
    "position": "Sr. Director IT Infrastructure & Operations",
    "connect_date": "06 Dec 2017"
  },
  {
    "firstname": "Natalie",
    "lastname": "Lelless",
    "email": "",
    "company": "Your Makeup Face",
    "position": "Lead Makeup Artist/ Owner",
    "connect_date": "06 Dec 2017"
  },
  {
    "firstname": "Antonina",
    "lastname": "Skrypnyk",
    "email": "",
    "company": "SoftServe",
    "position": "Client Partner, Head of Financial Services Lab",
    "connect_date": "05 Dec 2017"
  },
  {
    "firstname": "Roger",
    "lastname": "Gunter",
    "email": "",
    "company": "Aetna",
    "position": "Chief Executive Officer - Virginia Medicaid",
    "connect_date": "05 Dec 2017"
  },
  {
    "firstname": "Tonya",
    "lastname": "Bakritzes",
    "email": "",
    "company": "Isobar",
    "position": "Chief Marketing Officer",
    "connect_date": "04 Dec 2017"
  },
  {
    "firstname": "Jeff",
    "lastname": "Maling",
    "email": "",
    "company": "Arundel Way Partners, LLC",
    "position": "Founder",
    "connect_date": "04 Dec 2017"
  },
  {
    "firstname": "Ralph",
    "lastname": "Welborn",
    "email": "",
    "company": "CapImpact",
    "position": "CEO",
    "connect_date": "04 Dec 2017"
  },
  {
    "firstname": "Ray",
    "lastname": "Noppe",
    "email": "",
    "company": "Advinia Health Care",
    "position": "Group CTO",
    "connect_date": "02 Dec 2017"
  },
  {
    "firstname": "Maksym",
    "lastname": "Palamarenko",
    "email": "",
    "company": "N-iX",
    "position": "Director Of Software Development Office",
    "connect_date": "02 Dec 2017"
  },
  {
    "firstname": "Pavlo",
    "lastname": "Kononchuk",
    "email": "",
    "company": "Intellias",
    "position": "Program Error Manager",
    "connect_date": "02 Dec 2017"
  },
  {
    "firstname": "Alex",
    "lastname": "Miasoiedov",
    "email": "",
    "company": "Verizon",
    "position": "Principal Software Engineer",
    "connect_date": "01 Dec 2017"
  },
  {
    "firstname": "Nataliya",
    "lastname": "Saturska",
    "email": "",
    "company": "Romexsoft",
    "position": "Sevice Manager",
    "connect_date": "01 Dec 2017"
  },
  {
    "firstname": "Kelly",
    "lastname": "Johnson",
    "email": "",
    "company": "Lead Forensics",
    "position": "Data Processor",
    "connect_date": "01 Dec 2017"
  },
  {
    "firstname": "Jill",
    "lastname": "Klein",
    "email": "",
    "company": "Darktrace",
    "position": "Account Director",
    "connect_date": "30 Nov 2017"
  },
  {
    "firstname": "Nataliya",
    "lastname": "Ostrovska",
    "email": "",
    "company": "Romexsoft",
    "position": "Head of Recruitment",
    "connect_date": "29 Nov 2017"
  },
  {
    "firstname": "Justin",
    "lastname": "Fier",
    "email": "",
    "company": "Darktrace",
    "position": "Director for Cyber Intelligence and Analysis",
    "connect_date": "29 Nov 2017"
  },
  {
    "firstname": "Chris",
    "lastname": "Kamykowski",
    "email": "",
    "company": "House of Forgings LLC",
    "position": "Vice President of Sales and Operations",
    "connect_date": "29 Nov 2017"
  },
  {
    "firstname": "Randy",
    "lastname": "Cheek",
    "email": "",
    "company": "Awake Security",
    "position": "VP of Sales",
    "connect_date": "29 Nov 2017"
  },
  {
    "firstname": "Dominic",
    "lastname": "Lloyd",
    "email": "",
    "company": "Vestigo Ventures",
    "position": "Advisory Board Member",
    "connect_date": "28 Nov 2017"
  },
  {
    "firstname": "Mark",
    "lastname": "Murphy",
    "email": "",
    "company": "Canonical Ltd.",
    "position": "Director, Global Accounts, IoT Sales & Alliances",
    "connect_date": "28 Nov 2017"
  },
  {
    "firstname": "Kristen",
    "lastname": "Ablamsky",
    "email": "",
    "company": "Wethos",
    "position": "Co-founder",
    "connect_date": "28 Nov 2017"
  },
  {
    "firstname": "Ryan",
    "lastname": "Fickel, PMP, CISSP, CISM",
    "email": "",
    "company": "Advisors Excel",
    "position": "Chief Technology Officer",
    "connect_date": "27 Nov 2017"
  },
  {
    "firstname": "Pablo",
    "lastname": "Sande",
    "email": "",
    "company": "Garrison Technology",
    "position": "Tech Lead",
    "connect_date": "27 Nov 2017"
  },
  {
    "firstname": "Tom",
    "lastname": "Erickson",
    "email": "",
    "company": "XFormative",
    "position": "Chairman",
    "connect_date": "26 Nov 2017"
  },
  {
    "firstname": "Thomas",
    "lastname": "Obrey",
    "email": "",
    "company": "Salesforce",
    "position": "Commerce Cloud SI Partner Advisory Board Member",
    "connect_date": "26 Nov 2017"
  },
  {
    "firstname": "Shiann",
    "lastname": "Chambers",
    "email": "",
    "company": "Wilde Honey",
    "position": "Chief Bloom Babe",
    "connect_date": "25 Nov 2017"
  },
  {
    "firstname": "David J",
    "lastname": "Harter, FPS",
    "email": "",
    "company": "MassMutual",
    "position": "Vice President",
    "connect_date": "25 Nov 2017"
  },
  {
    "firstname": "Anthony",
    "lastname": "Matrone",
    "email": "",
    "company": "MassMutual",
    "position": "Financial Advisor",
    "connect_date": "24 Nov 2017"
  },
  {
    "firstname": "Jim",
    "lastname": "Coleman",
    "email": "",
    "company": "Trimble",
    "position": "Director Of Engineering",
    "connect_date": "23 Nov 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Vidich",
    "email": "",
    "company": "AdWerx",
    "position": "Adviser and Investor",
    "connect_date": "23 Nov 2017"
  },
  {
    "firstname": "Rodrigo",
    "lastname": "Díaz",
    "email": "",
    "company": "Shell",
    "position": "Digital Venture Lead / Senior Product Manager",
    "connect_date": "23 Nov 2017"
  },
  {
    "firstname": "Hussen Shambesh",
    "lastname": "(حسن شمبش)",
    "email": "",
    "company": "BP",
    "position": "Principal Software Engineer (contract) | Commodities Trading | ETRM | Cloud Digital Transformation",
    "connect_date": "22 Nov 2017"
  },
  {
    "firstname": "Marianna",
    "lastname": "Stavridi",
    "email": "",
    "company": "Workable",
    "position": "Business Development Representative",
    "connect_date": "20 Nov 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Sanders",
    "email": "",
    "company": "Attract AR & VR",
    "position": "CTO",
    "connect_date": "20 Nov 2017"
  },
  {
    "firstname": "Jeremy",
    "lastname": "Achin",
    "email": "",
    "company": "DataRobot",
    "position": "Data Scientist and CEO",
    "connect_date": "19 Nov 2017"
  },
  {
    "firstname": "Erik",
    "lastname": "Mathew Popovic",
    "email": "",
    "company": "Software-Nation",
    "position": "President",
    "connect_date": "17 Nov 2017"
  },
  {
    "firstname": "Shane",
    "lastname": "Soetaniman",
    "email": "",
    "company": "Travelio",
    "position": "Corporate Strategy",
    "connect_date": "17 Nov 2017"
  },
  {
    "firstname": "Vadim",
    "lastname": "Kudriavtcev",
    "email": "",
    "company": "LaunchByte.io",
    "position": "Chief Technical Officer",
    "connect_date": "17 Nov 2017"
  },
  {
    "firstname": "Phil",
    "lastname": "Austin",
    "email": "",
    "company": "RealityMine",
    "position": "VP of Data Services",
    "connect_date": "17 Nov 2017"
  },
  {
    "firstname": "Keisa",
    "lastname": "Jackson",
    "email": "",
    "company": "Raizlabs",
    "position": "Office Manager /Executive Assistant to CEO",
    "connect_date": "17 Nov 2017"
  },
  {
    "firstname": "Rob",
    "lastname": "Weisberg",
    "email": "",
    "company": "Invaluable",
    "position": "Chief Executive Officer",
    "connect_date": "16 Nov 2017"
  },
  {
    "firstname": "Cindy",
    "lastname": "Scholz",
    "email": "",
    "company": "Compass Inc.",
    "position": "Real Estate Adviser/Founder of the Scholz Team at Compass",
    "connect_date": "15 Nov 2017"
  },
  {
    "firstname": "Andy",
    "lastname": "Palmer",
    "email": "",
    "company": "Tamr",
    "position": "Co-Founder & CEO",
    "connect_date": "15 Nov 2017"
  },
  {
    "firstname": "Irina",
    "lastname": "Neruk",
    "email": "",
    "company": "Lohika",
    "position": "Researcher",
    "connect_date": "15 Nov 2017"
  },
  {
    "firstname": "Imi",
    "lastname": "Borbas",
    "email": "",
    "company": "Wireless Logic Ltd",
    "position": "Contract Software Engineer (Remote)",
    "connect_date": "15 Nov 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Broome",
    "email": "",
    "company": "Northgate Information Solutions",
    "position": "Group CTO",
    "connect_date": "14 Nov 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "Pell",
    "email": "",
    "company": "Drillinginfo",
    "position": "Advisor",
    "connect_date": "12 Nov 2017"
  },
  {
    "firstname": "Leonid",
    "lastname": "Bobovich",
    "email": "",
    "company": "SAP Innovation Center Network",
    "position": "Machine Learning and Artificial Intelegence Expert Development Architect",
    "connect_date": "11 Nov 2017"
  },
  {
    "firstname": "Peter",
    "lastname": "Glennie",
    "email": "",
    "company": "Voodoo Park Ltd",
    "position": "CTO",
    "connect_date": "10 Nov 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Restivo",
    "email": "",
    "company": "Bullhorn",
    "position": "Chief Revenue Officer",
    "connect_date": "10 Nov 2017"
  },
  {
    "firstname": "M",
    "lastname": "T",
    "email": "",
    "company": "FoundrFindr",
    "position": "Co-Founder",
    "connect_date": "10 Nov 2017"
  },
  {
    "firstname": "Marcus",
    "lastname": "Kornegay, MBA, MS",
    "email": "",
    "company": "Novant Health",
    "position": "Director of Operations and Portfolio Performance",
    "connect_date": "09 Nov 2017"
  },
  {
    "firstname": "Chuck",
    "lastname": "Goldman",
    "email": "",
    "company": "SkyX Systems Corp.",
    "position": "Executive Advisor",
    "connect_date": "09 Nov 2017"
  },
  {
    "firstname": "Jessica",
    "lastname": "Berrisford",
    "email": "",
    "company": "Red Badger",
    "position": "Senior Agile Delivery Lead (Consultant)",
    "connect_date": "09 Nov 2017"
  },
  {
    "firstname": "Ali",
    "lastname": "Tavanayan",
    "email": "",
    "company": "Manage Petro - The Online Fuel Delivery Solution",
    "position": "Project Manager, Director",
    "connect_date": "08 Nov 2017"
  },
  {
    "firstname": "Сергей",
    "lastname": "Михайлов",
    "email": "",
    "company": "Do IT Programming Solutions",
    "position": "Full-stack Developer",
    "connect_date": "08 Nov 2017"
  },
  {
    "firstname": "Anton",
    "lastname": "Bromot",
    "email": "",
    "company": "Opporty.Com",
    "position": "Full-stack Developer",
    "connect_date": "08 Nov 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Winfield",
    "email": "",
    "company": "CashFlows",
    "position": "Director, Group Operations",
    "connect_date": "08 Nov 2017"
  },
  {
    "firstname": "Andy",
    "lastname": "Harding",
    "email": "",
    "company": "Arcadia",
    "position": "Chief Digital Officer (Interim)",
    "connect_date": "08 Nov 2017"
  },
  {
    "firstname": "Richard",
    "lastname": "Moseley",
    "email": "",
    "company": "Rapid7",
    "position": "SVP International; EMEA, APAC & LATAM",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Oliver",
    "lastname": "Williamson",
    "email": "",
    "company": "CircuitHub",
    "position": "Head of Sales",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Rob",
    "lastname": "Steel",
    "email": "",
    "company": "Poq",
    "position": "Head of Professional Services",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Hoffman",
    "email": "",
    "company": "Right Networks",
    "position": "Senior Systems Engineer",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Jasper",
    "lastname": "Smith",
    "email": "",
    "company": "PlayStack",
    "position": "Co Founder and Executive Chairman",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Elliott",
    "lastname": "Edwards",
    "email": "",
    "company": "Poq",
    "position": "Account Lead - North America",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Knebel",
    "email": "",
    "company": "Bauer Xcel Media",
    "position": "Senior Digital Engineer",
    "connect_date": "07 Nov 2017"
  },
  {
    "firstname": "Mark",
    "lastname": "Hastings",
    "email": "",
    "company": "Property Brands",
    "position": "Board Member",
    "connect_date": "06 Nov 2017"
  },
  {
    "firstname": "Alan",
    "lastname": "Watson",
    "email": "",
    "company": "BARRON MCCANN HOLDINGS LIMITED",
    "position": "Executive Chairman Owner Barron McCann Holdings Ltd",
    "connect_date": "06 Nov 2017"
  },
  {
    "firstname": "Eric",
    "lastname": "Krassow",
    "email": "",
    "company": "Amazon Web Services",
    "position": "Business Manager",
    "connect_date": "05 Nov 2017"
  },
  {
    "firstname": "Jude",
    "lastname": "McColgan",
    "email": "",
    "company": "Localytics",
    "position": "CEO",
    "connect_date": "04 Nov 2017"
  },
  {
    "firstname": "James D.",
    "lastname": "Antonino",
    "email": "",
    "company": "James D. Antonino; Noise on Neptune",
    "position": "Sales, Marketing and Business Development Consultant",
    "connect_date": "03 Nov 2017"
  },
  {
    "firstname": "Daniel",
    "lastname": "Theobald",
    "email": "",
    "company": "Vecna Robotics",
    "position": "Co-Founder and Chief Innovation Officer",
    "connect_date": "03 Nov 2017"
  },
  {
    "firstname": "Nir",
    "lastname": "Elbaz",
    "email": "",
    "company": "Oz Interactive",
    "position": "Managing Director",
    "connect_date": "02 Nov 2017"
  },
  {
    "firstname": "David",
    "lastname": "Laing",
    "email": "",
    "company": "101 Ways",
    "position": "CFO",
    "connect_date": "31 Oct 2017"
  },
  {
    "firstname": "Steve",
    "lastname": "Schaefer",
    "email": "",
    "company": "The Industry Team",
    "position": "CEO",
    "connect_date": "31 Oct 2017"
  },
  {
    "firstname": "Hannah",
    "lastname": "Lowry",
    "email": "",
    "company": "Ecrebo",
    "position": "CFO",
    "connect_date": "31 Oct 2017"
  },
  {
    "firstname": "Justin",
    "lastname": "Mullen",
    "email": "",
    "company": "Datalytyx",
    "position": "Co-Founder and Managing Director",
    "connect_date": "30 Oct 2017"
  },
  {
    "firstname": "Brian",
    "lastname": "Kibby, CEO Knewton",
    "email": "",
    "company": "Knewton",
    "position": "CEO",
    "connect_date": "30 Oct 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Jordan",
    "email": "",
    "company": "Knewton",
    "position": "SVP Partnerships",
    "connect_date": "30 Oct 2017"
  },
  {
    "firstname": "Alina",
    "lastname": "Dudyk",
    "email": "",
    "company": "SoftConstruct",
    "position": "Senior IT Recruiter",
    "connect_date": "27 Oct 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Webber",
    "email": "",
    "company": "HubSpot",
    "position": "Director of Information Technology",
    "connect_date": "27 Oct 2017"
  },
  {
    "firstname": "Vlad",
    "lastname": "Shimov",
    "email": "",
    "company": "Lohika",
    "position": "Chief Delivery Officer",
    "connect_date": "26 Oct 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Pici",
    "email": "",
    "company": "HubSpot",
    "position": "Director of Product, Growth + Mobile",
    "connect_date": "26 Oct 2017"
  },
  {
    "firstname": "Matthew",
    "lastname": "Gostelow",
    "email": "",
    "company": "RV - Recruitment to Recruitment",
    "position": "Founder",
    "connect_date": "26 Oct 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Green",
    "email": "",
    "company": "Tryzens",
    "position": "Sales Director : UK&I",
    "connect_date": "26 Oct 2017"
  },
  {
    "firstname": "Haorong",
    "lastname": "Li",
    "email": "",
    "company": "Veson Nautical",
    "position": "Vice President of Product",
    "connect_date": "25 Oct 2017"
  },
  {
    "firstname": "Kate",
    "lastname": "Bogdanova",
    "email": "",
    "company": "Serious Software",
    "position": "IT Recruiter/Partnerships Coordinator (HR)",
    "connect_date": "25 Oct 2017"
  },
  {
    "firstname": "Colin",
    "lastname": "Reposa",
    "email": "",
    "company": "Syrinx Consulting Corporation",
    "position": "Vice President, Resource Management",
    "connect_date": "25 Oct 2017"
  },
  {
    "firstname": "Ryan Bonnici 🚀",
    "lastname": "CMO at G2 Crowd",
    "email": "",
    "company": "G2 Crowd",
    "position": "Chief Marketing Officer",
    "connect_date": "25 Oct 2017"
  },
  {
    "firstname": "Dan",
    "lastname": "Tyre",
    "email": "",
    "company": "HubSpot",
    "position": "Executive",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Sam",
    "lastname": "Mallikarjunan",
    "email": "",
    "company": "Harvard University",
    "position": "Instructor",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Howard",
    "lastname": "Kingston",
    "email": "",
    "company": "Escape Velocity",
    "position": "Founder",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Broome",
    "email": "",
    "company": "Northgate Information Solutions",
    "position": "Group CTO",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Donny",
    "lastname": "Payne",
    "email": "",
    "company": "Bullhorn",
    "position": "Vice President of Sales",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Horobey",
    "email": "",
    "company": "SoftServe",
    "position": "Senior Web Developer",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Nagle",
    "email": "",
    "company": "Interactions LLC",
    "position": "Chief Product Officer and CTO",
    "connect_date": "24 Oct 2017"
  },
  {
    "firstname": "Benjamin",
    "lastname": "Geyerhahn",
    "email": "",
    "company": "Workers Benefit Fund",
    "position": "CEO, Co-Founder",
    "connect_date": "23 Oct 2017"
  },
  {
    "firstname": "Igor",
    "lastname": "Vikhtov",
    "email": "",
    "company": "Lohika",
    "position": "Software Developer",
    "connect_date": "23 Oct 2017"
  },
  {
    "firstname": "Matthew",
    "lastname": "Buskell",
    "email": "",
    "company": "Skillsoft",
    "position": "Area VP EMEA",
    "connect_date": "20 Oct 2017"
  },
  {
    "firstname": "Andrey",
    "lastname": "Biryuk",
    "email": "",
    "company": "Luxoft",
    "position": "Lead Software Engineer",
    "connect_date": "17 Oct 2017"
  },
  {
    "firstname": "Villi",
    "lastname": "Burdiuzha",
    "email": "",
    "company": "The Product Engine",
    "position": "QA Automation Engineer",
    "connect_date": "17 Oct 2017"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Symonds",
    "email": "",
    "company": "Cyber Acuity",
    "position": "Chief Operating Officer",
    "connect_date": "17 Oct 2017"
  },
  {
    "firstname": "Guillaume",
    "lastname": "Raillard",
    "email": "",
    "company": "Global interactive marketing online",
    "position": "CTO",
    "connect_date": "17 Oct 2017"
  },
  {
    "firstname": "Sasha",
    "lastname": "Milia",
    "email": "",
    "company": "OUTSOURCING COMPANY",
    "position": "Senior QA Engineer (Manual/Automation)",
    "connect_date": "16 Oct 2017"
  },
  {
    "firstname": "Guy",
    "lastname": "Magrath",
    "email": "",
    "company": "RS Components",
    "position": "Chief Digital Officer (CDO)",
    "connect_date": "15 Oct 2017"
  },
  {
    "firstname": "Dawn H.",
    "lastname": "Collins",
    "email": "",
    "company": "freelance",
    "position": "research, lecturing, film and editing",
    "connect_date": "14 Oct 2017"
  },
  {
    "firstname": "John",
    "lastname": "White",
    "email": "",
    "company": "Radical Company",
    "position": "Managing Director",
    "connect_date": "12 Oct 2017"
  },
  {
    "firstname": "Nathan",
    "lastname": "Roberts",
    "email": "",
    "company": "Rainbird Technologies",
    "position": "Head Of Research And Development",
    "connect_date": "12 Oct 2017"
  },
  {
    "firstname": "Anna",
    "lastname": "Woodward",
    "email": "",
    "company": "Rainbird Technologies",
    "position": "Human Resources Manager",
    "connect_date": "12 Oct 2017"
  },
  {
    "firstname": "Stuart",
    "lastname": "Clews",
    "email": "",
    "company": "Ten10 Group",
    "position": "Sales and Marketing Director",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Edward",
    "lastname": "Relf",
    "email": "",
    "company": "busuu",
    "position": "Chief Marketing Officer",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Malcolm",
    "lastname": "Graham",
    "email": "",
    "company": "Northgate Public Services",
    "position": "Operations Director",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Wilding",
    "lastname": "Ian",
    "email": "",
    "company": "Hangar 75",
    "position": "Founder",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Philip",
    "lastname": "Green",
    "email": "",
    "company": "Egremont Group",
    "position": "Advisor",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Piero",
    "lastname": "Buttazzo",
    "email": "",
    "company": "Mr & Mrs Smith",
    "position": "Team Lead",
    "connect_date": "11 Oct 2017"
  },
  {
    "firstname": "Sergiy",
    "lastname": "Golubyev",
    "email": "",
    "company": "NGO ''Association of Women of Ukraine \"Diya\"",
    "position": "Project Manager",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Sophie",
    "lastname": "Eden",
    "email": "",
    "company": "Gordon & Eden",
    "position": "Co-Founder",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Hayden J.",
    "lastname": "Carter",
    "email": "",
    "company": "Royal Air Force (RAF)",
    "position": "Aeronautical Engineer Supervisor",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "Turner",
    "email": "",
    "company": "Seldon",
    "position": "Business Development",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Alexander",
    "lastname": "Kotov",
    "email": "",
    "company": "Greenice.net",
    "position": "Frontend Developer",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Dr. Edward",
    "lastname": "Challis",
    "email": "",
    "company": "re:infer",
    "position": "Co-founder",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "Chris",
    "lastname": "Holden",
    "email": "",
    "company": "AXON VIBE",
    "position": "Software Engineer",
    "connect_date": "10 Oct 2017"
  },
  {
    "firstname": "David",
    "lastname": "Buckingham",
    "email": "",
    "company": "Ecrebo",
    "position": "CEO",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Hassan",
    "lastname": "Hajji",
    "email": "",
    "company": "Ecrebo",
    "position": "Co-Founder & President",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Ed",
    "lastname": "Tribe",
    "email": "",
    "company": "Journee",
    "position": "Co-Founder",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Dmitriy",
    "lastname": "Starodub",
    "email": "",
    "company": "K-3 Soft",
    "position": "PHP Laravel Developer",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Mark",
    "lastname": "Walters",
    "email": "",
    "company": "Awin Global",
    "position": "CEO",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Edward",
    "lastname": "Griffith",
    "email": "",
    "company": "LoveCrafts",
    "position": "Founder and CEO",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Adam",
    "lastname": "Street",
    "email": "",
    "company": "Bazaarvoice",
    "position": "Enterprise Sales Director",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Roger",
    "lastname": "Sowerbutts",
    "email": "",
    "company": "Blueprint Software Systems",
    "position": "Vice President international - EMEA and APAC",
    "connect_date": "09 Oct 2017"
  },
  {
    "firstname": "Jon",
    "lastname": "Mullen",
    "email": "",
    "company": "comparethemarket.com",
    "position": "Associate Director, Product Engineering",
    "connect_date": "08 Oct 2017"
  },
  {
    "firstname": "Alessia",
    "lastname": "Sannazzaro",
    "email": "",
    "company": "Code & Wander",
    "position": "Project Director",
    "connect_date": "08 Oct 2017"
  },
  {
    "firstname": "Georgina",
    "lastname": "Horwood",
    "email": "",
    "company": "Randstad",
    "position": "Senior IT Consultant",
    "connect_date": "06 Oct 2017"
  },
  {
    "firstname": "Harry",
    "lastname": "Ramsden",
    "email": "",
    "company": "Reco Group",
    "position": "Senior Java Recruitment Consultant",
    "connect_date": "06 Oct 2017"
  },
  {
    "firstname": "Dan",
    "lastname": "Widing",
    "email": "",
    "company": "ProdPerfect",
    "position": "Founder and CEO",
    "connect_date": "05 Oct 2017"
  },
  {
    "firstname": "Mat",
    "lastname": "Barrow",
    "email": "",
    "company": "Tech Leaders North",
    "position": "Co-Founder",
    "connect_date": "05 Oct 2017"
  },
  {
    "firstname": "Tatiana",
    "lastname": "Krasota",
    "email": "",
    "company": "Master Of Code Global",
    "position": "Director Of Partnerships",
    "connect_date": "05 Oct 2017"
  },
  {
    "firstname": "Andy",
    "lastname": "Neale",
    "email": "",
    "company": "Echobox",
    "position": "JavaScript Engineer",
    "connect_date": "05 Oct 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Benn",
    "email": "",
    "company": "Not Just Computers Limited",
    "position": "Director",
    "connect_date": "03 Oct 2017"
  },
  {
    "firstname": "Daniel",
    "lastname": "Hartveld",
    "email": "",
    "company": "Red Ant",
    "position": "CTO",
    "connect_date": "02 Oct 2017"
  },
  {
    "firstname": "Rob",
    "lastname": "Earles",
    "email": "",
    "company": "Comensura",
    "position": "Senior Business Development Manager",
    "connect_date": "27 Sep 2017"
  },
  {
    "firstname": "Sergii",
    "lastname": "Garkusha",
    "email": "",
    "company": "Workday",
    "position": "Software Development Engineer",
    "connect_date": "27 Sep 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Stewart",
    "email": "",
    "company": "MatchPint",
    "position": "Head Of Operations",
    "connect_date": "26 Sep 2017"
  },
  {
    "firstname": "Jose Carlos",
    "lastname": "Alcaide Garcia-Hidalgo",
    "email": "",
    "company": "ZEAL Network SE",
    "position": "General Product Manager",
    "connect_date": "26 Sep 2017"
  },
  {
    "firstname": "Stephanie",
    "lastname": "Pommares",
    "email": "",
    "company": "Deliveroo",
    "position": "Business Development Manager",
    "connect_date": "25 Sep 2017"
  },
  {
    "firstname": "Alex",
    "lastname": "O'Shaughnessy",
    "email": "",
    "company": "WorldRemit",
    "position": "Chief Marketing Officer",
    "connect_date": "25 Sep 2017"
  },
  {
    "firstname": "Matt",
    "lastname": "White",
    "email": "",
    "company": "Beyond",
    "position": "Tech Manager",
    "connect_date": "25 Sep 2017"
  },
  {
    "firstname": "David",
    "lastname": "Tavergnier",
    "email": "",
    "company": "Sequel Business Solutions Ltd",
    "position": "Product Owner",
    "connect_date": "25 Sep 2017"
  },
  {
    "firstname": "Mark",
    "lastname": "Weston",
    "email": "",
    "company": "Trade Me",
    "position": "Head of Product - Motors Dealers",
    "connect_date": "21 Sep 2017"
  },
  {
    "firstname": "Peter",
    "lastname": "Wetherell",
    "email": "",
    "company": "Wetherell",
    "position": "Founder & Chief Executive",
    "connect_date": "19 Sep 2017"
  },
  {
    "firstname": "Dave",
    "lastname": "Marsland",
    "email": "",
    "company": "Paddle",
    "position": "VP of Engineering",
    "connect_date": "19 Sep 2017"
  },
  {
    "firstname": "Nadya",
    "lastname": "Thorman",
    "email": "",
    "company": "Delivery Associates",
    "position": "Project Leader",
    "connect_date": "19 Sep 2017"
  },
  {
    "firstname": "Carlos Juan",
    "lastname": "Craviotto",
    "email": "",
    "company": "MYTRIAPP",
    "position": "Technical Lead",
    "connect_date": "18 Sep 2017"
  },
  {
    "firstname": "Tim",
    "lastname": "Holman",
    "email": "",
    "company": "2-sec",
    "position": "CEO and Founder",
    "connect_date": "18 Sep 2017"
  },
  {
    "firstname": "Lance",
    "lastname": "Plunkett",
    "email": "",
    "company": "Found.Cloud",
    "position": "Founder/CEO",
    "connect_date": "13 Sep 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Thomas",
    "email": "",
    "company": "Straightforward Media",
    "position": "Managing Director",
    "connect_date": "13 Sep 2017"
  },
  {
    "firstname": "Leanne",
    "lastname": "Purcell",
    "email": "",
    "company": "Treatwell.com",
    "position": "City Manager - Dublin, Ireland",
    "connect_date": "13 Sep 2017"
  },
  {
    "firstname": "Rachel",
    "lastname": "Grigg",
    "email": "",
    "company": "Voodoo Park Ltd",
    "position": "COO and Founder at Voodoo Park",
    "connect_date": "12 Sep 2017"
  },
  {
    "firstname": "Nick",
    "lastname": "Wright",
    "email": "",
    "company": "Esper HQ",
    "position": "Director",
    "connect_date": "12 Sep 2017"
  },
  {
    "firstname": "Syd",
    "lastname": "Nadim",
    "email": "",
    "company": "Clock Limited",
    "position": "★ Exec. Chairman and founder",
    "connect_date": "12 Sep 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Paterson",
    "email": "",
    "company": "Beamery",
    "position": "Co-Founder and CTO",
    "connect_date": "08 Sep 2017"
  },
  {
    "firstname": "George",
    "lastname": "Dunning",
    "email": "",
    "company": "Bud",
    "position": "Co Founder",
    "connect_date": "07 Sep 2017"
  },
  {
    "firstname": "David",
    "lastname": "Lawlor",
    "email": "",
    "company": "Enforcd",
    "position": "Chief Operating Officer",
    "connect_date": "07 Sep 2017"
  },
  {
    "firstname": "Holly",
    "lastname": "Jones",
    "email": "",
    "company": "Venturi Ltd",
    "position": "Senior JavaScript Recruiter",
    "connect_date": "06 Sep 2017"
  },
  {
    "firstname": "Ola",
    "lastname": "Denslaw",
    "email": "",
    "company": "Venturi Ltd",
    "position": "Development Consultant",
    "connect_date": "06 Sep 2017"
  },
  {
    "firstname": "Josh",
    "lastname": "Hart",
    "email": "",
    "company": "yu life",
    "position": "Co-Founder, Chief Digital & Technology Officer",
    "connect_date": "06 Sep 2017"
  },
  {
    "firstname": "Mike",
    "lastname": "Wilde",
    "email": "",
    "company": "Appy Monkey",
    "position": "Managing Director",
    "connect_date": "05 Sep 2017"
  },
  {
    "firstname": "James",
    "lastname": "Cronin",
    "email": "",
    "company": "Hidden Football",
    "position": "Founder & Freelance Writer",
    "connect_date": "05 Sep 2017"
  },
  {
    "firstname": "John",
    "lastname": "Levin",
    "email": "",
    "company": "Certua",
    "position": "Co Founder and director",
    "connect_date": "04 Sep 2017"
  },
  {
    "firstname": "Arif",
    "lastname": "Meherali",
    "email": "",
    "company": "eBay",
    "position": "UK Curator, Buyer Experience",
    "connect_date": "04 Sep 2017"
  },
  {
    "firstname": "James",
    "lastname": "Turner",
    "email": "",
    "company": "delineate.",
    "position": "Founder & CEO",
    "connect_date": "03 Sep 2017"
  },
  {
    "firstname": "Dave",
    "lastname": "McKerral",
    "email": "",
    "company": "evestor",
    "position": "Chief Technology Officer",
    "connect_date": "03 Sep 2017"
  },
  {
    "firstname": "Haakon",
    "lastname": "Overli",
    "email": "",
    "company": "Dawn Capital",
    "position": "General Partner",
    "connect_date": "03 Sep 2017"
  },
  {
    "firstname": "Chris",
    "lastname": "Gledhill",
    "email": "",
    "company": "Secco Aura",
    "position": "Chief Executive Officer",
    "connect_date": "01 Sep 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Rolph",
    "email": "",
    "company": "Yoyo Wallet",
    "position": "CEO & Co-founder",
    "connect_date": "01 Sep 2017"
  },
  {
    "firstname": "Alina",
    "lastname": "Pavlova",
    "email": "",
    "company": "DevCom",
    "position": "HR Manager",
    "connect_date": "01 Sep 2017"
  },
  {
    "firstname": "Piers",
    "lastname": "Chead",
    "email": "",
    "company": "LegalZoom UK",
    "position": "Managing Director",
    "connect_date": "31 Aug 2017"
  },
  {
    "firstname": "Ben",
    "lastname": "Sansom",
    "email": "",
    "company": "Ever Increasing Circles Ltd",
    "position": "Director",
    "connect_date": "31 Aug 2017"
  },
  {
    "firstname": "Kenny",
    "lastname": "Fraser",
    "email": "",
    "company": "Triscribe Ltd",
    "position": "CEO",
    "connect_date": "31 Aug 2017"
  },
  {
    "firstname": "Phoebe Chibuzo",
    "lastname": "Hugh",
    "email": "",
    "company": "Brolly",
    "position": "Founder & CEO",
    "connect_date": "31 Aug 2017"
  },
  {
    "firstname": "Adi",
    "lastname": "Ben-Ari",
    "email": "",
    "company": "Applied Blockchain",
    "position": "Founder / CEO",
    "connect_date": "30 Aug 2017"
  },
  {
    "firstname": "Felice",
    "lastname": "Curcelli",
    "email": "",
    "company": "LiberCloud",
    "position": "Co-founder, Head of Products Development",
    "connect_date": "30 Aug 2017"
  },
  {
    "firstname": "Johnny",
    "lastname": "Bridges",
    "email": "",
    "company": "artificial.",
    "position": "Founder",
    "connect_date": "30 Aug 2017"
  },
  {
    "firstname": "Eddie",
    "lastname": "Godshalk,  MBA",
    "email": "",
    "company": "Growth Maps LLC",
    "position": "Helping Real Estate Investors, Brokers & Financial Advisors  Providing Improved Data & Reporting.",
    "connect_date": "30 Aug 2017"
  },
  {
    "firstname": "Rod",
    "lastname": "Homer",
    "email": "",
    "company": "FusionExperience",
    "position": "Chairman",
    "connect_date": "29 Aug 2017"
  },
  {
    "firstname": "Benn",
    "lastname": "Achilleas",
    "email": "",
    "company": "Sports Buff",
    "position": "Sports Buff",
    "connect_date": "27 Aug 2017"
  },
  {
    "firstname": "Vic",
    "lastname": "Morgan",
    "email": "",
    "company": "Venture Stream",
    "position": "Managing Director & Founder",
    "connect_date": "27 Aug 2017"
  },
  {
    "firstname": "George",
    "lastname": "Brasher",
    "email": "",
    "company": "HP",
    "position": "Managing Director - United Kingdom and Ireland, Vice President and General Manager",
    "connect_date": "25 Aug 2017"
  },
  {
    "firstname": "Christian",
    "lastname": "Owens",
    "email": "",
    "company": "Paddle",
    "position": "Founder, CEO",
    "connect_date": "24 Aug 2017"
  },
  {
    "firstname": "Scott",
    "lastname": "Miller",
    "email": "",
    "company": "Finca Organica Nosara",
    "position": "Co-Founder",
    "connect_date": "24 Aug 2017"
  },
  {
    "firstname": "Paul",
    "lastname": "Hodel",
    "email": "",
    "company": "Research Partnership",
    "position": "Lead PHP, Postgresql Developer and Linux Administration",
    "connect_date": "24 Aug 2017"
  },
  {
    "firstname": "Barnaby",
    "lastname": "Hussey-Yeo",
    "email": "",
    "company": "Cleo AI",
    "position": "Founder & CEO",
    "connect_date": "24 Aug 2017"
  },
  {
    "firstname": "Venkatesh",
    "lastname": "Akula",
    "email": "",
    "company": "ClicQA",
    "position": "Founder & CEO",
    "connect_date": "24 Aug 2017"
  },
  {
    "firstname": "Dom",
    "lastname": "Bracher",
    "email": "",
    "company": "Tapdaq",
    "position": "Co-Founder",
    "connect_date": "23 Aug 2017"
  },
  {
    "firstname": "Alastair",
    "lastname": "Thomson",
    "email": "",
    "company": "Tapdaq",
    "position": "Head of Partnerships",
    "connect_date": "23 Aug 2017"
  },
  {
    "firstname": "Nuno",
    "lastname": "Job",
    "email": "",
    "company": "YLD",
    "position": "CEO",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Dr. Ben",
    "lastname": "Maruthappu",
    "email": "",
    "company": "Cera - CeraCare.co.uk",
    "position": "Co-founder & CEO",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Marc",
    "lastname": "Sloan",
    "email": "",
    "company": "Context Scout",
    "position": "CEO and Co-Founder",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Mihnea",
    "lastname": "Spirescu",
    "email": "",
    "company": "Contino",
    "position": "Senior Consultant",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Simon",
    "lastname": "Altschuler",
    "email": "",
    "company": "Trololo",
    "position": "Owner",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Clare",
    "lastname": "Ward (nee Daintith)",
    "email": "",
    "company": "Strategic People",
    "position": "Founder",
    "connect_date": "22 Aug 2017"
  },
  {
    "firstname": "Madeleine",
    "lastname": "Odell",
    "email": "",
    "company": "EMAP",
    "position": "Senior Subscriptions Sales Consultant",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Artur",
    "lastname": "Kisiolek",
    "email": "",
    "company": "Artificial Labs",
    "position": "CTO",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Leonard",
    "lastname": "Newnham",
    "email": "",
    "company": "LoopMe",
    "position": "Chief Data Scientist",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Stephen",
    "lastname": "Upstone",
    "email": "",
    "company": "LoopMe",
    "position": "CEO & Founder",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Liam",
    "lastname": "Negus-Fancey",
    "email": "",
    "company": "Verve Software",
    "position": "Founder & President",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Luke",
    "lastname": "McNeice",
    "email": "",
    "company": "Lola Tech",
    "position": "Founder & Chief Software Architect",
    "connect_date": "21 Aug 2017"
  },
  {
    "firstname": "Yadvender",
    "lastname": "Singh Yadav",
    "email": "",
    "company": "Pitzo India Private Limited",
    "position": "Chief Executive Officer",
    "connect_date": "19 Aug 2017"
  },
  {
    "firstname": "Stuart",
    "lastname": "Hill",
    "email": "",
    "company": "Farfetch",
    "position": "VP, Logistics",
    "connect_date": "19 Aug 2017"
  },
  {
    "firstname": "Żaneta",
    "lastname": "Baran",
    "email": "",
    "company": "zanetabaran.com",
    "position": "Freelance Blogger",
    "connect_date": "19 Aug 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "Mortimer",
    "email": "",
    "company": "Gumtree.com",
    "position": "Consultant - Gumtree Motors",
    "connect_date": "19 Aug 2017"
  },
  {
    "firstname": "Brendan",
    "lastname": "Moore",
    "email": "",
    "company": "Marvel Prototyping",
    "position": "Co-Founder",
    "connect_date": "19 Aug 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "Salter",
    "email": "",
    "company": "Salter Brothers",
    "position": "Founder & CEO",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Zvezdomir",
    "lastname": "Zlatinov",
    "email": "",
    "company": "eMerchantPay Ltd",
    "position": "Software Engineer",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Callum",
    "lastname": "Negus-Fancey",
    "email": "",
    "company": "Verve",
    "position": "Founder and CEO",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Babek",
    "lastname": "Ismayil",
    "email": "",
    "company": "OneDome",
    "position": "Founder & CEO",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Steve Nico",
    "lastname": "Williams",
    "email": "",
    "company": "Populous World",
    "position": "Chief Executive Officer",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Edward",
    "lastname": "Maslaveckas",
    "email": "",
    "company": "Bud",
    "position": "Co-Founder",
    "connect_date": "18 Aug 2017"
  },
  {
    "firstname": "Chris",
    "lastname": "Harding",
    "email": "",
    "company": "ConnectMed",
    "position": "Co-founder & CTO",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Olivier",
    "lastname": "Stapylton-Smith",
    "email": "",
    "company": "Moteefe",
    "position": "Sales & Marketing Director, Co-founder",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Tom",
    "lastname": "Marius",
    "email": "",
    "company": "CEMEX",
    "position": "Web Architect",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Juned",
    "lastname": "Jable",
    "email": "",
    "company": "RedBluecat Ltd",
    "position": "Director",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Dan",
    "lastname": "Mortimer",
    "email": "",
    "company": "Red Ant",
    "position": "CEO",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "Poxton",
    "email": "",
    "company": "Parallel Consulting",
    "position": "Managing Consultant - Data & Analytics",
    "connect_date": "17 Aug 2017"
  },
  {
    "firstname": "Joanne",
    "lastname": "Spector",
    "email": "",
    "company": "Startups: Earthbourne, Green Dot, Switch Concepts",
    "position": "Co-founder and Private Investor in startups and private companies",
    "connect_date": "16 Aug 2017"
  },
  {
    "firstname": "Robin",
    "lastname": "Doran",
    "email": "",
    "company": "IntuDigital",
    "position": "CTO",
    "connect_date": "16 Aug 2017"
  },
  {
    "firstname": "Aymeric",
    "lastname": "Souleau",
    "email": "",
    "company": "Kamet Ventures",
    "position": "Head of Product and Data at Sphere",
    "connect_date": "16 Aug 2017"
  },
  {
    "firstname": "Peter",
    "lastname": "Kowalczyk",
    "email": "",
    "company": "Aurity",
    "position": "Architect React, React-Native, Redux contractor (founder)",
    "connect_date": "15 Aug 2017"
  },
  {
    "firstname": "Marina",
    "lastname": "Liutenko",
    "email": "",
    "company": ":eSolutions Odessa",
    "position": "Recruitment Specialist",
    "connect_date": "15 Aug 2017"
  },
  {
    "firstname": "Jan",
    "lastname": "Medvesek, Ph.D.",
    "email": "",
    "company": "Emotech LTD",
    "position": "Co Founder",
    "connect_date": "14 Aug 2017"
  },
  {
    "firstname": "Samuel",
    "lastname": "Skinner",
    "email": "",
    "company": "City Pantry",
    "position": "Senior Account Manager",
    "connect_date": "14 Aug 2017"
  },
  {
    "firstname": "Parminder",
    "lastname": "Toor",
    "email": "",
    "company": "Parallel Consulting",
    "position": "Contracts Director",
    "connect_date": "14 Aug 2017"
  },
  {
    "firstname": "Carolin",
    "lastname": "Fleissner",
    "email": "",
    "company": "Wayve",
    "position": "Autonomous Vehicles",
    "connect_date": "14 Aug 2017"
  },
  {
    "firstname": "Sefton",
    "lastname": "Monk",
    "email": "",
    "company": "Preqin",
    "position": "Senior Technical Consultant",
    "connect_date": "13 Aug 2017"
  },
  {
    "firstname": "Alex",
    "lastname": "Appelbe",
    "email": "",
    "company": "Metis Labs",
    "position": "Co-Founder & CEO",
    "connect_date": "13 Aug 2017"
  },
  {
    "firstname": "Takuya",
    "lastname": "Homma",
    "email": "",
    "company": "Quipper Limited",
    "position": "Founding Member & Indonesia Country Manager",
    "connect_date": "13 Aug 2017"
  },
  {
    "firstname": "Jon",
    "lastname": "Anthony",
    "email": "",
    "company": "adappt Technology Partner (ISO 9001:2015)",
    "position": "Managing Director and Founder",
    "connect_date": "12 Aug 2017"
  },
  {
    "firstname": "Josh",
    "lastname": "Murphy",
    "email": "",
    "company": "Premier Group Recruitment",
    "position": "Lead Development Consultant",
    "connect_date": "12 Aug 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Downes",
    "email": "",
    "company": "PowWowNow",
    "position": "SVP & General Manager PowWowNow, PGi Commercial,  ReadyTalk & iMC.",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Robin",
    "lastname": "Tombs",
    "email": "",
    "company": "Yoti",
    "position": "Co Founder and CEO",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Patrik",
    "lastname": "Fuhrmann",
    "email": "",
    "company": "SO Connect",
    "position": "Software Engineer",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Richard",
    "lastname": "Ford",
    "email": "",
    "company": "HP",
    "position": "Enterprise Solutions Director UKI",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Matt",
    "lastname": "Burrell",
    "email": "",
    "company": "Solid Code Solutions",
    "position": "Owner",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Murshid",
    "lastname": "M. Ali",
    "email": "",
    "company": "Norsk Solar",
    "position": "Investment Director",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Dr. Andrew",
    "lastname": "Fenna",
    "email": "",
    "company": "Royal London",
    "position": "Senior Software Developer",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Tim",
    "lastname": "Wilson",
    "email": "",
    "company": "Centiment.io",
    "position": "Co-founder & CTO",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Jamie",
    "lastname": "Bolding",
    "email": "",
    "company": "Jungle Creations",
    "position": "Founder & CEO",
    "connect_date": "11 Aug 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Atlas",
    "email": "",
    "company": "Not yet but soon",
    "position": "CTO",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Jakub",
    "lastname": "Žák",
    "email": "",
    "company": "Anyway Solutions s.r.o.",
    "position": "Founder",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Sam",
    "lastname": "Altarafi",
    "email": "",
    "company": "Parallel Consulting Limited",
    "position": "Managing Director",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Alexandre",
    "lastname": "Gruca",
    "email": "",
    "company": "Joivy",
    "position": "Co-Founder",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Julian",
    "lastname": "Pittam",
    "email": "",
    "company": "certua",
    "position": "Non Executive Director and Advisory Board Member",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Iain",
    "lastname": "Richardson",
    "email": "",
    "company": "Arrk Group",
    "position": "Lead Android Developer (contract)",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Salvatore",
    "lastname": "Minetti",
    "email": "",
    "company": "prospex",
    "position": "Founder + CEO",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Sam",
    "lastname": "Nixon",
    "email": "",
    "company": "Babylon Health",
    "position": "Senior Product Manager",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Alex",
    "lastname": "Newell",
    "email": "",
    "company": "Parallel Consulting",
    "position": "Principal Consultant",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Jose E.",
    "lastname": "Pettoruti",
    "email": "",
    "company": "Currencycloud",
    "position": "Director Of Engineering",
    "connect_date": "10 Aug 2017"
  },
  {
    "firstname": "Elen",
    "lastname": "Purgina",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Douglas",
    "lastname": "Horner",
    "email": "",
    "company": "Energize Recruitment Solutions",
    "position": "Senior Consultant",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Gaspar",
    "lastname": "Iriondo",
    "email": "",
    "company": "B-Ethereum",
    "position": "Lead Developer",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Nicolas",
    "lastname": "Taborisky",
    "email": "",
    "company": "Theodo",
    "position": "US CEO",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Marcus",
    "lastname": "de Wilde",
    "email": "",
    "company": "ThreatInformer",
    "position": "Business Development Director",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Colin",
    "lastname": "Sykes",
    "email": "",
    "company": "Mercurytide",
    "position": "Commercial Manager",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Chris",
    "lastname": "Wigley",
    "email": "",
    "company": "QuantumBlack",
    "position": "Solution Partner",
    "connect_date": "09 Aug 2017"
  },
  {
    "firstname": "Hatim",
    "lastname": "Siyawala",
    "email": "",
    "company": "Siya Tech Ventures",
    "position": "Managing Director",
    "connect_date": "08 Aug 2017"
  },
  {
    "firstname": "Vishnu",
    "lastname": "Hariharan, CFA",
    "email": "",
    "company": "Osper",
    "position": "CFO",
    "connect_date": "08 Aug 2017"
  },
  {
    "firstname": "Samuel",
    "lastname": "Cropper",
    "email": "",
    "company": "Tiberius Geldard",
    "position": "Founder, Director",
    "connect_date": "08 Aug 2017"
  },
  {
    "firstname": "Tom",
    "lastname": "Salfield",
    "email": "",
    "company": "Wikifactory",
    "position": "CoFounder and CEO",
    "connect_date": "08 Aug 2017"
  },
  {
    "firstname": "Anita",
    "lastname": "Rajdev",
    "email": "",
    "company": "TH_NK",
    "position": "Chief Operating Officer",
    "connect_date": "08 Aug 2017"
  },
  {
    "firstname": "Mohammed",
    "lastname": "Tayeb",
    "email": "",
    "company": "Medicalchain.com",
    "position": "Co-Founder",
    "connect_date": "06 Aug 2017"
  },
  {
    "firstname": "Simon Connor",
    "lastname": "Roberts",
    "email": "",
    "company": "Mercateo",
    "position": "Managing Director UK & Ireland",
    "connect_date": "05 Aug 2017"
  },
  {
    "firstname": "Vishal",
    "lastname": "Chhatralia",
    "email": "",
    "company": "RS Components",
    "position": "Global Vice President / Head of Digital",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Jonathan",
    "lastname": "Raynor",
    "email": "",
    "company": "Docebo",
    "position": "Account Executive",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Angela",
    "lastname": "Roldan",
    "email": "",
    "company": "Artsy",
    "position": "Gallery Partnerships Manager",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Oleksandr",
    "lastname": "Stasyk",
    "email": "",
    "company": "SolarWinds MSP",
    "position": "Senior Software Developer",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Robert",
    "lastname": "Dagge",
    "email": "",
    "company": "Dynistics",
    "position": "Managing Director",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Adam",
    "lastname": "Stone",
    "email": "",
    "company": "Rokk Media Ltd",
    "position": "CEO/Majority Shareholder",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Ben",
    "lastname": "Carey",
    "email": "",
    "company": "Bubbla Limited",
    "position": "Co-Founder, CEO and CTO",
    "connect_date": "04 Aug 2017"
  },
  {
    "firstname": "Natan",
    "lastname": "Kous",
    "email": "",
    "company": "Nesco Consulting",
    "position": "Аутсорсинг Лидогенерации в LinkedIn для IT (Outsourcing/Outstaffing/B2B Saas/Digital)",
    "connect_date": "03 Aug 2017"
  },
  {
    "firstname": "Amardeep Singh",
    "lastname": "Shakhon",
    "email": "",
    "company": "Freelance",
    "position": "Executive UX Consultant",
    "connect_date": "03 Aug 2017"
  },
  {
    "firstname": "Alvar",
    "lastname": "Lumberg",
    "email": "",
    "company": "TransferWise",
    "position": "Engineering Lead, Platform",
    "connect_date": "03 Aug 2017"
  },
  {
    "firstname": "Clemens",
    "lastname": "Aichholzer",
    "email": "",
    "company": "HireVue",
    "position": "Senior Vice President",
    "connect_date": "03 Aug 2017"
  },
  {
    "firstname": "Jon",
    "lastname": "Wilkins",
    "email": "",
    "company": "Karmarama",
    "position": "Chairman",
    "connect_date": "03 Aug 2017"
  },
  {
    "firstname": "Rob",
    "lastname": "Hopson",
    "email": "",
    "company": "Karmarama",
    "position": "Business Director",
    "connect_date": "02 Aug 2017"
  },
  {
    "firstname": "Lee",
    "lastname": "West",
    "email": "",
    "company": "M-Brain Global",
    "position": "Managing Director (UK and Middle East)",
    "connect_date": "02 Aug 2017"
  },
  {
    "firstname": "James",
    "lastname": "Denton-Clark",
    "email": "",
    "company": "Karmarama",
    "position": "Managing Director",
    "connect_date": "02 Aug 2017"
  },
  {
    "firstname": "Jason",
    "lastname": "Gaved",
    "email": "",
    "company": "Calibre",
    "position": "Director",
    "connect_date": "02 Aug 2017"
  },
  {
    "firstname": "Lisa",
    "lastname": "Voronkova",
    "email": "",
    "company": "Emotion Laboratories",
    "position": "CEO",
    "connect_date": "02 Aug 2017"
  },
  {
    "firstname": "Ben",
    "lastname": "Wynne-Simmons",
    "email": "",
    "company": "Fliplet",
    "position": "Head of Growth Marketing",
    "connect_date": "01 Aug 2017"
  },
  {
    "firstname": "Ron",
    "lastname": "Kalifa",
    "email": "",
    "company": "WorldPay",
    "position": "Deputy Chairman",
    "connect_date": "01 Aug 2017"
  },
  {
    "firstname": "Andrew",
    "lastname": "McAllister",
    "email": "",
    "company": "Sequel Business Solutions Ltd",
    "position": "CTO",
    "connect_date": "29 Jul 2017"
  },
  {
    "firstname": "Greg",
    "lastname": "Webb",
    "email": "",
    "company": "Siteskills ltd",
    "position": "Founding Partner",
    "connect_date": "29 Jul 2017"
  },
  {
    "firstname": "Ciprian",
    "lastname": "Spiridon",
    "email": "",
    "company": "Landmark Group",
    "position": "Software Engineering Manager",
    "connect_date": "28 Jul 2017"
  },
  {
    "firstname": "David",
    "lastname": "Kerr",
    "email": "",
    "company": "Client Server Ltd",
    "position": "Director",
    "connect_date": "28 Jul 2017"
  },
  {
    "firstname": "Dimitris",
    "lastname": "Vassiliadis",
    "email": "",
    "company": "EXUS",
    "position": "Director - Product and Business Development",
    "connect_date": "28 Jul 2017"
  },
  {
    "firstname": "Marcello",
    "lastname": "Silvestri",
    "email": "",
    "company": "WIREDMARK LTD",
    "position": "Managing Director",
    "connect_date": "28 Jul 2017"
  },
  {
    "firstname": "Anastasia",
    "lastname": "Ihnatenko",
    "email": "",
    "company": "DIGIS",
    "position": "UI/UX",
    "connect_date": "20 Jul 2017"
  },
  {
    "firstname": "Peter",
    "lastname": "Suprinovich",
    "email": "",
    "company": "TIS Group of Terminals",
    "position": "Director (Trans Concordia LLC)",
    "connect_date": "07 Jul 2017"
  },
  {
    "firstname": "Andriy",
    "lastname": "Bugayevskyy",
    "email": "",
    "company": "Cryptonomos",
    "position": "Head Of Delivery Projects and Business Development",
    "connect_date": "07 Jul 2017"
  },
  {
    "firstname": "Beth",
    "lastname": "Rodgers",
    "email": "",
    "company": "Nixor Resource Consulting Ltd",
    "position": "Recruitment Consultant",
    "connect_date": "03 Jul 2017"
  },
  {
    "firstname": "Jerry",
    "lastname": "Blaze",
    "email": "",
    "company": "Eastern Scientific Inc",
    "position": "Business Develpment & Sales",
    "connect_date": "29 Jun 2017"
  },
  {
    "firstname": "Ian",
    "lastname": "Griffith",
    "email": "",
    "company": "PwC",
    "position": "Senior Associate, M&A Advisory",
    "connect_date": "14 Jun 2017"
  },
  {
    "firstname": "Александр",
    "lastname": "Мыльченко",
    "email": "",
    "company": "Upwork",
    "position": "Full-stack Developer",
    "connect_date": "02 Jun 2017"
  },
  {
    "firstname": "Nataliya",
    "lastname": "Piskun(Bogoslavets)",
    "email": "",
    "company": "NetForce Ukraine LLC",
    "position": "Sales&Account Manager",
    "connect_date": "30 May 2017"
  },
  {
    "firstname": "Tanya",
    "lastname": "Lipovich",
    "email": "",
    "company": "StubHub",
    "position": "Sr. Marketing Manager",
    "connect_date": "25 May 2017"
  },
  {
    "firstname": "Dmitriy",
    "lastname": "Vishnevskiy",
    "email": "",
    "company": "Devmatic",
    "position": "Co-Founder, CEO",
    "connect_date": "20 May 2017"
  },
  {
    "firstname": "Anton",
    "lastname": "Vuchkan",
    "email": "",
    "company": "AVE idea",
    "position": "Co-founder",
    "connect_date": "20 May 2017"
  },
  {
    "firstname": "George",
    "lastname": "Niculae",
    "email": "",
    "company": "eZuce, Inc.",
    "position": "Senior Software Developer / Software Architect",
    "connect_date": "10 May 2017"
  },
  {
    "firstname": "Vitaly",
    "lastname": "Pochtar",
    "email": "",
    "company": "GlobMill",
    "position": "CEO",
    "connect_date": "10 May 2017"
  },
  {
    "firstname": "Douglas",
    "lastname": "Hubler",
    "email": "",
    "company": "Self-Employed",
    "position": "Independent Software Consultant",
    "connect_date": "09 May 2017"
  },
  {
    "firstname": "Alena",
    "lastname": "Savchenko",
    "email": "",
    "company": "NAKIVO",
    "position": "Head of HR Department",
    "connect_date": "19 Apr 2017"
  },
  {
    "firstname": "Barbora",
    "lastname": "Zavadilová",
    "email": "",
    "company": "Job-IT.cz",
    "position": "Account Manager",
    "connect_date": "11 Apr 2017"
  },
  {
    "firstname": "Michael",
    "lastname": "Kelly",
    "email": "",
    "company": "Melillo Consulting",
    "position": "Director Of Business Development",
    "connect_date": "09 Apr 2017"
  },
  {
    "firstname": "Clément",
    "lastname": "de Noirmont",
    "email": "",
    "company": "LFPI",
    "position": "Investment Analyst",
    "connect_date": "27 Mar 2017"
  },
  {
    "firstname": "Vita",
    "lastname": "Jasaite",
    "email": "",
    "company": "Aciety",
    "position": "Business Developer",
    "connect_date": "27 Mar 2017"
  },
  {
    "firstname": "Andrii",
    "lastname": "Chmykhalov",
    "email": "",
    "company": "Студия Brander",
    "position": "Recruiter/Sales Analyst",
    "connect_date": "27 Mar 2017"
  },
  {
    "firstname": "Rajat",
    "lastname": "Lala",
    "email": "",
    "company": "Soluloid Inc",
    "position": "CEO & Founder",
    "connect_date": "24 Mar 2017"
  },
  {
    "firstname": "Ilya",
    "lastname": "Azovtsev 🚀",
    "email": "",
    "company": "Docsify",
    "position": "Head of Growth",
    "connect_date": "24 Mar 2017"
  },
  {
    "firstname": "Georgiy",
    "lastname": "Novytsky",
    "email": "",
    "company": "A-Soft",
    "position": "QA manual",
    "connect_date": "24 Mar 2017"
  },
  {
    "firstname": "💡-  Pierre",
    "lastname": "Castermans  -💡",
    "email": "",
    "company": "Transparen",
    "position": "Director of business development",
    "connect_date": "23 Mar 2017"
  },
  {
    "firstname": "Sergey",
    "lastname": "Cherednichenko",
    "email": "",
    "company": "OSSystem",
    "position": "Frontend Engineer",
    "connect_date": "23 Mar 2017"
  },
  {
    "firstname": "Anton",
    "lastname": "Fefelov",
    "email": "",
    "company": "PAF Advisers",
    "position": "Founder & CEO",
    "connect_date": "12 Mar 2017"
  },
  {
    "firstname": "Roman",
    "lastname": "Voznyy",
    "email": "",
    "company": "Innoteka",
    "position": "Founder & CEO",
    "connect_date": "06 Mar 2017"
  },
  {
    "firstname": "Viktor",
    "lastname": "Chekh",
    "email": "",
    "company": "Sombra Inc.",
    "position": "CEO, Co-founder",
    "connect_date": "01 Mar 2017"
  },
  {
    "firstname": "Henry",
    "lastname": "Obegi",
    "email": "",
    "company": "Shoki",
    "position": "CEO",
    "connect_date": "28 Feb 2017"
  },
  {
    "firstname": "Liliia",
    "lastname": "Stetsiuk",
    "email": "",
    "company": "BotsCrew",
    "position": "IT Recruiter",
    "connect_date": "24 Feb 2017"
  },
  {
    "firstname": "Bogdan",
    "lastname": "Makh",
    "email": "",
    "company": "Levi9 Ukraine",
    "position": "Recruitment Team Lead",
    "connect_date": "22 Feb 2017"
  },
  {
    "firstname": "Max",
    "lastname": "Makarenko",
    "email": "",
    "company": "Docsify",
    "position": "Founder & CEO",
    "connect_date": "22 Feb 2017"
  },
  {
    "firstname": "Eddie",
    "lastname": "Terpan",
    "email": "",
    "company": "AutoDoc",
    "position": "PHP/JS Software Developer",
    "connect_date": "22 Feb 2017"
  },
  {
    "firstname": "Maksym",
    "lastname": "Negoda",
    "email": "",
    "company": "MIRS Corporation",
    "position": "IT Program Manager",
    "connect_date": "16 Feb 2017"
  },
  {
    "firstname": "Sangitha",
    "lastname": "K",
    "email": "",
    "company": "Confidential",
    "position": "Technical Recruitment Consultant",
    "connect_date": "16 Feb 2017"
  },
  {
    "firstname": "Avraham",
    "lastname": "Vasilyev",
    "email": "",
    "company": "Avraham Vasilyev Marketing",
    "position": "manager",
    "connect_date": "15 Feb 2017"
  },
  {
    "firstname": "Yura",
    "lastname": "Matiishyn",
    "email": "",
    "company": "Apiko",
    "position": "HR Generalist",
    "connect_date": "10 Feb 2017"
  },
  {
    "firstname": "Maria",
    "lastname": "Tsuprik",
    "email": "",
    "company": "OSSystem",
    "position": "HR Account Manager",
    "connect_date": "09 Feb 2017"
  },
  {
    "firstname": "Helen",
    "lastname": "Bokancha",
    "email": "",
    "company": "Netcracker Technology",
    "position": "HR Specialist",
    "connect_date": "08 Feb 2017"
  },
  {
    "firstname": "Rostislav",
    "lastname": "Bakhmetov",
    "email": "",
    "company": "Infopulse GmbH",
    "position": "Software Developer",
    "connect_date": "03 Feb 2017"
  },
  {
    "firstname": "Pavel",
    "lastname": "Obod",
    "email": "",
    "company": "Sloboda studio",
    "position": "Founder & CEO",
    "connect_date": "03 Feb 2017"
  },
  {
    "firstname": "Sergii",
    "lastname": "Miakshynov",
    "email": "",
    "company": "Sombra Inc.",
    "position": "Co-Founder, COO",
    "connect_date": "01 Feb 2017"
  },
  {
    "firstname": "Oleg",
    "lastname": "Kutafin",
    "email": "",
    "company": "GamePoint",
    "position": "QA Automation Engineer",
    "connect_date": "31 Jan 2017"
  },
  {
    "firstname": "Дмитрий",
    "lastname": "Поломарчук",
    "email": "",
    "company": "Индивидуальный предприниматель",
    "position": "Художник",
    "connect_date": "25 Jan 2017"
  },
  {
    "firstname": "Oleg",
    "lastname": "Dudka",
    "email": "",
    "company": "Think2Solve",
    "position": "PHP Developer",
    "connect_date": "19 Jan 2017"
  },
  {
    "firstname": "Natalia",
    "lastname": "Harbar",
    "email": "",
    "company": "intive - never settle",
    "position": "IT Recruitment Specialist",
    "connect_date": "16 Jan 2017"
  },
  {
    "firstname": "Frederic",
    "lastname": "Oster",
    "email": "",
    "company": "FO-Consulting",
    "position": "Owner & Business Consultant",
    "connect_date": "14 Jan 2017"
  },
  {
    "firstname": "Anton",
    "lastname": "Skrypnyk",
    "email": "",
    "company": "KindGeek",
    "position": "CEO",
    "connect_date": "09 Jan 2017"
  },
  {
    "firstname": "Chan",
    "lastname": "Chan",
    "email": "",
    "company": "Panda New Capital",
    "position": "Senior investment Manager",
    "connect_date": "05 Jan 2017"
  },
  {
    "firstname": "Konstantin",
    "lastname": "Pristavskiy",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "26 Dec 2016"
  },
  {
    "firstname": "Артем",
    "lastname": "Ермаков",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "08 Dec 2016"
  },
  {
    "firstname": "Anna",
    "lastname": "Khivrenko",
    "email": "",
    "company": "EPAM Systems",
    "position": "Business Analyst",
    "connect_date": "23 Nov 2016"
  },
  {
    "firstname": "Artem",
    "lastname": "Gunko",
    "email": "",
    "company": "ZEO Alliance",
    "position": "Talent Acquisition Lead",
    "connect_date": "23 Nov 2016"
  },
  {
    "firstname": "Oleksii",
    "lastname": "Ilchyshyn",
    "email": "",
    "company": "Singularika",
    "position": "VP of Business Development",
    "connect_date": "22 Nov 2016"
  },
  {
    "firstname": "Maryna",
    "lastname": "Vyshnyvetska",
    "email": "",
    "company": "Kenaz GmbH",
    "position": "CEO, Founder",
    "connect_date": "21 Nov 2016"
  },
  {
    "firstname": "Vladimir",
    "lastname": "Koval",
    "email": "",
    "company": "Singularika",
    "position": "Founder and CEO",
    "connect_date": "21 Nov 2016"
  },
  {
    "firstname": "Anna",
    "lastname": "Pavlovskaya",
    "email": "",
    "company": "LOGIMATIKA",
    "position": "Business Development Manager",
    "connect_date": "16 Nov 2016"
  },
  {
    "firstname": "Vladan",
    "lastname": "Golubovic",
    "email": "",
    "company": "DREAMSDESIGN TECHNOLOGIES",
    "position": "CEO",
    "connect_date": "11 Nov 2016"
  },
  {
    "firstname": "Igor",
    "lastname": "Iermolenko",
    "email": "",
    "company": "Limestone Digital",
    "position": "Head of sales at Limestone",
    "connect_date": "08 Nov 2016"
  },
  {
    "firstname": "Simon",
    "lastname": "Hall",
    "email": "",
    "company": "5xThinking",
    "position": "CEO & Founder",
    "connect_date": "07 Nov 2016"
  },
  {
    "firstname": "Chris",
    "lastname": "Maciejewski",
    "email": "",
    "company": "VoIPstudio.com",
    "position": "Director",
    "connect_date": "03 Nov 2016"
  },
  {
    "firstname": "Denis",
    "lastname": "Dovgopoliy",
    "email": "",
    "company": "Digital Transformation Institute Ukraine",
    "position": "Board Member",
    "connect_date": "23 Oct 2016"
  },
  {
    "firstname": "Mykhailo",
    "lastname": "Maryniak",
    "email": "",
    "company": "Noltic",
    "position": "Managing Partner",
    "connect_date": "17 Oct 2016"
  },
  {
    "firstname": "Vojtech",
    "lastname": "Zahorsky",
    "email": "",
    "company": "Hiremotely.com",
    "position": "CEO and Founder",
    "connect_date": "11 Oct 2016"
  },
  {
    "firstname": "Kate",
    "lastname": "Korzhenko",
    "email": "",
    "company": "OSSystem",
    "position": "HR Manager",
    "connect_date": "10 Oct 2016"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Morozov",
    "email": "",
    "company": "Knauf",
    "position": "Lead Software Developer",
    "connect_date": "06 Oct 2016"
  },
  {
    "firstname": "Максим",
    "lastname": "Стоянов",
    "email": "",
    "company": "G-Apps-Script",
    "position": "Редактор, Дизайнер, Верстальщик",
    "connect_date": "05 Oct 2016"
  },
  {
    "firstname": "Marina",
    "lastname": "Smovzh",
    "email": "",
    "company": "IQOSA",
    "position": "Architect",
    "connect_date": "03 Oct 2016"
  },
  {
    "firstname": "Chaz",
    "lastname": "Englander",
    "email": "",
    "company": "Fat Llama",
    "position": "Co-founder & CEO",
    "connect_date": "30 Sep 2016"
  },
  {
    "firstname": "Maria",
    "lastname": "Pliushko",
    "email": "",
    "company": "Hyperion Tech",
    "position": "Marketing Executive",
    "connect_date": "23 Sep 2016"
  },
  {
    "firstname": "David",
    "lastname": "Wiltshire",
    "email": "",
    "company": "Bottletop",
    "position": "Director",
    "connect_date": "15 Sep 2016"
  },
  {
    "firstname": "☁️ Greg",
    "lastname": "Kelleher ☁️",
    "email": "",
    "company": "Cambridge Software Cooperative",
    "position": "Co-Founder",
    "connect_date": "14 Sep 2016"
  },
  {
    "firstname": "Sergii",
    "lastname": "Karabovych",
    "email": "",
    "company": "Asseco Data Systems",
    "position": "Młodszy operator",
    "connect_date": "14 Sep 2016"
  },
  {
    "firstname": "Yurii",
    "lastname": "Ivashyn",
    "email": "",
    "company": "Upwork",
    "position": "Frontend Developer",
    "connect_date": "08 Sep 2016"
  },
  {
    "firstname": "Александр",
    "lastname": "Истомин",
    "email": "",
    "company": "AverAlexTeam",
    "position": "Full Stack Developer",
    "connect_date": "08 Sep 2016"
  },
  {
    "firstname": "Caressa",
    "lastname": "Moy",
    "email": "",
    "company": "Chase Technology Consultants, LLC",
    "position": "Recruiting Intelligence Team Lead",
    "connect_date": "01 Sep 2016"
  },
  {
    "firstname": "Søren Peter",
    "lastname": "Nielsen",
    "email": "",
    "company": "MakerDAO",
    "position": "Head of Product",
    "connect_date": "01 Sep 2016"
  },
  {
    "firstname": "Sergey",
    "lastname": "Efremov",
    "email": "",
    "company": "Database Works Inc",
    "position": "Senior Software Quality Assurance Engineer",
    "connect_date": "31 Aug 2016"
  },
  {
    "firstname": "Виктория",
    "lastname": "Шеманаева",
    "email": "",
    "company": "La Escandella Украіна",
    "position": "Начальник отдела продаж",
    "connect_date": "25 Aug 2016"
  },
  {
    "firstname": "Yana",
    "lastname": "Rabinovich",
    "email": "",
    "company": "FL.ru (Free-lance.ru)",
    "position": "Senior 1C developer Project Manager",
    "connect_date": "22 Aug 2016"
  },
  {
    "firstname": "Victoria",
    "lastname": "Kirienko",
    "email": "",
    "company": "Onilab",
    "position": "Business Development Manager",
    "connect_date": "19 Aug 2016"
  },
  {
    "firstname": "Andrey",
    "lastname": "Usik",
    "email": "",
    "company": "ABP",
    "position": "QA Automation Engineer",
    "connect_date": "19 Aug 2016"
  },
  {
    "firstname": "Andrew",
    "lastname": "Vorobiov",
    "email": "",
    "company": "Freelance",
    "position": "Business Motivation Coach",
    "connect_date": "19 Aug 2016"
  },
  {
    "firstname": "Ira",
    "lastname": "Prokopchuk",
    "email": "",
    "company": "Strauss Group Inc.",
    "position": "Дизайн и разработка рекламных носителей для компании «Strauss Group»",
    "connect_date": "27 Jul 2016"
  },
  {
    "firstname": "Matthias",
    "lastname": "Jost",
    "email": "",
    "company": "IWB Industrielle Werke Basel",
    "position": "Microsoft Azure Cloud Engineer",
    "connect_date": "20 Jul 2016"
  },
  {
    "firstname": "Sergey",
    "lastname": "Korin",
    "email": "",
    "company": "Ruta hotels & resorts",
    "position": "Co-owner, Director",
    "connect_date": "19 Jul 2016"
  },
  {
    "firstname": "Kristina",
    "lastname": "Hambardzumyan",
    "email": "",
    "company": "PlanetVerify",
    "position": "Recruitment Consultant",
    "connect_date": "29 Jun 2016"
  },
  {
    "firstname": "Diana",
    "lastname": "Tereshchenko",
    "email": "",
    "company": "Provectus",
    "position": "Head of PR Department",
    "connect_date": "29 Jun 2016"
  },
  {
    "firstname": "Renat",
    "lastname": "Sapegin",
    "email": "",
    "company": "Mobilife+",
    "position": "Java Developer",
    "connect_date": "20 Jun 2016"
  },
  {
    "firstname": "Basil",
    "lastname": "Miller",
    "email": "",
    "company": "Wizeline",
    "position": "Android Developer",
    "connect_date": "06 Jun 2016"
  },
  {
    "firstname": "Sergey",
    "lastname": "Ivchenko",
    "email": "",
    "company": "freelance",
    "position": "Designer / front-end",
    "connect_date": "23 May 2016"
  },
  {
    "firstname": "Natalia",
    "lastname": "Datskiv",
    "email": "",
    "company": "Lviv IT Step",
    "position": "Sales and Marketing Manager",
    "connect_date": "19 May 2016"
  },
  {
    "firstname": "Nickolay",
    "lastname": "Volkov",
    "email": "",
    "company": "MIRS Corporation",
    "position": "UX Architect",
    "connect_date": "10 May 2016"
  },
  {
    "firstname": "Oleg",
    "lastname": "Kirilenko",
    "email": "",
    "company": "Overall Future",
    "position": "Co-Owner and Chief Marketing Officer at Overall Future",
    "connect_date": "03 May 2016"
  },
  {
    "firstname": "Jason",
    "lastname": "Collum",
    "email": "",
    "company": "Collum Planning",
    "position": "Financial Planner / Financial Security Advisor",
    "connect_date": "28 Apr 2016"
  },
  {
    "firstname": "Сергей",
    "lastname": "Остапчук",
    "email": "",
    "company": "Intrasystems, LLC",
    "position": "KAM",
    "connect_date": "25 Apr 2016"
  },
  {
    "firstname": "Med",
    "lastname": "Jones (Yones)",
    "email": "",
    "company": "International Institute of Management",
    "position": "President",
    "connect_date": "25 Apr 2016"
  },
  {
    "firstname": "Chander",
    "lastname": "Thakur",
    "email": "",
    "company": "APN Healthcare Solutions",
    "position": "Healthcare Recruiter",
    "connect_date": "14 Apr 2016"
  },
  {
    "firstname": "Joey",
    "lastname": "Zhou",
    "email": "",
    "company": "Cyborgenic",
    "position": "Managing Partner",
    "connect_date": "11 Apr 2016"
  },
  {
    "firstname": "Maksym",
    "lastname": "Tkachov",
    "email": "",
    "company": "Компьютерная школа Hillel (IT School)",
    "position": "Trainer",
    "connect_date": "08 Apr 2016"
  },
  {
    "firstname": "Christina",
    "lastname": "Voytseschuk",
    "email": "",
    "company": "DNA325 - We Develop and Grow IT companies",
    "position": "IT Technical Recruiter",
    "connect_date": "29 Mar 2016"
  },
  {
    "firstname": "Mark",
    "lastname": "Wynter",
    "email": "",
    "company": "deepconnect.cloud",
    "position": "CEO Co-Founder",
    "connect_date": "14 Mar 2016"
  },
  {
    "firstname": "Conor",
    "lastname": "Coffey",
    "email": "",
    "company": "Conor Coffey & Associates",
    "position": "Principal",
    "connect_date": "11 Mar 2016"
  },
  {
    "firstname": "Sebastian",
    "lastname": "Moc",
    "email": "",
    "company": "Jet Story",
    "position": "Sales Executive",
    "connect_date": "09 Mar 2016"
  },
  {
    "firstname": "Nazzareno",
    "lastname": "Gorni",
    "email": "",
    "company": "MailUp Group",
    "position": "CEO",
    "connect_date": "24 Feb 2016"
  },
  {
    "firstname": "Jesse",
    "lastname": "Tayler",
    "email": "",
    "company": "Beyond Agile The Book",
    "position": "Book Author",
    "connect_date": "18 Feb 2016"
  },
  {
    "firstname": "Zhenya",
    "lastname": "Rozinskiy",
    "email": "",
    "company": "Stealth Mode Startup",
    "position": "CEO & Co-founder",
    "connect_date": "18 Feb 2016"
  },
  {
    "firstname": "Can",
    "lastname": "Arslan",
    "email": "",
    "company": "secucloud",
    "position": "Key Account Manager",
    "connect_date": "17 Feb 2016"
  },
  {
    "firstname": "Serhiy",
    "lastname": "Zemskov",
    "email": "",
    "company": "ATB market",
    "position": "Regional Managing Director",
    "connect_date": "17 Feb 2016"
  },
  {
    "firstname": "Rabih",
    "lastname": "Boulmona",
    "email": "",
    "company": "MindGeek",
    "position": "Product Operations Manager",
    "connect_date": "16 Feb 2016"
  },
  {
    "firstname": "Elliot",
    "lastname": "Wald",
    "email": "",
    "company": "HW Search & Selection Ltd - IT Recruitment Agency",
    "position": "Director - IT Recruitment Agency",
    "connect_date": "15 Feb 2016"
  },
  {
    "firstname": "Michael",
    "lastname": "Tribolet",
    "email": "",
    "company": "YipTV, LLC.",
    "position": "Chairman, CEO and Co-Founder",
    "connect_date": "09 Feb 2016"
  },
  {
    "firstname": "Michael",
    "lastname": "Boyko",
    "email": "",
    "company": "FourSeasons",
    "position": "Racing cyclist",
    "connect_date": "09 Feb 2016"
  },
  {
    "firstname": "Таня",
    "lastname": "Степаненко",
    "email": "",
    "company": "pampik.com",
    "position": "Content manager (Middle)",
    "connect_date": "09 Feb 2016"
  },
  {
    "firstname": "Olga",
    "lastname": "Mamonova",
    "email": "",
    "company": "Turnkey Lender",
    "position": "HR Manager",
    "connect_date": "02 Feb 2016"
  },
  {
    "firstname": "Vyacheslav",
    "lastname": "Suhoy",
    "email": "",
    "company": "Petrosoft LLC",
    "position": "QA Engineer",
    "connect_date": "20 Jan 2016"
  },
  {
    "firstname": "Paul",
    "lastname": "Ryazanov",
    "email": "",
    "company": "MageCloud",
    "position": "CEO",
    "connect_date": "13 Jan 2016"
  },
  {
    "firstname": "Valeriia",
    "lastname": "Muravska",
    "email": "",
    "company": "Wirex",
    "position": "QA Engineer",
    "connect_date": "13 Jan 2016"
  },
  {
    "firstname": "Anastasiya",
    "lastname": "Markuts",
    "email": "",
    "company": "Teamvoy - Software Engineers & Consultants",
    "position": "Business Development and HR manager",
    "connect_date": "13 Jan 2016"
  },
  {
    "firstname": "Aliona",
    "lastname": "Melnik",
    "email": "",
    "company": "Soft Industry Alliance Ltd.",
    "position": "Business Development Manager",
    "connect_date": "28 Dec 2015"
  },
  {
    "firstname": "Malak",
    "lastname": "Khalil",
    "email": "",
    "company": "Object Zero",
    "position": "Founder – Lead developer",
    "connect_date": "17 Dec 2015"
  },
  {
    "firstname": "Viktoria",
    "lastname": "Levitskaya",
    "email": "",
    "company": "OSSystem",
    "position": "Sales Manager",
    "connect_date": "17 Dec 2015"
  },
  {
    "firstname": "Denis",
    "lastname": "Regeda",
    "email": "",
    "company": "Autodoc",
    "position": "QA Engineer",
    "connect_date": "11 Dec 2015"
  },
  {
    "firstname": "Yossi",
    "lastname": "Mlynsky",
    "email": "",
    "company": "Upstack.co",
    "position": "Founder",
    "connect_date": "10 Dec 2015"
  },
  {
    "firstname": "Anna",
    "lastname": "Agaltsova",
    "email": "",
    "company": "BrainCombinator",
    "position": "International Recruiter",
    "connect_date": "05 Dec 2015"
  },
  {
    "firstname": "Viktor",
    "lastname": "Serbul",
    "email": "",
    "company": "Kernel",
    "position": "Chief Instrumentation and Automation Engineer in the Division of Production Assets",
    "connect_date": "05 Dec 2015"
  },
  {
    "firstname": "Julia",
    "lastname": "Norets",
    "email": "",
    "company": "Luxoft",
    "position": "IT researcher",
    "connect_date": "30 Nov 2015"
  },
  {
    "firstname": "Kseniya",
    "lastname": "Efremenkova",
    "email": "",
    "company": "WebGladiolus",
    "position": "Business Development Manager",
    "connect_date": "27 Nov 2015"
  },
  {
    "firstname": "Markus",
    "lastname": "Levin",
    "email": "",
    "company": "XYO Network",
    "position": "Co-Founder",
    "connect_date": "27 Nov 2015"
  },
  {
    "firstname": "Anastasia",
    "lastname": "Molentor",
    "email": "",
    "company": "24option",
    "position": "Head of Kiev HR Department",
    "connect_date": "24 Nov 2015"
  },
  {
    "firstname": "Andrew",
    "lastname": "Kinyak",
    "email": "",
    "company": "Adtelligent",
    "position": "Head of Core Development",
    "connect_date": "16 Nov 2015"
  },
  {
    "firstname": "Вячеслав",
    "lastname": "Юренко",
    "email": "",
    "company": "4 Limes",
    "position": "Учредитель",
    "connect_date": "14 Nov 2015"
  },
  {
    "firstname": "Yuriy",
    "lastname": "Yurenko",
    "email": "",
    "company": "Ozolio Inc.",
    "position": "Web Software Developer",
    "connect_date": "11 Nov 2015"
  },
  {
    "firstname": "Ivan",
    "lastname": "Moiseev",
    "email": "",
    "company": "AdBakers",
    "position": "Middle PHP Developer",
    "connect_date": "05 Nov 2015"
  },
  {
    "firstname": "Alexander",
    "lastname": "Markovskiy",
    "email": "",
    "company": "EWOTAC LTD",
    "position": "Director",
    "connect_date": "04 Nov 2015"
  },
  {
    "firstname": "Inna",
    "lastname": "Klymenko",
    "email": "",
    "company": "GlobalLogic",
    "position": "Recruiter",
    "connect_date": "04 Nov 2015"
  },
  {
    "firstname": "Alexander",
    "lastname": "Ivanchenko",
    "email": "",
    "company": "Onix-Systems",
    "position": "Head of Sales",
    "connect_date": "29 Oct 2015"
  },
  {
    "firstname": "Bogdan Christian",
    "lastname": "Stanache",
    "email": "",
    "company": "KAMPIUS GROUP",
    "position": "Software Development Manager",
    "connect_date": "19 Oct 2015"
  },
  {
    "firstname": "Olga",
    "lastname": "Bilyk",
    "email": "",
    "company": "PPD",
    "position": "PA II",
    "connect_date": "16 Oct 2015"
  },
  {
    "firstname": "Yevgeniy",
    "lastname": "Orlovskiy",
    "email": "",
    "company": "European Guard Consulting",
    "position": "CBDO",
    "connect_date": "02 Oct 2015"
  },
  {
    "firstname": "Roger",
    "lastname": "Salomó",
    "email": "",
    "company": "Elymobile",
    "position": "CEO",
    "connect_date": "29 Sep 2015"
  },
  {
    "firstname": "Maria",
    "lastname": "Saifudinova",
    "email": "",
    "company": "MAD HEADS Association",
    "position": "Chief Executive Officer, Founder",
    "connect_date": "25 Sep 2015"
  },
  {
    "firstname": "Anastasia",
    "lastname": "Lymar",
    "email": "",
    "company": "OSSystem",
    "position": "Business Development Manager",
    "connect_date": "16 Sep 2015"
  },
  {
    "firstname": "Sergey",
    "lastname": "Stepanov",
    "email": "",
    "company": "SECL Group",
    "position": "UI\\UX designer",
    "connect_date": "07 Sep 2015"
  },
  {
    "firstname": "Виолета",
    "lastname": "Сидорова",
    "email": "",
    "company": "",
    "position": "",
    "connect_date": "07 Sep 2015"
  },
  {
    "firstname": "Andrey",
    "lastname": "Kuznetsov",
    "email": "",
    "company": "Marino Yoga Fest",
    "position": "Founder",
    "connect_date": "26 Aug 2015"
  },
  {
    "firstname": "Hennadii V.",
    "lastname": "Pysanka",
    "email": "",
    "company": "O2IT: Outsourcing2IT",
    "position": "Co-founder, CEO",
    "connect_date": "19 Aug 2015"
  },
  {
    "firstname": "Christine",
    "lastname": "Nebeker",
    "email": "",
    "company": "Marriott International",
    "position": "Partner Relations Specialist",
    "connect_date": "19 Aug 2015"
  },
  {
    "firstname": "Alexandr",
    "lastname": "Korenevich",
    "email": "",
    "company": "IMPA",
    "position": "Руководитель представительства",
    "connect_date": "12 Aug 2015"
  },
  {
    "firstname": "Dasha",
    "lastname": "Boieva",
    "email": "",
    "company": "Intellias",
    "position": "Recruiter",
    "connect_date": "10 Aug 2015"
  },
  {
    "firstname": "Olha",
    "lastname": "Vovchenko",
    "email": "",
    "company": "AnyDev Recruiting Agency",
    "position": "IT Recruiter",
    "connect_date": "21 Jul 2015"
  },
  {
    "firstname": "Igor",
    "lastname": "Glynianyi",
    "email": "",
    "company": "SKILLERS (ex Venbest Recruiting)",
    "position": "Director Of Recruitment, Operations And Business Development",
    "connect_date": "13 Jul 2015"
  },
  {
    "firstname": "Valentyn",
    "lastname": "Dudinov",
    "email": "",
    "company": "PLS Logistics Services",
    "position": "Oracle Developer",
    "connect_date": "11 Jun 2015"
  },
  {
    "firstname": "Mari",
    "lastname": "Turchina",
    "email": "",
    "company": "UKLON",
    "position": "HR manager",
    "connect_date": "06 Jun 2015"
  },
  {
    "firstname": "Taisiia",
    "lastname": "Mliuzan",
    "email": "",
    "company": "CNA International IT Recruitment",
    "position": "Managing Partner",
    "connect_date": "03 Jun 2015"
  },
  {
    "firstname": "Nicolás",
    "lastname": "Andrade",
    "email": "",
    "company": "SSENSE",
    "position": "Senior Software Developer",
    "connect_date": "28 May 2015"
  },
  {
    "firstname": "Annie",
    "lastname": "Bucklin, PM-LPC, SSBB, CSM, CMS",
    "email": "",
    "company": "ITER8, LLC Digital Project Management",
    "position": "CEO and Founder",
    "connect_date": "27 May 2015"
  },
  {
    "firstname": "Dr. Hakan Mutlu",
    "lastname": "Sonmez",
    "email": "",
    "company": "Insider.",
    "position": "Country Director & Product Manager",
    "connect_date": "19 May 2015"
  },
  {
    "firstname": "Yuliia",
    "lastname": "Honcharenko",
    "email": "",
    "company": "Action Contre la Faim, Bangladesh mission",
    "position": "Head of Human Recourse Department",
    "connect_date": "17 May 2015"
  },
  {
    "firstname": "Kate",
    "lastname": "Kurnevich",
    "email": "",
    "company": "Clickky",
    "position": "Strategic Partnerships Executive",
    "connect_date": "04 May 2015"
  },
  {
    "firstname": "Ihor",
    "lastname": "Gevorkyan",
    "email": "",
    "company": "Wirex",
    "position": "Senior Front End developer",
    "connect_date": "20 Apr 2015"
  },
  {
    "firstname": "David",
    "lastname": "Johnson",
    "email": "",
    "company": "Aflac",
    "position": "Regional Broker Coordinator",
    "connect_date": "07 Apr 2015"
  },
  {
    "firstname": "Borodin",
    "lastname": "Alexandr",
    "email": "",
    "company": "Diasoft",
    "position": "Department manager",
    "connect_date": "06 Apr 2015"
  },
  {
    "firstname": "Vladimir",
    "lastname": "Popik",
    "email": "",
    "company": "AutoDoc",
    "position": "PHP developer",
    "connect_date": "02 Apr 2015"
  },
  {
    "firstname": "Alexander",
    "lastname": "Soloviev",
    "email": "",
    "company": "Wildix",
    "position": "unix system administrator",
    "connect_date": "01 Apr 2015"
  },
  {
    "firstname": "Ann",
    "lastname": "Sokolova",
    "email": "",
    "company": "Envion Software",
    "position": "Full Stack Developer",
    "connect_date": "01 Apr 2015"
  },
  {
    "firstname": "Yaroslav",
    "lastname": "Pryvalikhin",
    "email": "",
    "company": "Intellias",
    "position": "DevOps Engineer",
    "connect_date": "30 Mar 2015"
  },
  {
    "firstname": "Ирина",
    "lastname": "Цветкова",
    "email": "",
    "company": "ОАО \"МТС-Банк\"",
    "position": "Бухгалтер",
    "connect_date": "19 Mar 2015"
  },
  {
    "firstname": "Laurie",
    "lastname": "Anderson",
    "email": "",
    "company": "MATRIX Resources",
    "position": "Sr. Technical Recruiter",
    "connect_date": "12 Mar 2015"
  },
  {
    "firstname": "Timofey",
    "lastname": "Yashchenko",
    "email": "",
    "company": "Green Cargo Shipping",
    "position": "sales, logistic manager",
    "connect_date": "27 Feb 2015"
  },
  {
    "firstname": "Igor",
    "lastname": "Glushchenko",
    "email": "",
    "company": "KnubiSoft",
    "position": "Business Development Manager",
    "connect_date": "23 Feb 2015"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Shulha",
    "email": "",
    "company": "EPAM Systems",
    "position": "Android Developer",
    "connect_date": "23 Feb 2015"
  },
  {
    "firstname": "Shiva",
    "lastname": "Shankar",
    "email": "",
    "company": "Techdemocracy LLC",
    "position": "Sr Technical Recruiter",
    "connect_date": "23 Feb 2015"
  },
  {
    "firstname": "Maksim",
    "lastname": "Mysak",
    "email": "",
    "company": "Creative Webmedia Pvt.Ltd.",
    "position": "Full-stack software developer",
    "connect_date": "23 Feb 2015"
  },
  {
    "firstname": "Alex",
    "lastname": "Rogalskyi",
    "email": "",
    "company": "DB2 Limited",
    "position": "Chief Operating Officer",
    "connect_date": "15 Feb 2015"
  },
  {
    "firstname": "Serhii",
    "lastname": "Tsykalovskyi",
    "email": "",
    "company": "Домен-Хостинг",
    "position": "PHP developer",
    "connect_date": "12 Feb 2015"
  },
  {
    "firstname": "OLEG",
    "lastname": "KOT",
    "email": "",
    "company": "MIO group",
    "position": "import manager",
    "connect_date": "12 Feb 2015"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Juny",
    "email": "",
    "company": "Design studio Dmitry Juny",
    "position": "Арт-директор",
    "connect_date": "09 Feb 2015"
  },
  {
    "firstname": "Mila",
    "lastname": "Lancova",
    "email": "",
    "company": "Penna",
    "position": "Recruitment Campaign Coordinator",
    "connect_date": "02 Feb 2015"
  },
  {
    "firstname": "Chris",
    "lastname": "Wessell",
    "email": "",
    "company": "Pinnacle Recruiting & Staffing LLC",
    "position": "Partner & Co-Founder",
    "connect_date": "02 Feb 2015"
  },
  {
    "firstname": "Sergei",
    "lastname": "Artimenia",
    "email": "",
    "company": "Gersis Software",
    "position": "CEO",
    "connect_date": "29 Jan 2015"
  },
  {
    "firstname": "Victoria",
    "lastname": "Nepota",
    "email": "",
    "company": "Playtini",
    "position": "Front End Developer",
    "connect_date": "29 Jan 2015"
  },
  {
    "firstname": "Eugene",
    "lastname": "Yavaev",
    "email": "",
    "company": "Starbuildr",
    "position": "Elixir-developer",
    "connect_date": "27 Jan 2015"
  },
  {
    "firstname": "Daniel Podgaichenko",
    "lastname": "Montreal SEO Expert",
    "email": "",
    "company": "MindGeek",
    "position": "Senior SEO Specialist Montreal",
    "connect_date": "22 Jan 2015"
  },
  {
    "firstname": "Marina",
    "lastname": "Korolevich",
    "email": "",
    "company": "Tapgerine",
    "position": "Senior Affiliate Manager",
    "connect_date": "16 Jan 2015"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Gudko",
    "email": "",
    "company": "Wargaming",
    "position": "Project Manager",
    "connect_date": "13 Jan 2015"
  },
  {
    "firstname": "Lili",
    "lastname": "Safarova-Gates",
    "email": "",
    "company": "Large WebSite Design",
    "position": "Manager",
    "connect_date": "06 Jan 2015"
  },
  {
    "firstname": "John",
    "lastname": "Harris",
    "email": "",
    "company": "Enjoy English  www.enjoy-english.com.ua",
    "position": "Owner",
    "connect_date": "18 Dec 2014"
  },
  {
    "firstname": "Stefano",
    "lastname": "Ferreri",
    "email": "",
    "company": "New System Telephone S.r.l.",
    "position": "Responsabile Tecnico",
    "connect_date": "18 Dec 2014"
  },
  {
    "firstname": "John",
    "lastname": "HARRIS",
    "email": "",
    "company": "Easy English Ukraine",
    "position": "Owner",
    "connect_date": "02 Dec 2014"
  },
  {
    "firstname": "Andrey",
    "lastname": "Solomenniy",
    "email": "",
    "company": "FreeLance",
    "position": "Developer",
    "connect_date": "30 Nov 2014"
  },
  {
    "firstname": "Maxim",
    "lastname": "Shiryaev",
    "email": "",
    "company": "BINTIME",
    "position": "Senior PHP Developer",
    "connect_date": "31 Oct 2014"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Bugera",
    "email": "",
    "company": "SLC Praha",
    "position": "Project Manager",
    "connect_date": "20 Oct 2014"
  },
  {
    "firstname": "Оксана",
    "lastname": "Парфёнова",
    "email": "",
    "company": "ООО \"Мед Лайф\"",
    "position": "Главный бухгалтер",
    "connect_date": "16 Oct 2014"
  },
  {
    "firstname": "Vyacheslav",
    "lastname": "Volovyk",
    "email": "",
    "company": "Organizational design and structure reengineering",
    "position": "Freelancer",
    "connect_date": "01 Oct 2014"
  },
  {
    "firstname": "Karina",
    "lastname": "Karpenko",
    "email": "",
    "company": "1988 Company",
    "position": "Head Of Recruitment",
    "connect_date": "30 Sep 2014"
  },
  {
    "firstname": "Alexander",
    "lastname": "Solomentsev",
    "email": "",
    "company": "Luxoft",
    "position": "Senior Test Lead",
    "connect_date": "18 Sep 2014"
  },
  {
    "firstname": "Serhii",
    "lastname": "Titok",
    "email": "",
    "company": "HUBBER.PRO",
    "position": "VP of Business Development",
    "connect_date": "27 Aug 2014"
  },
  {
    "firstname": "Kirsten",
    "lastname": "Braun",
    "email": "",
    "company": "AT&T",
    "position": "HR Consultant",
    "connect_date": "21 Aug 2014"
  },
  {
    "firstname": "Denys",
    "lastname": "Burenok",
    "email": "",
    "company": "KnubiSoft",
    "position": "Founder & CEO",
    "connect_date": "21 Aug 2014"
  },
  {
    "firstname": "Dmytro",
    "lastname": "Goncharenko",
    "email": "",
    "company": "DataDrivenDecisions.Guru",
    "position": "Head of Conversion Rate Optimization at",
    "connect_date": "18 Aug 2014"
  },
  {
    "firstname": "Vitaliy",
    "lastname": "Kushnirenko",
    "email": "",
    "company": "Netcracker Technology",
    "position": "Team Lead",
    "connect_date": "13 Aug 2014"
  },
  {
    "firstname": "Yanina",
    "lastname": "Shram",
    "email": "",
    "company": "Fre Lance",
    "position": "Ex/R in IT | IT Recruiter",
    "connect_date": "08 Aug 2014"
  },
  {
    "firstname": "Aнтoн",
    "lastname": "C.",
    "email": "",
    "company": "ICOadm.in",
    "position": "Product Owner",
    "connect_date": "07 Aug 2014"
  },
  {
    "firstname": "Lucas",
    "lastname": "DeMaio",
    "email": "",
    "company": "WikiGrads",
    "position": "Founder",
    "connect_date": "06 Aug 2014"
  },
  {
    "firstname": "Антон",
    "lastname": "Чехов",
    "email": "",
    "company": "Роскосмос",
    "position": "Системный архитектор, руководитель проектов",
    "connect_date": "04 Aug 2014"
  },
  {
    "firstname": "Mathieu",
    "lastname": "Landry",
    "email": "",
    "company": "Le Blogue du Québec",
    "position": "Fondateur",
    "connect_date": "04 Aug 2014"
  },
  {
    "firstname": "Victor",
    "lastname": "Svistunov",
    "email": "",
    "company": "Grid Dynamics",
    "position": "Engineering Manager for Big Data",
    "connect_date": "26 Jul 2014"
  },
  {
    "firstname": "John",
    "lastname": "Ioannides",
    "email": "",
    "company": "GeoLogismiki",
    "position": "Owner",
    "connect_date": "09 Jul 2014"
  },
  {
    "firstname": "Светлана",
    "lastname": "Орманжи",
    "email": "",
    "company": "OSSystem",
    "position": "технический писатель, QA engineer, hr менеджер",
    "connect_date": "20 Jun 2014"
  },
  {
    "firstname": "Viacheslav",
    "lastname": "Ustimenko",
    "email": "",
    "company": "Self-Employed",
    "position": "International Lawyer",
    "connect_date": "16 Jun 2014"
  },
  {
    "firstname": "Edgar J.",
    "lastname": "Glen",
    "email": "",
    "company": "ALLDATA",
    "position": "Business Solutions Manager",
    "connect_date": "11 Jun 2014"
  },
  {
    "firstname": "Julia",
    "lastname": "Shtokalo",
    "email": "",
    "company": "IT HR service",
    "position": "Founder",
    "connect_date": "29 May 2014"
  },
  {
    "firstname": "Sr",
    "lastname": "Analyst",
    "email": "",
    "company": "IBIS",
    "position": "Sr. Analyst",
    "connect_date": "21 Apr 2014"
  },
  {
    "firstname": "Kristina",
    "lastname": "Piltyay",
    "email": "",
    "company": "XOresearch",
    "position": "Business Development Manager",
    "connect_date": "08 Apr 2014"
  },
  {
    "firstname": "Tanya",
    "lastname": "Potorzhinskaya",
    "email": "",
    "company": "Best IT Staff",
    "position": "Owner, recruitment agency",
    "connect_date": "08 Apr 2014"
  },
  {
    "firstname": "Светлана",
    "lastname": "Кудря",
    "email": "",
    "company": "DataArt",
    "position": "QA Engineer",
    "connect_date": "07 Apr 2014"
  },
  {
    "firstname": "Steve",
    "lastname": "Osler",
    "email": "",
    "company": "Wildix GmbH",
    "position": "Co-Founder",
    "connect_date": "31 Mar 2014"
  },
  {
    "firstname": "Tyler",
    "lastname": "Monks",
    "email": "",
    "company": "Dell EMC",
    "position": "Senior Analyst, Service Delivery",
    "connect_date": "26 Mar 2014"
  },
  {
    "firstname": "George Christine",
    "lastname": "Janga",
    "email": "",
    "company": "Synchrony Financial",
    "position": "Sr. Representative, Skip Agent",
    "connect_date": "21 Mar 2014"
  },
  {
    "firstname": "David",
    "lastname": "Snodgrass",
    "email": "",
    "company": "Webnovas Technologies Inc",
    "position": "Chief Coffee-Getter",
    "connect_date": "09 Mar 2014"
  },
  {
    "firstname": "swapna",
    "lastname": "priya",
    "email": "",
    "company": "Radical IT Solutions Pvt Ltd",
    "position": "CEO Radical It Solutions Pvt Ltd",
    "connect_date": "06 Mar 2014"
  },
  {
    "firstname": "Sebastien",
    "lastname": "CHOTARD",
    "email": "",
    "company": "ALLO JOE",
    "position": "CEO & Co-Founder",
    "connect_date": "17 Feb 2014"
  },
  {
    "firstname": "Татьяна",
    "lastname": "Большакова",
    "email": "",
    "company": "ООО \"НеваТранс\"",
    "position": "ген.директор",
    "connect_date": "13 Feb 2014"
  },
  {
    "firstname": "Вячеслав",
    "lastname": "ООО Авангард",
    "email": "",
    "company": "Магнат Недвижимость",
    "position": "руководитель",
    "connect_date": "13 Feb 2014"
  },
  {
    "firstname": "Anatoliy",
    "lastname": "Talismanov",
    "email": "",
    "company": "Golden Choice",
    "position": "Co-founder & CEO",
    "connect_date": "12 Feb 2014"
  },
  {
    "firstname": "Alessandro",
    "lastname": "Kozyreff",
    "email": "",
    "company": "Ollsent",
    "position": "CEO & Founder",
    "connect_date": "06 Feb 2014"
  },
  {
    "firstname": "Julia",
    "lastname": "Yarosh",
    "email": "",
    "company": "OS Consulting",
    "position": "Commercial Director",
    "connect_date": "27 Jan 2014"
  },
  {
    "firstname": "Дмитрий",
    "lastname": "Филатов",
    "email": "",
    "company": "Digitally Inspired Ltd",
    "position": "Frontend Engineer",
    "connect_date": "27 Jan 2014"
  },
  {
    "firstname": "Sergey",
    "lastname": "Semko",
    "email": "",
    "company": "OSSystem",
    "position": "Team Leader",
    "connect_date": "27 Jan 2014"
  },
  {
    "firstname": "Natalia",
    "lastname": "Barth",
    "email": "",
    "company": "Templar Shield",
    "position": "Risk Analyst",
    "connect_date": "21 Jan 2014"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Amirov",
    "email": "",
    "company": "InfiNet Wireless",
    "position": "Deputy CTO",
    "connect_date": "21 Jan 2014"
  },
  {
    "firstname": "Tarek",
    "lastname": "Khalifa",
    "email": "",
    "company": "Reitmans Canada Ltée/Ltd",
    "position": "Manager eCommerce Web Development",
    "connect_date": "16 Jan 2014"
  },
  {
    "firstname": "Татьяна",
    "lastname": "Мартынюк",
    "email": "",
    "company": "Instagram",
    "position": "smm",
    "connect_date": "14 Jan 2014"
  },
  {
    "firstname": "Konstantin",
    "lastname": "Foksha",
    "email": "",
    "company": "Workrocks",
    "position": "CEO",
    "connect_date": "14 Jan 2014"
  },
  {
    "firstname": "Julia",
    "lastname": "Sakhno",
    "email": "",
    "company": "Sintez Technologies",
    "position": "Technical Support Operator",
    "connect_date": "04 Jan 2014"
  },
  {
    "firstname": "Adam",
    "lastname": "Avery",
    "email": "",
    "company": "Society Invite",
    "position": "Affiliate Manager",
    "connect_date": "31 Dec 2013"
  },
  {
    "firstname": "Dario",
    "lastname": "D'Aversa",
    "email": "",
    "company": "SSENSE",
    "position": "Senior Web Developer",
    "connect_date": "30 Dec 2013"
  },
  {
    "firstname": "Александр",
    "lastname": "Клепов",
    "email": "",
    "company": "ОАО НИИАС",
    "position": "Начальник отдела функциональных решений",
    "connect_date": "29 Dec 2013"
  },
  {
    "firstname": "Erdem",
    "lastname": "Iscen",
    "email": "",
    "company": "Air Canada",
    "position": "Manager, Cyber Threat Intelligence and Vulnerability Management",
    "connect_date": "24 Dec 2013"
  },
  {
    "firstname": "Lee",
    "lastname": "O'Dwyer",
    "email": "",
    "company": "Alcumus",
    "position": "Sales Manager - Monitoring Services Alcumus Sypol",
    "connect_date": "29 Nov 2013"
  },
  {
    "firstname": "Fathhi",
    "lastname": "Mohamed",
    "email": "",
    "company": "Yoho Bed",
    "position": "Fun Continues",
    "connect_date": "25 Nov 2013"
  },
  {
    "firstname": "Tatyana",
    "lastname": "Chenskaya",
    "email": "",
    "company": "Fartskriver Solutions AS",
    "position": "Project Coordinator",
    "connect_date": "22 Nov 2013"
  },
  {
    "firstname": "Konstantin",
    "lastname": "Kostritsa",
    "email": "",
    "company": "Lattelecom",
    "position": "Partnership and Key Account Manager for Datacentre Services, Ukraine",
    "connect_date": "12 Nov 2013"
  },
  {
    "firstname": "Oleksandr",
    "lastname": "Topuzanov",
    "email": "",
    "company": "Digitally Inspired Ltd",
    "position": "OBIEE developer",
    "connect_date": "11 Nov 2013"
  },
  {
    "firstname": "Tatyana",
    "lastname": "Pravorskaya",
    "email": "",
    "company": "LabsTech",
    "position": "Head of IT Recruitment",
    "connect_date": "07 Nov 2013"
  },
  {
    "firstname": "John",
    "lastname": "Ashton",
    "email": "",
    "company": "MindGeek",
    "position": "Lead Software Developer",
    "connect_date": "06 Nov 2013"
  },
  {
    "firstname": "Michelle",
    "lastname": "Lofgren",
    "email": "",
    "company": "MindGeek",
    "position": "HR Business Partner",
    "connect_date": "04 Nov 2013"
  },
  {
    "firstname": "Alexander",
    "lastname": "Panasiuk",
    "email": "",
    "company": "NetCracker",
    "position": "Software Engineer",
    "connect_date": "03 Nov 2013"
  },
  {
    "firstname": "Kateryna",
    "lastname": "Maslova",
    "email": "",
    "company": "Dynamix S.A",
    "position": "Business Development  Manager",
    "connect_date": "31 Oct 2013"
  },
  {
    "firstname": "Andrey",
    "lastname": "Grechko",
    "email": "",
    "company": "Comodo",
    "position": "Head of Comodo Accounts Management Platform Development",
    "connect_date": "28 Oct 2013"
  },
  {
    "firstname": "Kateryna",
    "lastname": "Hubaryeva",
    "email": "",
    "company": "Luxoft",
    "position": "Global HR Director",
    "connect_date": "25 Oct 2013"
  },
  {
    "firstname": "Anna",
    "lastname": "Zhivotinskaya",
    "email": "",
    "company": "OSSystem",
    "position": "HR Manager",
    "connect_date": "23 Oct 2013"
  },
  {
    "firstname": "Юлия",
    "lastname": "Чирва",
    "email": "",
    "company": "LLC \"Apeiron System\"",
    "position": "Recruiter",
    "connect_date": "23 Oct 2013"
  },
  {
    "firstname": "Alexander",
    "lastname": "Lazarev",
    "email": "",
    "company": "Lamarin LTD",
    "position": "Ceo",
    "connect_date": "22 Oct 2013"
  },
  {
    "firstname": "Peter",
    "lastname": "Giannopoulos",
    "email": "",
    "company": "OtolaneSoft Corporation",
    "position": "Director of Research and Development / Chief Architect",
    "connect_date": "18 Oct 2013"
  },
  {
    "firstname": "Dimitri",
    "lastname": "Osler",
    "email": "",
    "company": "Wildix srl",
    "position": "CTO",
    "connect_date": "18 Oct 2013"
  },
  {
    "firstname": "Konstantin",
    "lastname": "Kirilyonkov",
    "email": "",
    "company": "Lohika",
    "position": "Java Engineer",
    "connect_date": "21 Sep 2013"
  },
  {
    "firstname": "Андрей",
    "lastname": "Кравец",
    "email": "",
    "company": "",
    "position": "руководитель проекта",
    "connect_date": "19 Sep 2013"
  },
  {
    "firstname": "Alexander",
    "lastname": "Pokidin",
    "email": "",
    "company": "OSSystem",
    "position": "Web Developer",
    "connect_date": "04 Sep 2013"
  },
  {
    "firstname": "Sam",
    "lastname": "Chughtai",
    "email": "",
    "company": "Stealth",
    "position": "Board Of Directors",
    "connect_date": "02 Sep 2013"
  },
  {
    "firstname": "Alexander",
    "lastname": "Evterev",
    "email": "",
    "company": "SAP",
    "position": "Senior Service Account Executive",
    "connect_date": "29 Aug 2013"
  },
  {
    "firstname": "Alla",
    "lastname": "Serebriakova",
    "email": "",
    "company": "NetCracker",
    "position": "HR Business Partner",
    "connect_date": "28 Aug 2013"
  },
  {
    "firstname": "Simon",
    "lastname": "Ashley",
    "email": "",
    "company": "Kingfisher Digital, part of Kingfisher Plc",
    "position": "Delivery Lead (Contract)",
    "connect_date": "22 Aug 2013"
  },
  {
    "firstname": "Vera",
    "lastname": "Galaktionova",
    "email": "",
    "company": "Ringostat",
    "position": "UI/UX Designer",
    "connect_date": "20 Aug 2013"
  },
  {
    "firstname": "Anna",
    "lastname": "Starodub",
    "email": "",
    "company": "Ciklum",
    "position": "Senior HR People Partner / HRBP",
    "connect_date": "19 Aug 2013"
  },
  {
    "firstname": "Vladimir",
    "lastname": "Kovalchuk",
    "email": "",
    "company": "Ninja Geeks Sp. z o.o.",
    "position": "PHP Developer",
    "connect_date": "19 Aug 2013"
  },
  {
    "firstname": "Victor",
    "lastname": "Oberemok",
    "email": "",
    "company": "",
    "position": "Owner",
    "connect_date": "04 Aug 2013"
  },
  {
    "firstname": "Anastasia",
    "lastname": "Zhavoronkina",
    "email": "",
    "company": "Self-Employed",
    "position": "HR People Partner/Agile HR",
    "connect_date": "25 Jul 2013"
  },
  {
    "firstname": "Naiem",
    "lastname": "A.",
    "email": "",
    "company": "Mindgeek",
    "position": "Technical Director of Development",
    "connect_date": "22 Jul 2013"
  },
  {
    "firstname": "Sam",
    "lastname": "Bridge",
    "email": "",
    "company": "InRhythm",
    "position": "Senior Technical Recruiter",
    "connect_date": "17 Jul 2013"
  },
  {
    "firstname": "Lora",
    "lastname": "Paslova",
    "email": "",
    "company": "RingCentral",
    "position": "Sr.Manager of Odessa Team",
    "connect_date": "04 Jun 2013"
  },
  {
    "firstname": "Erinn",
    "lastname": "Murray",
    "email": "",
    "company": "Meiden America",
    "position": "Marketer",
    "connect_date": "30 May 2013"
  },
  {
    "firstname": "Andrey",
    "lastname": "Rabeshko",
    "email": "",
    "company": "",
    "position": "Web Developer",
    "connect_date": "07 May 2013"
  },
  {
    "firstname": "Julie",
    "lastname": "Wilson",
    "email": "",
    "company": "DFO Global Performance Commerce",
    "position": "Affiliate Operations Coordinator",
    "connect_date": "17 Apr 2013"
  },
  {
    "firstname": "Nataliia",
    "lastname": "Saranskova (Talalay)",
    "email": "",
    "company": "EIS Group",
    "position": "HR Director, Ukraine & Belarus",
    "connect_date": "17 Apr 2013"
  },
  {
    "firstname": "Sergey",
    "lastname": "Andreev",
    "email": "",
    "company": "Provectus / Men's Wearhouse",
    "position": "Senior QA (Outsourcing R&D team for Men's Wearhouse)",
    "connect_date": "13 Apr 2013"
  },
  {
    "firstname": "Nikolaj",
    "lastname": "Mankov",
    "email": "",
    "company": "PriceSquid ltd",
    "position": "Founder and CEO",
    "connect_date": "13 Apr 2013"
  },
  {
    "firstname": "Tomasz",
    "lastname": "Rakowski",
    "email": "",
    "company": "Index Exchange",
    "position": "Senior Architect",
    "connect_date": "12 Apr 2013"
  },
  {
    "firstname": "Ibrahim",
    "lastname": "Sabbagh",
    "email": "",
    "company": "MindGeek",
    "position": "Director of Software Engineering",
    "connect_date": "04 Apr 2013"
  },
  {
    "firstname": "Aaron",
    "lastname": "Gunton",
    "email": "",
    "company": "Huntress",
    "position": "Senior IT Recruiter / Senior IT Consultant - Specialising in Development, Test, Design and DevOps",
    "connect_date": "03 Apr 2013"
  },
  {
    "firstname": "Taras",
    "lastname": "Remez",
    "email": "",
    "company": "LifeStreet Corporation",
    "position": "Senior JavaScript Developer",
    "connect_date": "28 Mar 2013"
  },
  {
    "firstname": "Yulia",
    "lastname": "Venger",
    "email": "",
    "company": "GUID, IT Recruitment agency",
    "position": "CEO&Founder",
    "connect_date": "25 Mar 2013"
  },
  {
    "firstname": "Sergii",
    "lastname": "Vasalatii",
    "email": "",
    "company": "Do IT Programming Solutions LP",
    "position": "Technical Writer",
    "connect_date": "21 Mar 2013"
  },
  {
    "firstname": "Chris",
    "lastname": "Joubert",
    "email": "",
    "company": "ITCO Solutions, Inc.",
    "position": "Workforce Solutions Manager",
    "connect_date": "16 Mar 2013"
  },
  {
    "firstname": "Anna",
    "lastname": "Komar",
    "email": "",
    "company": "Luxoft",
    "position": "Middle Java Developer",
    "connect_date": "13 Mar 2013"
  },
  {
    "firstname": "Stanislav",
    "lastname": "Enjeevsky",
    "email": "",
    "company": "ABP",
    "position": "ASP.NET C# programmer",
    "connect_date": "13 Mar 2013"
  },
  {
    "firstname": "Sai Ram",
    "lastname": "Maalin",
    "email": "",
    "company": "Genesis Online Research & Technology Services",
    "position": "HR Recruiter",
    "connect_date": "26 Feb 2013"
  },
  {
    "firstname": "Mindo",
    "lastname": "Silalahi",
    "email": "",
    "company": "Rapid Development",
    "position": "Lead Software Engineer",
    "connect_date": "16 Feb 2013"
  },
  {
    "firstname": "Gaynor",
    "lastname": "Franklin MCIPD",
    "email": "",
    "company": "Apex Group Ltd",
    "position": "HR Business Partner",
    "connect_date": "13 Feb 2013"
  },
  {
    "firstname": "BSK",
    "lastname": "LION II K Connections",
    "email": "",
    "company": "United Software Group Inc",
    "position": "Business Development Executive",
    "connect_date": "12 Feb 2013"
  },
  {
    "firstname": "Nickolay",
    "lastname": "Komar",
    "email": "",
    "company": "iSoftBet",
    "position": "DBD/DBA",
    "connect_date": "07 Feb 2013"
  },
  {
    "firstname": "Oleh",
    "lastname": "Khavruk",
    "email": "",
    "company": "Транс-Медіа, ТОВ",
    "position": "Директор",
    "connect_date": "06 Feb 2013"
  },
  {
    "firstname": "Ephrem",
    "lastname": "Saour",
    "email": "",
    "company": "SSENSE",
    "position": "Technical Director",
    "connect_date": "31 Jan 2013"
  },
  {
    "firstname": "Haytham",
    "lastname": "Ibrahim",
    "email": "",
    "company": "Amadeus IT Group",
    "position": "Infrastructure Specialist (devops)",
    "connect_date": "28 Jan 2013"
  },
  {
    "firstname": "Dani",
    "lastname": "Dabbous",
    "email": "",
    "company": "Capgemini",
    "position": "Consultant",
    "connect_date": "18 Jan 2013"
  },
  {
    "firstname": "Juri",
    "lastname": "Krehovetski",
    "email": "",
    "company": "AMP Credit Technologies",
    "position": "Regional Sales Manager, EMEA",
    "connect_date": "18 Jan 2013"
  },
  {
    "firstname": "Valentyn",
    "lastname": "Diduryk",
    "email": "",
    "company": "OSSystem",
    "position": "Developer",
    "connect_date": "03 Jan 2013"
  },
  {
    "firstname": "Maryna",
    "lastname": "Mazukh",
    "email": "",
    "company": "Qardio, Inc.",
    "position": "QA Engineer",
    "connect_date": "22 Nov 2012"
  },
  {
    "firstname": "Ray",
    "lastname": "Barber",
    "email": "",
    "company": "GeekSuit, LLC",
    "position": "CEO",
    "connect_date": "10 Nov 2012"
  },
  {
    "firstname": "Alexandr",
    "lastname": "Dovgopol",
    "email": "",
    "company": "Pockettour",
    "position": "Owner & CEO",
    "connect_date": "08 Nov 2012"
  },
  {
    "firstname": "Sergii",
    "lastname": "Shorkin",
    "email": "",
    "company": "Singularika",
    "position": "MEAN-stack developer",
    "connect_date": "01 Nov 2012"
  },
  {
    "firstname": "Slava",
    "lastname": "Ledenev",
    "email": "",
    "company": "MLSDev Inc.",
    "position": "Co-founder, Chief Business Development Officer",
    "connect_date": "12 Oct 2012"
  },
  {
    "firstname": "Yana",
    "lastname": "Karmanova",
    "email": "",
    "company": "Freelance",
    "position": "Freelance",
    "connect_date": "26 Sep 2012"
  },
  {
    "firstname": "Дмитрий",
    "lastname": "Низовцов",
    "email": "",
    "company": "MobiTeam",
    "position": "Коммерческий директор",
    "connect_date": "18 Sep 2012"
  },
  {
    "firstname": "Igor",
    "lastname": "Udovika",
    "email": "",
    "company": "Lohika",
    "position": "JavaScript/Ruby on the Rails developer, Rocket Fuel Inc project",
    "connect_date": "12 Sep 2012"
  },
  {
    "firstname": "Semyen",
    "lastname": "P",
    "email": "",
    "company": "Scopic Software",
    "position": "Senior PHP Developer",
    "connect_date": "09 Sep 2012"
  },
  {
    "firstname": "Dmitry",
    "lastname": "Shkurat",
    "email": "",
    "company": "Ortnec Services Ltd",
    "position": "CEO",
    "connect_date": "06 Sep 2012"
  },
  {
    "firstname": "Ravindra",
    "lastname": "E",
    "email": "",
    "company": "Saimarks",
    "position": "Technical Recruiter",
    "connect_date": "14 Jul 2012"
  },
  {
    "firstname": "Vikas",
    "lastname": "Gupta",
    "email": "",
    "company": "Apple",
    "position": "iPhone Program Manager",
    "connect_date": "05 Jul 2012"
  },
  {
    "firstname": "Marietta",
    "lastname": "Parsekyan",
    "email": "",
    "company": "8bit group",
    "position": "Global HR Director",
    "connect_date": "27 Jun 2012"
  },
  {
    "firstname": "Rosina",
    "lastname": "Hagopian",
    "email": "",
    "company": "Nuance Communications",
    "position": "Sr. Talent Acquisition Consultant",
    "connect_date": "19 Jun 2012"
  },
  {
    "firstname": "Naveen",
    "lastname": "Zeegmo",
    "email": "",
    "company": "Zeegmo",
    "position": "Business Development",
    "connect_date": "06 Jun 2012"
  },
  {
    "firstname": "ZZWolf",
    "lastname": "Tester",
    "email": "",
    "company": "ZZWolf",
    "position": "Tester",
    "connect_date": "21 May 2012"
  },
  {
    "firstname": "Yury",
    "lastname": "Galustov",
    "email": "",
    "company": "TimeTarget",
    "position": "Software Development Manager",
    "connect_date": "07 May 2012"
  },
  {
    "firstname": "Lana",
    "lastname": "Optimum",
    "email": "",
    "company": "Optimum Web",
    "position": "Marketing Manager",
    "connect_date": "07 May 2012"
  },
  {
    "firstname": "Zakhar",
    "lastname": "Matula",
    "email": "",
    "company": "Lviv IT! LLC",
    "position": "CEO",
    "connect_date": "29 Mar 2012"
  },
  {
    "firstname": "Alexey",
    "lastname": "Sereda",
    "email": "",
    "company": "Cartrack",
    "position": "Cartrack UA manager",
    "connect_date": "22 Mar 2012"
  },
  {
    "firstname": "Kris",
    "lastname": "Souza",
    "email": "",
    "company": "KriArt Marketing",
    "position": "SEO Executive",
    "connect_date": "06 Feb 2012"
  },
  {
    "firstname": "Jennifer",
    "lastname": "Brown",
    "email": "",
    "company": "KPMG",
    "position": "Recruitment Business Partner onsite at KPMG (via Lorien)",
    "connect_date": "12 Dec 2011"
  },
  {
    "firstname": "Svetlana",
    "lastname": "Yushchenko",
    "email": "",
    "company": "TechInsight",
    "position": "HR and IT Recruiter",
    "connect_date": "23 Nov 2011"
  },
  {
    "firstname": "Oleg",
    "lastname": "Khimich",
    "email": "",
    "company": "ProcessMaker",
    "position": "Engineering Manager",
    "connect_date": "03 Nov 2011"
  },
  {
    "firstname": "Aleksey",
    "lastname": "Olmezov",
    "email": "",
    "company": "INTERSOG",
    "position": "Senior Software developer / Database architect",
    "connect_date": "22 Sep 2011"
  },
  {
    "firstname": "Chistyakov",
    "lastname": "Denis",
    "email": "",
    "company": "Steelkiwi Inc.",
    "position": "Sr. Project Manager",
    "connect_date": "07 Sep 2011"
  },
  {
    "firstname": "Vladimir",
    "lastname": "Kravtsov",
    "email": "",
    "company": "Luxoft",
    "position": "Senior Oracle Database Developer",
    "connect_date": "07 Sep 2011"
  },
  {
    "firstname": "Ирина",
    "lastname": "Даньшина",
    "email": "",
    "company": "ООО «Агентство инноваций»",
    "position": "Руководитель дизайн-проектов, архитектор. Начальник департамента дизайна и архитектуры",
    "connect_date": "02 Aug 2011"
  },
  {
    "firstname": "Herman",
    "lastname": "Klushin",
    "email": "",
    "company": "NAX Systems",
    "position": "Developer",
    "connect_date": "15 Jul 2011"
  },
  {
    "firstname": "Viktoriya",
    "lastname": "Krut",
    "email": "",
    "company": "Alcor",
    "position": "Senior IT Recruitment Consultant",
    "connect_date": "07 Jan 2011"
  },
  {
    "firstname": "Ruslan",
    "lastname": "Baidan",
    "email": "",
    "company": "Docler Holding Luxembourg",
    "position": "PHP Developer",
    "connect_date": "22 Nov 2010"
  },
  {
    "firstname": "Alex",
    "lastname": "Pospelov",
    "email": "",
    "company": "Precise Farming Stealth Project",
    "position": "Senior Researcher",
    "connect_date": "19 Apr 2010"
  },
  {
    "firstname": "Vadim",
    "lastname": "Chernega",
    "email": "",
    "company": "DIGI117",
    "position": "President & CEO",
    "connect_date": "10 Mar 2009"
  }
]