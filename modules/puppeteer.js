const emojiLogger = require('console-emoji');
const puppeteer = require('puppeteer');
const { getCookies } = require('../Db/Cookies');
const { emojis, log } = require('./common');
const sendEmail = require('../Controllers/emailSenderController');
// const {sendMsgToAdmin} = require('../Controllers/botController');

let browser = null;

const pageScroller = async (page, pause) => {
    console.log('I am scrolling this page now )');

    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            try {
                const maxScroll = Number.MAX_SAFE_INTEGER;
                let lastScroll = 0;
                
                const interval = setInterval(() => {
                    window.scrollBy(0, 100);
                    const scrollTop = document.documentElement.scrollTop;
                    
                    if (scrollTop === maxScroll || scrollTop === lastScroll) {
                        clearInterval(interval);
                        resolve();
                    } else {
                        lastScroll = scrollTop;
                    }
                }, 100);
            } catch (err) {
                console.log(err);
                reject(err.toString());
            }
        });
    });

    if (pause) await page.waitFor(pause);
};


/**
 * @param {Puppeteer Page} page Page constant
 * @param {Exception} error Error from catch
 * @param {String} errorLocation Location of the error
 * @param {String} extraInfo More infor about the error
 * @param {String} imageName More infor about the error
 */
const errorHandler = async (page, error, errorLocation, extraInfo, imageName) => {
    const emailFields = {
        error: true,
        errorMsg: error,
        errorLocation: errorLocation,
        extraInfo: extraInfo ? extraInfo : 'No extra information'
    };

    emojiLogger(`${extraInfo}`, 'warn');
    emojiLogger(`An Error occured, Sending email to system administrator ${emojis.typing}`, 'err');

    if (page) {
        await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
        await page.waitFor(500);
    
        await uploadToDrive(imageName);
    }

    try {
        await sendEmail(emailFields);
    } catch (err) {
        await sendMsgToAdmin(`I could not send email\n${err}`);
    }
};

const closeAndWait = async (page, randNo, close) => {
    if (close) {
        await page.click('button.msg-overlay-bubble-header__control.js-msg-close');
    }
    
    log(`Waiting for ${randNo} seconds`);

    await page.waitFor(randNo);

    return;
};

const getCookie = owner => {
    return new Promise(async (res, rej) => {
        try {
            const [ { cookie } ] = await getCookies({owner});
            res(cookie);
        } catch (error) {
            rej(error);
        }
    });
};

const getBrowser = _ => {
    return new Promise(async (resolve, reject) => {
        if (!browser) {
            console.log('Opening a new browser');
            try {
                const hideBrowser = process.env.HIDE_BROWSER ? process.env.HIDE_BROWSER : 'false';
            
                if (hideBrowser === 'true') {
                    browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox','--disable-dev-shm-usage']});
                } else {
                    browser = await puppeteer.launch({
                        headless: false,
                        args: ['--disable-dev-shm-usage', '--window-size=1920,1080']
                    });
                }
                resolve(browser);
            } catch (error) {
                reject(`Error with browser ${error}`);
            }
        } else {
            console.log('Using existing browser');
            resolve(browser);
        }
    });
};

module.exports = {
    pageScroller, errorHandler, closeAndWait, getCookie, getBrowser
};
