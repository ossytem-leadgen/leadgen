#!/bin/sh
SCRIPT=$1

echo "Ready to run a script"
echo
case $SCRIPT in
    EMAIL_RES)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=9
        ;;
    MOBILE)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=12
        ;;
    REBOOT)
        pkill node
        ;;
    INBOX)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=8
        ;;
    CONNECT)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=1
        ;;
    CONNECT_STATS)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=17
        ;;
    GET_LEADS_PROFILE)
        cd /home/b.ibitoye/leadgen && node app.js SCRIPT=18
        ;;
    *)
        echo "You choose nothing, I guess you want to talk. How are you doing?"
        ;;
esac

echo
echo "Done"
