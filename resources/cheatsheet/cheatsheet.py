# ---------------------------------------------   THIS IS PYTHON SECTION ----------------------------------------------------

# THIS IS FOR WRITTING INTO FILES
# <-- To use a variable that is not a string just use the str() to convert your int or arr to a string -->
# output = soup.find_all('code')
# jobs_arr = str(output[10])

# ######    To get the search result for a technology (e.g NodeJS in USA) -> output[1]
# ######    To get the search result with a company's URL -> output[10]
# jobs_arr = str(output[10])
# job_file = open("./jobpage.txt", "w")
# with open('./jobpage.txt', 'a') as the_file:
#     the_file.write(jobs_arr)


# THIS IS FOR GETTING SPECIFIC TAGS IN BODY OF HTML
# <-- Use this to prevent LinkedIn from knowing you are executing a script. Fake browser -->
# urlopener= urllib2.build_opener()
# urlopener.addheaders = [('User-agent', 'Mozilla/5.0')]

# tag = soup.code
# tag['id'] = 'facetResultsModule'
# name_box = soup.find('h3', attrs={'id': 'ember2757'})


# USING GHROME DRIVER FORSCRAPPING (To run the code you need )
# import time
# from selenium import webdriver
# driver = webdriver.Chrome()
# driver.get(company_url)
# time.sleep(5)
# linkedin_page = driver.page_source
# soup = BeautifulSoup(linkedin_page, 'html.parser') 
# print(soup.prettify())
# time.sleep(5)
# driver.quit()
# Links: 
# https://www.reddit.com/r/learnpython/comments/48mzuu/best_way_to_scrape_public_profile_content_from , http://chromedriver.chromium.org/getting-started 


# USE THIS TO BEAUTIFY THE OUTPUT OF THE PAGE 
# print(soup.prettify())

# HTTP 999 ERROR FIX
# https://stackoverflow.com/questions/24226781/changing-user-agent-in-python-3-for-urrlib-request-urlopen

# THIS IS THE LINKED COOKIES OF ROBERT CLARK
#     let robertCookie = [ // cookie exported by google chrome plugin editthiscookie
#       {
#           "name": "JSESSIONID",
#           "value": "ajax:5486731006696808980",
#           "domain": ".www.linkedin.com",
#       },
#       {
#         "name": "_lipt",
#         "value": "CwEAAAFllTP2zwNuekujwhsfJHoqD7Jhe3NuAEkhV5joQx8bfMFEQFKWmV02Ax1oGa8OkEdipItqmsCpE7Mh20Efw-NgZ6eWvvQqzP9hJnrjnhMy_TIcQ-AgMqBYWg",
#         "domain": ".linkedin.com",
#       },
#       {
#         "name": "bcookie",
#         "value": "v=2&021c6e5c-e8b6-4f0c-85e4-8a306cc81b0f",
#         "domain": ".linkedin.com",
#       },
#       {
#         "name": "bscookie",
#         "value": "v=1&201808311539523e2d42d5-080c-4776-812c-e83078caabf0AQFov6D93iY4eNpPSGzw512a5Lb0LAYC",
#         "domain": ".www.linkedin.com",
#       },
#       {
#         "name": "lang",
#         "value": "v=2&lang=en-us",
#         "domain": ".www.linkedin.com",
#       },
#       {
#         "name": "li_at",
#         "value": "AQEDASiI27UBJ1CZAAABZZCmGOYAAAFltLKc5k0AO8X604L9zTxyvelYEOdapb9XwZCCqbKIfWqo9Bj_Acpubh_71p-Pj2Y7kQldcHbjohMd0W3Tsf4PV4DDaF3Z9DFWxC6MlCm_IWg-WykDt9f0Qvfg",
#         "domain": ".linkedin.com",
#       },
#       {
#         "name": "liap",
#         "value": "true",
#         "domain": ".linkedin.com",
#       },
#       {
#         "name": "lidc",
#         "value": "b=VB81:g=1347:u=2:i=1535806528:t=1535891395:s=AQFR4cOjjZjxJIfkjCwvcNQUNJkF3xRY",
#         "domain": ".linkedin.com",
#       },
#       {
#         "name": "spectroscopyId",
#         "value": "5984edfe-eb4a-409d-81e9-204916ab0eb9",
#         "domain": ".www.linkedin.com",
#       },
#       {
#         "name": "sl",
#         "value": "v=1&3VVi6",
#         "domain": ".www.linkedin.com",
#       },
#       {
#         "name": "visit",
#         "value": "v=1&G",
#         "domain": "www.linkedin.com",
#       },
#   ];

# ---------------------------------------------   THIS IS NODEJS SECTION ----------------------------------------------------


#    THIS BLOCK IS FOR SIGNING IN 
#  await page.goto(signin, {waitUntil: 'networkidle2'});
#  await page.waitFor(8593);
#  await page.type('#session_key-login', process.env.LINKEDIN_USER)
#  await page.waitFor(1000);
#  await page.type('#session_password-login', process.env.LINKEDIN_PWD)
#  await page.waitFor(4000);
#  await page.click('[name="signin"]')
#  await page.waitForNavigation() */
#  await page.goto(employee, {waitUntil: 'networkidle2'});


#             MONGODB INSERT FORMAT
#  let employees = [{
#      "fullname":"Joshua Moses",
#      "position":"Frontend Developer",
#      "company":"ossystem",
#      "company_url":"ossystem.com",
#      "country":"Algeria",
#      "linkedin_url":"linkedin.com/Joshua",
#      "email":"Joshua@gmail.com",
#      "phone":"+38047534593459"
#  },{
#      "fullname":"Bill Gates",
#      "position":"Senior Developer",
#      "company":"ossystem",
#      "company_url":"ossystem.com",
#      "country":"Balgeria",
#      "linkedin_url":"linkedin.com/Gates",
#      "email":"gates@gmail.com",
#      "phone":"+38047534593459"
#  }];

#  allData.insertEmployees(employees);

# let company = {
#     "name": "Ossystem",
#     "linkedin_url":"linkedin.com/ossystem",
#     "technologies":["ReactJS", "NodeJS", "Symfony"],
#     "employees_link":"linkedin.com/ossystem/emp",
#     "country":"Ukraine",
#     "company_url":"ossystem.com",
# }

""" 
                                LINKEDIN WEB SCRAPPING PROCESS              
                                ------------------------------

Get [SEARCH URL] for request from url.json file

Pupptter makes request to [SEARCH URL] and save html in [SEARCH RESULT FILE]

Rename [SEARCH RESULT FILE] from .txt to .html. Like this:

fs.rename('../JobTextResult/searchRese.html', '../JobTextResult/searchRese.pdf', (err) => {
    if (err) throw err;
  fs.stat('../JobTextResult/searchRese.pdf', (err, stats) => {
    if (err) throw err;
    console.log(`stats: ${JSON.stringify(stats)}`);
  });
});
Save file like this:
let stream = fs.createWriteStream("./pages/searchResult.txt");
        stream.once('open', function() {
        stream.write(file_to_save);
        stream.end();
      });

Give the [SEARCH RESULT FILE] to Function 1 ([SEARCH RESULT FILE])

# Function 1: getUrlFromSearchRes(searchResult)
            Return json array with company name [COMPANY NAME] and url [COMPANY LINKEDIN URL]

    Get the array and loop through each of them

    Foreach of [COMPANY LINKEDIN URL]:

            Pupptter makes request to [COMPANY LINKEDIN URL] and save html in file [COMPANY FILE]

            Rename the file from .txt to .html. Like this:

            fs.rename('../JobTextResult/searchRese.html', '../JobTextResult/searchRese.pdf', (err) => {
                if (err) throw err;
                fs.stat('../JobTextResult/searchRese.pdf', (err, stats) => {
                    if (err) throw err;
                    console.log(`stats: ${JSON.stringify(stats)}`);
                });
            });

            Give [COMPANY FILE] to Function 2 ([COMPANY FILE])

        # Function 2: checkIfRecruiting(companyPage2) 

            If true [LEAVE FOREACH AND MOVE TO NEXT COMPANY LINKEDIN URL]

            else Give [COMPANY FILE] to Function 3 ([COMPANY FILE])

        # Function 3: getCompanyPageData(companyPage2)
                    Return [COMPANY DATA] () => linkedin url [COMPANY LINKEDIN URL], 
                    technologies [TECHNOLOGIES], country [COUNTRY], 
                    website [WEBSITE], employees url [EMPLOYEES URL]

            Puppetter makes request to [EMPLOYEES URL] from [COMPANY DATA] in Function 3
                and save data in file [EMPLOYEES FILE]
            

            Give [EMPLOYEES FILE] to Function 4 ([EMPLOYEES FILE])

        # Function 4: getAllEmployeeList(employees)
                    Take only those who are in authority
                    Get fullname, position, company, company url, country, linkedin url
                    Save them in the database


        RETURN SUCCESS, LEAVE FOREACH AND MOVE TO NEXT URL


"""

"""
   THIS CODE WAS IN INDEX.JS BEFORE I DECIDED TO CREATE A NEW SCRIPT FOR FASTER RESULT
   
   console.log("\n\nGotten search result now waiting for 35.688 seconds");
      await page.waitFor(35688);
      // await page.close();

      if(Array.isArray(searchOutput) && searchOutput.length){
        // let i=0;
        for(let search_res of searchOutput) {
        //   (async function(i){
        //     setTimeout( async () => {
              // let search_res = searchOutput[2]
              let company_linkedin_url = search_res.company_url;
              console.log("\n\n2. Making request to company url");
              // page = await browser.newPage();
              
              const companyResponse = await page.goto(company_linkedin_url, {  
                timeout: 120000,
                waitUntil: 'networkidle2',
              });
              if (companyResponse._status < 400) {
                console.log("\n\nWaiting for 20.392 seconds");
                await page.waitFor(5392);
          
                const companyHandle = await page.evaluateHandle(() => document.body);
                const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
                let companyPage = await resultcompanyHandle.jsonValue();
                
                console.log("\n\n3. Checking if it is recruiting");        
                let ifRecruiting = scrapper.checkIfRecruiting(companyPage);

                if(ifRecruiting === false){
                  console.log("\n\nHurray, This is not recruiting :))");
                  console.log("\n\n4. Lets grab company data since it is not a recruitment agency");
                  let companyData = scrapper.getCompanyPageData(companyPage);

                  // Lets get all company data in a json object
                  let company_table_data = {
                      "name" : search_res.company_name,
                      "linkedin_url" : company_linkedin_url,
                      "technologies" : companyData.specialty,
                      "employees_link" : companyData.full_employees_url,
                      "country" : companyData.country,
                      "company_url" : companyData.website
                  };

                  console.log("Companies data", company_table_data);
                //INSERT COMPANY DATA INTO TABLE => company_table_data
                try{
                    mongodb.insertCompany(company_table_data);
                } catch(err){
                    console.log('Error inserting company into Mongodb', err)
                }
                console.log("\n\nWaiting for 20.392 seconds");
                await page.waitFor(20392);
                console.log("\n\n5. Let's make another request to employees url");
                const employeesResponse = await page.goto(company_table_data.employees_link, {
                  timeout: 120000,
                  waitUntil: 'networkidle2',
                });
              
                if (employeesResponse._status < 400) {
                  console.log("\n\nWaiting for 15.729 seconds");
                  await page.waitFor(15729);
                  const employeesHandle = await page.evaluateHandle(() => document.body);
                  const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                  let employeesPage = await resultEmployeesHandle.jsonValue();
                  let employeesData = scrapper.getAllEmployeeList(employeesPage);                
                  // Lets get all employees data in a json object if there is a result
                  console.log("\n\n6. Lets get all employees data in a json object if there are bosses");
                  if(employeesData.length){
                      let employee_table_data = [];
                      for(let i = 0; i < employeesData.length; i++){
                          employee_table_data.push({
                              "fullname" : employeesData[i].name,
                              "position" : employeesData[i].position,
                              "company" : search_res.company_name,
                              "company_url" : companyData.website,
                              "country" : companyData.country,
                              "linkedin_url" : company_linkedin_url,
                              "email" : "",
                              "phone" : ""
                          });
                      }
                    console.log("\n\nEmployees data", employee_table_data); 
                     // INSERT EMPLOYEE DATA TO TABLE IF EXISTS => employee_table_data
                      try{
                          mongodb.insertEmployees(employee_table_data);
                      } catch(err){
                          console.log('Error inserting employees into Mongodb', err)
                      } 
                    } else {
                      console.log("No boss in this company"); 
                    }
                    await employeesHandle.dispose();
                  } else {
                    console.log("\n\nNo Boss on the 1st page of this company");
                  }
                  
                } else {
                  console.log("\n\nThis is a recruiting company :(");
                }      
                await companyHandle.dispose();
                 
                  
              }
              // console.log("This is the iteration value now: ");  
        //     }, i * (90728 + i))
        //   }(i));
        // i++;
          console.log("\n\nWaiting for 30.392 seconds. About to move to next URL");
          await page.waitFor(30392);
        };
      } else {
          console.log('No search result');
      } 

"""