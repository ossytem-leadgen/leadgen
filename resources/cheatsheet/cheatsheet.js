  /*               CREATING A NEW SHEET IN COMPANY SPREADSHEET                   
setTimeout(()=>{
  let color = gen().rgb();
  sheets.spreadsheets.batchUpdate({
    spreadsheetId: company_spreadsheet_id,
    resource: {
      "requests": [
        {
          "addSheet": {
            "properties": {
              "title": today.date,
              "tabColor": {
                "red": color.r,
                "green": color.g,
                "blue": color.b,
              },
            }
          }
        }
      ]
    }
  }, async (err, res) => {
      if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
      companies_sheet_id = res.data.replies[0].addSheet.properties.sheetId;
      console.log('\nCreated a new sheet for companyspreadsheet and this is the id: ',companies_sheet_id)
  });  
}, 1500)*/

   
  /*                          INSERT TITLE ON FIRST ROW ON THE NEW COMPANY SHEET                    
setTimeout(()=>{
  sheets.spreadsheets.values.update({
    spreadsheetId: company_spreadsheet_id,
    range: "'"+today.date+"'",
    valueInputOption: 'RAW',
    resource: { 
        "values": [
            ["Name", "Company Url", "Linkedin Link", "Technologies", "Employees Link", "Country"]
        ]
    }
}, async (err, res) => {
    if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
    console.log('\nAdded title to new companies sheet. Updated range:', res.data.updatedRange)
  }); 
 
}, 3000) */
 

  /*               MAKE FIRST ROW IN A SHEET BOLD,CENTER & WIDER                    
setTimeout(()=>{
  sheets.spreadsheets.batchUpdate({
    spreadsheetId: company_spreadsheet_id,
    resource: {
      "requests": [
        {
          "updateDimensionProperties": {
            "range": {
              "sheetId": companies_sheet_id,
              "dimension": "COLUMNS",
              "startIndex": 0,
              "endIndex": 6
            },
            "properties": {
              "pixelSize": 260
            },
            "fields": "pixelSize"
          }
        },
        {
          "repeatCell": {
            "range": {
                "sheetId": companies_sheet_id,
                "startColumnIndex": 0,
                "endColumnIndex": 10,
                "startRowIndex": 0,
                "endRowIndex": 1
            },
            "cell": {
              "userEnteredFormat": {
                "textFormat": {
                  "bold": true, "fontSize": 10,"fontFamily": "Merriweather"
                },
                "horizontalAlignment": "CENTER"
              }
            },
            "fields": 'userEnteredFormat',
          }
        }
      ]
    }
  }, async (err, res) => {
      if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
      console.log('\nStyled column width: 260px -> title row: bold=true, size=10, font=merriweather, textAlign=center',res.data)
  });
}, 4000)*/


/*              1. CREATING A NEW SHEET IN EMPLOYEES SPREADSHEET                   
setTimeout( () => {
  let color = gen().rgb();
  sheets.spreadsheets.batchUpdate({
    spreadsheetId: employees_spreadsheet_id,
    resource: {
      "requests": [
        {
          "addSheet": {
            "properties": {
              "title": today.date,
              "tabColor": {
                "red": color.r,
                "green": color.g,
                "blue": color.b,
              },
            }
          }
        }
      ]
    }
  }, async (err, res) => {
      if (err) return console.log('employeesspreadsheet The API returned an error: ' + err);
      employees_sheet_id = res.data.replies[0].addSheet.properties.sheetId;
      console.log('\nCreated a employeesspreadsheet and this is the id: ', employees_sheet_id)
  });  
}, 10000)*/


/*                       2.   INSERT TITLE FOR EMPLOYEES SPREADSHEET                             
setTimeout(()=>{
sheets.spreadsheets.values.update({
    spreadsheetId: employees_spreadsheet_id,
    range: "'"+today.date+"'",
    valueInputOption: 'RAW',
    resource: { 
        "values": [
            ["First Name", "Last Name", "Position", "Company Name", "Company Link", "Country", "Linkedin Link", "Email", "Telephone"]
        ]
    }
}, async (err, res) => {
    if (err) return console.log('employeesspreadsheet The API returned an error: ' + err);
    console.log('\nAdded title to new employees sheet. Updated range:', res.data.updatedRange)
  }); 
}, 11000)*/ 
 
  /*             3.   MAKE FIRST ROW IN D EMPLOYEES SHEET BOLD,CENTER & WIDER                    
setTimeout( () => {
    sheets.spreadsheets.batchUpdate({
      spreadsheetId: employees_spreadsheet_id,
      resource: {
        "requests": [
          {
            "updateDimensionProperties": {
              "range": {
                "sheetId": employees_sheet_id,
                "dimension": "COLUMNS",
                "startIndex": 0,
                "endIndex": 9
              },
              "properties": {
                "pixelSize": 200
              },
              "fields": "pixelSize"
            }
          },
          {
            "repeatCell": {
              "range": {
                  "sheetId": employees_sheet_id,
                  "startColumnIndex": 0,
                  "endColumnIndex": 9,
                  "startRowIndex": 0,
                  "endRowIndex": 1
              },
              "cell": {
                "userEnteredFormat": {
                  "textFormat": {
                    "bold": true, "fontSize": 10,"fontFamily": "Merriweather"
                  },
                  "horizontalAlignment": "CENTER"
                }
              },
              "fields": 'userEnteredFormat',
            }
          }
        ]
      }
    }, async (err, res) => {
        if (err) return console.log('employeesspreadsheet The API returned an error: ' + err);
        console.log('\nStyled employees first column width: 260px -> title row: bold=true, size=10, font=merriweather, textAlign=center',res.data)
    });
}, 12000)*/


/*        GET ALL DATA FROM COMPANY SPREADSHEET AND INSERT IT BACK INTO MONGODB 

  sheets.spreadsheets.values.get({
    spreadsheetId: company_spreadsheet_id,
    range: '18-09-2018!A2:F',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    const rows = res.data.values;
    if (rows.length) {
      let company_table_data = [];
      rows.forEach((elem, index) => {
        company_table_data[index] = {
          "name" : elem[0],
          "linkedin_url" : elem[2],
          "technologies" : elem[3] == null ? '' : elem[3],
          "employees_link" : elem[4],
          "country" : elem[5] == null ? '' : elem[5],
          "company_url" : elem[1],
          "created_at" : "18-09-2018"
        };
      });
      try{
        mongodb.insertCompany(company_table_data);
      } catch(err){
          console.log('Error inserting company into Mongodb', err)
      }
    } else {
      console.log('No data found.');
    }
  }); */


  /*                           CREATE A NEW SPREADSHEET                              
  sheets.spreadsheets.create({
    resource: {
        properties:{
            title: "Sheet1"
        }
    }
  }, (err, response) => {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    } else {
        console.log("Added", response);
    }
  }); */

  // INSERT A NEW FIELD INTO MULTIPLE MONGODB DOCUMENT WITHOUT CHANGING THE EXISTING DATA
  db.getCollection('company').update({created_at: "2018-10-01"}, {$set: {batch_number: "batch1"}}, { upsert: false, multi: true })
  db.getCollection('company').update({}, {$set: {batch_number: ""}}, { upsert: false, multi: true })
  db.getCollection('company').update({}, {$set: {response_count:{ positive: 0, negative: 0 } } }, { upsert: false, multi: true })

  db.getCollection('spreadsheet_data').update({batch_number: "batch1", lead_title_ranking: "1"}, {$set: {script_status: "Start"}}, { upsert: false, multi: true })
 
  // UPDATE A FIELD IN A MONGODB DOCUMENT
  db.getCollection('company').update({ name: "company_name" }, {$set: { description: "company_description" }})

  // DELET A FIELD COMPLETLY FROM MONGODB DOCUMENT
  db.getCollection('employees-pages-url-queue').update({}, { $unset: { ack: 1 } }, { multi: true })

  // RENAME A FIELD IN MONGODB DOCUMENT
  db.getCollection('generated_leads').updateMany( {}, { $rename: { "lead_title": "position" } } )

  db.getCollection('employees-pages-url-queue').update({}, { $unset: { deleted: 1 } }, { multi: true })

  // REVERT EMPLOYEES QUEUE SO THAT WE CAN ADD IT BACK INTO OUR DB
  db.getCollection('employees-queue').update({}, { $unset: { deleted: 1, ack:1 } }, { multi: true })

  // COMPARE TWO DIFFERENT FIELDS
  db.getCollection('oleg_contacts').find({firstname: {$exists: true}, $expr: {$eq: ["$firstname", "$lastname"]}});


  // CHANGE EACH VALUE IN EACH DOCUMENT USING REGEX
  db.getCollection('spreadsheet_data').find().forEach(function(doc) {
    doc.sheetrange = doc.sheetrange.replace(/""/g, 'Y');
    db.getCollection('spreadsheet_data').save(doc);
  });

  // WRITE JS INSIDE OF MONGODB QUERIES
  db.getCollection('sales_leads').find({$where: "!Array.isArray(this.url) && this.url.length"})

  // GET ONLY A FIELD IN FROM A COLLECTION
  db.getCollection('sales_leads').find({name: "Best"}, {"name":true, "_id":false})

//  Change all fields from string to integer
db.getCollection('spreadsheet_data').find().forEach(function(doc) {
    doc.lead_title_ranking = parseInt(doc.lead_title_ranking)
    db.getCollection('spreadsheet_data').save(doc);
});
    /*                          ADD COMPANIES DATA INTO THE SPREADSHEET ONE AFTER THE OTHER                           
  if(companies.length){
    console.log(`There are ${companies.length} companies to insert`);
    for(let i=0; i<companies.length; i++){
      (function(i){
          setTimeout(() => {
              company_name = companies[i].name;
              company_url = companies[i].company_url;
              linkedin_url = companies[i].linkedin_url;
              employees_link = companies[i].employees_link;
              country = companies[i].country ? companies[i].country : '-';
              date = companies[i].created_at ? companies[i].created_at : '10-09-2018';
              let tech = companies[i].technologies
              technologies = tech ? (Array.isArray(tech) ? (tech.length > 1 ? tech.join(', ') : '-') : tech) : '-';
              
              sheets.spreadsheets.values.append({
              spreadsheetId: company_spreadsheet_id,
              range: 'Companies',
              valueInputOption: 'RAW',
              resource: { 
                  "values": [
                      [company_name, company_url, linkedin_url, technologies, employees_link, country, date]
                  ]
              }
          }, (err, res) => {
              if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
              let spreadSheetId = res.data.spreadsheetId
              let tableRange = res.data.tableRange;
              console.log(`\nCompany Spreadsheet updated: ${spreadSheetId}\nColumns Changed: ${tableRange}\n`)
            });
            if((i + 1) == companies.length){
              setTimeout(() => { insertEmp(); }, 1000)
            }
          }, i * 1000)
      }(i))  
    }
  } 
*/  

  /*                 4.         ADD EMPLOYEES DATA INTO THE SPREADSHEET ONE AFTER THE OTHER                        

function insertEmp() {
  setTimeout( () => {
    console.log(`There are ${employees.length} employees to insert`)
    if (employees.length){
      for (let i = 0; i<employees.length; i++) {
        (function(i){
            setTimeout(() => {
                let fullname = employees[i].fullname.split(' ');
                firstname = fullname[0];
                lastname = fullname[1];
                position = employees[i].position;            
                company_name = employees[i].company;
                company_url = employees[i].company_url;
                linkedin_url = employees[i].linkedin_url;
                country = employees[i].country
                email = employees[i].email;
                phone = employees[i].phone;
                date = employees[i].created_at ? employees[i].created_at : '10-09-2018';
                
                // Spread Sheet Manipulation
                sheets.spreadsheets.values.append({
                spreadsheetId: employees_spreadsheet_id,
                range: 'Employees',
                valueInputOption: 'RAW',
                resource: { 
                    "values": [
                        [firstname, lastname, position, company_name, company_url, country, linkedin_url, email, phone, date]
                    ]
                }
            }, (err, res) => {
                if (err) return console.log('employeesspreadsheetThe API returned an error: ' + err);
                let spreadSheetId = res.data.spreadsheetId
                let tableRange = res.data.tableRange;
                console.log(`\nEmployeesspreadsheet Spreadsheet updated: ${spreadSheetId}\nColumns Changed: ${tableRange}\n\n`)
              });
              if((i + 1) == employees.length){
                logTime();
              }
            }, i * 1000)
        }(i))  
      }
    } else {
      console.log('No new employees data for Google sheets');
    }
  }, 1000) 
}
*/


// 3 grouping for the amount of days in the month
let group1 = 28;
let group2 = 30;
let group3 = 31;

//Months that have 28 days
let group1Month = 2; //February

//Months that have 30 days
let group2Months = [4, 6,11]; //April, June, November

//Months that have 31 days
let group3Months = [1,3,5,7,8,9,10,12]; //January, March, May, July, August, September, October, December


// I used this funciton to check all the companies which did not have any employee in our database
// So that next time when the script runs it goes back to the company and takes its employees
const batchnumber = "batch2";
// (async () => {
//     let allCompanies = await mongodb.allCompanies('batchnumber', batchnumber)
//     let args = {$ne : "N/A"}
//     let fields = {
//         "batch_number": batchnumber, 
//         "category" : args
//     }
//     let sheetData = await getOlegContacts(fields);
//     let comarr = [];
//     let other = [];
//     let others = [];
//     for (let data of sheetData) {
//         let { company } = data;

//         if(comarr.includes(company)) {
//             // console.log(company)
//             // if(company === "7"){
//             //     comarr.push(company)
//             // }
//         } else { comarr.push(company) }
//     }
//     console.log('Orig',sheetData.length)

//     console.log('1st',comarr.length)

//     // for (let b=0; b < other.length; b++) {
//     //     (function(b){
//     //         setTimeout( async () => {
//     //             await await updateGeneratedLeads(other[b], "sheetrange", "")
//     //         }, b * 2000);
//     //     })(b)
//     // }
//     // console.log(other[1])
//     // console.log('Total',other.length + comarr.length)
//     // console.log(other[1])
//     // console.log(comarr.length + other.length)
//     // console.log(sheetData.length)
//     // for(let { comp} of comarr)  
//     // console.log(comarr.length)
//     // let rem = []
//     // for(let comp of allCompanies){
//     //     let { name, _id } = comp;
//     //     if(comarr.includes(name)) {
//     //         // console.log(company)
//     //     } else { rem.push({ name, _id }) }
//     // }  
//     // console.log(rem.length)
//     // console.log('Total: ', comarr.length + rem.length)

//     // for(let i=0; i<rem.length; i++) {
//     //     (function(i){
//     //         setTimeout(async () => {
//     //             let { name, _id } =  rem[i];
//     //             console.log(name)
//     //             await mongodb.updateCompany(_id, "took_all_employees", false)
//     //         }, 10000)
//     //     })(i)
//     // }
//     // console.log(allEmployees[1])

    
// })()

// } else if(Array.isArray(empArr) && empArr.length > 50) {

    //THIS WAS THE INITIAL LOOP I USED TO TAKE ONLY 50 PEOPLE FROM THE QUEUE
    //     for(let i=0; i < 50; i++){
    //         console.log('First')
    //         let doesEmployeeExist = await mongodb.doesEmployeeExist(empArr[i].fullname, empArr[i].position);
    //         if (doesEmployeeExist == false) {
    //             employee_table_data.push({
    //                 "fullname" : empArr[i].fullname,
    //                 "position" : empArr[i].position,
    //                 "company" : empArr[i].company,
    //                 "company_url" : empArr[i].company_url,
    //                 "country" : empArr[i].country,
    //                 "linkedin_url" : empArr[i].linkedin_url,
    //                 "email" : empArr[i].email,
    //                 "phone" : empArr[i].name,
    //                 "created_at" : empArr[i].created_at,
    //                 "merged" : false
    //             });
    //             let ifCompExist = await mongodb.doesCompanyExist(empArr[i].company)
        
    //             if(ifCompExist == false){
    //                 console.log("Company has not being added before")
    //                 companiesArr.forEach((elem) => {
    //                     if(elem.name == empArr[i].company){
    //                         console.log("Saving this company data", elem.name)
    //                         try{
    //                             mongodb.insertCompany(elem)
    //                         } catch(err){
    //                             console.log('Error inserting company into Mongodb', err)
    //                         }
    //                     }
    //                 })
    //             }
    //         } else {
    //             console.log('Employee already exists')
    //         }
    //     }
    // }

//LAUNCH A BROWSER
// const browser = await puppeteer.launch({
//   headless: false
// });

// const page = await browser.newPage();

// try {
//   await page.setCookie(...cookies.acc1);
// } catch (error) {
//   console.error(error);
// }
// const pageResponse = await page.goto(pageUrl, {
//   timeout: 120000,
//   waitUntil: 'networkidle2',
// });

// if (pageResponse._status < 400) {
//   await page.setViewport({ width: 1366, height: 868 });
//   const pageHandle = await page.evaluateHandle(() => document.body);
//   const resultpageHandle = await page.evaluateHandle(body => body.innerHTML, pageHandle);
//   let html = await resultpageHandle.jsonValue();
//   let peopleUrls = scrapper.getPeopleUrls(html);
//   await pageHandle.dispose();
// }

// await browser.close();

//Google Drive helpful Object 

// const obj = { kind: 'drive#fileList',
//   incompleteSearch: false,
//   files: [ { kind: 'drive#file',
//        id: '1FJXSFQEL1eHQ8OOwDEy7pGBkwC8xyrt6Eex0Rl7v3bI',
//        name: 'Untitled document',
//        mimeType: 'application/vnd.google-apps.document' },
//      { kind: 'drive#file',
//        id: '1GZHsC9PzjAYUuTL_lLRiJ9WwmWmEa4wz',
//        name: 'Script Error',
//        mimeType: 'application/vnd.google-apps.folder' },
//      { kind: 'drive#file',
//        id: '1uAbKufk5yo60Eni_sgZ8TeGOBrxyuiwk',       name: '84142.png',       mimeType: 'image/png' },     { kind: 'drive#file',
//        id: '1Ha6rdYyS-EsQmZHUw8KhtMsTakOUS6UO',
//        name: 'error101.png',
//        mimeType: 'image/png' },
//      { kind: 'drive#file',
//        id: '1qKN3csRAWwR8-RRC546uQfwcyqJuVp5MqOagNgPacdg',
//        name: 'Отчет по кандидатам ',
//        mimeType: 'application/vnd.google-apps.spreadsheet' } ]
// };

//Get the list of files in a google drive account

// drive.files.list({
//   name:'Script Error',
//   mimeType: 'application/vnd.google-apps.folder'
// }, (err, res) => {
//       if (err) return console.log('Error from API', err);
//       console.log(res.data);
// });


// Move file from local to vm using scp
// scp -r /home/best/Projects/leadgen b.ibitoye@10.1.15.122:/home/b.ibitoye

// MONGODB CONNECTION TO mLab
// mongo ds151513.mlab.com:51513/leadgendata -u <dbuser> -p <dbpassword>

// mongoimport -h ds151513.mlab.com:51513 -d leadgendata -c oleg_leads -u Best -p best_leadGen_11:20-2018 --file /home/best/Desktop/olegLeadsExport.json

// mongoimport -h ds151513.mlab.com:51513 -d leadgendata -c employees -u Best -p best_leadGen_11:20-2018 --file /home/best/Desktop/employeesExport.json

// mongoimport -h ds151513.mlab.com:51513 -d leadgendata -c statistics -u Best -p best_leadGen_11:20-2018 --file /home/best/Desktop/statisticsExport.json

// mongoimport -h ds151513.mlab.com:51513 -d leadgendata -c spreadsheet_data -u Best -p best_leadGen_11:20-2018 --file /home/best/Desktop/spreadsheetDataExport.json

// mongoimport -h ds151513.mlab.com:51513 -d leadgendata -c recruiting_companies -u Best -p best_leadGen_11:20-2018 --file /home/best/Desktop/recruitingCompaniesExport.json

// mongo ds151513.mlab.com:51513/leadgendata -u Best -p best_leadGen_11:20-2018

// Mounting a file from Virtual Machine to local
// sshfs b.ibitoye@10.1.15.122:/home/b.ibitoye/leadgen /home/best/Projects/remote-leadgen


const cheerio = require('cheerio');
const fs = require('fs');
const puppeteer = require('puppeteer');
const { cookies } = require('./helpers/constants');

const puppeteerTemplate = async () => {
    const browser = await puppeteer.launch({
        headless: false
    });
    const page = await browser.newPage();
    
    try {
        await page.setCookie(...cookies.boss);
    } catch (error) {
        console.error(error);
    }

    await page.goto('https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B%22484016%22%5D');
    await page.waitFor(5000);

    const companyHandle = await page.evaluateHandle(() => document.body);
    const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
    let html = await resultcompanyHandle.jsonValue();

    let stream = fs.createWriteStream('/home');
        stream.once('open', function() {
            stream.write(csv);
            stream.end();
    });
    const pathToFile = '/home/best/Projects/leadgen/resources/pages/employees1.html';

    let html = fs.readFileSync(pathToFile, 'utf8');

    const $ = cheerio.load(html);
}



const inbox1 = '/resources/pages/msgpages/inbox1.html';
const inbox2 = '/resources/pages/msgpages/inbox2.html';
const profile1 = '/resources/pages/msgpages/profile1.html';
const profile3 = '/resources/pages/msgpages/profile3.html';




const setTimezones = async () => {
  // const mobileLeads = await getMobileLeads({"chatted" : false, country: {$exists: true}});
  const mobileLeads = await getGeneratedLeads({time_diff: ""});
  console.log(len(mobileLeads));
  const countriesWithoutTz = [];

  let notFound = 0;
  let found = 0;
  
  for (const lead of mobileLeads) {
      const { _id, country } = lead;
      const timezone = getTimezones(country);

      if (timezone) {
          const ctime = getTimeNowFromTz(timezone);
          let time_diff = calcTimeDiff(ctime).toString();
          
          // Not "-1" and Not "0" but if "1"
          if (time_diff.charAt(0) !== "0" && time_diff.charAt(0) !== "-") {
              time_diff = "+" + time_diff;
          }

          // console.log(timezone, ' || ', country, ' || ', time_diff);
          
          found += 1;

          await updateGeneratedLeads({_id}, {timezone, time_diff});

          await sleep(0.2);
      } else {
          notFound += 1;
          await updateGeneratedLeads({_id}, {timezone: "", time_diff: "0"});

          await sleep(0.2);
          
          if (!countriesWithoutTz.includes(country)) countriesWithoutTz.push(country); 
      }
  }

  log('Found', found, '\nNot Found', notFound, "\nTotal", len(mobileLeads));

  console.log('\n\nCountries without TZ', countriesWithoutTz);

  process.exit();
};

// const country = "Armonk, New York";
// const timezone = getTimezones(country);
// const ctime = getTimeNowFromTz(timezone);

// console.log(ctime);

// let time_diff = calcTimeDiff(ctime).toString();

// console.log(time_diff);


//Using two tabs in one browser

puppeteer.launch({
    headless: false,
    args: ['--unlimited-storage', '--full-memory-crash-report']
}).then(async browser => {
    const promises = [];

    for (const functionParam of functionParams) {
        const [asyncFuntion, functionName] = functionParam;

        promises.push(browser.newPage().then(async page => {
            try {
                const cookie = await getCookie(CONSTANTS.COOKIES_OWNER.OLEG);
                await page.setCookie(...cookie);
            } catch (error) {
                console.error(error);
                process.exit(1);
            }

            await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2'});

            if (functionName && functionName === GET_ACCEPTED_CONNECTS) {
                await page.click('button.msg-overlay-bubble-header__button');
            }
        
            if (await page.$(closeMsgBox) !== null) await page.click(closeMsgBox);
        
            const runInterval = () => {
                const interval = setInterval(async () => {
                    await asyncFuntion(page, closeMsgBox);
        
                    clearInterval(interval);
        
                    runInterval();
                }, 30000);
            };
        
            runInterval();

            await page.waitFor(6783);
        }));
    }

    await Promise.all(promises);
});

// Add new leads to olegs contact collection with data from linkedin 
// downloaded connects list
const { connects } = require('../modules/data/connects');
for (const newLead of connects) {
    peopleNo += 1;
    const { firstname, lastname } = newLead;

    let found = false;
    let lead = {
        lead_name: "", country: "", linkedin_url: ""
    };

    if (len(firstname)) {
        lead.lead_name = firstname.concat(' ', lastname);
        console.log(`\n${peopleNo} `, lead.lead_name);

        const gLead = await getGeneratedLeads({lead_name: lead.lead_name});
        const mLead = await getMobileLeads({lead_name: lead.lead_name});
        const sLead = await getSalesLeads({lead_name: lead.lead_name});

        if (len(gLead)) {
            const [{ lead_name, country, lead_linkedin_url }] = gLead;

            if (len(country)) {
                lead.lead_name = lead_name;
                lead.country = country;
                lead.linkedin_url = lead_linkedin_url;

                foundgLead += 1;
                found = true;
                log('FOUND=======================');
            }
        } else if (len(mLead)) {
            const [{ lead_name, country, linkedin_url }] = mLead;

            if (len(country)) {
                lead.lead_name = lead_name;
                lead.country = country;
                lead.linkedin_url = linkedin_url;

                foundmLead += 1;
                found = true;
                log('FOUND=======================');
            }
        } else if (len(sLead)) {
            let [{ lead_name, country, url }] = sLead;

            if (len(country)) {
                lead.lead_name = lead_name;
                lead.country = country;
                lead.linkedin_url = url;

                foundsLead += 1;
                found = true;
                log('FOUND=======================');
            }
        }

        const oLead = await getOlegContacts({lead_name: lead.lead_name});

        if (!len(oLead)) await addOlegContact({...lead});

        // if (found) {
        //     process.exit();
        // }
    }
}