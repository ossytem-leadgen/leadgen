const puppeteer = require('puppeteer');
require('dotenv').config();

const { CONSTANTS } = require('../helpers/constants');
const { getCompany } = require('../Db/Companies');
const { getEmployee } = require('../Db/Employees');
const { addRecruitingCompany, getRecruitingCompany } = require('../Db/RecruitingCompanies');
const { addEmployeeLinkToQueue, addCompanyDataToQueue, addEmployeeDataToQueue } = require('../Db/Queues/addToQueue');
const { getCompanyLinkFromQueue, getQueueSize } = require('../Db/Queues/getFromQueue');
const { genRandNum, date, time, log, getCookie, len } = require('../modules');
const { checkIfRecruiting, getCompanyPageData, getAllEmployeeList } = require('../modules/scrappers');

let startTime = new Date().getTime();
let randNo;
// Change this numbers for wait time between requests
const { MIN_TIME, MAX_TIME } = CONSTANTS.SCRIPT_PAUSE;
const { COMPANY_LINK } = CONSTANTS.DB.COLLECTIONS.QUEUE;

console.log(`\n\n--------------------------THIS IS THE LOG FOR COMPANIES CRAWLER SCRIPT [SECOND SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);

const getCompanyDataFromPage = async () => {
    // let queueSize = await getQueueSize(COMPANY_LINK);
    const companies = await getCompany({country: "Belarus",took_all_employees: false});

    if (len(companies)) {
        console.log('The size of the queue is ', len(companies));

        const browser = await puppeteer.launch({
            headless: false
        });

        const page = await browser.newPage();

        try {
            const account = process.argv[3];

            let cookie = [];

            switch (account) {
                case 'acc1':
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
                    break;
                case 'acc2':
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.KORZHENKO);
                    break;
                default:
                    cookie = await getCookie(CONSTANTS.COOKIES_OWNER.LEVITSKAYA);
            }

            await page.setCookie(...cookie);

        } catch (error) {
            console.error(error);
        }

        // for (let i = 0; i < queueSize; i++) {
        for (const belaCompany of companies) {
            const { _id, name, linkedin_url, country } = belaCompany;
            try {
                let link = linkedin_url;

                let existingCompany = [];
                if (link) {
                    console.log('Companys link is:',link);
                    let company_linkedin_url = link;
                    console.log("\n\n1. Making request to company url");

                    const companyResponse = await page.goto(company_linkedin_url, {
                        timeout: 120000,
                        waitUntil: 'networkidle2',
                    });

                    if (companyResponse._status < 400) {
                        randNo = genRandNum(10000, 20000);
                        console.log(`\n\nOn companing page now waiting for ${randNo} seconds`);
                        await page.waitFor(randNo);

                        const companyHandle = await page.evaluateHandle(() => document.body);
                        const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
                        let companyPage = await resultcompanyHandle.jsonValue();

                        console.log("\n\n2. Checking if it is recruiting");
                        let ifRecruiting = checkIfRecruiting(companyPage);
                        let companyData = getCompanyPageData(companyPage);

                        if (ifRecruiting === false) {
                            console.log("\n\nHurray, This is not recruiting :)");
                            console.log("\n\n3. Lets grab company data since it is not a recruitment agency");
                            let doesCompanyExist = await getCompany({ name: companyData.name});
                            let didntTakeAllEmployees = await getCompany({ name: companyData.name,  took_all_employees: "false" });

                            // if (!doesCompanyExist || didntTakeAllEmployees) {
                            console.log('\nNew Company or we did not finish working with it');
                            // Lets get all company data in a json object
                            // Always update the batch number
                            // if (!doesCompanyExist) {
                            let company_table_data = {
                                "name" : companyData.name,
                                "linkedin_url" : company_linkedin_url,
                                "technologies" : companyData.specialty,
                                "employees_link" : companyData.full_employees_url,
                                country,
                                "company_url" : companyData.website,
                                "created_at" : date(),
                                "took_all_employees" : true,
                                "description" : companyData.description,
                                "batch_number" : 5
                            };
                            console.log("Companies data", company_table_data);
                            //INSERT COMPANY DATA INTO TABLE => company_table_data
                            try{
                                await addCompanyDataToQueue(company_table_data);
                            } catch(err){
                                console.log('Error inserting company into Mongodb', err)
                            }
                            // }
                            randNo = genRandNum(MIN_TIME, MAX_TIME);

                            console.log(`\n\nWaiting for ${randNo} seconds THEN make another request to employees url`);

                            await page.waitFor(randNo);

                            if(companyData.full_employees_url != 'null'){
                                console.log("\n\n4. Let's make another request to employees url");
                                //This concatination is specifically for Greater Boston Area
                                let employees_link = companyData.full_employees_url
                                const employeesResponse = await page.goto(employees_link, {
                                    timeout: 120000,
                                    waitUntil: 'networkidle2',
                                });

                                await page.waitFor(3000);

                                if (employeesResponse._status < 400) {
                                    await page.evaluate(async () => {
                                        await new Promise((resolve, reject) => {
                                            try {
                                                const maxScroll = Number.MAX_SAFE_INTEGER;
                                                let lastScroll = 0;
                                                const interval = setInterval(() => {
                                                    window.scrollBy(0, 100);
                                                    const scrollTop = document.documentElement.scrollTop;
                                                    if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                                        clearInterval(interval);
                                                        resolve();
                                                    } else {
                                                        lastScroll = scrollTop;
                                                    }
                                                }, 100);
                                            } catch (err) {
                                                console.log(err);
                                                reject(err.toString());
                                            }
                                        });
                                    });

                                    await page.waitFor(3000);

                                    const employeesHandle = await page.evaluateHandle(() => document.body);
                                    const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
                                    const employeesPage = await resultEmployeesHandle.jsonValue();
                                    const employees = getAllEmployeeList(employeesPage);
                                    console.log('\n\nEmployees',employees);

                                    // Lets get all employees data in a json object if there is a result
                                    console.log("\n\n5. Lets get all employees data in a json object if there are bosses");
                                    if (Array.isArray(employees[0]) && employees[0].length) {
                                        let employee_table_data = [];
                                        for (let i = 0; i < employees[0].length; i++) {
                                            let doesEmployeeExist = await getEmployee({ fullname: employees[0][i].name, position: employees[0][i].position });
                                            if (!doesEmployeeExist) {
                                                employee_table_data.push({
                                                    "fullname" : employees[0][i].name,
                                                    "position" : employees[0][i].position,
                                                    "company" : companyData.name,
                                                    "company_url" : companyData.website,
                                                    "country" : employees[0][i].country,
                                                    "linkedin_url" : employees[0][i].employee_url,
                                                    "email" : "No email",
                                                    "phone" : "No Phone number",
                                                    "created_at" : date()
                                                });
                                            }
                                        }
                                        console.log("\n\nEmployees data", employee_table_data);
                                        // INSERT EMPLOYEE DATA TO TABLE IF EXISTS => employee_table_data
                                        try{
                                            await addEmployeeDataToQueue(employee_table_data);
                                        } catch(err){
                                            console.log('Error adding company data to queue', err)
                                        }
                                    } else {
                                        console.log("No boss in this company");
                                    }

                                    if (employees[1] && employees[1]) {
                                        // Generate urls [employee_page_url + &page=2]
                                        console.log("Generating url paginations and passing to queue");
                                        let employeePaginationNo = employees[1];
                                        let employeePaginationsUrls = [];
                                        // if(employeePaginationNo <= 5){
                                        for (let i=2; i<=employeePaginationNo; i++) {
                                            // this url is for Greater boston area
                                            let url = `${employees_link}&page=${i}`;
                                            let company = companyData.name;
                                            let company_url = companyData.website;
                                            employeePaginationsUrls.push({url, company, company_url});
                                        }
                                        // }
                                        // else if(employeePaginationNo > 5){
                                        //     for(let i=2; i<=5; i++){
                                        //         let url = `${employees_link}&page=${i}`;
                                        //         let company = companyData.name;
                                        //         let company_url = companyData.website
                                        //         employeePaginationsUrls.push({url, company, company_url})
                                        //     }
                                        // }
                                        console.log(employeePaginationsUrls)
                                        // Pass the number of employee pages to queue for next script

                                        if (Array.isArray(employeePaginationsUrls) && employeePaginationsUrls.length){
                                            for (let paginate of employeePaginationsUrls) {
                                                await addEmployeeLinkToQueue(paginate)
                                            }
                                        }
                                    }
                                    await employeesHandle.dispose();
                                } else {
                                    console.log("\n\nError with employees page");
                                }
                            }
                            // } else {
                            //     existingCompany.push(companyData.name)
                            //     console.log('Company Exists, no need to take it', companyData.name)
                            // }

                            randNo = genRandNum(MIN_TIME, MAX_TIME);

                            console.log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);

                            await page.waitFor(randNo);
                        } else {
                            console.log("\n\nThis is a recruiting company, Let us black list it for future purposes:(");
                            let recruitingCompany = await getRecruitingCompany({ name: companyData.name });

                            if (!recruitingCompany) {
                                console.log('This is a new recruitment company, lets save it');
                                let recruiter = { "name" : companyData.name }
                                console.log("\nRecruitment Company data", recruiter);
                                try{
                                    await addRecruitingCompany(recruiter);
                                } catch(err){
                                    console.log('\nError inserting company into Mongodb', err)
                                }
                            } else {
                                console.log('\nThis recruitment company has already been added');
                            }

                            randNo = genRandNum(MIN_TIME, MAX_TIME);

                            // let newSize = await getQueueSize(COMPANY_LINK);

                            // console.log('\nThe queue size is now', newSize)

                            console.log(`\nDone with the recruitment company waiting for ${randNo} seconds then move to next company`);
                            await page.waitFor(randNo);
                        }
                        await companyHandle.dispose();
                    }

                    // let newSize = await getQueueSize(COMPANY_LINK);
                    // console.log('\nThe queue size is now', newSize)
                    // if (i == (queueSize - 1)) {
                    //     console.log('The Queue is now empty');
                    //     let endTimeMilli =  new Date().getTime() - startTime;
                    //     let endTimeMin = Math.floor(endTimeMilli / 60000);
                    //     let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);
                    //     if (endTimeSec == 60){
                    //         endTimeMin = endTimeMin + 1;
                    //         endTimeSec = 0;
                    //     }
                    //     console.log('Existing Companies:', existingCompany)
                    //     console.log(`Script 2 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);
                    //     await browser.close();
                    //     process.exit();
                    // }
                } else {
                    await browser.close();
                    process.exit();
                }
            } catch(error){
                console.error('Error with puppetter: ',error)
            }
        }
        process.on("unhandledRejection", (reason, p) => {
            console.error("ERROR", p, "reason:", reason);
            browser.close();
        });

    } else {
        console.log('The Queue is empty');
        process.exit();
    }
}

getCompanyDataFromPage();

module.exports = getCompanyDataFromPage;
