const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const gen = require('color-generator');
const { date, time } = require('../../modules');
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'],
      TOKEN_PATH = '/tokens/sheetstoken.json',
      company_spreadsheet_id = '1u5X2-bQq5oAHNPQP_u6a2G8jDfeAc1kZEXog7Kskpjk',
      employees_spreadsheet_id = '16CyrR1TvB0mqYWpjMXAvUJLFQy68RsZgAC85GdDhhTg',
      startTime = new Date().getTime(),
      inserted_companies = false,
      inserted_employees = false;

console.log(`\n\n--------------------------THIS IS THE LOG FOR GOOGLE SPREADSHEET SCRIPT [FIRST SCRIPT] [${date()} || ${time()}] -----------------------------\n\n`);

fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(content), listMajors);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Insert into Company and then into Employees Spreadsheet
 * @see https://docs.google.com/spreadsheets/d/16CIL1UoBUTouH1KYooDLRiexpikMKdZG-Gi3ek8Bw10/edit?usp=sharing
 * @see https://docs.google.com/spreadsheets/d/16CyrR1TvB0mqYWpjMXAvUJLFQy68RsZgAC85GdDhhTg/edit?usp=sharing
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
async function listMajors(auth) {
  // const companies = await mongodb.allCompanies();
  // const employees = await mongodb.allEmployees();
  const sheets = google.sheets({version: 'v4', auth});

  let company_name,
      company_url,
      linkedin_url,
      technologies,
      employees_link,
      country,
      firstname,
      lastname,
      position,
      email,
      phone,
      companies_sheet_id,
      employees_sheet_id,
      todaysDate;

  /*                          ADD COMPANIES DATA INTO THE SPREADSHEET ONE AFTER THE OTHER       */                      
  if(companies.length){
    console.log(`There are ${companies.length} companies to insert`);
    for(let i=0; i<companies.length; i++){
      (function(i){
          setTimeout(() => {
              company_name = companies[i].name;
              company_url = companies[i].company_url;
              linkedin_url = companies[i].linkedin_url;
              employees_link = companies[i].employees_link;
              country = companies[i].country ? companies[i].country : '-';
              todaysDate = companies[i].created_at ? companies[i].created_at : '10-09-2018';
              let tech = companies[i].technologies
              technologies = tech ? (Array.isArray(tech) ? (tech.length > 1 ? tech.join(', ') : '-') : tech) : '-';
              
              sheets.spreadsheets.values.append({
              spreadsheetId: company_spreadsheet_id,
              range: 'Companies',
              valueInputOption: 'RAW',
              resource: { 
                  "values": [
                      [company_name, company_url, linkedin_url, technologies, employees_link, country, todaysDate]
                  ]
              }
          }, (err, res) => {
              if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
              let spreadSheetId = res.data.spreadsheetId
              let tableRange = res.data.tableRange;
              console.log(`\nCompany Spreadsheet updated: ${spreadSheetId}\nColumns Changed: ${tableRange}\n`)
            });
            if((i + 1) == companies.length){
              inserted_companies = true;
            }
          }, i * 1000)
      }(i))  
    }
  } 

  /*                 4.         ADD EMPLOYEES DATA INTO THE SPREADSHEET ONE AFTER THE OTHER        */                 

if (inserted_companies === true) {
  setTimeout( () => {
    console.log(`There are ${employees.length} employees to insert`)
    if (employees.length){
      for (let i = 0; i<employees.length; i++) {
        (function(i){
            setTimeout(() => {
                let fullname = employees[i].fullname.split(' ');
                firstname = fullname[0];
                lastname = fullname[1];
                position = employees[i].position;            
                company_name = employees[i].company;
                company_url = employees[i].company_url;
                linkedin_url = employees[i].linkedin_url;
                country = employees[i].country
                email = employees[i].email;
                phone = employees[i].phone;
                todaysDate = employees[i].created_at ? employees[i].created_at : '10-09-2018';
                
                // Spread Sheet Manipulation
                sheets.spreadsheets.values.append({
                spreadsheetId: employees_spreadsheet_id,
                range: 'Employees',
                valueInputOption: 'RAW',
                resource: { 
                    "values": [
                        [firstname, lastname, position, company_name, company_url, country, linkedin_url, email, phone, todaysDate]
                    ]
                }
            }, (err, res) => {
                if (err) return console.log('employeesspreadsheetThe API returned an error: ' + err);
                let spreadSheetId = res.data.spreadsheetId
                let tableRange = res.data.tableRange;
                console.log(`\nEmployeesspreadsheet Spreadsheet updated: ${spreadSheetId}\nColumns Changed: ${tableRange}\n\n`)
              });
              if((i + 1) == employees.length){
                inserted_employees = true;
              }
            }, i * 2000)
        }(i))  
      }
    } else {
      console.log('No new employees data for Google sheets');
    }
  }, 1000) 
}
                 
  if (inserted_employees === true) {
    let endTimeMilli =  new Date().getTime() - startTime;
    let endTimeMin = Math.floor(endTimeMilli / 60000);
    let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);

    if(endTimeSec == 60){
        endTimeMin = endTimeMin + 1;
        endTimeSec = 0;
    }
    console.log(`Script 4 took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);
  }
}

  // https://docs.google.com/spreadsheets/d/16CIL1UoBUTouH1KYooDLRiexpikMKdZG-Gi3ek8Bw10/edit?usp=sharing
  // https://docs.google.com/spreadsheets/d/1HFL_RRlRcXBb1aRv8LK-O2V5hCyaJUzqu47lE5f5avc/edit?usp=sharing



