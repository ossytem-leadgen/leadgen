//Run every 8am
const getMsgUrl = async () => {
    log('Fetching Message Url has been triggered');
    
    const url = URLS.messages;
    const searchField = 'input.msg-search-form__search-field';
    const yesterdayInboxes = await getInbox({date: reduceDay(1), linkedin_url: ""});

    if (yesterdayInboxes.length) {
        const cancelSearch = 'button.msg-search-form__cancel-search';
        const hideBrowser = process.env.HIDE_BROWSER ? process.env.HIDE_BROWSER : 'false';
        
        let browser = null;
        
        if (hideBrowser === 'true') {
            browser = await puppeteer.launch({ args: ['--no-sandbox'] });
        } else {
            browser = await puppeteer.launch({ headless: false });
        }
        
        const page = await browser.newPage();
    
        await page.setCookie(...cookies.boss);

        await page.goto(url, { timeout: 250000, waitUntil: 'networkidle2'});

        for (let inbox of yesterdayInboxes) {
            let randNum = genRandNum(MIN_TIME, MAX_TIME);
            
            try {
                let { _id, name } = inbox;
                
                await page.type(searchField, name, { delay: 50 });
                await page.waitFor(6754);

                const mainMsgArea = await page.$("h2.msg-entity-lockup__entity-title");

                if (mainMsgArea) {
                    const searchResultName = await page.evaluate(element => element.textContent, mainMsgArea);
                    
                    console.log('LeadName: ',searchResultName.trim());

                    if (searchResultName.trim() == name) {
                        let new_linkedin_url = await page.evaluate((sel) => {
                        return document.getElementsByClassName(sel)[0].href;
                        }, 'msg-conversation-listitem__link');
            
                        console.log('Thread url: ', new_linkedin_url);
                        
                        inbox.linkedin_url = new_linkedin_url;
                        
                        await updateInbox({_id, _id}, {linkedin_url: new_linkedin_url});
                    } else {
                        // url = https://www.linkedin.com/messaging/?searchTerm=firstname%20lastname
                        const regexSpaceCheck = /\s/gi;
                        const new_linkedin_url = URLS.messages.concat('?searchTerm=', name.replace(regexSpaceCheck, '%20'));
                        
                        inbox.linkedin_url = new_linkedin_url;
                        
                        await updateInbox({_id, _id}, {linkedin_url: new_linkedin_url});
                        log('The names are not similar');
                    }

                    await pushToSheet(inbox);

                } else {
                    log('Could not get the h2');
                }

                if (await page.$(cancelSearch) !== null) await page.click(cancelSearch);

                log(`I am waiting for ${randNum}`);

                await page.waitFor(randNum);
            } catch (error) {
                const imageName = `${date()}-${time()}.png`;
                const errorLocation = 'inboxController.getMsgUrl()';
                const extraInfo = `CURRENT LEAD DATA:\nLead ID: ${_id}\nLead Name:${name}\nCurrent Time:${time()}\n. Image name in google drive: ${imageName}`;
                
                await errorHandler(page, error, errorLocation, extraInfo, imageName);
                
                log('An error occured. Sending it to the administrator', error);

                await page.waitFor(randNum);
            }
        }
        await browser.close();
        
        process.exit();
    } else {
        log('There was no lead yesterday or they all have linkedin urls');
        process.exit();
    }
}