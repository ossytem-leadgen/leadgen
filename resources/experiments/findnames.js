const puppeteer = require('puppeteer');
const fs = require('fs')

const scrapper = require('../../modules/scrapper');


try{
    (async () => {
        let browser = await puppeteer.launch({
          headless: false
        });
        let page = await browser.newPage();

        await page.setViewport({ width: 1280, height: 800 })

        let empWitNoName = await mongodb.employeesWithoutName();

        for(let i = 0; i < empWitNoName.length; i++){
            (function(i){
                setTimeout( async () => {
                    let position = empWitNoName[i].position,
                        company = empWitNoName[i].company,
                        country = empWitNoName[i].country,
                        linkedinUrl = 'https://www.linkedin.com';
                    
                    await page.goto('https://google.com', { waitUntil: 'networkidle2' });
                    let searchInput = `${position} ${company} ${country} ${linkedinUrl}`;
                    console.log('Searching for: ', searchInput)
                    await page.type('input[id=lst-ib]', searchInput, { delay: 50 });
                    await page.click('input[name="btnK"]');
                    await page.waitFor(2000);

                    const searchHandle = await page.evaluateHandle(() => document.body);
                    const resultHandle = await page.evaluateHandle(body => body.innerHTML, searchHandle);
                    let html = await resultHandle.jsonValue();
// class="rc"
                    let stream = fs.createWriteStream(`./resources/JobTextResult/${i}_lnkdnGoogleRes.txt`);
                        stream.once('open', function() {
                        stream.write(html);
                        stream.end();
                    });
                    await page.screenshot({ path: `${i}.png`, fullPage: true })
                }, i * 25000);
            }(i))
        }

    //   await page.goto('https://google.com', { waitUntil: 'networkidle2' });

    // //   await page.setViewport({ width: 1366, height: 768 });
    //   await page.type('input[id=lst-ib]', 'apostle johnson suleman', { delay: 100 })
      
    // //   
    //   await page.click('input[name="btnK"]')
    //   await page.waitForSelector('h3 a')
    //   await page.waitFor(5000);
    
  
    process.on("unhandledRejection", (reason, p) => {
      console.error("ERROR", p, "reason:", reason);
      browser.close();
    });
  
    //   await browser.close();
    })(); 
  
  } catch(error){
    console.error('Error with puppetter: ',error)
  }
