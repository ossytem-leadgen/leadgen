const scrapper = require('../../modules/scrapper');
const { date } = require('../../modules')

const employeesGrabber = (paginationData, page) => {

    return new Promise( async (resolve, reject) => {
        console.log(paginationData.url)
        const employeesResponse = await page.goto(paginationData.url, {
            timeout: 120000,
            waitUntil: 'networkidle0',
        });
        if (employeesResponse._status < 400) {
            await page.evaluate(async () => {
                await new Promise((resolve, reject) => {
                    try {
                        const maxScroll = Number.MAX_SAFE_INTEGER;
                        let lastScroll = 0;
                        const interval = setInterval(() => {
                            window.scrollBy(0, 100);
                            const scrollTop = document.documentElement.scrollTop;
                            if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                clearInterval(interval);
                                resolve();
                            } else {
                            lastScroll = scrollTop;
                            }
                        }, 100);
                    } catch (err) {
                        console.log(err);
                        reject(err);
                        reject(err.toString());
                    }
                });
            });

            const employeesHandle = await page.evaluateHandle(() => document.body);
            const resultEmployeesHandle = await page.evaluateHandle(body => body.innerHTML, employeesHandle);
            let employeesPage = await resultEmployeesHandle.jsonValue();
            let employees = scrapper.getAllEmployeeList(employeesPage);  

            console.log('\n\nEmployees',employees)
            // Lets get all employees data in a json object if there is a result
            console.log("\n\n1. Lets get all employees data in a json object if there are bosses");

            if (Array.isArray(employees[0]) && employees[0].length) {
                let employee_table_data = [];
                for (let i = 0; i < employees[0].length; i++) {
                    let doesEmployeeExist = await mongodb.getEmployee({ fullname: employees[0][i].name, position: employees[0][i].position });
                    if (doesEmployeeExist === false) {
                        employee_table_data.push({
                            "fullname" : employees[0][i].name,
                            "position" : employees[0][i].position,
                            "company" : paginationData.company,
                            "company_url" : paginationData.company_url,
                            "country" : employees[0][i].country,
                            "linkedin_url" : employees[0][i].employee_url,
                            "email" : "No email",
                            "phone" : "No Phone number",
                            "created_at" : date
                        });                                                    
                    } else {
                        console.log('This employee already exists in our db');
                    }
                }
                console.log("\n\nEmployees data", employee_table_data); 

                // INSERT EMPLOYEE DATA TO TABLE IF EXISTS => employee_table_data
                try {
                    mongodb.insertEmployees(employee_table_data);
                    resolve('inserted');
                } catch(err) {
                    console.log('Error inserting employees into Mongodb', err);
                    reject(err);
                }
            } else {
                console.log("No boss in this company"); 
            }
            await employeesHandle.dispose();
        } else {
            console.log("\n\nError with employees page");
        }
    });
}

exports.employeesGrabber = employeesGrabber;