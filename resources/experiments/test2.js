const puppeteer = require('puppeteer');
const { cookies } = require('../helpers/constants');
const path = require('path');
const scrapper = require('../../modules/scrapper');
// const { genRandNum, log } = require('../modules');
// const sendEmail = require('./emailSenderController');

// (async () => {
//     await sendEmail('Someone has replied', 'From the api to you');
    
// })()

let randNo;

let minTime = 10000;
let maxTime = 15000;

// ( async () => {
//     let employees = await mongodb.allEmployees("merged", false);

//     if(employees.length) {
//         for(let i=0; i < employees.length; i++) {
//             // (function(i) {
//             //     setTimeout(async () => {
//                     let companyData = await mongodb.allCompanies("name", employees[i].company);
//                     log(`\nEmployee data: ${employees[i].fullname} ${companyData}`)
//                     // setTimeout(async () => {
//                         let sheetData = {
//                             "company" : companyData[0] !== undefined ? companyData[0].name : employees[i].company,
//                             "country" : companyData[0] !== undefined ? companyData[0].country : "",
//                             "headcount" : "",
//                             "description" : companyData[0] !== undefined ? companyData[0].description : "",
//                             "linkedin_url" : companyData[0] !== undefined ? companyData[0].linkedin_url : "",
//                             "company_url" : companyData[0] !== undefined ? companyData[0].company_url : "",
//                             "category" : "",
//                             "lead_name" : employees[i].fullname,
//                             "lead_linkedin_url" : employees[i].linkedin_url,
//                             "position" : employees[i].position,
//                             "lead_title_ranking" : "",
//                             "lead_email" : employees[i].email,
//                             "date_connect_sent" : "",
//                             "date_connect_expire" : "",
//                             "date_connect_accepted" : "",
//                             "status" : "",
//                             "date_last_interaction" : "",
//                             "found_by_script1" : "Y",
//                             "found_by_script2" : "N",
//                             "found_by_script3" : "N",
//                             "date_found_by_script1" : employees[i].created_at,
//                             "date_found_by_script2" : "",
//                             "date_found_by_script3" : "",
//                             "created_at" : employees[i].created_at,
//                             "script_status" : "Not Connected",
//                             "uploaded" : false
//                         };
//                         console.log(employees[i].fullname)
//                         await mongodb.updateEmployees(employees[i].fullname, "merged", true)
//                         await mongodb.insertSpreadsheetData(sheetData)
//                     // }, 1000)
                    
//             //     }, i * 10000)
//             // })(i)
//         }        
//     } else {
//         log(`All the employees and companies collection has all been merged`)
//     }
    
// })()


// try{
//   (async () => {
    //   let browser = await puppeteer.launch({
    //       headless: false
    //   });
//       let page = await browser.newPage();
      
//       try {
//       await page.setCookie(...cookies);
//       } catch (error) {
//       console.error(error)
//       }
//       let compArr = await mongodb.allCompanies();
//       console.log(compArr.length)
//       if(compArr.length < 0){
//           for(let comp of compArr){
//             let url = comp.linkedin_url;
//             console.log("\n1. Making request to company url\n");              
//             const companyResponse = await page.goto(url, { timeout: 120000, waitUntil: 'networkidle2' });

//             if (companyResponse._status < 400) {
//                 const companyHandle = await page.evaluateHandle(() => document.body);
//                 const resultcompanyHandle = await page.evaluateHandle(body => body.innerHTML, companyHandle);
//                 let companyPage = await resultcompanyHandle.jsonValue();
//                 let companyData = scrapper.getCompanyPageData(companyPage);
//                 let name = companyData.name;
//                 let field = 'description';
//                 let input = companyData.description;
//                 console.log(`Gotten company data\nName: ${name}\nDescription: ${input}`)
//                 randNo = genRandNum(minTime, maxTime);
//                 log(`The random number is ${randNo}`)
//                 await page.waitFor(randNo);

//                 mongodb.updateCompany(name, field, input);
//                 await page.waitFor(1000);
//                 await companyHandle.dispose();
//                 await page.waitFor(1000);
//             }    
//         }
//       }
      

//       process.on("unhandledRejection", (reason, p) => {
//           console.error("ERROR", p, "reason:", reason);
//           browser.close();
//       });             
//   })(); 
// } catch(error){
//   console.error('Error with puppetter: ',error)
// }

// try {
    // (async () => {
        // let connectUrlFile = fs.readFileSync('/home/best/Projects/leadgen/modules/url.json', 'utf8');
    //     let browser = await puppeteer.launch({
    //         headless: false
    //     });
    //   const page = await browser.newPage()

      // for(let s =0 ; s<size; s++) { 
        // log(s)
        // await page.goto('https://sarneeh.github.io/reaptcha/')
        // await page.waitFor(2000);
        // await page.click('.recaptcha-checkbox-checkmark');
        // await page.waitFor(5000) 
        // const frame = await page.frames().find(f => f.name() === 'iframeResult');
        // const button = await frame.$('#recaptcha-anchor');await this.page.waitFor(2000);        
//         button.click();
        // await page.click('#recaptcha-anchor')
        // await page.waitForSelector('ytd-thumbnail.ytd-video-renderer')
        // await page.screenshot({path: 'youtube_fm_dreams_list.png'})
        // const videos = await page.$$('ytd-thumbnail.ytd-video-renderer')
        // await videos[2].click()
        // await page.waitForSelector('.html5-video-container')
        // await page.waitFor(35000)
    //   }
//       await browser.close()
//     })()
//   } catch (err) {
//     console.error(err)
// }

//Functionality for uploading files to linkedin

try {
    (async () => {
        const url = 'https://www.linkedin.com/feed/';
        const denis = 'Denys Stukalenko';
        const nameSearchResBtn = 'button.msg-connections-typeahead__search-result';
        const filePath = path.relative(process.cwd(), __dirname + '/BusinessProposal.txt');
        const sendBtn = '.msg-form__send-button';
        const closeMsgBox = 'button.msg-overlay-bubble-header__control.js-msg-close';
        const newFollowupMsg = 'This is from the script';

        const browser = await puppeteer.launch({
            headless: false
        });

        const page = await browser.newPage();
        await page.setCookie(...cookies.best);

        await page.goto(url);
        await page.waitFor(3000);

        await page.click('button.msg-overlay-bubble-header__control');
        await page.waitFor(1000);
        await page.type('.msg-connections-typeahead__search-field.msg-connections-typeahead__search-field--no-recipients', denis, { delay: 50 });
        await page.waitFor(2000);

        if (await page.$(nameSearchResBtn) !== null) {
            await page.click(nameSearchResBtn);

            await page.waitFor(5000);

            console.log('Uploading file.....');

            const fileInput = await page.$('input[name=file]');

            const totalCloseBtn = (await page.$$(closeMsgBox)).length;
            console.log('This is the number of close btn', totalCloseBtn);
            console.log(typeof totalCloseBtn);

            for( let i=0; i < totalCloseBtn; i++) {
                console.log(i)
            }
            // if (fileInput) {
            //     await fileInput.uploadFile(filePath);
            //     console.log('Uploaded .................');
                
                
            //     await page.type('.msg-form__contenteditable', newFollowupMsg, { delay: 50 });
                
            //     await page.waitFor(2500);
                
            //     if (await page.$(sendBtn) !== null) await page.click(sendBtn);
            // } else {
            //     console.log('FIle input could not be found');
            // }
            await page.waitFor(50000);
        }
        await browser.close();
    })()
  } catch (err) {
    console.error(err)
}

const createLeadObject = async (name,position,country,linkedin_url) => {
    let result = {
        "fullname" : name,
        "position" : position,
        "country" : country,
        "linkedin_url" : linkedin_url,
        "ifChatted" : false,
        "num_msg_sent" : 0,
        "date_msg_one_sent" : "",
        "date_msg_one_read" : "",
        "date_msg_one_responded" : "",
        "date_msg_two_sent" : "",
        "date_msg_two_read" : "",
        "date_msg_two_responded" : "",
        "date_msg_three_sent" : "",
        "date_msg_three_read" : "",
        "date_msg_three_responded" : "",
        "date_msg_four_sent" : "",
        "date_msg_four_read" : "",
        "date_msg_four_responded" : "",
        "date_msg_five_sent" : "",
        "date_msg_five_read" : "",
        "date_msg_five_responded" : ""
    }
    return result;
}

const convertCsvToJson = async () => {
    const csvPath = 'messages.csv';
    const jsonPath = '../resources/files/messages.json';

    const csv_path = path.format({
        root: '/ignored',
        dir: `${process.cwd()}/resources/files`,
        base: 'messages.csv'
      });

    csv()
    .fromFile(csv_path)
    .then( (jsonObj) => {
        jsonfile.writeFile(jsonPath, jsonObj, function (err) { 
            console.error(err); 
        });
    });
}

const convertJsonToCsv = async () => {
    let olegsLeads = await getOlegContacts({linkedin_url:{$ne:""}})
    // const jsonPath = '../resources/files/connects.json';
    const csvPath = path.format({
        root: '/ignored',
        dir: `${process.cwd()}/resources/files`,
        base: 'extensiveConnects.csv'
      });
      jsonexport(olegsLeads,function(err, csv){
        if(err) return console.log(err);
        let stream = fs.createWriteStream(csvPath);
        stream.once('open', function() {
            stream.write(csv);
            stream.end();
        });
    });
}
const getTimeZones = async () => {
    let olegsLeads = await getOlegContacts({sendMessage: true});
  
    const browser = await puppeteer.launch({
        headless: false
      });    
      const page = await browser.newPage();
  
      await page.setViewport({ width: 1366, height: 868 });
  
      const pageResponse = await page.goto(url, {
        timeout: 120000,
        waitUntil: 'networkidle2',
      });
      
      if (pageResponse._status < 400) {
        let unikCountry = [];
  
        for(lead of olegsLeads) {
            let { country } = lead;
            country = country.trim()
            if(unikCountry.includes(country)) {
                
            } else {
                unikCountry.push(country);
            }
            await page.type('.input-citypicker', country, { delay: 50 });
            await page.waitFor(1000);
            await page.click('button.msg-overlay-bubble-header__control');
            await page.waitFor(10000);
            let newCountry = country.replace(/Area/g, '').trim();
            log('\n\nCountry: ', country);
            // <div id="tl-df-i" class="tl-dt">6 hours behind<br>Odesa</div>
  
            randNo = genRandNum(MIN_TIME, MAX_TIME);
            log(`\n\nWaiting for ${randNo} seconds. About to move to next URL`);
            await page.waitFor(randNo);
        }
        console.log(unikCountry)
      }
      await browser.close();
  }
  
  //Get leads from linkedin
  const getConnectFromLinkedin = async () => {
  
    try{
        (async () => {
            const url = 'https://www.linkedin.com/search/results/people/v2/?facetNetwork=%5B%22F%22%5D&origin=MEMBER_PROFILE_CANNED_SEARCH';
            const nextBtn = 'button.next';
            
            const browser = await puppeteer.launch();
  
            const page = await browser.newPage();
            await page.setViewport({ width: 1366, height: 868 });
            
            try {
                await page.setCookie(...cookies.boss);
            } catch (error) {
                randNo = genRandNum(MIN_TIME, MAX_TIME);
                const errorLocation = 'Setting cookies in olegsLeadsController.js, function getConnectFromLinkedin()';
                const imageName = `${date()}-${time()}.png`;
                const extraInfo = `I was trying to set the cookies with Olegs account while it fell apart. Image name in google drive: ${imageName}`;
                
                const emailFields = {
                    error: true,
                    errorMsg: error,
                    errorLocation: errorLocation,
                    extraInfo: extraInfo
                };
  
                await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
                await page.waitFor(500);
  
                await uploadToDrive(imageName);
                await sendEmail(emailFields);
            }
            
            const searchResponse = await page.goto(url, {
              timeout: 250000,
              waitUntil: 'networkidle2'
            });
            
            for (let i = 0; i < 200; i++) {
                    console.log("\n\n1. Let's make request to employees url");
                    await page.waitFor(4760);
                    
                    if (searchResponse._status < 400) {
                        await page.evaluate(async () => {
                            await new Promise(async (resolve, reject) => {
  
                                try {
                                    const maxScroll = Number.MAX_SAFE_INTEGER;
                                    let lastScroll = 0;
                                    
                                    const interval = setInterval(() => {
                                        window.scrollBy(0, 100);
                                        const scrollTop = document.documentElement.scrollTop;
                                        
                                        if (scrollTop === maxScroll || scrollTop === lastScroll) {
                                            clearInterval(interval);
                                            resolve();
                                        } else {
                                            lastScroll = scrollTop;
                                        }
                                    }, 100);
                                } catch (error) {                
                                    randNo = genRandNum(MIN_TIME, MAX_TIME);
                                    const errorLocation = 'olegsLeadsController.js, function getConnectFromLinkedin()';
                                    const imageName = `${date()}-${time()}.png`;
                                    const extraInfo = `I was trying Scrolling through page while it fell apart. Image name in google drive: ${imageName}`;
  
                                    const emailFields = {
                                        error: true,
                                        errorMsg: error,
                                        errorLocation: errorLocation,
                                        extraInfo: extraInfo
                                    };
                    
                                    await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
                                    await page.waitFor(500);
  
                                    await uploadToDrive(imageName);
                                    await sendEmail(emailFields);
                                    reject(error.toString());
                                }
                            });
                        });
  
                        const connectHandle = await page.evaluateHandle(() => document.body);
                        const resultConnectHandle = await page.evaluateHandle(body => body.innerHTML, connectHandle);
                        const connectPage = await resultConnectHandle.jsonValue();
                        const leads = getAllEmployeeList(connectPage);
                        
                        console.log('\n\Leads',leads);
                        console.log("\n\n2. Lets push the leads into the queue");
                        
                        if (Array.isArray(leads) && leads.length) {
                            let leads_table_data = [];
  
                            for (let i = 0; i < leads.length; i++) {
                                const { name, position, country, employee_url } = leads[i]
                                const leadData = await getGeneratedLeads({lead_name : name,  lead_linkedin_url : employee_url});
  
                                if (leadData.length) {
                                    if(leadData.status === "Existing"){
                                        let insertObj = createLeadObject(name, position, country, employee_url);
                                        leads_table_data.push(insertObj);
                                    }
                                } else if(!leadData.length) {
                                    let insertObj = createLeadObject(name, position, country, employee_url);
  
                                    leads_table_data.push(insertObj);
                                } else {
                                    console.log('This lead already exists in our db');
                                }
                            }
  
                            console.log("\n\Leads data length", leads_table_data.length);
  
                            try {
                                await addOlegContactDataToQueue(leads_table_data);
                            } catch(error) {          
                                randNo = genRandNum(MIN_TIME, MAX_TIME);
                                const errorLocation = 'olegsLeadsController.js, function getConnectFromLinkedin()';
                                const imageName = `${date()}-${time()}.png`;
                                const extraInfo = `Saving data into the db while it fell apart. Image name in google drive: ${imageName}`;
                                const emailFields = {
                                    error: true,
                                    errorMsg: error,
                                    errorLocation: errorLocation,
                                    extraInfo: extraInfo
                                };
  
                                await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
                                await page.waitFor(500);
                        
                                await uploadToDrive(imageName);
                                await sendEmail(emailFields);
                            }
                        } else {
                            console.log("No lead on this page and this should not happen"); 
                        }
  
                    await connectHandle.dispose();
  
                    } else {
                        console.error("\n\nError with lead page");
                    }
  
                    await page.waitFor(5982);
  
                    if (await page.$(nextBtn) !== null) {
  
                        await page.click(nextBtn);
  
                        randNo = genRandNum(MIN_TIME, MAX_TIME);
  
                        console.log(`\n\nWaiting for ${randNo} seconds then move to the next page`);
  
                        await page.waitFor(randNo);
                    } else {
                        console.log('We have gone through all the pages');
  
                        let endTimeMilli =  new Date().getTime() - startTime;
                        let endTimeMin = Math.floor(endTimeMilli / 60000);
                        let endTimeSec = ((endTimeMilli % 60000) / 1000).toFixed(0);
  
                        if(endTimeSec == 60){
                            endTimeMin = endTimeMin + 1;
                            endTimeSec = 0;
                        }
  
                        console.log(`\nConnects hunter took ${endTimeMin}:${endTimeSec < 10 ? '0' : ''}${endTimeSec} seconds`);
  
                        await browser.close();
  
                        process.exit();
                    }
                }
  
                process.on("unhandledRejection", (reason, p) => {
                console.error("ERROR", p, "reason:", reason);
                browser.close();
            });          
        })(); 
    } catch(error) {      
        randNo = genRandNum(MIN_TIME, MAX_TIME);
        const errorLocation = 'olegsLeadsController.js, function getConnectFromLinkedin()';
        const imageName = `${date()}-${time()}.png`;
        const extraInfo = `Error with puppetter. Image name in google drive: ${imageName}`;
        
        const emailFields = {
            error: true,
            errorMsg: error,
            errorLocation: errorLocation,
            extraInfo: extraInfo
        };
  
        await page.screenshot({ path: `resources/error/${imageName}`, fullPage: true });
        await page.waitFor(500);
  
        await uploadToDrive(imageName);
        await sendEmail(emailFields);
    }
  }

  const divideOlegLeadsIntoGroups = async () => {
    let olegsLeads = await getOlegContacts({fullname:{$ne:""}}, 900, 300);
    // There are 2 groups of people people with messages and those without messages
    console.log('List of all messages', olegMsgs.messages.length);
    console.log('List of leads', olegsLeads.length);
    console.log('List of leads in json', connect.connects.length);
    //1. Loop through each on olegs contact
    //2. then do a foreach of the messages array 
    //3. check if the name of olegs lead apears in the to or from of messages array
    //4. if it apears that means there has been a message (update contact ifChatted to true)
    //5. else there was no chat between them so leave it at false don't change anything

    //1
    for (let i=0; i < olegsLeads.length; i++) {
        let { _id, fullname, ifChatted } = olegsLeads[i];
        console.log('\nName', fullname);
        //2
        for(let val of olegMsgs.messages) {
            //3
            if((val.From === fullname || val.To === fullname) && ifChatted === false) {
               console.log('Found a message', fullname);
               //4
               await updateOlegContacts({ _id: _id}, { ifChatted: true });
               break;
            }//5
        }
    }

    //Algoritm for updating the data of olegs lead (country, if in usa and if we should follow them up or not)
    //1. Loop through each on olegs contact
    //2. then do a foreach of the contacts array 
    //3. Check for the name in the contacts array
    //4. Get country, send message and the country
    //5. Replace the country, isUSA and sendMessage fields

        //1
        for (let i=0; i < olegsLeads.length; i++) {
            let { _id, fullname } = olegsLeads[i];
            let dbfullname = fullname
            console.log('\nName', dbfullname);
            //2
            for(let val of connect.connects) {
                let { fullname, country, followUp, inUsa } = val;
                //3
                if(fullname === dbfullname) {
                   console.log('Found that lead now lets do magic', dbfullname);
                   //4
                   if (inUsa === undefined) {
                       await updateOlegContacts({ _id: _id}, { country: country, sendMessage: followUp });
                   } else {
                        await updateOlegContacts({ _id: _id}, { country: country, sendMessage: followUp, inUSA: inUsa });
                   }
                   break;
                }//5
            }
        }
}


// Lets begin with the process from A -Z 

// Get url from url.json

// Pupptter makes request and save file

// Rename file [searchResult] and pass it to getUrlFromSearchRes()
// let searchOutput = getUrlFromSearchRes(searchResult);

// if(Array.isArray(searchOutput) && searchOutput.length){
//      for(let search_res of searchOutput) {

//         //Let use only the url from data and pass it to puppetter
//         let company_linkedin_url = search_res.company_url;
        
//         // Lets assume puppetter made d req and saved file. Now we have file.[COMPANY FILE]

//         // Passing file [companyPage2] to checkIfRecruiting()        
//         let isRecruiting = checkIfRecruiting(companyPage2);
//         if(isRecruiting === true) break;

//         //Lets grab company data since it is not a recruitment agency
//         let companyData = getCompanyPageData(companyPage2);

//         // Lets get all company data in a json object
//         let company_table_data = {
//             "name" : search_res.company_name,
//             "linkedin_url" : company_linkedin_url,
//             "technologies" : companyData.specialty,
//             "employees_link" : companyData.full_employees_url,
//             "country" : companyData.country,
//             "company_url" : companyData.website
//         };

//         //INSERT COMPANY DATA INTO TABLE => company_table_data
//         try{
//             insertCompany(company_table_data);
//         } catch(err){
//             console.log('Error inserting company into Mongodb', err)
//         }
//         // Pass the company_employees_url to puppetter to make another request

//         // Pupptter makes request and save file [EMPLOYEES FILE]

//         // Rename file [EMPLOYEES FILE] and pass it to getUrlFromSearchRes()  
//         // The following employees are only those in authority
//         let employeesData = getAllEmployeeList(employees);
        
//         // Lets get all employees data in a json object if there is a result
//         if(employeesData.length){
//             let employee_table_data = [];
//             for(let i = 0; i < employeesData.length; i++){
//                 employee_table_data.push({
//                     "fullname" : employeesData[i].name,
//                     "position" : employeesData[i].position,
//                     "company" : search_res.company_name,
//                     "company_url" : companyData.website,
//                     "country" : companyData.country,
//                     "linkedin_url" : company_linkedin_url,
//                     "email" : "",
//                     "phone" : ""
//                 });
//             }

//             // INSERT EMPLOYEE DATA TO TABLE IF EXISTS => employee_table_data
//             try{
//                 insertEmployees(employee_table_data);
//             } catch(err){
//                 console.log('Error inserting employees into Mongodb', err)
//             }
//         }
//     };

// } else {
//     console.log('No search result');
// }