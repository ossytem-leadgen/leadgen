const request = require('request');
const qs = require('querystring');

const API_KEY = "1b4b258638cdd8a8c39b860a798c1b537373aa40",
      hunter_url = "https://api.hunter.io/v2/email-finder",
      domain = "https://www.symphonyretailai.com",
      fullname = "Paul Dillon",
      names = fullname.split(' '),
      first_name = names[0],
      last_name = names[names.length - 1];

//https://api.hunter.io/v2/email-finder?domain=asana.com&first_name=Dustin&last_name=Moskovitz&api_key=1b4b258638cdd8a8c39b860a798c1b537373aa40
let req_url = hunter_url + '?' + qs.stringify({ domain: domain, first_name: first_name, last_name: last_name, api_key: API_KEY });

request.get(req_url, (error, response) => {
    if(error) throw error;

    try{
        let jsonBody = JSON.parse(response.body);
        let email = jsonBody.data.email;
        let score = jsonBody.data.score;
        
        if(email === null) {
            console.log('Email is empty, Not saving anything');
        } else {
            console.log('Lets save the email: ',email);
            console.log('Score', score);
        }
        
        // console.log(response.body.data.email)
    } catch(error){
        console.log('Error while making request to hunter for email', error)
    }
    
})