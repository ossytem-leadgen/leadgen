const puppeteer = require('puppeteer');
const { cookies,URLS } = require('../../helpers/constants');

// (async () => {
//   const browser = await puppeteer.launch();
//   const page = await browser.newPage();
//   await page.goto('https://facebook.com');
//   await page.screenshot({path: 'example1.png'});

//   await browser.close();
// })();

(async () => {
    const browser = await puppeteer.launch({
        headless: false
    });
    const page = await browser.newPage();
    await page.goto(URLS.home);
    await page.setCookie(...cookies.acc2);
    await page.waitFor(3600000);
  
    await browser.close();
  })();
  
  // Statistics
  const statisticsData = async (batch_number, statisticsDate) => {
    let connect_sent = await getGeneratedLeads({ "date_connect_sent" : statisticsDate, "batch_number" : batch_number });
    let connect_accepted = await getGeneratedLeads({ "date_connect_accepted": statisticsDate, "batch_number": batch_number });
    let first_msg = await getGeneratedLeads({ "status": "1st message", "date_last_interaction": statisticsDate, "batch_number": batch_number });
    let second_msg = await getGeneratedLeads({ "status": "2nd message", "date_last_interaction": statisticsDate, "batch_number":batch_number });
    let third_msg = await getGeneratedLeads({ "status": "3rd message", "date_last_interaction": statisticsDate, "batch_number": batch_number });
    let fourth_msg = await getGeneratedLeads({ "status": "4th message", "date_last_interaction": statisticsDate, "batch_number": batch_number });
    let responded = await getGeneratedLeads({ "status": "Responded", "date_last_interaction": statisticsDate, "batch_number": batch_number });

    let result = {
        "connect_sent": connect_sent.length,
        "connect_accepted": connect_accepted.length,
        "first_msg": first_msg.length,
        "second_msg": second_msg.length,
        "third_msg": third_msg.length,
        "fourth_msg": fourth_msg.length,
        "responded": responded.length,
        "date": statisticsDate
    };

    return result;
};

//Statistics Batchupdate
const statisticsToSheet = async () => {
    let b1 = await statisticsData('batch1', "2018-10-25");
    // let b2 = await statisticsData('batch2', date());
    // let b3 = await statisticsData('batch3', date());
    // let b4 = await statisticsData('batch4', date());

    sheets.spreadsheets.values.batchUpdate({
        spreadsheetId: leadgen_spreadsheet_id,
        resource: { 
            valueInputOption: 'USER_ENTERED',
            "data": [
                {
                    range: "Batch 1 Stats!AZ5",
                    majorDimension: "COLUMNS",
                    values: [ [date(),b1.connect_sent, b1.connect_accepted, b1.first_msg, b1.second_msg, b1.third_msg, b1.fourth_msg] ]
                },
                // {
                //     range: "Batch 2 Stats!N6:N14",
                //     majorDimension: "COLUMNS",
                //     values: [ [b2.connect_sent, b2.connect_accepted, b2.first_msg, b2.second_msg, b2.third_msg, b2.fourth_msg] ]
                // },
                // {
                //     range: "Batch 3 Stats!M6:M14",
                //     majorDimension: "COLUMNS",
                //     values: [ [b3.connect_sent, b3.connect_accepted, b3.first_msg, b3.second_msg, b3.third_msg, b3.fourth_msg] ]
                // },
                // {
                //     range: "Batch 4 Stats!M6:M14",
                //     majorDimension: "COLUMNS",
                //     values: [ [b4.connect_sent, b4.connect_accepted, b4.first_msg, b4.second_msg, b4.third_msg, b4.fourth_msg] ]
                // },
            ]
        }
    }, async (err, res) => {
        if (err) return console.log('companyspreadsheet The API returned an error: ' + err);
        log('Pushed into sheet');
    });
};


const leadsCompaniesCategorized = async (batchnumber) => {
    const args = {$ne : "N/A"};
    const sheetData = await getGeneratedLeads({batch_number: batchnumber, category: args});
    let comarr = [];

    for (let data of sheetData) {
        let { company } = data;

        if (!comarr.includes(company)) {
            comarr.push(company)
        }
    }

    const res = {
        employees : sheetData.length,
        companies : comarr.length
    }

    return res;
};